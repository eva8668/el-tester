#include "toolbox.h"
#include <utility.h>
#include <ansi_c.h>
#include "Chr6310A.h"
#include <cvirte.h>		
#include <userint.h>
#include "chr6310A_example.h"

// Constants
#define CTRL_ENABLED	0	// Not dimmed
#define CTRL_DISABLED	1	// Dimmed

// Function prototypes
int CVICALLBACK initSessionParams(void);
int CVICALLBACK abortApplication(ViStatus error);
void startMeasureThread();
int closeConnection();
void toggleComponents(int enable);
int CVICALLBACK loopDynamicDischargeThread(void *functionData);
int CVICALLBACK fetchVolCurrThread(void *functionData);
int CVICALLBACK stepDynamicCurrThread(void *functionData);

// Global variables
static ViSession vi;
static int panelHandle;
volatile int quit = 0, loopDdQuit = 0, quitStepThread = 0;
static CmtThreadFunctionID loopFunctionID, stepFunctionID, fetchFunctionID;

static int			mRadioID = 0;
static ViReal64		curr_dh, curr_dl, curr_0;
static double		t_dh, t_dl, t_0;

// Application entry point
int main (int argc, char *argv[])
{
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "chr6310A_example.uir", PANEL)) < 0)
		return -1;
	DisplayPanel (panelHandle);
	RunUserInterface ();
	DiscardPanel (panelHandle);
	return 0;
}

/********************************************************/
/* Callbacks of UI components                           */
/********************************************************/
int CVICALLBACK next(int panel, int control, int event,
                     void *callbackData, int eventData1, int eventData2)
{
	ViStatus	error = VI_SUCCESS; 
	int 		resource=0,gpib_addr=0,com_port=0,baudrate=0,simulate=0;
	char 		ResName[20]="";
	char 		ID[512]="", Model[512]="";
	
	switch (event)
		{
		case EVENT_COMMIT:
			// Set up parameters
			resource = 1; // RS-232C protocol
			
			GetCtrlVal (panelHandle, PANEL_RING_COMPORT, &com_port);
			//com_port = 3; // COM port 3 (1-based)
			
			GetCtrlVal (panelHandle, PANEL_RING_BAUDRATE, &baudrate);
			//baudrate = 9600; // baudrate 9600 bits
			
			GetCtrlVal (panelHandle, PANEL_CB_SIMULATE, &simulate);
			//simulate = 1; // Use simulation for demo
			
			// Start to create protocol session
			if (resource==1)	
				sprintf (ResName,"ASRL%d::INSTR",com_port);
			else
				sprintf (ResName,"GPIB0::%d::INSTR",gpib_addr);
				
			if (simulate)
			{
				if ((Chr6310A_InitWithOptions (ResName, VI_TRUE, VI_FALSE,
                          "Simulate=1,RangeCheck=1,QueryInstrStatus=1,Cache=1", &vi)) !=0)
				{                
                    MessagePopup ("Simulation Error","Can not simulate CHROMA 6310A DC electronic load.");
                    QuitUserInterface (0);
                    break;
                }    
            }              
			else
			{
				if ((Chr6310A_InitInterface (ResName, VI_TRUE, VI_FALSE, &vi, baudrate, ID, ""))!=0)
				{
					if (ConfirmPopup ("Initial Error", "Initial CHROMA 6310A DC electronic load error. Do you want to quit?"))
					{
						QuitUserInterface (0); 
						break;
					}	
					break;
				}
			}
			
			// Set up initial parameters
			initSessionParams();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup ("Help", "After finished the above setting, continue with the next control panel.");
			break;
		}

Error: 			
	if (error!=0)
		abortApplication (error); 
	return error;
}

int  CVICALLBACK QUIT(int panel, int event, void *callbackData, int eventData1, int eventData2)
{
	switch (event) {
	case EVENT_CLOSE:
		QuitUserInterface (0);
		closeConnection();
		break;
	}
	return 0;
}

int CVICALLBACK StopMeasure(int panel, int control, int event,
                            void *callbackData, int eventData1, int eventData2)
{
	switch (event) {
	case EVENT_COMMIT:
		closeConnection();
		
		// Play a sound for fun...
		if (CVILowLevelSupportDriverLoaded() == 1) {
			StartPCSound(440);
			Delay(0.5);
			StopPCSound();
		} else {
			Beep();
		}
		break;
	}
	return 0;
}

/********************************************************/
/* Private function                                     */
/********************************************************/
int CVICALLBACK initSessionParams(void)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	vsr=0,isr=0, high=0,center=0,low=0;
	ViInt32		GetChannelLoad=0;
	int			output_state=0, channel, mode;
	ViReal64	curr_vol=0.0;
	char 		ID[512]="", model[512]="",msg[512]="";
	
	// First, disable all controllable UI components
	toggleComponents(CTRL_DISABLED);
	
	// Optional: Get instrument ID and model
	checkErr( Chr6310A_GetAttributeViString (vi, "", CHR6310A_ATTR_ID_QUERY_RESPONSE, 512, ID));
	checkErr( Chr6310A_GetAttributeViString (vi, "", CHR6310A_ATTR_INSTRUMENT_MODEL, 512, model)); 
	sprintf (msg, "*IDN? : ID [%s], Model [%s]\n", ID, model);
	//MessagePopup ("Get ID", msg);
	
	// Set up remote conf before any communication
	checkErr( Chr6310A_Set_Configure_Remote (vi, VI_TRUE) );
	
	// Set up working channel ( #1 for test )
	GetCtrlVal (panelHandle, PANEL_RING_CHAN, &channel);
	checkErr( Chr6310A_Set_Channel_Load (vi, channel) );
	
	// Optional: Get channel info
	checkErr( Chr6310A_Get_Channel_Load (vi, &GetChannelLoad) );
	sprintf (msg, "%sChannel ID: %d\n", msg, GetChannelLoad);
	//MessagePopup ("Channel Info", msg);
	
	// Display debug message
	sprintf (msg, "%sConnected. Start measuring...", msg);
	SetCtrlVal (panelHandle, PANEL_TEXTMSG_DEBUG, msg);
	//MessagePopup ("Get ID", msg);
	
	// Set working mode (CCH)
	GetCtrlVal (panelHandle, PANEL_RING_MODE, &mode);
	checkErr( Chr6310A_Set_Mode (vi, mode) );
	
	// Decide what kind of mode
	switch (mRadioID) {
		case PANEL_RADIO_DYNA_DIS_KYMCO: {
			// Get current value
			GetCtrlVal (panelHandle, PANEL_NUM_CURR_DH, &curr_dh);
			GetCtrlVal (panelHandle, PANEL_NUM_CURR_DL, &curr_dl);
			GetCtrlVal (panelHandle, PANEL_NUM_CURR_I0, &curr_0);
			GetCtrlVal (panelHandle, PANEL_NUM_TIME_DH, &t_dh);
			GetCtrlVal (panelHandle, PANEL_NUM_TIME_DL, &t_dl);
			GetCtrlVal (panelHandle, PANEL_NUM_TIME_I0, &t_0);
			
			// Start thread to loop through dynamic discharge currents
			loopDdQuit = 0;
			CmtScheduleThreadPoolFunction(DEFAULT_THREAD_POOL_HANDLE, loopDynamicDischargeThread, NULL, &loopFunctionID);
			
			// Start thread to measure Voltage and Current
			startMeasureThread();
			break;
		}
		case PANEL_RADIO_STATIC_DISCH: {
			// Set initial current to specified value
			GetCtrlVal (panelHandle, PANEL_NUM_STATIC_CURR, &curr_vol);
			checkErr( Chr6310A_Set_CC_Static_L1 (vi, curr_vol) );
	
			// Set channel "ON"
			checkErr( Chr6310A_Set_Load_State(vi, VI_TRUE) );
			
			// Start thread to measure Voltage and Current
			startMeasureThread();
			break;
		}
		case PANEL_RADIO_DYNA_STEP:
		case PANEL_RADIO_DYNA_RATE: {
			ViReal64	initCurr, targetCurr, currSlewRate;
			GetCtrlVal(panelHandle, PANEL_NUM_DYNA_INIT_CURR, &initCurr);
			GetCtrlVal(panelHandle, PANEL_NUM_DYNA_TARGET_CURR, &targetCurr);
			GetCtrlVal(panelHandle, PANEL_NUM_DYNA_CURR_RATE, &currSlewRate);
			
			if (mRadioID == PANEL_RADIO_DYNA_RATE) {
				// Set initial current
				checkErr( Chr6310A_Set_CC_Static_L1(vi, initCurr) );
				
				// Set slew rate
				checkErr( Chr6310A_Set_CC_Static_Rise_Slew_Rate(vi, currSlewRate / 1000) );
				checkErr( Chr6310A_Set_CC_Static_Fall_Slew_Rate(vi, currSlewRate / 1000) );
	
				// Set channel "ON"
				checkErr( Chr6310A_Set_Load_State(vi, VI_TRUE) );
				
				// Start thread to measure Voltage and Current
				startMeasureThread();
				
				// Set target current
				checkErr( Chr6310A_Set_CC_Static_L1(vi, targetCurr) );
			} else {
				ViReal64 funcData[3] = { initCurr,targetCurr, currSlewRate };
				
				// Start thread to controll step currents
				quitStepThread = 0;
				CmtScheduleThreadPoolFunction(DEFAULT_THREAD_POOL_HANDLE, stepDynamicCurrThread, funcData, &stepFunctionID);
				
				// Start thread to measure Voltage and Current
				startMeasureThread();
			}
			break;
		}
	}
	
Error: 			
	if (error != 0) {
		abortApplication (error);
	}
	return error;
}

void startMeasureThread() {
	// Start thread to measure Voltage and Current
	quit = 0;
	CmtScheduleThreadPoolFunction(DEFAULT_THREAD_POOL_HANDLE, fetchVolCurrThread, NULL, &fetchFunctionID);
}

int closeConnection()
{
	ViStatus	error = VI_SUCCESS;
	
	// First, enable all controllable components
	toggleComponents(CTRL_ENABLED);
	
	// Display stopping msg and disable stop button
	SetCtrlVal (panelHandle, PANEL_TEXTMSG_DEBUG, "Stop measuring. Please wait ......");
	SetCtrlAttribute(panelHandle, PANEL_BTN_STOP, ATTR_DIMMED, CTRL_DISABLED);
	
	// Then, close EL connection
	if (vi != NULL) {
		// First, stop looping dynamic discharge currents
		if (loopFunctionID != NULL) {
			loopDdQuit = 1;
			CmtWaitForThreadPoolFunctionCompletion(DEFAULT_THREAD_POOL_HANDLE, loopFunctionID, OPT_TP_PROCESS_EVENTS_WHILE_WAITING);
			CmtReleaseThreadPoolFunctionID(DEFAULT_THREAD_POOL_HANDLE, loopFunctionID);
			loopFunctionID = 0;
		} else if (stepFunctionID != NULL) {
			quitStepThread = 1;
			CmtWaitForThreadPoolFunctionCompletion(DEFAULT_THREAD_POOL_HANDLE, stepFunctionID, OPT_TP_PROCESS_EVENTS_WHILE_WAITING);
			CmtReleaseThreadPoolFunctionID(DEFAULT_THREAD_POOL_HANDLE, stepFunctionID);
			stepFunctionID = 0;
		}
		
		// Then, stop measuring
		if (fetchFunctionID != NULL) {
			quit = 1;
			CmtWaitForThreadPoolFunctionCompletion(DEFAULT_THREAD_POOL_HANDLE, fetchFunctionID, OPT_TP_PROCESS_EVENTS_WHILE_WAITING);
			CmtReleaseThreadPoolFunctionID(DEFAULT_THREAD_POOL_HANDLE, fetchFunctionID);
			fetchFunctionID = 0;
		}
		
		// Set channel "OFF"
		checkErr( Chr6310A_Set_Load_State (vi, VI_FALSE) );
		//Delay (0.5);

		// Set remote "OFF"
		checkErr( Chr6310A_Set_Configure_Remote (vi, VI_FALSE) );
		//Delay (0.5);
	
		// Close session
		checkErr( Chr6310A_close (vi) );
		Delay (0.5);
		vi = 0;
	}
	
	// Clear message and enable stop button
	SetCtrlVal(panelHandle, PANEL_TEXTMSG_DEBUG, "");
	SetCtrlAttribute(panelHandle, PANEL_BTN_STOP, ATTR_DIMMED, CTRL_ENABLED);
	
Error: 			
	if (error != 0) {
		abortApplication (error);
	}
	return error;
}

void toggleComponents(int enable)
{
	SetCtrlAttribute(panelHandle, PANEL_RING_COMPORT, ATTR_DIMMED, enable);
	SetCtrlAttribute(panelHandle, PANEL_RING_BAUDRATE, ATTR_DIMMED, enable);
	SetCtrlAttribute(panelHandle, PANEL_RING_CHAN, ATTR_DIMMED, enable);
	SetCtrlAttribute(panelHandle, PANEL_RING_MODE, ATTR_DIMMED, enable);
	SetCtrlAttribute(panelHandle, PANEL_CB_SIMULATE, ATTR_DIMMED, enable);
}

int CVICALLBACK loopDynamicDischargeThread(void *functionData)
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean	isLoadOn = VI_FALSE;
	
	// Set initial current to zero
	checkErr( Chr6310A_Set_CC_Static_L1 (vi, 0.0) );
	
	// Set channel "ON"
	checkErr( Chr6310A_Set_Load_State(vi, VI_TRUE) );
	
	while (!loopDdQuit) {
		// Set current Idh
		checkErr( Chr6310A_Set_CC_Static_L1 (vi, curr_dh) );
		if (loopDdQuit) break;
		Delay(t_dh);
		if (loopDdQuit) break;
		
		// Set current to Idl
		checkErr( Chr6310A_Set_CC_Static_L1 (vi, curr_dl) );
		if (loopDdQuit) break;
		Delay(t_dl);
		if (loopDdQuit) break;
		
		// Set current to I0
		checkErr( Chr6310A_Set_CC_Static_L1 (vi, curr_0) );
		if (loopDdQuit) break;
		Delay(t_0);
	}

Error: 			
	if (error != 0) {
		abortApplication (error);
	}
	return error;
}

int CVICALLBACK fetchVolCurrThread(void *functionData)
{
	ViStatus	error = VI_SUCCESS;
	
	// Measure Voltage and Current
	char		str[60];
	ViReal64	current=0,voltage=0;
	
	// Disable start button
	SetCtrlAttribute(panelHandle, PANEL_BTN_NEXT, ATTR_DIMMED, CTRL_DISABLED);
	
	// Set up text color to red (Highlighted)
	SetCtrlAttribute(panelHandle, PANEL_MEASURE_CURR, ATTR_TEXT_COLOR, VAL_RED);
	SetCtrlAttribute(panelHandle, PANEL_MEASURE_VOL, ATTR_TEXT_COLOR, VAL_RED);
	
	while (!quit) {
		Delay (0.5);
		
		checkErr( Chr6310A_Measure_Voltage (vi, &voltage) );
		sprintf(str, "%.4f", voltage);
		SetCtrlVal(panelHandle, PANEL_MEASURE_VOL, str);
		
		checkErr( Chr6310A_Measure_Current (vi, &current) );
		sprintf(str, "%.4f", current);
		SetCtrlVal(panelHandle, PANEL_MEASURE_CURR, str);
		
		//printf("Measure Voltage: %f, Current: %f\n", voltage, current);
	}
	//MessagePopup("Measure results", msg);
	
	// Enable start button
	SetCtrlAttribute(panelHandle, PANEL_BTN_NEXT, ATTR_DIMMED, CTRL_ENABLED);
	
	// Set up text color to black (non-highlighted)
	SetCtrlAttribute(panelHandle, PANEL_MEASURE_CURR, ATTR_TEXT_COLOR, VAL_BLACK);
	SetCtrlAttribute(panelHandle, PANEL_MEASURE_VOL, ATTR_TEXT_COLOR, VAL_BLACK);
	
Error: 			
	if (error != 0) {
		abortApplication (error);
	}
	return error;
}

int CVICALLBACK stepDynamicCurrThread(void *functionData)
{
	ViStatus	error = VI_SUCCESS;
	ViReal64	*currData = (ViReal64 *) functionData;
	ViReal64	initCurr = currData[0],
				targetCurr = currData[1],
				currSlewRate = currData[2];
	ViReal64	curr_now = initCurr;
	
	// Set initial current
	checkErr( Chr6310A_Set_CC_Static_L1(vi, initCurr) );
	
	// Set slew rate
	checkErr( Chr6310A_Set_CC_Static_Rise_Slew_Rate(vi, 0.8) );
	checkErr( Chr6310A_Set_CC_Static_Fall_Slew_Rate(vi, 0.8) );
	
	// Set channel "ON"
	checkErr( Chr6310A_Set_Load_State(vi, VI_TRUE) );
	
	while (curr_now < targetCurr && !quitStepThread) {
		Delay(1.0);
		
		// Set current by step
		curr_now = ((curr_now + currSlewRate) > targetCurr) ? targetCurr : (curr_now + currSlewRate);
		checkErr( Chr6310A_Set_CC_Static_L1(vi, curr_now) );
	}
	
Error: 			
	if (error != 0) {
		abortApplication (error);
	}
	return error;
}

int CVICALLBACK abortApplication(ViStatus error)
{
	char		message[200]="";
	
	// First, enable all controllable components
	toggleComponents(CTRL_ENABLED);
	
	if (error>=0xbffa4001 && error <=0xbffa4003) // out of range
	{
		if (error==0xbffa4001)
			sprintf (message, "The value you passed is out of range.");
		else if (error==0xbffa4002)			
			sprintf (message, "The value you passed is out of range of voltage limit.");
		else if (error==0xbffa4003)
			sprintf (message, "The value you passed is out of range of current limit.");
		MessagePopup ("Error", message); 
	}
	else if (error >= 3220963328)  //0xBFFC00000
	{
		MessagePopup ("Error", "I/O or System error occured. Program now abort.");  		
		QuitUserInterface (0);
		Chr6310A_close (vi);
		Delay (0.5); 
	} else {
		if (ConfirmPopup ("Initial Error", "Initial CHROMA 6310A DC electronic load error. Do you want to quit?"))
		{
			QuitUserInterface (0);
			if (vi != NULL) {
				Chr6310A_Set_Configure_Remote (vi, VI_FALSE);
				Delay (0.5); 
				Chr6310A_close (vi);
				Delay (0.5); 
			}
		}
	}
	
	return 0;
}

int CVICALLBACK radioBtnToggle (int panel, int control, int event,
								void *callbackData, int eventData1, int eventData2)
{
	int value;
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlAttribute(panel, control, ATTR_CTRL_VAL, &value);
			
			SetCtrlAttribute(panel, PANEL_RADIO_DYNA_DIS_KYMCO, ATTR_CTRL_VAL, 0);
			SetCtrlAttribute(panel, PANEL_RADIO_STATIC_DISCH, ATTR_CTRL_VAL, 0);
			SetCtrlAttribute(panel, PANEL_RADIO_DYNA_STEP, ATTR_CTRL_VAL, 0);
			SetCtrlAttribute(panel, PANEL_RADIO_DYNA_RATE, ATTR_CTRL_VAL, 0);
			
			SetCtrlAttribute(panel, control, ATTR_CTRL_VAL, value);
			
			mRadioID = (value != 0) ? control : 0;
			break;
	}
	return 0;
}
