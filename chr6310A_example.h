/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2014. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1       /* callback function: QUIT */
#define  PANEL_BTN_NEXT                   2       /* control type: command, callback function: next */
#define  PANEL_CB_SIMULATE                3       /* control type: radioButton, callback function: (none) */
#define  PANEL_RING_COMPORT               4       /* control type: ring, callback function: (none) */
#define  PANEL_RING_BAUDRATE              5       /* control type: ring, callback function: (none) */
#define  PANEL_RING_CHAN                  6       /* control type: ring, callback function: (none) */
#define  PANEL_BTN_STOP                   7       /* control type: command, callback function: StopMeasure */
#define  PANEL_MEASURE_CURR               8       /* control type: string, callback function: (none) */
#define  PANEL_MEASURE_VOL                9       /* control type: string, callback function: (none) */
#define  PANEL_RING_MODE                  10      /* control type: ring, callback function: (none) */
#define  PANEL_NUM_CURR_I0                11      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_TIME_I0                12      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_CURR_DL                13      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_TIME_DL                14      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_CURR_DH                15      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_TIME_DH                16      /* control type: numeric, callback function: (none) */
#define  PANEL_TEXTMSG                    17      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_2                  18      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_3                  19      /* control type: textMsg, callback function: (none) */
#define  PANEL_RADIO_DYNA_DIS_KYMCO       20      /* control type: radioButton, callback function: radioBtnToggle */
#define  PANEL_RADIO_STATIC_DISCH         21      /* control type: radioButton, callback function: radioBtnToggle */
#define  PANEL_RADIO_DYNA_STEP            22      /* control type: radioButton, callback function: radioBtnToggle */
#define  PANEL_RADIO_DYNA_RATE            23      /* control type: radioButton, callback function: radioBtnToggle */
#define  PANEL_NUM_STATIC_CURR            24      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_DYNA_INIT_CURR         25      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_DYNA_TARGET_CURR       26      /* control type: numeric, callback function: (none) */
#define  PANEL_NUM_DYNA_CURR_RATE         27      /* control type: numeric, callback function: (none) */
#define  PANEL_TEXTMSG_5                  28      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_4                  29      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_DEBUG              30      /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK next(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QUIT(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK radioBtnToggle(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK StopMeasure(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
