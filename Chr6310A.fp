s��        �9  + P�  Bh   �   ����                               Chr6310A    Programmable DC Electronic Load               � � ��ViInt16  �  � ��ViInt32  � � ��ViReal64     � ��ViRsrc     	� 	��ViBoolean     	� 	��ViSession     � ��ViStatus     �  ViChar[]     � ��ViChar     � ��ViString     	�  ViInt16[]     	�  ViInt32[]     
�  	ViReal64[]     � 	 
ViBoolean[]     � ��ViConstString     � ��ViAttr   �    This instrument driver contains programming support for the Programmable DC Electronic Load.  This driver has all the functions that IVI and VXIplug&play require.  

Note:  This driver requires the VISA and IVI libraries.       �    This class contains high-level test and measurement functions.  
These functions call other instrument driver functions to configure and perform complete instrument operations.
     �    This class contains functions and sub-classes that configure the instrument.  The class includes high-level functions that configure multiple instrument settings as well as low-level functions that set, get, and check individual attribute values.
     T    This class contains sub-classes for the set, get, and check attribute functions.       �    This class contains functions that set an attribute to a new value.  There are typesafe functions for each attribute data type.     �    This class contains functions that obtain the current value of an attribute.  There are typesafe functions for each attribute data type.     �    This class contains functions that obtain the current value of an attribute.  There are typesafe functions for each attribute data type.     m    This class contains functions and sub-classes that initiate instrument operations and report their status.
     _    This class contains functions and sub-classes that transfer data to and from the instrument.
    _    This class contains functions and sub-classes that control common instrument operations.  These functions include many of functions that VXIplug&play require, such as reset, self-test, revision query, error query, and error message.  This class also contains functions that access IVI error infomation, lock the session, and perform instrument I/O.
     R    This class contains functions that retrieve and clear the IVI error information.     ?    This class contains functions that retrieve coercion records.     k    This class contains functions that lock and unlock IVI instrument driver sessions for multithread safefy.     F    This class contains functions that send and receive instrument data.    E    This function performs the following initialization actions:

- Creates a new IVI instrument driver session.

- Opens a session to the specified device using the interface and address you specify for the Resource Name parameter.

- If the ID Query parameter is set to VI_TRUE, this function queries the instrument ID and checks that it is valid for this instrument driver.

- If the Reset parameter is set to VI_TRUE, this function resets the instrument to a known state.

- Sends initialization commands to set the instrument to the state necessary for the operation of the instrument driver.

- Returns a ViSession handle that you use to identify the instrument in all subsequent instrument driver function calls.

Note:  This function creates a new session each time you invoke it. Although you can open more than one IVI session for the same resource, it is best not to do so.  You can use the same session in multiple program threads.  You can use the Chr6310A_LockSession and Chr6310A_UnlockSession functions to protect sections of code that require exclusive access to the resource.

    	G    Pass the resource name of the device to initialize.

You can also pass the name of a virtual instrument or logical name that you configure with the IVI Configuration utility.  The virtual instrument identifies a specific device and specifies the initial settings for the session.  A logical Name identifies a particular virtual instrument.

Refer to the following table below for the exact grammar to use for this parameter.  Optional fields are shown in square brackets ([]).

Syntax
------------------------------------------------------
GPIB[board]::<primary address>[::secondary address]::INSTR
VXI[board]::<logical address>::INSTR
GPIB-VXI[board]::<logical address>::INSTR
ASRL<port>::INSTR
<LogicalName>
[VInstr->]<VirtualInstrument>

If you do not specify a value for an optional field, the following values are used:

Optional Field - Value
------------------------------------------------------
board - 0
secondary address - none (31)

The following table contains example valid values for this parameter.

"Valid Value" - Description
------------------------------------------------------
"GPIB::22::INSTR" - GPIB board 0, primary address 22 no
                    secondary address
"GPIB::22::5::INSTR" - GPIB board 0, primary address 22
                       secondary address 5
"GPIB1::22::5::INSTR" - GPIB board 1, primary address 22
                        secondary address 5
"VXI::64::INSTR" - VXI board 0, logical address 64
"VXI1::64::INSTR" - VXI board 1, logical address 64
"GPIB-VXI::64::INSTR" - GPIB-VXI board 0, logical address 64
"GPIB-VXI1::64::INSTR" - GPIB-VXI board 1, logical address 64
"ASRL2::INSTR" - COM port 2
"SampleInstr" - Logical name "SampleInstr"
"VInstr->xyz432" - Virtual Instrument "xyz432"
"xyz432" - Logical Name or Virtual Instrument "xyz432"

/*=CHANGE:===================================================* 

Modify the following default value so that it reflects the default address for your instrument.  You must make the corresponding change to the Default Value entry for the control.

 *================================================END=CHANGE=*/ 
Default Value:  "ASRL1::INSTR"

Note: You specify the resource name with the "VInstr->" if you have the logical name that is the same as the virtual instrument name and you want to explicitly use the virtual instrument name. Otherwise, the driver uses the logical name.        Specify whether you want the instrument driver to perform an ID Query.

Valid Range:
VI_TRUE  (1) - Perform ID Query (Default Value)
VI_FALSE (0) - Skip ID Query

When you set this parameter to VI_TRUE, the driver verifies that the instrument you initialize is a type that this driver supports.  

Circumstances can arise where it is undesirable to send an ID Query command string to the instrument.  When you set this parameter to VI_FALSE, the function initializes the instrument without performing an ID Query.     �    Specify whether you want the to reset the instrument during the initialization procedure.

Valid Range:
VI_TRUE  (1) - Reset Device (Default Value)
VI_FALSE (0) - Don't Reset

    �    Returns a ViSession handle that you use to identify the instrument in all subsequent instrument driver function calls.

Notes:

(1) This function creates a new session each time you invoke it.  This is useful if you have multiple physical instances of the same type of instrument.  

(2) Avoid creating multiple concurrent sessions to the same physical instrument.  Although you can create more than one IVI session for the same resource, it is best not to do so.  A better approach is to use the same IVI session in multiple execution threads.  You can use functions Chr6310A_LockSession and Chr6310A_UnlockSession to protect sections of code that require exclusive access to the resource.

    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFC0101  ID Query not supported.
3FFC0102  Reset not supported.

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

BFFA0000  Cannot load IVI engine.
BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.
BFFA0054  Bad channel name in Channel List.

BFFC0011  Instrument returned invalid response to ID Query.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
    f =   �  �    Resource Name                     � : �       ID Query                          � =� �       Reset Device                      z �C �  �    Instrument Handle                 8#����  �    Status                          ����  z��                                            "GPIB0::1::INSTR"   Yes VI_TRUE No VI_FALSE   Yes VI_TRUE No VI_FALSE    	           	           FCopyright 1998 National Instruments Corporation. All Rights Reserved.   E    This function performs the following initialization actions:

- Creates a new IVI instrument driver session.

- Opens a session to the specified device using the interface and address you specify for the Resource Name parameter.

- If the ID Query parameter is set to VI_TRUE, this function queries the instrument ID and checks that it is valid for this instrument driver.

- If the Reset parameter is set to VI_TRUE, this function resets the instrument to a known state.

- Sends initialization commands to set the instrument to the state necessary for the operation of the instrument driver.

- Returns a ViSession handle that you use to identify the instrument in all subsequent instrument driver function calls.

Note:  This function creates a new session each time you invoke it. Although you can open more than one IVI session for the same resource, it is best not to do so.  You can use the same session in multiple program threads.  You can use the Chr6310A_LockSession and Chr6310A_UnlockSession functions to protect sections of code that require exclusive access to the resource.

    	G    Pass the resource name of the device to initialize.

You can also pass the name of a virtual instrument or logical name that you configure with the IVI Configuration utility.  The virtual instrument identifies a specific device and specifies the initial settings for the session.  A logical Name identifies a particular virtual instrument.

Refer to the following table below for the exact grammar to use for this parameter.  Optional fields are shown in square brackets ([]).

Syntax
------------------------------------------------------
GPIB[board]::<primary address>[::secondary address]::INSTR
VXI[board]::<logical address>::INSTR
GPIB-VXI[board]::<logical address>::INSTR
ASRL<port>::INSTR
<LogicalName>
[VInstr->]<VirtualInstrument>

If you do not specify a value for an optional field, the following values are used:

Optional Field - Value
------------------------------------------------------
board - 0
secondary address - none (31)

The following table contains example valid values for this parameter.

"Valid Value" - Description
------------------------------------------------------
"GPIB::22::INSTR" - GPIB board 0, primary address 22 no
                    secondary address
"GPIB::22::5::INSTR" - GPIB board 0, primary address 22
                       secondary address 5
"GPIB1::22::5::INSTR" - GPIB board 1, primary address 22
                        secondary address 5
"VXI::64::INSTR" - VXI board 0, logical address 64
"VXI1::64::INSTR" - VXI board 1, logical address 64
"GPIB-VXI::64::INSTR" - GPIB-VXI board 0, logical address 64
"GPIB-VXI1::64::INSTR" - GPIB-VXI board 1, logical address 64
"ASRL2::INSTR" - COM port 2
"SampleInstr" - Logical name "SampleInstr"
"VInstr->xyz432" - Virtual Instrument "xyz432"
"xyz432" - Logical Name or Virtual Instrument "xyz432"

/*=CHANGE:===================================================* 

Modify the following default value so that it reflects the default address for your instrument.  You must make the corresponding change to the Default Value entry for the control.

 *================================================END=CHANGE=*/ 
Default Value:  "ASRL1::INSTR"

Note: You specify the resource name with the "VInstr->" if you have the logical name that is the same as the virtual instrument name and you want to explicitly use the virtual instrument name. Otherwise, the driver uses the logical name.        Specify whether you want the instrument driver to perform an ID Query.

Valid Range:
VI_TRUE  (1) - Perform ID Query (Default Value)
VI_FALSE (0) - Skip ID Query

When you set this parameter to VI_TRUE, the driver verifies that the instrument you initialize is a type that this driver supports.  

Circumstances can arise where it is undesirable to send an ID Query command string to the instrument.  When you set this parameter to VI_FALSE, the function initializes the instrument without performing an ID Query.     �    Specify whether you want the to reset the instrument during the initialization procedure.

Valid Range:
VI_TRUE  (1) - Reset Device (Default Value)
VI_FALSE (0) - Don't Reset

    �    Returns a ViSession handle that you use to identify the instrument in all subsequent instrument driver function calls.

Notes:

(1) This function creates a new session each time you invoke it.  This is useful if you have multiple physical instances of the same type of instrument.  

(2) Avoid creating multiple concurrent sessions to the same physical instrument.  Although you can create more than one IVI session for the same resource, it is best not to do so.  A better approach is to use the same IVI session in multiple execution threads.  You can use functions Chr6310A_LockSession and Chr6310A_UnlockSession to protect sections of code that require exclusive access to the resource.

    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFC0101  ID Query not supported.
3FFC0102  Reset not supported.

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

BFFA0000  Cannot load IVI engine.
BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.
BFFA0054  Bad channel name in Channel List.

BFFC0011  Instrument returned invalid response to ID Query.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    Sets the baud rate into PC serial port.
   
Parameters :
 600    | BAUD 600,
 1200   | BAUD 1200,
 2400   | BAUD 2400,
 4800   | BAUD 4800,
 9600   | BAUD 9600,

     "    Return the instrument ID string.    m    You can use this control to set the initial value of certain attributes for the session.  The following table lists the attributes and the name you use in this parameter to identify the attribute.

Name              Attribute Defined Constant   
--------------------------------------------
RangeCheck        CHR6310A_ATTR_RANGE_CHECK
QueryInstrStatus  CHR6310A_ATTR_QUERY_INSTR_STATUS   
Cache             CHR6310A_ATTR_CACHE   
Simulate          CHR6310A_ATTR_SIMULATE  
RecordCoercions   CHR6310A_ATTR_RECORD_COERCIONS

The format of this string is, "AttributeName=Value" where AttributeName is the name of the attribute and Value is the value to which the attribute will be set.  To set multiple attributes, separate their assignments with a comma.  

If you pass NULL or an empty string for this parameter, the session uses the default values for the attributes.   You can override the default values by assigning a value explicitly in a string you pass for this parameter.  You do not have to specify all of the attributes and may leave any of them out.  If you do not specify one of the attributes, its default value will be used.  

The default values for the attributes are shown below:

    Attribute Name     Default Value
    ----------------   -------------
    RangeCheck         VI_TRUE
    QueryInstrStatus   VI_TRUE
    Cache              VI_TRUE
    Simulate           VI_FALSE
    RecordCoercions    VI_FALSE
    

The following are the valid values for ViBoolean attributes:

    True:     1, True, or VI_TRUE
    False:    0, False, or VI_FALSE


Default Value:
       "Simulate=0,RangeCheck=1,QueryInstrStatus=1,Cache=1"
    ,0 =   � �    Resource Name                     5 : �       ID Query                          7� =� �       Reset Device                      8D# 	 �  �    Instrument Handle                 ;#����  �    Status                          ����  z��                                           C� o  � �    Baud Rate                         Do �  � ,    ID String                         D� �  � �    Option String                                3GPIB 1 "GPIB0::1::INSTR" RS232 COM1 "ASRL1::INSTR"   Yes VI_TRUE No VI_FALSE   Yes VI_TRUE No VI_FALSE    	           	           FCopyright 1998 National Instruments Corporation. All Rights Reserved.              0600 600 1200 1200 2400 2400 4800 4800 9600 9600    	            5"Simulate=0,RangeCheck=1,QueryInstrStatus=1,Cache=1"   K    This function performs the following initialization actions:

- Creates a new IVI instrument driver and optionally sets the initial state of the following session attributes:

    CHR6310A_ATTR_RANGE_CHECK         
    CHR6310A_ATTR_QUERY_INSTR_STATUS  
    CHR6310A_ATTR_CACHE               
    CHR6310A_ATTR_SIMULATE            
    CHR6310A_ATTR_RECORD_COERCIONS    

- Opens a session to the specified device using the interface and address you specify for the Resource Name parameter.

- If the ID Query parameter is set to VI_TRUE, this function queries the instrument ID and checks that it is valid for this instrument driver.

- If the Reset parameter is set to VI_TRUE, this function resets the instrument to a known state.

- Sends initialization commands to set the instrument to the state necessary for the operation of the instrument driver.

- Returns a ViSession handle that you use to identify the instrument in all subsequent instrument driver function calls.

Note:  This function creates a new session each time you invoke it. Although you can open more than one IVI session for the same resource, it is best not to do so.  You can use the same session in multiple program threads.  You can use the Chr6310A_LockSession and Chr6310A_UnlockSession functions to protect sections of code that require exclusive access to the resource.

    	G    Pass the resource name of the device to initialize.

You can also pass the name of a virtual instrument or logical name that you configure with the IVI Configuration utility.  The virtual instrument identifies a specific device and specifies the initial settings for the session.  A logical Name identifies a particular virtual instrument.

Refer to the following table below for the exact grammar to use for this parameter.  Optional fields are shown in square brackets ([]).

Syntax
------------------------------------------------------
GPIB[board]::<primary address>[::secondary address]::INSTR
VXI[board]::<logical address>::INSTR
GPIB-VXI[board]::<logical address>::INSTR
ASRL<port>::INSTR
<LogicalName>
[VInstr->]<VirtualInstrument>

If you do not specify a value for an optional field, the following values are used:

Optional Field - Value
------------------------------------------------------
board - 0
secondary address - none (31)

The following table contains example valid values for this parameter.

"Valid Value" - Description
------------------------------------------------------
"GPIB::22::INSTR" - GPIB board 0, primary address 22 no
                    secondary address
"GPIB::22::5::INSTR" - GPIB board 0, primary address 22
                       secondary address 5
"GPIB1::22::5::INSTR" - GPIB board 1, primary address 22
                        secondary address 5
"VXI::64::INSTR" - VXI board 0, logical address 64
"VXI1::64::INSTR" - VXI board 1, logical address 64
"GPIB-VXI::64::INSTR" - GPIB-VXI board 0, logical address 64
"GPIB-VXI1::64::INSTR" - GPIB-VXI board 1, logical address 64
"ASRL2::INSTR" - COM port 2
"SampleInstr" - Logical name "SampleInstr"
"VInstr->xyz432" - Virtual Instrument "xyz432"
"xyz432" - Logical Name or Virtual Instrument "xyz432"

/*=CHANGE:===================================================* 

Modify the following default value so that it reflects the default address for your instrument.  You must make the corresponding change to the Default Value entry for the control.

 *================================================END=CHANGE=*/ 
Default Value:  "ASRL1::INSTR"

Note: You specify the resource name with the "VInstr->" if you have the logical name that is the same as the virtual instrument name and you want to explicitly use the virtual instrument name. Otherwise, the driver uses the logical name.        Specify whether you want the instrument driver to perform an ID Query.

Valid Range:
VI_TRUE  (1) - Perform ID Query (Default Value)
VI_FALSE (0) - Skip ID Query

When you set this parameter to VI_TRUE, the driver verifies that the instrument you initialize is a type that this driver supports.  

Circumstances can arise where it is undesirable to send an ID Query command string to the instrument.  When you set this parameter to VI_FALSE, the function initializes the instrument without performing an ID Query.     �    Specify whether you want the to reset the instrument during the initialization procedure.

Valid Range:
VI_TRUE  (1) - Reset Device (Default Value)
VI_FALSE (0) - Don't Reset

    �    Returns a ViSession handle that you use to identify the instrument in all subsequent instrument driver function calls.

Notes:

(1) This function creates a new session each time you invoke it.  This is useful if you have multiple physical instances of the same type of instrument.  

(2) Avoid creating multiple concurrent sessions to the same physical instrument.  Although you can create more than one IVI session for the same resource, it is best not to do so.  A better approach is to use the same IVI session in multiple execution threads.  You can use functions Chr6310A_LockSession and Chr6310A_UnlockSession to protect sections of code that require exclusive access to the resource.

    	_    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFC0101  ID Query not supported.
3FFC0102  Reset not supported.

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

BFFA0000  Cannot load IVI engine.
BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.
BFFA0049  Missing option name (nothing before the '=').
BFFA004A  Missing option value (nothing after the '=').
BFFA004B  Bad option name.
BFFA004C  Bad option value.
BFFA0054  Bad channel name in Channel List

BFFC0011  Instrument returned invalid response to ID Query.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
    m    You can use this control to set the initial value of certain attributes for the session.  The following table lists the attributes and the name you use in this parameter to identify the attribute.

Name              Attribute Defined Constant   
--------------------------------------------
RangeCheck        CHR6310A_ATTR_RANGE_CHECK
QueryInstrStatus  CHR6310A_ATTR_QUERY_INSTR_STATUS   
Cache             CHR6310A_ATTR_CACHE   
Simulate          CHR6310A_ATTR_SIMULATE  
RecordCoercions   CHR6310A_ATTR_RECORD_COERCIONS

The format of this string is, "AttributeName=Value" where AttributeName is the name of the attribute and Value is the value to which the attribute will be set.  To set multiple attributes, separate their assignments with a comma.  

If you pass NULL or an empty string for this parameter, the session uses the default values for the attributes.   You can override the default values by assigning a value explicitly in a string you pass for this parameter.  You do not have to specify all of the attributes and may leave any of them out.  If you do not specify one of the attributes, its default value will be used.  

The default values for the attributes are shown below:

    Attribute Name     Default Value
    ----------------   -------------
    RangeCheck         VI_TRUE
    QueryInstrStatus   VI_TRUE
    Cache              VI_TRUE
    Simulate           VI_FALSE
    RecordCoercions    VI_FALSE
    

The following are the valid values for ViBoolean attributes:

    True:     1, True, or VI_TRUE
    False:    0, False, or VI_FALSE


Default Value:
       "Simulate=0,RangeCheck=1,QueryInstrStatus=1,Cache=1"
    S� =   �  �    Resource Name                     \� : �       ID Query                          ^� =� �       Reset Device                      _� �Y �  �    Instrument Handle                 bl#����  �    Status                          ����   P��                                           k� �  � �    Option String                      "GPIB0::1::INSTR"   Yes VI_TRUE No VI_FALSE   Yes VI_TRUE No VI_FALSE    	           	           FCopyright 1998 National Instruments Corporation. All Rights Reserved.    5"Simulate=0,RangeCheck=1,QueryInstrStatus=1,Cache=1"    )    It sets the voltage of sink current on.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
        It sets the voltage of sink current on.

Model Range:
when mode is CCL,CCH,CCDL,CCDH 
63101A|63102A|63103A|63106A|63107A|63112A|63123A  
Low:0 ~ 16(V) , High:0 ~ 80(V)
63105A|63108A  
Low:0 ~ 125(V) , High:0 ~ 500(V)
63110A
Low:0 ~ 100(V) , High:0 ~ 500(V)
63113A
Low:0 ~ 60(V) , High:0 ~ 300(V)

when mode is CRL
63101A|63102A|63103A|63106A|63107A|63112A|63123A :0 ~ 16(V)
63105A|63108A :0 ~ 125(V)
63110A :0 ~ 100(V)
63113A :0 ~ 60(V)

when mode is CRH,CV
63101A|63102A|63103A|63106A|63107A|63112A|63123A :0 ~ 80(V)
63105A|63108A|63110A :0 ~ 500(V)
63113A :0 ~ 300(V)

when mode is CPL,CPH
63101A|63102A|63103A|63106A|63107A|63112A|63123A :0 ~ 80(V)
63105A|63108A :0 ~ 500(V)

when mode is LEDL
63110A :0 ~ 100(V)
63113A :0 ~ 60(V)

when mode is LEDH
63110A :0 ~ 500(V)
63113A :0 ~ 300(V)
    t�����  �    Status                            y}& 
  �  �    Instrument Handle                 z3 � � � �    Configure Von                      	            ?�      �      ��      ?�                   3    It sets the voltage measurement range in CC mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    	    It sets the voltage measurement range in CC mode.

Model Range: 
63101A|63102A|63103A|63106A|63107A|63112A 
0 ~ 16(V) is Low measurement range
17 ~ 80(V) is High measurement range

63105A|63108A  
0 ~ 125(V) is Low measurement range
126 ~ 500(V) is High measurement range

63123A 
0 ~ 24(V) is Low measurement range
25 ~ 120(V) is High measurement range

63110A
0 ~ 100(V) is Low measurement range
101 ~ 500(V) is High measurement range

63113A
0 ~ 60(V) is Low measurement range
61 ~ 300(V) is High measurement range

    ~i����  �    Status                            �'   �  �    Instrument Handle                 �� � � � �    Voltage Range                      	            ?�      �      ��      @T                   !    It sets the action type of Von.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     <    It sets the action type of Von.  

Parameter:

0:OFF
1:ON
    ������  �    Status                            ��# 
  �  �    Instrument Handle                 �C � �  �    Voltage Latch                      	               On 1 Off 0        It resets the Von signal.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �g����  �    Status                            �# 
  �  �    Instrument Handle                  	               C    It sets if the load module will do Auto Load On during power-on.     �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     \    It sets if the load module will do Auto Load On during power-on. 

Parameter:

0:OFF
1:ON
    ������  �    Status                            �<# 
  �  �    Instrument Handle                 �� � �  �    Auto Load                          	               On 1 Off 0    2    It sets the Auto Load On to LOAD ON or PROGRAM.     �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     Q    It sets the Auto Load On to LOAD ON or PROGRAM. 

Parameter:

0:PROGRAM
1:LOAD
    �M����  �    Status                            ��# 
  �  �    Instrument Handle                 �� � �  �    Auto Mode                          	              Load 1 Program 0    7    It sets the buffer sound of load module to ON or OFF.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     P    It sets the buffer sound of load module to ON or OFF.

Parameter:

0:OFF
1:ON
    �����  �    Status                            ��# 
  �  �    Instrument Handle                 �n � �  �    Sound                              	               On 1 Off 0    B    It sets the status of remote control (only effective in RS232C).    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     [    It sets the status of remote control (only effective in RS232C).

Parameter:

0:OFF
1:ON
    ������  �    Status                            �z# 
  �  �    Instrument Handle                 �0 � �  �    Remote                             	               On 1 Off 0    .    It stores the data of CONFigure into EEPROM.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    ������  �    Status                            �3# 
  �  �    Instrument Handle                  	               �    The value at the setting of load module as LOADON is the one changed by the rotary knob (UPDATED/1) or the original set value (OLD/0).    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    The value at the setting of load module as LOADON is the one changed by the rotary knob (UPDATED/1) or the original set value (OLD/0).

Parameter:

0:UPDATE
1:OLD
    ������  �    Status                            ��# 
  �  �    Instrument Handle                 �V � �  �    Load                               	               Update 1 Old 0    +    It sets the timing function to ON or OFF.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     D    It sets the timing function to ON or OFF.

Parameter:

0:OFF
1:ON
    ������  �    Status                            ��& 
  �  �    Instrument Handle                 �Z � � �  �    Timing State                       	               On 1 Off 0    6    It sets the voltage for Timing function at time out.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    9    It sets the voltage for Timing function at time out.

Model Range: 
63101A|63102A|63103A|63106A|63107A|63112A
Low:0 ~ 16(V) , High:0 ~ 80(V)

63105A|63108A  
Low:0 ~ 125(V) , High:0 ~ 500(V)

63110A
Low:0 ~ 100(V) , High:0 ~ 500(V)

63113A
Low:0 ~ 60(V) , High:0 ~ 300(V)

63123A
Low:0 ~ 24(V) , High:0 ~ 120(V)    ¡����  �    Status                            �N& 
  �  �    Instrument Handle                 � � � � �    Timing Trigger                     	            ?�      �      ��                           8    It sets timeout for Timing function from 1ms to 24 hr.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     R    It sets timeout for Timing function from 1ms to 24 hr.

Range: 1 ~ 86400000(ms)
    �_����  �    Status                            �& 
  �  �    Instrument Handle                 �� � � � �    Timing Timeout                     	               ����                  "    It sets VOFF function ON or OFF.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     ;    It sets VOFF function ON or OFF.

Parameter:

0:OFF
1:ON
    �����  �    Status                            չ& 
  �  �    Instrument Handle                 �o � � �  �    Voff State                         	               On 1 Off 0    $    It sets the final loading voltage.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the final loading voltage.

Model Range:
when mode is CCL,CCH,CCDL,CCDH 
63101A|63102A|63103A|63106A|63107A|63112A|63123A  
Low:0 ~ 16(V) , High:0 ~ 80(V)
63105A|63108A  
Low:0 ~ 125(V) , High:0 ~ 500(V)
63110A
Low:0 ~ 100(V) , High:0 ~ 500(V)

when mode is CRL
63101A|63102A|63103A|63106A|63107A|63112A|63123A :0 ~ 16(V)
63105A|63108A :0 ~ 125(V)
63110A :0 ~ 100(V)

when mode is CRH,CV
63101A|63102A|63103A|63106A|63107A|63112A:0 ~ 80(V)
63105A|63108A|63110A :0 ~ 500(V)
63123A :0 ~ 120(V)

when mode is CPL,CPH
63101A|63102A|63103A|63106A|63107A|63112A|63123A :0 ~ 80(V)
63105A|63108A :0 ~ 500(V)

when mode is LEDL
63110A :0 ~ 100(V)
63113A :0 ~ 60(V)

when mode is LEDH
63110A :0 ~ 500(V)
63113A :0 ~ 300(V)    כ����  �    Status                            �H& 
  �  �    Instrument Handle                 �� � � � �    Configure Voff Final V             	            ?�      �      ��                           6    It sets the average number of times for measurement.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     F    It sets the average number of times for measurement.

Range: 1 ~ 64
    ������  �    Status                            �& 
  �  �    Instrument Handle                 �S � � � �    Measure Average                    	               ����      
           &    It sets the Digital IO to ON or OFF.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     ?    It sets the Digital IO to ON or OFF.

Parameter:

0:OFF
1:ON
    �����  �    Status                            �B& 
  �  �    Instrument Handle                 �� � � �  �    Digital I/O                        	               On 1 Off 0    A    It sets if change the MEAS key on the Module to Static/Dynamic.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     Z    It sets if change the MEAS key on the Module to Static/Dynamic.

Parameter:

0:OFF
1:ON
    �E����  �    Status                            ��& 
  �  �    Instrument Handle                 � � � �  �    Key                                	               On 1 Off 0    O    It sets to reply new or old Model Name when querying the device's model name.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     i    It sets to reply new or old Model Name when querying the device's model name.

Parameter:

0:NEW
1:OLD
    �����  �    Status                            ��& 
  �  �    Instrument Handle                 �� � � �  �    Echo                               	               OLD 1 NEW 0    9    It sets to CRL&LEDL I Range 

Parameter:

0:Low
1:High
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �� y  �  �    LEDLCRL Range                     �� �����  �    Status                            � �  �  �    Instrument Handle                  High 1 Low 0    	               #    It returns the setting Von value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     #    It returns the setting Von value.   >����  �    Status                           �& 
  �  �    Instrument Handle                � � � �  �    Get Configure Von                  	               	               It returns the Voltage range.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
          It returns the Voltage range.
   �����  �    Status                           [& 
  �  �    Instrument Handle                 � � �  �    Get_Configure_Voltage_Range        	               	           $    It returns the action type of Von.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     $    It returns the action type of Von.    ����  �    Status                           �& 
  �  �    Instrument Handle                � � � �  �    Get Configure Voltage Latch        	               	           (    It returns the status of Auto Load On.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     (    It returns the status of Auto Load On.   �����  �    Status                           G& 
  �  �    Instrument Handle                � � � �  �    Get Configure Auto Load            	               	           0    It returns the execution type of Auto Load On.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     0    It returns the execution type of Auto Load On.    ����  �    Status                            �& 
  �  �    Instrument Handle                !� � � �  �    Get Configure Auto Mode            	               	           C    It returns the control status of the load module's buzzer sound.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     C    It returns the control status of the load module's buzzer sound.
   "�����  �    Status                           'n& 
  �  �    Instrument Handle                ($ � � �  �    Get Configure Sound                	               	           D    It returns the load module as LOADON setting to be UPDATE or OLD.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     E    It returns the load module as LOADON setting to be UPDATE or OLD.

   )v����  �    Status                           .#& 
  �  �    Instrument Handle                .� � � �  �    Get Configure Load                 	               	           9    It returns the timing function setting to be ON or OFF.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     E    It returns the load module as LOADON setting to be UPDATE or OLD.

   0"����  �    Status                           4�& 
  �  �    Instrument Handle                5� � � �  �    Get Configure Timing State         	               	           (    It returns the voltage set at timeout.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     (    It returns the voltage set at timeout.   6�����  �    Status                           ;j& 
  �  �    Instrument Handle                <  � � �  �    Get Configure Timing Trigger       	               	               It returns the timeout set.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
         It returns the timeout set.   =0����  �    Status                           A�& 
  �  �    Instrument Handle                B� � � �  �    Get Configure Timing Timeout       	               	            7    It returns the VOFF function setting to be ON or OFF.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     8    It returns the VOFF function setting to be ON or OFF.
   C�����  �    Status                           H_& 
  �  �    Instrument Handle                I � � �  �    Get Configure Voff State           	               	           +    It returns the final loading voltage set.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     +    It returns the final loading voltage set.   JC����  �    Status                           N�& 
  �  �    Instrument Handle                O� � � �  �    Get Configure Voff Final V         	               	           #    It returns the average times set.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     #    It returns the average times set.   P�����  �    Status                           Ul& 
  �  �    Instrument Handle                V" � � �  �    Get Configure Measure Average      	               	            4    It returns the Digital IO setting to be ON or OFF.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     5    It returns the Digital IO setting to be ON or OFF.
   WD����  �    Status                           [�& 
  �  �    Instrument Handle                \� � � �  �    Get Configure Digital IO           	               	               It returns the key setting.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
         It returns the key setting.
   ]�����  �    Status                           bq& 
  �  �    Instrument Handle                c' � � �  �    Get Configure Key                  	               	           /    It returns the ECHO setting to be NEW or OLD.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     0    It returns the ECHO setting to be NEW or OLD.
   d?����  �    Status                           h�& 
  �  �    Instrument Handle                i� � � �  �    Get Configure Echo                 	               	          �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     1    It returns the CRL&LEDL I Range 

1:High
0:Low
   j�����  �    Status                           oB#   �  �    Instrument Handle                o� � � �  �    LEDLCRL Range                      	               	           k    It selects a specific channel by which the coming channel-specific command will be received and executed.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It selects a specific channel by which the coming channel-specific command will be received and executed.

Range: 6312A (1 ~ 4)
       6314A (1 ~ 8)   q_����  �    Status                           v& 	  �  �    Instrument Handle                v� � � � �    Channel                            	               ����                 +    It enables or disables the load module.      �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     D    It enables or disables the load module.  

Parameter:

0:OFF
1:ON
   xY����  �    Status                           }# 
  �  �    Instrument Handle                }� � �  �    Channel Active                     	               On 1 Off 0    U    It sets the load module to receive synchronized command action of RUN ABORT or not.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     n    It sets the load module to receive synchronized command action of RUN ABORT or not.

Parameter:

0:OFF
1:ON
   "����  �    Status                           ��# 
  �  �    Instrument Handle                �� � �  �    Channel Synchronized               	              On 1 Off 0    *    It returns the current specific channel.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     *    It returns the current specific channel.   ������  �    Status                           ��# 
  �  �    Instrument Handle                �M � � �  �    Get Channel Load                   	               	            .    It returns the current min specific channel.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     .    It returns the current min specific channel.   �p����  �    Status                           �# 
  �  �    Instrument Handle                �� � � �  �    Get Min of Channel Load            	               	            .    It returns the current max specific channel.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     .    It returns the current max specific channel.   ������  �    Status                           ��# 
  �  �    Instrument Handle                �] � � �  �    Get Max of Channel Load            	               	            Q    It returns to the load module and makes it receive synchronized command status.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     Q    It returns to the load module and makes it receive synchronized command status.   ������  �    Status                           �T# 
  �  �    Instrument Handle                �
 � � �  �    Get Channel Synchronized           	               	            4    This query requests the module to identify itself.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     4    This query requests the module to identify itself.   �Z����  �    Status                           �! 
  �  �    Instrument Handle                �� � � �  �    Get Channel ID                     	               	            G    The LOAD command makes the electronic load active/on or inactive/off.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     a    The LOAD command makes the electronic load active/on or inactive/off. 

Parameter:

0:OFF
1:ON
   �����  �    Status                           ��# 
  �  �    Instrument Handle                �f � �  �    Load State                         	               On 1 Off 0    =    It activates or inactivates the short-circuited simulation.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     V    It activates or inactivates the short-circuited simulation.

Parameter:

0:OFF
1:ON
   ������  �    Status                           �~# 
  �  �    Instrument Handle                �4 � �  �    Load Short State                   	               On 1 Off 0    7    It sets the mode of short key in the electronic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     U    It sets the mode of short key in the electronic load.

Parameter:

0:HOLD
1:TOGGLE
   ������  �    Status                           �;# 
  �  �    Instrument Handle                �� � � �  �    Load Short Key                     	               TOGGLE 1 HOLD 0    ?    This command resets or returns status of the electronic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �W����  �    Status                           �&   �  �    Instrument Handle                  	               .    It clears all data and return it to default.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �j����  �    Status                           �&   �  �    Instrument Handle                  	               '    It saves the current data as default.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �v����  �    Status                           �#&   �  �    Instrument Handle                  	               .    It returns if the electronic load is active.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     .    It returns if the electronic load is active.   ͉����  �    Status                           �6& 
  �  �    Instrument Handle                �� � � �  �    Get Load State                     	               	           1    It returns the short-circuit simulation state.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     1    It returns the short-circuit simulation state.
   �����  �    Status                           ��& 
  �  �    Instrument Handle                �y � � �  �    Get Load Short State               	               	           :    It returns the mode of short key in the electronic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     :    It returns the mode of short key in the electronic load.   گ����  �    Status                           �\& 
  �  �    Instrument Handle                � � � �  �    Get Load Short Key                 	               	           (    It returns the electronic load status.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It returns the electronic load status.

Bit Position             7   6   5   4   3   2   1   0
Bit Name         -   -   -   -   -   OT  RV  OP  OV  OC
Bit Weight              128  64  32  16  8   4   2   1   �?����  �    Status                           ��& 
  �  �    Instrument Handle                � � � �  �    Load Protection Status             	               	            '    It sets all electronic loads to "ON".    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �c����  �    Status                           �&   �  �    Instrument Handle                  	               (    It sets all electronic loads to "OFF".    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �p����  �    Status                           �&   �  �    Instrument Handle                  	               6    It sets the the display mode of the electronic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the the display mode of the electronic load.

Parameter:
0 :L   
It displays the voltage and current values of channel L.

1 :LPI 
It displays the power and current values of channel L.

2 :LVP 
It displays the voltage and power values of channel L.

3 :R   
It displays the voltage and current values of channel R.

4 :RPI 
It displays the power and current values of channel R.

5 :RVP 
It displays the voltage and power values of channel R.

6 :LRV 
It displays the voltage value of channel L and channel R.

7 :LRI 
It displays the current value of channel L and channel R.

8 :LRP 
It displays the power value of channel L and channel R.
   �����  �    Status                           �8& 
  �  �    Instrument Handle                �� e �� �    Display                            	                       	   2L 0 LPI 1 LVP 2 R 3 RPI 4 RVP 5 LRV 6 LRI 7 LRP 8    �    This command clears the following registers
1.  Channel Status Event registers for all channels
2.  Channel Summary Event register
3.  Questionable Status Event register
4.  Standard Event Status Event register
5.  Operation Status Event register
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �t����  �    Status                           !&   �  �    Instrument Handle                  	               �    This command sets the condition of the Standard Event Status Enable register, which determines which events of the Standard Event Status Event register are allowed to set the ESB of the Status Byte register.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    This command programs the Standard Event Status Enable Register bits.  If one or more of the enable events of the Standard Event Status is set, the ESB of Status Byte Register is set too.        

Range: 0 ~ 255

Bit Position             7   6   5   4   3   2   1   0
Bit Name    - - -   -    -   -   CME EXE DDE QYE -   -

CME = Command error enable
DDE = Device-dependent
errorEXE = Execution error enable
QYE = Query error enable
   +����  �    Status                           	�&   �  �    Instrument Handle                
� � � � �    Set ESE                            	               ����                  �    This command causes the interface to set the OPC bit (bit 0) of the Standard Event Status register when the Electronic Frame (6310A) has completed all pending operations. 
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �����  �    Status                           r&   �  �    Instrument Handle                  	               �    This command restores the electronic load to a state that was previously stored in memory with the *SAV command to the specified location. 

Range: 1 ~ 101

    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    This command restores the electronic load to a state that was                         previously stored in memory with the *SAV command to the                          specified location.

Range: 1 ~ 101
   I����  �    Status                           �&   �  �    Instrument Handle                � � � � �    Set RCL                            	               ����                 =    This command forces an ABORt, *CLS, LOAD=PROT=CLE command.     �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �����  �    Status                            :&   �  �    Instrument Handle                  	               �    This command stores the present state of the single electronic load and the states of all channels of the multiple loads in a specified location in memory.

Range: 1 ~ 100    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    This command stores the present state of the single electronic load and the states of all channels of the multiple loads in a specified location in memory.

Range: 1 ~ 100   "����  �    Status                           &�&   �  �    Instrument Handle                '� � � � �    Set SAV                            	               ����                 �     This command sets the condition of the Service Request Enable register, which determines which events of the Status Byte register (see *STB) are allowed to set the MSS( Master Status Summary) bit.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �     This command sets the condition of the Service Request Enable register, which determines which events of the Status Byte register (see *STB) are allowed to set the MSS( Master Status Summary) bit.

Range: 0 ~ 255
   )�����  �    Status                           .y&   �  �    Instrument Handle                // � � � �    Set SRE                            	               ����                  �    This command programs the Standard Event Status Enable Register bits.If one or more of the enable events of the Standard Event Status is set, the ESB of Status Byte Register is set too.        
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     l    This query reads the Standard Event Status Register. Reading the Standard Event Status Register clears it.   1�����  �    Status                           6N'   �  �    Instrument Handle                7 � � �  �    Get ESR                            	               	            G    This query requests the Electronic Frame (6310A) to identify itself.     �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     W    Return the instrument ID string.You must pass a ViChar array with at least 256 bytes.   8�����  �    Status                           =/'   �  �    Instrument Handle                =� � � �  �    Get Identification String          	               	            N    This query returns an ASCII "1" when all pending operations are completed. 
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     l    This query reads the Standard Event Status Register. Reading the Standard Event Status Register clears it.   ?U����  �    Status                           D'   �  �    Instrument Handle                D� � � �  �    Get OPC                            	               	            �    This command returns the types of Electronic Frame (6314A).If channel does not exist, it returns 0.  If channel exists, it returns the types like 63103A, 63102A, 63107R, 63107L..
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    This command returns the types of Electronic Frame (6314A).If channel does not exist, it returns 0.  If channel exists, it returns the types like 63103A, 63102A, 63107R, 63107L..   F�����  �    Status                           KQ'   �  �    Instrument Handle                L � � �  �    Get RDT                            	               	           �    This query reads the Status Byte register. Note that the MSS     (Master Summary Status) bit instead of RQS bit is returned in Bit 6.  This bit indicates if the electronic load has at least one reason for requesting service.  *STB? does not clear the Status Byte register, which is cleared only when subsequent action has cleared all its set bits.

Bit Position             7    6    5    4    3    2    1    0
Bit Name    - - -   -    -   MSS   ESB  MAV  QUES CSUM -    -    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    This query reads the Status Byte register. Note that the MSS     (Master Summary Status) bit instead of RQS bit is returned in Bit 6.  This bit indicates if the electronic load has at least one reason for requesting service.  *STB? does not clear the Status Byte register, which is cleared only when subsequent action has cleared all its set bits.

Bit Position             7    6    5    4    3    2    1    0
Bit Name    - - -   -    -   MSS   ESB  MAV  QUES CSUM -    -   O`����  �    Status                           T'   �  �    Instrument Handle                T� � � �  �    Get STB                            	               	           �    This function sets the value of a ViInt32 attribute.

This is a low-level function that you can use to set the values of instrument-specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid or is different than the value you specify. 

This instrument driver contains high-level functions that set most of the instrument attributes.  It is best to use the high-level driver functions as much as possible.  They handle order dependencies and multithread locking for you.  In addition, they perform status checking only after setting all of the attributes.  In contrast, when you set multiple attributes using the SetAttribute functions, the functions check the instrument status after each call.

Also, when state caching is enabled, the high-level functions that configure multiple attributes perform instrument I/O only for the attributes whose value you change.  Thus, you can safely call the high-level functions without the penalty of redundant instrument I/O.


     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 

        Pass the value to which you want to set the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none    C    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViInt32 type.   
  If you choose to see all IVI attributes, the data types appear
  to the right of the attribute names in the list box. 
  Attributes with data types other than ViInt32 are dim.  If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to set the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
   \3-   �  �    Instrument Handle                \�#����  �    Status                           d� � � �  �    Attribute Value                 ���� � ���                                          f� = � � �    Attribute ID                     l =  �  �    Channel Name                           	               .Press <ENTER> for a list of 
value constants.                0    ""   �    This function sets the value of a ViReal64 attribute.

This is a low-level function that you can use to set the values of instrument-specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid or is different than the value you specify. 

This instrument driver contains high-level functions that set most of the instrument attributes.  It is best to use the high-level driver functions as much as possible.  They handle order dependencies and multithread locking for you.  In addition, they perform status checking only after setting all of the attributes.  In contrast, when you set multiple attributes using the SetAttribute functions, the functions check the instrument status after each call.

Also, when state caching is enabled, the high-level functions that configure multiple attributes perform instrument I/O only for the attributes whose value you change.  Thus, you can safely call the high-level functions without the penalty of redundant instrument I/O.


     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
        Pass the value to which you want to set the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none    B    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViReal64
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box.
  Attributes with data types other than ViReal64 are dim.  If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to set the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
   s�-   �  �    Instrument Handle                t=#����  �    Status                           {� � � �  �    Attribute Value                 ���� � ���                                          ~ = � � �    Attribute ID                     �^ =  �  �    Channel Name                           	               .Press <ENTER> for a list of 
value constants.                0    ""   �    This function sets the value of a ViString attribute.

This is a low-level function that you can use to set the values of instrument-specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid or is different than the value you specify. 

This instrument driver contains high-level functions that set most of the instrument attributes.  It is best to use the high-level driver functions as much as possible.  They handle order dependencies and multithread locking for you.  In addition, they perform status checking only after setting all of the attributes.  In contrast, when you set multiple attributes using the SetAttribute functions, the functions check the instrument status after each call.

Also, when state caching is enabled, the high-level functions that configure multiple attributes perform instrument I/O only for the attributes whose value you change.  Thus, you can safely call the high-level functions without the penalty of redundant instrument I/O.


     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
        Pass the value to which you want to set the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none    A    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViString
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box.
  Attributes with data types other than ViString are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to set the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
   ��-   �  �    Instrument Handle                ��#����  �    Status                           �G � � �  �    Attribute Value                 ���� � ���                                          �b = � � �    Attribute ID                     �� =  �  �    Channel Name                           	               .Press <ENTER> for a list of 
value constants.                0    ""   �    This function sets the value of a ViBoolean attribute.

This is a low-level function that you can use to set the values of instrument-specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid or is different than the value you specify. 

This instrument driver contains high-level functions that set most of the instrument attributes.  It is best to use the high-level driver functions as much as possible.  They handle order dependencies and multithread locking for you.  In addition, they perform status checking only after setting all of the attributes.  In contrast, when you set multiple attributes using the SetAttribute functions, the functions check the instrument status after each call.

Also, when state caching is enabled, the high-level functions that configure multiple attributes perform instrument I/O only for the attributes whose value you change.  Thus, you can safely call the high-level functions without the penalty of redundant instrument I/O.


     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
        Pass the value to which you want to set the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none    C    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViBoolean
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box.
  Attributes with data types other than ViBoolean are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to set the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
   �-   �  �    Instrument Handle                ��#����  �    Status                           �� � � �  �    Attribute Value                 ���� � ���                                          �� = � � �    Attribute ID                     �� =  �  �    Channel Name                           	               .Press <ENTER> for a list of 
value constants.                0    ""   �    This function sets the value of a ViSession attribute.

This is a low-level function that you can use to set the values of instrument-specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid or is different than the value you specify. 

This instrument driver contains high-level functions that set most of the instrument attributes.  It is best to use the high-level driver functions as much as possible.  They handle order dependencies and multithread locking for you.  In addition, they perform status checking only after setting all of the attributes.  In contrast, when you set multiple attributes using the SetAttribute functions, the functions check the instrument status after each call.

Also, when state caching is enabled, the high-level functions that configure multiple attributes perform instrument I/O only for the attributes whose value you change.  Thus, you can safely call the high-level functions without the penalty of redundant instrument I/O.


     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
        Pass the value to which you want to set the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to set the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    C    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViSession
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box.
  Attributes with data types other than ViSession are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   �o-   �  �    Instrument Handle                �)#����  �    Status                           �� � � �  �    Attribute Value                  �  =  �  �    Channel Name                     � = � � �    Attribute ID                    ���� � ���                                                	               ""                0    .Press <ENTER> for a list of 
value constants.   �    This function queries the value of a ViInt32 attribute.

You can use this function to get the values of instrument- specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid. 

     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    �    Returns the current value of the attribute.  Pass the address of a ViInt32 variable.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has named constants as valid values, you can view a
  list of the constants by pressing <ENTER> on this control.  
  Select a value by double-clicking on it or by selecting it and 
  then pressing <ENTER>.  
     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to obtain the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    �    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Help text is
  shown for each attribute.  Select an attribute by 
  double-clicking on it or by selecting it and then pressing
  <ENTER>.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViInt32 type. 
  If you choose to see all IVI attributes, the data types appear
  to the right of the attribute names in the list box.  
  Attributes with data types other than ViInt32 are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   ͧ-   �  �    Instrument Handle                �a#����  �    Status                           � � � �  �    Attribute Value                  �� =  �  �    Channel Name                     �� = � � �    Attribute ID                           	           	            ""                0   �    This function queries the value of a ViReal64 attribute.

You can use this function to get the values of instrument- specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid. 

     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    �    Returns the current value of the attribute.  Pass the address of a ViReal64 variable.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has named constants as valid values, you can view a
  list of the constants by pressing <ENTER> on this control.  
  Select a value by double-clicking on it or by selecting it and 
  then pressing <ENTER>.  
     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to obtain the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    �    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Help text is
  shown for each attribute.  Select an attribute by 
  double-clicking on it or by selecting it and then pressing
  <ENTER>.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViReal64
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box. 
  Attributes with data types other than ViReal64 are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   �}-   �  �    Instrument Handle                �7#����  �    Status                           �� � � �  �    Attribute Value                  � =  �  �    Channel Name                     � = � � �    Attribute ID                           	           	           ""                0   4    This function queries the value of a ViString attribute.

You can use this function to get the values of instrument- specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid. 

You must provide a ViChar array to serve as a buffer for the value.  You pass the number of bytes in the buffer as the Buffer Size parameter.  If the current value of the attribute, including the terminating NUL byte, is larger than the size you indicate in the Buffer Size parameter, the function copies Buffer Size - 1 bytes into the buffer, places an ASCII NUL byte at the end of the buffer, and returns the buffer size you must pass to get the entire value.  For example, if the value is "123456" and the Buffer Size is 4, the function places "123" into the buffer and returns 7.

If you want to call this function just to get the required buffer size, you can pass 0 for the Buffer Size and VI_NULL for the Attribute Value buffer.  

If you want the function to fill in the buffer regardless of the   number of bytes in the value, pass a negative number for the Buffer Size parameter.  


     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    
�    Reports the status of this operation.

If the function succeeds and the buffer you pass is large enough to hold the entire value, the function returns 0.   

If the current value of the attribute, including the terminating NUL byte, is larger than the size you indicate in the Buffer Size parameter, the function copies Buffer Size - 1 bytes into the buffer, places an ASCII NUL byte at the end of the buffer, and returns the buffer size you must pass to get the entire value.  For example, if the value is "123456" and the Buffer Size is 4, the function places "123" into the buffer and returns 7.

If the function fails for some other reason, it returns a negative error code.  For more information on error codes, refer to the Status return value control in one of the other function panels.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    &    The buffer in which the function returns the current value of the attribute.  The buffer must be of type ViChar and have at least as many bytes as indicated in the Buffer Size parameter.

If the current value of the attribute, including the terminating NUL byte, contains more bytes that you indicate in this parameter, the function copies Buffer Size - 1 bytes into the buffer, places an ASCII NUL byte at the end of the buffer, and returns the buffer size you must pass to get the entire value.  For example, if the value is "123456" and the Buffer Size is 4, the function places "123" into the buffer and returns 7.

If you specify 0 for the Buffer Size parameter, you can pass VI_NULL for this parameter.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has named constants as valid values, you can view a
  list of the constants by pressing <ENTER> on this control.  
  Select a value by double-clicking on it or by selecting it and 
  then pressing <ENTER>.  
     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to obtain the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    �    Pass the number of bytes in the ViChar array you specify for the Attribute Value parameter.  

If the current value of the attribute, including the terminating NUL byte, contains more bytes that you indicate in this parameter, the function copies Buffer Size - 1 bytes into the buffer, places an ASCII NUL byte at the end of the buffer, and returns the buffer size you must pass to get the entire value.  For example, if the value is "123456" and the Buffer Size is 4, the function places "123" into the buffer and returns 7.

If you pass a negative number, the function copies the value to the buffer regardless of the number of bytes in the value.

If you pass 0, you can pass VI_NULL for the Attribute Value buffer parameter.
    �    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Help text is
  shown for each attribute.  Select an attribute by 
  double-clicking on it or by selecting it and then pressing
  <ENTER>.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViString
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box. 
  Attributes with data types other than ViString are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   ��-   �  �    Instrument Handle                ��#����  �    Status or Required Size          > � L � �    Attribute Value                  l =  �  �    Channel Name                     s =� �  �    Buffer Size                      
V = � � �    Attribute ID                           	           	            ""    512                0   �    This function queries the value of a ViBoolean attribute.

You can use this function to get the values of instrument- specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid. 

     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    �    Returns the current value of the attribute.  Pass the address of a ViBoolean variable.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has named constants as valid values, you can view a
  list of the constants by pressing <ENTER> on this control.  
  Select a value by double-clicking on it or by selecting it and 
  then pressing <ENTER>.  
     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to obtain the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    �    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Help text is
  shown for each attribute.  Select an attribute by 
  double-clicking on it or by selecting it and then pressing
  <ENTER>.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViBoolean
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box. 
  Attributes with data types other than ViBoolean are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   /-   �  �    Instrument Handle                �#����  �    Status                           � � � �  �    Attribute Value                  e =  �  �    Channel Name                     l = � � �    Attribute ID                           	           	            ""                0   �    This function queries the value of a ViSession attribute.

You can use this function to get the values of instrument- specific attributes and inherent IVI attributes.  If the attribute represents an instrument state, this function performs instrument I/O in the following cases:

- State caching is disabled for the entire session or for the particular attribute.

- State caching is enabled and the currently cached value is invalid. 

     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    �    Returns the current value of the attribute.  Pass the address of a ViSession variable.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has named constants as valid values, you can view a
  list of the constants by pressing <ENTER> on this control.  
  Select a value by double-clicking on it or by selecting it and 
  then pressing <ENTER>.  
     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to obtain the value of the attribute. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    �    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Help text is
  shown for each attribute.  Select an attribute by 
  double-clicking on it or by selecting it and then pressing
  <ENTER>.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViSession
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box. 
  Attributes with data types other than ViSession are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   %-   �  �    Instrument Handle                %�#����  �    Status                           -� � � �  �    Attribute Value                  /A =  �  �    Channel Name                     0H = � � �    Attribute ID                           	           	            ""                0    S    This function checks the validity of a value you specify for a ViInt32 attribute.     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    (    Pass the value which you want to verify as a valid value for the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to check the attribute value. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    @    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViInt32 type. 
  If you choose to see all IVI attributes, the data types appear
  to the right of the attribute names in the list box. 
  Attributes with data types other than ViInt32 are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   6�-   �  �    Instrument Handle                7=#����  �    Status                           >� � � �  �    Attribute Value                  A) =  �  �    Channel Name                    ���� � ���                                          B( = � � �    Attribute ID                           	               ""    .Press <ENTER> for a list of 
value constants.                0    T    This function checks the validity of a value you specify for a ViReal64 attribute.     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    (    Pass the value which you want to verify as a valid value for the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to check the attribute value. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    B    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViReal64
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box. 
  Attributes with data types other than ViReal64 are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   Ig-   �  �    Instrument Handle                J!#����  �    Status                           Q� � � �  �    Attribute Value                  T =  �  �    Channel Name                    ���� � ���                                          U = � � �    Attribute ID                           	               ""    .Press <ENTER> for a list of 
value constants.                0    T    This function checks the validity of a value you specify for a ViString attribute.     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    (    Pass the value which you want to verify as a valid value for the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to check the attribute value. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    B    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViString
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box. 
  Attributes with data types other than ViString are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   \M-   �  �    Instrument Handle                ]#����  �    Status                           d� � � �  �    Attribute Value                  f� =  �  �    Channel Name                    ���� � ���                                          g� = � � �    Attribute ID                           	               ""    .Press <ENTER> for a list of 
value constants.                0    U    This function checks the validity of a value you specify for a ViBoolean attribute.     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    (    Pass the value which you want to verify as a valid value for the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to check the attribute value. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    D    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViBoolean
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box. 
  Attributes with data types other than ViBoolean are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   o4-   �  �    Instrument Handle                o�#����  �    Status                           w� � � �  �    Attribute Value                  y� =  �  �    Channel Name                    ���� � ���                                          z� = � � �    Attribute ID                           	               ""    .Press <ENTER> for a list of 
value constants.                0    U    This function checks the validity of a value you specify for a ViSession attribute.     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0001  Instrument error. Call Chr6310A_error_query.
BFFA000C  Invalid attribute.
BFFA000D  Attribute is not writable.
BFFA000E  Attribute is not readable.
BFFA000F  Invalid parameter.
BFFA0010  Invalid value.
BFFA0012  Attribute not supported.
BFFA0013  Value not supported.
BFFA0014  Invalid type.
BFFA0015  Types do not match.
BFFA0016  Attribute already has a value waiting to be updated.
BFFA0018  Not a valid configuration.
BFFA0019  Requested item does not exist or value not available.
BFFA001A  Requested attribute value not known.
BFFA001B  No range table.
BFFA001C  Range table is invalid.
BFFA001F  No channel table has been built for the session.
BFFA0020  Channel name specified is not valid.
BFFA0044  Channel name required.
BFFA0045  Channel name not allowed.
BFFA0046  Attribute not valid for channel.
BFFA0047  Attribute must be channel based.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
3FFF0085  The status value you passed is unknown. 
    (    Pass the value which you want to verify as a valid value for the attribute.

From the function panel window, you can use this control as follows.

- If the attribute currently showing in the Attribute ID ring
  control has constants as valid values, you can view a list of
  the constants by pressing <ENTER> on this control.  Select a
  value by double-clicking on it or by selecting it and then
  pressing <ENTER>.  

  Note:  Some of the values might not be valid depending on the
  current settings of the instrument session.

Default Value: none     �    If the attribute is channel-based, this parameter specifies the name of the channel on which to check the attribute value. If the attribute is not channel-based, then pass VI_NULL or an empty string.

Valid Channel Names:  1

Default Value:  ""
    D    Pass the ID of an attribute.

From the function panel window, you can use this control as follows.

- Click on the control or press <ENTER>, <spacebar>, or
  <ctrl-down arrow>, to display a dialog box containing a
  hierarchical list of the available attributes.  Attributes 
  whose value cannot be set are dim.  Help text is shown for 
  each attribute.  Select an attribute by double-clicking on it  
  or by selecting it and then pressing <ENTER>.

  Read-only attributes appear dim in the list box.  If you 
  select a read-only attribute, an error message appears.

  A ring control at the top of the dialog box allows you to see 
  all IVI attributes or only the attributes of the ViSession
  type.  If you choose to see all IVI attributes, the data types
  appear to the right of the attribute names in the list box. 
  Attributes with data types other than ViSession are dim. If
  you select an attribute data type that is dim, LabWindows/CVI
  transfers you to the function panel for the corresponding
  function that is consistent with the data type.

- If you want to enter a variable name, press <CTRL-T> to change
  this ring control to a manual input box.

- If the attribute in this ring control has named constants as  
  valid values, you can view the constants by moving to the 
  Attribute Value control and pressing <ENTER>.
   �-   �  �    Instrument Handle                ��#����  �    Status                           �� � � �  �    Attribute Value                  �� =  �  �    Channel Name                    ���� � ���                                          �� = � � �    Attribute ID                           	               ""    .Press <ENTER> for a list of 
value constants.                0    =    This command sets operational modes of the electronic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    This command sets operational modes of the electronic load.

Parameter: 
0|CCL, 1|CCH , 2|CCDL , 3|CCDH , 4|CRL , 5|CRH , 6|CV , 7|CPL , 8|CPH , 9|LEDL , 10|LEDH

Note:
1. 63107A(R) not support CCH,CCDH,CPH.
2. 63110A not support CCDL,CCDH,CPL,CPH.
3. 63105A when V_Range is HIGH ,not support CCL,CCDL.
4. 63108A when V_Range is HIGH ,not support CCL,CCDL.
5.  63110A not support CPL,CPH.
6. only 63110A,63113A support LEDL,LEDH.
   ������  �    Status                           ��& 
  �  �    Instrument Handle                �Q e �� �    Mode                               	                          FCCL 0 CCH 1 CCDL 2 CCDH 3 CRL 4 CRH 5 CV 6 CPL 7 CPH 8 LEDL 9 LEDH 10    9    It returns the operational mode of the electronic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     :    It returns the operational mode of the electronic load.
   �N����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � �  �    Get Mode                           	               	            ;    It sets the Static Load Current of constant current mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the Static Load Current of constant current mode.

Model Range: 
63101A  
Low:0 ~ 4(A) , High:0 ~ 40(A)

63102A  
Low:0 ~ 2(A) , High:0 ~ 20(A)

63103A  
Low:0 ~ 6(A) , High:0 ~ 60(A)

63105A (V Range is HIGH) High:0 ~ 10(A)
63105A (V Range is LOW)  Low: 0 ~ 1(A) , High:0 ~ 10(A)

63106A  
Low:0 ~ 12(A) , High:0 ~ 120(A)

63107A(R) Low:0 ~ 5(A)
63107A(L) Low:0 ~ 4(A) , High:0 ~ 40(A)

63108A (V Range is HIGH) High:0 ~ 20(A)
63108A (V Range is LOW)  Low:0 ~ 2(A) , High:0 ~ 20(A)

63110A  
Low:0 ~ 0.6(A) , High:0 ~ 2(A)

63112A  
Low:0 ~ 24(A) , High:0 ~ 240(A)

63123A  
Low:0 ~ 7(A) , High:0 ~ 70(A)

63113A  
Low:0 ~ 5(A) , High:0 ~ 20(A)   ������  �    Status                           ��& 
  �  �    Instrument Handle                �T � � � �    CC Static L1                       	            ?�      �      ��                           ;    It sets the Static Load Current of constant current mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
        It sets the Static Load Current of constant current mode.

Model Range: 
63101A  
Low:0 ~ 4(A) , High:0 ~ 40(A)

63102A  
Low:0 ~ 2(A) , High:0 ~ 20(A)

63103A  
Low:0 ~ 6(A) , High:0 ~ 60(A)

63105A  
High:0 ~ 10(A)

63106A  
Low:0 ~ 12(A) , High:0 ~ 120(A)

63107A(R) Low:0 ~ 5(A)
63107A(L) Low:0 ~ 4(A) , High:0 ~ 40(A)

63108A  
Low:0 ~ 2(A) , High:0 ~ 20(A)

63110A  
Low:0 ~ 0.6(A) , High:0 ~ 2(A)

63112A  
Low:0 ~ 24(A) , High:0 ~ 240(A)

63123A  
Low:0 ~ 7(A) , High:0 ~ 70(A)

63113A  
Low:0 ~ 5(A) , High:0 ~ 20(A)   �����  �    Status                           ��& 
  �  �    Instrument Handle                �k � � � �    CC Static L2                       	            ?�      �      ��                           E    It sets the current rise slew rate of constant current static mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    ~    It sets the current rise slew rate of constant current static mode.

Model Range: 
63101A  
Low:0.00064 ~ 0.160(A/us) , High:0.0064 ~ 1.6(A/us)

63102A 
Low:0.00032 ~ 0.008(A/us) , High:0.0032 ~ 0.8(A/us)

63103A 
Low:0.001 ~ 0.25(A/us) , High:0.01 ~ 2.5(A/us)

63105A (V Range is HIGH) High:0.0016 ~ 0.4(A/us)
63105A (V Range is LOW)  Low:0.00016 ~ 0.04(A/us)
63105A (V Range is LOW)  High:0.0016 ~ 0.4(A/us)

63106A 
Low:0.002 ~ 0.5(A/us) , High:0.02 ~ 5(A/us)

63107A(R) Low:0.0008 ~ 0.2(A/us)
63107A(L) Low:0.00064 ~ 0.16(A/us) , High:0.0064 ~ 1.6(A/us)

63108A (V Range is HIGH) High:0.0032 ~ 0.8(A/us)
63108A (V Range is LOW)  Low:0.00032 ~ 0.08(A/us)
63108A (V Range is LOW)  High:0.0032 ~ 0.8(A/us)

63110A 
Not Support

63112A 
Low:0.004 ~ 1(A/us) , High:0.04 ~ 10(A/us)

63123A 
Low:0.001 ~ 0.25(A/us) , High:0.01 ~ 2.5(A/us)

63113A
Low:0.008 ~ 0.2 (A/us) , High:0.032 ~ 0.8 (A/us)   ������  �    Status                           �V& 
  �  �    Instrument Handle                � � � � �    CC Static Rise Slew Rate           	            ?�      �      ��      ?pbM���             E    It sets the current fall slew rate of constant current static mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the current fall slew rate of constant current static mode.

Model Range: 
63101A  
Low:0.00064 ~ 0.160(A/us) , High:0.0064 ~ 1.6(A/us)

63102A 
Low:0.00032 ~ 0.008(A/us) , High:0.0032 ~ 0.8(A/us)

63103A 
Low:0.001 ~ 0.25(A/us) , High:0.01 ~ 2.5(A/us)

63105A (V Range is HIGH) High:0.0016 ~ 0.4(A/us)
63105A (V Range is LOW)  Low:0.00016 ~ 0.04(A/us)
63105A (V Range is LOW)  High:0.0016 ~ 0.4(A/us)

63106A 
Low:0.002 ~ 0.5(A/us) , High:0.02 ~ 5(A/us)

63113A
Low:0.008 ~ 0.2 (A/us) , High:0.032 ~ 0.8 (A/us)

63107A(R) Low:0.0008 ~ 0.2(A/us)
63107A(L) Low:0.00064 ~ 0.16(A/us) , High:0.0064 ~ 1.6(A/us)

63108A (V Range is HIGH) High:0.0032 ~ 0.8(A/us)
63108A (V Range is LOW)  Low:0.00032 ~ 0.08(A/us)
63108A (V Range is LOW)  High:0.0032 ~ 0.8(A/us)

63110A 
Not Support

63112A 
Low:0.004 ~ 1(A/us) , High:0.04 ~ 10(A/us)

63123A 
Low:0.001 ~ 0.25(A/us) , High:0.01 ~ 2.5(A/us)

63113A
Low:0.008 ~ 0.2 (A/us) , High:0.032 ~ 0.8 (A/us)   ������  �    Status                           �f& 
  �  �    Instrument Handle                � � � � �    CC Static Fall Slew Rate           	            ?�      �      ��      ?pbM���             @    It sets the Dynamic Load Current during constant current mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    W    It sets the Dynamic Load Current during constant current mode.

Model Range: 
63101A  
Low:0 ~ 4(A) , High:0 ~ 40(A)

63102A  
Low:0 ~ 2(A) , High:0 ~ 20(A)

63103A  
Low:0 ~ 6(A) , High:0 ~ 60(A)

63105A (V Range is HIGH) High:0 ~ 10(A)
63105A (V Range is LOW)  Low: 0 ~ 1(A) , High:0 ~ 10(A)

63106A  
Low:0 ~ 12(A) , High:0 ~ 120(A)

63107A(R) Low:0 ~ 5(A)
63107A(L) Low:0 ~ 4(A) , High:0 ~ 40(A)

63108A (V Range is HIGH) High:0 ~ 20(A)
63108A (V Range is LOW)  Low:0 ~ 2(A) , High:0 ~ 20(A)

63110A  
Not Support

63123A  
Low:0 ~ 7(A) , High:0 ~ 70(A)

63113A  
Low:0 ~ 5(A) , High:0 ~ 20(A)   ������  �    Status                           Ϊ& 
  �  �    Instrument Handle                �` � � � �    CC Dynamic L1                      	            ?�      �      ��                           @    It sets the Dynamic Load Current during constant current mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the Dynamic Load Current during constant current mode.

Model Range: 
63101A  
Low:0 ~ 4(A) , High:0 ~ 40(A)

63102A  
Low:0 ~ 2(A) , High:0 ~ 20(A)

63103A  
Low:0 ~ 6(A) , High:0 ~ 60(A)

63105A (V Range is HIGH) High:0 ~ 10(A)
63105A (V Range is LOW)  Low: 0 ~ 1(A) , High:0 ~ 10(A)

63106A  
Low:0 ~ 12(A) , High:0 ~ 120(A)

63107A(R) Low:0 ~ 5(A)
63107A(L) Low:0 ~ 4(A) , High:0 ~ 40(A)

63108A (V Range is HIGH) High:0 ~ 20(A)
63108A (V Range is LOW)  Low:0 ~ 2(A) , High:0 ~ 20(A)

63110A  
Not Support

63112A  
Low:0 ~ 24(A) , High:0 ~ 240(A)

63123A  
Low:0 ~ 7(A) , High:0 ~ 70(A)

63113A  
Low:0 ~ 5(A) , High:0 ~ 20(A)   ������  �    Status                           ׎& 
  �  �    Instrument Handle                �D � � � �    CC Dynamic L2                      	            ?�      �      ��                           F    It sets the current rise slew rate of constant current dynamic mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
        It sets the current rise slew rate of constant current dynamic mode.

Model Range: 
63101A  
Low:0.00064 ~ 0.160(A/us) , High:0.0064 ~ 1.6(A/us)

63102A 
Low:0.00032 ~ 0.008(A/us) , High:0.0032 ~ 0.8(A/us)

63103A 
Low:0.001 ~ 0.25(A/us) , High:0.01 ~ 2.5(A/us)

63105A (V Range is HIGH) High:0.0016 ~ 0.4(A/us)
63105A (V Range is LOW)  Low:0.00016 ~ 0.04(A/us)
63105A (V Range is LOW)  High:0.0016 ~ 0.4(A/us)

63106A 
Low:0.002 ~ 0.5(A/us) , High:0.02 ~ 5(A/us)

63107A(R) Low:0.0008 ~ 0.2(A/us)
63107A(L) Low:0.00064 ~ 0.16(A/us) , High:0.0064 ~ 1.6(A/us)

63108A (V Range is HIGH) High:0.0032 ~ 0.8(A/us)
63108A (V Range is LOW)  Low:0.00032 ~ 0.08(A/us)
63108A (V Range is LOW)  High:0.0032 ~ 0.8(A/us)

63110A 
Not Support

63112A 
Low:0.004 ~ 1(A/us) , High:0.04 ~ 10(A/us)

63123A 
Low:0.001 ~ 0.25(A/us) , High:0.01 ~ 2.5(A/us)

63113A
Low:0.008 ~ 0.2 (A/us) , High:0.032 ~ 0.8 (A/us)   ������  �    Status                           �& 
  �  �    Instrument Handle                �X � � � �    CC Dynamic Rise Slew Rate          	            ?�      �      ��      ?pbM���             F    It sets the current fall slew rate of constant current dynamic mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
        It sets the current fall slew rate of constant current dynamic mode.

Model Range: 
63101A  
Low:0.00064 ~ 0.160(A/us) , High:0.0064 ~ 1.6(A/us)

63102A 
Low:0.00032 ~ 0.008(A/us) , High:0.0032 ~ 0.8(A/us)

63103A 
Low:0.001 ~ 0.25(A/us) , High:0.01 ~ 2.5(A/us)

63105A (V Range is HIGH) High:0.0016 ~ 0.4(A/us)
63105A (V Range is LOW)  Low:0.00016 ~ 0.04(A/us)
63105A (V Range is LOW)  High:0.0016 ~ 0.4(A/us)

63106A 
Low:0.002 ~ 0.5(A/us) , High:0.02 ~ 5(A/us)

63107A(R) Low:0.0008 ~ 0.2(A/us)
63107A(L) Low:0.00064 ~ 0.16(A/us) , High:0.0064 ~ 1.6(A/us)

63108A (V Range is HIGH) High:0.0032 ~ 0.8(A/us)
63108A (V Range is LOW)  Low:0.00032 ~ 0.08(A/us)
63108A (V Range is LOW)  High:0.0032 ~ 0.8(A/us)

63110A 
Not Support

63112A 
Low:0.004 ~ 1(A/us) , High:0.04 ~ 10(A/us)

63123A 
Low:0.001 ~ 0.25(A/us) , High:0.01 ~ 2.5(A/us)

63113A
Low:0.008 ~ 0.2 (A/us) , High:0.032 ~ 0.8 (A/us)   �����  �    Status                           �& 
  �  �    Instrument Handle                �j � � � �    CC Dynamic Fall Slew Rate          	            ?�      �      ��      ?pbM���             4    It sets the duration parameter T1 of dynamic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     d    It sets the duration parameter T1 of dynamic load.(not support 63110A) 

Range: 0.000025 ~ 50(S)

   �����  �    Status                           ��& 
  �  �    Instrument Handle                �j � � � �    CC Dynamic T1                      	            ?�      �      ��      >�6��C-             4    It sets the duration parameter T2 of dynamic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     c    It sets the duration parameter T2 of dynamic load.(not support 63110A) 


Range: 0.000025 ~ 50(S)   ������  �    Status                           ��& 
  �  �    Instrument Handle                �O � � � �    CC Dynamic T2                      	            ?�      �      ��      >�6��C-             5    It returns the set current value of Static Load L1.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     5    It returns the set current value of Static Load L1.   ������  �    Status                           ~& 
  �  �    Instrument Handle                4 � � �  �    Get CC Static L1                   	               	           5    It returns the set current value of Static Load L2.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     5    It returns the set current value of Static Load L2.   i����  �    Status                           	& 
  �  �    Instrument Handle                	� � � �  �    Get CC Static L2                   	               	           /    It returns the rise slew rate of static load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     E    It returns the rise slew rate of static load.(not support 63110A) 
   
�����  �    Status                           �& 
  �  �    Instrument Handle                ^ � � �  �    Get CC Static Rise Slew Rate       	               	           /    It returns the fall slew rate of static load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     E    It returns the fall slew rate of static load.(not support 63110A) 
   �����  �    Status                           J& 
  �  �    Instrument Handle                  � � �  �    Get CC Static Fall Slew Rate       	               	           4    It returns the setting current in dynamic load L1.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     J    It returns the setting current in dynamic load L1.(not support 63110A) 
   D����  �    Status                           �& 
  �  �    Instrument Handle                � � � �  �    Get CC Dynamic L1                  	               	           J    It returns the setting current in dynamic load L2.(not support 63110A) 
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     4    It returns the setting current in dynamic load L2.   ����  �    Status                           #�& 
  �  �    Instrument Handle                $i � � �  �    Get CC Dynamic L2                  	               	           0    It returns the rise slew rate of dynamic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     F    It returns the rise slew rate of dynamic load.(not support 63110A) 
   %�����  �    Status                           *E& 
  �  �    Instrument Handle                *� � � �  �    Get CC Dynamic Rise Slew Rate      	               	           0    It returns the fall slew rate of dynamic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     F    It returns the fall slew rate of dynamic load.(not support 63110A) 
   ,<����  �    Status                           0�& 
  �  �    Instrument Handle                1� � � �  �    Get CC Dynamic Fall Slew Rate      	               	           /    It returns the dynamic duration parameter T1.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     E    It returns the dynamic duration parameter T1.(not support 63110A) 
   2�����  �    Status                           7�& 
  �  �    Instrument Handle                8B � � �  �    Get CC Dynamic T1                  	               	           /    It returns the dynamic duration parameter T2.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     E    It returns the dynamic duration parameter T2.(not support 63110A) 
   9�����  �    Status                           >.& 
  �  �    Instrument Handle                >� � � �  �    Get CC Dynamic T2                  	               	           B    It sets the static resistance level of constant resistance mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    <    It sets the static resistance level of constant resistance mode.

Model Range: 
63101A  
Low:0.0375 ~ 150(ohm) , High:1.875 ~ 7500(ohm)

63102A  
Low:0.075 ~ 300(ohm) , High:3.75 ~ 15000(ohm)

63103A  
Low:0.025 ~ 100(ohm) , High:1.25 ~ 5000(ohm)

63105A  
Low:1.25 ~ 5000(ohm) , High:50 ~ 200(ohm)

63106A  
Low:0.0125 ~ 50(ohm) , High:0.625 ~ 2500(ohm)

63107A(R) Low:0.3 ~ 1200(ohm) , High:15 ~ 60000(ohm)
63107A(L) Low:0.0375 ~ 150(ohm) , High:1.875 ~ 7500(ohm)

63108A  
Low:0.625 ~ 2500(ohm) , High:25 ~ 100000(ohm)

63110A  
Low:3 ~ 1000(ohm) , High:10 ~ 10000(ohm)

63112A  
Low:0.00625 ~ 25(ohm) , High:0.3125 ~ 1250(ohm)

63123A  
CRL @ CH: 0.015 ~ 1500(ohm) 
CRL @ CL: 0.15 ~ 150(ohm)
CRH @ CH: 2  ~ 2000(ohm)
CRH @ CL: 11.5 ~ 11500(ohm)

63113A
CRL@CH: 0.2 ~ 200(ohm)
CRL@CL: 0.8 ~ 800(ohm)
CRH@CL: 4 ~4000(ohm)


   @6����  �    Status                           D�& 
  �  �    Instrument Handle                E� � � � �    CR Static L1                       	            ?�      �      ��                           B    It sets the static resistance level of constant resistance mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    :    It sets the static resistance level of constant resistance mode.

Model Range: 
63101A  
Low:0.0375 ~ 150(ohm) , High:1.875 ~ 7500(ohm)

63102A  
Low:0.075 ~ 300(ohm) , High:3.75 ~ 15000(ohm)

63103A  
Low:0.025 ~ 100(ohm) , High:1.25 ~ 5000(ohm)

63105A  
Low:1.25 ~ 5000(ohm) , High:50 ~ 200(ohm)

63106A  
Low:0.0125 ~ 50(ohm) , High:0.625 ~ 2500(ohm)

63107A(R) Low:0.3 ~ 1200(ohm) , High:15 ~ 60000(ohm)
63107A(L) Low:0.0375 ~ 150(ohm) , High:1.875 ~ 7500(ohm)

63108A  
Low:0.625 ~ 2500(ohm) , High:25 ~ 100000(ohm)

63110A  
Low:3 ~ 1000(ohm) , High:10 ~ 10000(ohm)

63112A  
Low:0.00625 ~ 25(ohm) , High:0.3125 ~ 1250(ohm)

63123A  
CRL @ CH: 0.015 ~ 1500(ohm) 
CRL @ CL: 0.15 ~ 150(ohm)
CRH @ CH: 2  ~ 2000(ohm)
CRH @ CL: 11.5 ~ 11500(ohm)

63113A
CRL@CH: 0.2 ~ 200(ohm)
CRL@CL: 0.8 ~ 800(ohm)
CRH@CL: 4 ~4000(ohm)
   J����  �    Status                           N�& 
  �  �    Instrument Handle                Od � � � �    CR Static L2                       	            ?�      �      ��                           >    It sets the resistive rise slew rate of constant resistance.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the resistive rise slew rate of constant resistance.

Model Range: 
63101A :0.0064 ~ 1.6(A/us)

63102A :0.0032 ~ 0.8(A/us)

63103A :0.01 ~ 2.5(A/us)

63105A :0.0016 ~ 0.4(A/us)

63106A :0.02 ~ 5(A/us)

63107A(R) :0.0008 ~ 0.200(A/us)
63107A(L) :0.0064 ~ 1.6(A/us)

63108A :0.0032 ~ 0.8(A/us)

63110A 
Not Support

63112A :0.04 ~ 10(A/us)

63123A :0.01 ~ 2.5(A/us)

63113A
Low:0.008 ~ 0.2 (A/us) , High:0.032 ~ 0.8 (A/us)   S�����  �    Status                           Xs& 
  �  �    Instrument Handle                Y) � � � �    CR Static Rise Slew Rate           	            ?�      �      ��      ?�z�G�{             >    It sets the resistive fall slew rate of constant resistance.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the resistive fall slew rate of constant resistance.

Model Range: 
63101A :0.0064 ~ 1.6(A/us)

63102A :0.0032 ~ 0.8(A/us)

63103A :0.01 ~ 2.5(A/us)

63105A :0.0016 ~ 0.4(A/us)

63106A :0.02 ~ 5(A/us)

63107A(R) :0.0008 ~ 0.200(A/us)
63107A(L) :0.0064 ~ 1.6(A/us)

63108A :0.0032 ~ 0.8(A/us)

63110A 
Not Support

63112A :0.04 ~ 10(A/us)

63123A :0.01 ~ 2.5(A/us)

63113A
Low:0.008 ~ 0.2 (A/us) , High:0.032 ~ 0.8 (A/us)   [�����  �    Status                           `�& 
  �  �    Instrument Handle                ab � � � �    CR Static Fall Slew Rate           	            ?�      �      ��      ?�z�G�{             9    It returns the set resistance of the value of Load L1.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     8    It returns the set resistance of the value of Load L1.   d3����  �    Status                           h�& 
  �  �    Instrument Handle                i� � � �  �    Get CR Static L1                   	               	           9    It returns the set resistance of the value of Load L2.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     8    It returns the set resistance of the value of Load L2.   j�����  �    Status                           o& 
  �  �    Instrument Handle                p5 � � �  �    Get CR Static L2                   	               	           #    It returns the CR rise slew rate.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     #    It returns the CR rise slew rate.   q[����  �    Status                           v& 
  �  �    Instrument Handle                v� � � �  �    Get CR Static Rise Slew Rate       	               	           #    It returns the CR fall slew rate.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     #    It returns the CR fall slew rate.   w�����  �    Status                           ||& 
  �  �    Instrument Handle                }2 � � �  �    Get CR Static Fall Slew Rate       	               	           B    It sets the voltage of static load during constant voltage mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It sets the voltage of static load during constant voltage mode.

Model Range: 
63101A|63102A|63103A|63106A|63107A|63112A

63105A|63108A|63110A :0 ~ 500(V)

63123A :0 ~ 120(V)

63113A :0 ~ 300(V)   ~b����  �    Status                           �& 
  �  �    Instrument Handle                �� � � � �    CV Static L1                       	            ?�      �      ��                           B    It sets the voltage of static load during constant voltage mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It sets the voltage of static load during constant voltage mode.

Model Range: 
63101A|63102A|63103A|63106A|63107A|63112A

63105A|63108A|63110A :0 ~ 500(V)

63123A :0 ~ 120(V)

63113A :0 ~ 300(V)   ������  �    Status                           �c& 
  �  �    Instrument Handle                � � � � �    CV Static L2                       	            ?�      �      ��                           5    It sets the current limit of constant voltage mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    ,    It sets the current limit of constant voltage mode.

Model Range: 
63101A :0 ~ 40(A)

63102A :0 ~ 20(A)

63103A :0 ~ 60(A)

63105A :0 ~ 10(A)

63106A :0 ~ 120(A)

63107A(R) :0 ~ 5(A)
63107A(L) :0 ~ 40(A)

63108A :0 ~ 20(A)

63110A :0 ~ 2(A)

63112A :0 ~ 240(A)

63123A :0 ~ 70(A)

63123A :0 ~ 20(A)   ������  �    Status                           ��& 
  �  �    Instrument Handle                �` � � � �    CV Current Limit                   	            ?�      �      ��                           (    It sets the response speed of CV mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     D    It sets the response speed of CV mode.

Parameter:

0:SLOW
1:FAST
   ������  �    Status                           �K# 
  �  �    Instrument Handle                � � �  �    CV Response Speed                  	               Fast 1 Slow 0    9    It returns the set resistance of the value of Load L1.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     8    It returns the set resistance of the value of Load L1.   �N����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � �  �    Get CV Static L1                   	               	           9    It returns the set resistance of the value of Load L2.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     8    It returns the set resistance of the value of Load L2.   ������  �    Status                           ��& 
  �  �    Instrument Handle                �P � � �  �    Get CV Static L2                   	               	           9    It returns the current limit of constant voltage mode.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     9    It returns the current limit of constant voltage mode.
   ������  �    Status                           �9& 
  �  �    Instrument Handle                �� � � �  �    Get CV Current Limit               	               	           +    It returns the response speed of CV mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     +    It returns the response speed of CV mode.   �����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � �  �    Get CV Response Speed              	               	           8    It sets the static power level of constant power mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the static power level of constant power mode.

Model Range: 
63101A  
Low:0 ~ 20(W) , High:0 ~ 200(W)

63102A  
Low:0 ~ 20(W) , High:0 ~ 100(W)

63103A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63105A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63106A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63107A(R) Low:0 ~ 30(W)
63107A(L) Low:0 ~ 30(W) , High:0 ~ 250(W)

63108A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63110A,63113A
Not Support

63112A  
Low:0 ~ 120(W) , High:0 ~ 1200(W)

63123A  
Low:0 ~ 35(W) , High:0 ~ 350(W)   ������  �    Status                           �\& 
  �  �    Instrument Handle                � � � � �    CP Static L1                       	            ?�      �      ��                           8    It sets the static power level of constant power mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the static power level of constant power mode.

Model Range: 
63101A  
Low:0 ~ 20(W) , High:0 ~ 200(W)

63102A  
Low:0 ~ 20(W) , High:0 ~ 100(W)

63103A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63105A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63106A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63107A(R) Low:0 ~ 30(W)
63107A(L) Low:0 ~ 30(W) , High:0 ~ 250(W)

63108A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63110A,63113A
Not Support

63112A  
Low:0 ~ 120(W) , High:0 ~ 1200(W)

63123A  
Low:0 ~ 35(W) , High:0 ~ 350(W)   �*����  �    Status                           ��& 
  �  �    Instrument Handle                Í � � � �    CP Static L2                       	            ?�      �      ��                           9    It sets the resistive rise slew rate of constant power.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    B    It sets the resistive rise slew rate of constant power.

Model Range: 
63101A  
Low:0.0032 ~ 0.8(W) , High:0.032 ~ 8(W)

63102A  
Low:0.0032 ~ 0.8(W) , High:0.016 ~ 4(W)

63103A  
Low:0.005 ~ 1.25(W) , High:0.050 ~ 12.5(W)

63105A  
Low:0.0048 ~ 1.2(W) , High:0.048 ~ 12(W)

63106A  
Low:0.01 ~ 2.5(W) , High:0.1 ~ 25(W)

63107A(R) Low:0.0048 ~ 1.2(W)
63107A(L) Low:0.0048 ~ 1.2(W) , High:0.04 ~ 10(W)

63108A  
Low:0.0096 ~ 2.4(W) , High:0.096 ~ 24(W)

63110A,63113A
Not Support

63112A  
Low:0.02 ~ 5(W) , High:0.2 ~ 50(W)

63123A
Low:0.005 ~ 1.25(W) , High:0.05 ~ 12.5(W)

   Ʀ����  �    Status                           �S& 
  �  �    Instrument Handle                �	 � � � �    CP Static Rise Slew Rate           	            ?�      �      ��      ?ə�����             9    It sets the resistive fall slew rate of constant power.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    @    It sets the resistive fall slew rate of constant power.

Model Range: 
63101A  
Low:0.0032 ~ 0.8(W) , High:0.032 ~ 8(W)

63102A  
Low:0.0032 ~ 0.8(W) , High:0.016 ~ 4(W)

63103A  
Low:0.005 ~ 1.25(W) , High:0.050 ~ 12.5(W)

63105A  
Low:0.0048 ~ 1.2(W) , High:0.048 ~ 12(W)

63106A  
Low:0.01 ~ 2.5(W) , High:0.1 ~ 25(W)

63107A(R) Low:0.0048 ~ 1.2(W)
63107A(L) Low:0.0048 ~ 1.2(W) , High:0.04 ~ 10(W)

63108A  
Low:0.0096 ~ 2.4(W) , High:0.096 ~ 24(W)

63110A,63113A
Not Support

63112A  
Low:0.02 ~ 5(W) , High:0.2 ~ 50(W)

63123A
Low:0.005 ~ 1.25(W) , High:0.05 ~ 12.5(W)   �n����  �    Status                           �& 
  �  �    Instrument Handle                �� � � � �    CP Static Fall Slew Rate           	            ?�      �      ��      ?ə�����             4    It returns the set power of the value of Load L1.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     4    IIt returns the set power of the value of Load L1.   �/����  �    Status                           ��& 
  �  �    Instrument Handle                ݒ � � �  �    Get CP Static L1                   	               	           4    It returns the set power of the value of Load L2.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     4    IIt returns the set power of the value of Load L2.   ������  �    Status                           �r& 
  �  �    Instrument Handle                �( � � �  �    Get CP Static L2                   	               	           #    It returns the CP rise slew rate.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     #    It returns the CP rise slew rate.   �J����  �    Status                           ��& 
  �  �    Instrument Handle                � � � �  �    Get CP Static Rise Slew Rate       	               	           #    It returns the CP fall slew rate.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     #    It returns the CP fall slew rate.   �����  �    Status                           �k& 
  �  �    Instrument Handle                �! � � �  �    Get CP Static Fall Slew Rate       	               	           &    It executes or cancels the OCP Test.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     ?    It executes or cancels the OCP Test.

Parameter:

0:OFF
1:ON
   �5����  �    Status                           ��# 
  �  �    Instrument Handle                �� � �  �    OCP Test                           	               On 1 Off 0    &    It sets the range for OCP execution.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     A    It sets the range for OCP execution.

Parameter:

0:LOW
1:HIGH
   ������  �    Status                           �w# 
  �  �    Instrument Handle                �- � �  �    OCP Range                          	               High 1 Low 0    .    It sets the start current for OCP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    m    It sets the start current for OCP test mode.

Model Range: 
63101A  
Low:0 ~ 4(A) , High:0 ~ 40(A)

63102A  
Low:0 ~ 2(A) , High:0 ~ 20(A)

63103A  
Low:0 ~ 6(A) , High:0 ~ 60(A)

63105A (V Range is HIGH) High:0 ~ 10(A)
63105A (V Range is LOW)  Low: 0 ~ 1(A) , High:0 ~ 10(A)

63106A  
Low:0 ~ 12(A) , High:0 ~ 120(A)

63107A(R) Low:0 ~ 5(A)
63107A(L) Low:0 ~ 4(A) , High:0 ~ 40(A)

63108A (V Range is HIGH) High:0 ~ 20(A)
63108A (V Range is LOW)  Low:0 ~ 2(A) , High:0 ~ 20(A)

63110A  
Not Support

63112A  
Low:0 ~ 24(A) , High:0 ~ 240(A)

63123A  
Low:0 ~ 7(A) , High:0 ~ 70(A)

63113A
Low:0 ~ 5(A) , High:0 ~ 20(A)   �k����  �    Status                           & 
  �  �    Instrument Handle                � � � � �    OCP Start Current                  	            ?�      �      ��                           ,    It sets the end current for OCP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    k    It sets the end current for OCP test mode.

Model Range: 
63101A  
Low:0 ~ 4(A) , High:0 ~ 40(A)

63102A  
Low:0 ~ 2(A) , High:0 ~ 20(A)

63103A  
Low:0 ~ 6(A) , High:0 ~ 60(A)

63105A (V Range is HIGH) High:0 ~ 10(A)
63105A (V Range is LOW)  Low: 0 ~ 1(A) , High:0 ~ 10(A)

63106A  
Low:0 ~ 12(A) , High:0 ~ 120(A)

63107A(R) Low:0 ~ 5(A)
63107A(L) Low:0 ~ 4(A) , High:0 ~ 40(A)

63108A (V Range is HIGH) High:0 ~ 20(A)
63108A (V Range is LOW)  Low:0 ~ 2(A) , High:0 ~ 20(A)

63110A  
Not Support

63112A  
Low:0 ~ 24(A) , High:0 ~ 240(A)

63123A  
Low:0 ~ 7(A) , High:0 ~ 70(A)

63113A
Low:0 ~ 5(A) , High:0 ~ 20(A)   Q����  �    Status                           �& 
  �  �    Instrument Handle                � � � � �    OCP End Current                    	            ?�      �      ��                           +    It sets the step count for OCP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     =    It sets the step count for OCP test mode.

Range: 1 ~ 1000
   4����  �    Status                           �& 
  �  �    Instrument Handle                � � � � �    OCP Step Count                     	               ����                 +    It sets the dwell time for OCP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     A    It sets the dwell time for OCP test mode.

Range: 1 ~ 1000(ms)
   �����  �    Status                           �& 
  �  �    Instrument Handle                8 � � � �    OCP Dwell Time                     	               ����                 0    It sets the trigger voltage for OCP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the trigger voltage for OCP test mode.

Model Range: 

63101A|63102A|63103A|63106A|63107A|63112A  
When CC Voltage Range is Low:0 ~ 16(V) , High:0 ~ 80(V)

63105A|63108A  
When CC Voltage Range is Low:0 ~ 125(V) , High:0 ~ 500(V)

63110A  
Not Support

63123A
When CC Voltage Range is Low:0 ~ 24(V) , High:0 ~ 120(V)

63113A
When CC Voltage Range is Low:0 ~ 60(V) , High:0 ~ 300(V)
   ����  �    Status                           #,'   �  �    Instrument Handle                #� � � � �    OCP Trigger Voltage                	            ?�      �      ��                           C    It sets the low level current of specification for OCP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the low level current of specification for OCP test mode.

Model Range: 
63101A  
Low:0 ~ 4(A) , High:0 ~ 40(A)

63102A  
Low:0 ~ 2(A) , High:0 ~ 20(A)

63103A  
Low:0 ~ 6(A) , High:0 ~ 60(A)

63105A (V Range is HIGH) High:0 ~ 10(A)
63105A (V Range is LOW)  Low: 0 ~ 1(A) , High:0 ~ 10(A)

63106A  
Low:0 ~ 12(A) , High:0 ~ 120(A)

63107A(R) Low:0 ~ 5(A)
63107A(L) Low:0 ~ 4(A) , High:0 ~ 40(A)

63108A (V Range is HIGH) High:0 ~ 20(A)
63108A (V Range is LOW)  Low:0 ~ 2(A) , High:0 ~ 20(A)

63110A  
Not Support

63112A  
Low:0 ~ 24(A) , High:0 ~ 240(A)

63123A  
Low:0 ~ 7(A) , High:0 ~ 70(A)

63113A
Low:0 ~ 5(A) , High:0 ~ 20(A)   &�����  �    Status                           +D& 
  �  �    Instrument Handle                +� � � � �    OCP Specification Low              	            ?�      �      ��                           D    It sets the high level current of specification for OCP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the high level current of specification for OCP test mode.

Model Range: 
63101A  
Low:0 ~ 4(A) , High:0 ~ 40(A)

63102A  
Low:0 ~ 2(A) , High:0 ~ 20(A)

63103A  
Low:0 ~ 6(A) , High:0 ~ 60(A)

63105A (V Range is HIGH) High:0 ~ 10(A)
63105A (V Range is LOW)  Low: 0 ~ 1(A) , High:0 ~ 10(A)

63106A  
Low:0 ~ 12(A) , High:0 ~ 120(A)

63107A(R) Low:0 ~ 5(A)
63107A(L) Low:0 ~ 4(A) , High:0 ~ 40(A)

63108A (V Range is HIGH) High:0 ~ 20(A)
63108A (V Range is LOW)  Low:0 ~ 2(A) , High:0 ~ 20(A)

63110A  
Not Support

63112A  
Low:0 ~ 24(A) , High:0 ~ 240(A)

63123A  
Low:0 ~ 7(A) , High:0 ~ 70(A)

63113A
Low:0 ~ 5(A) , High:0 ~ 20(A)   /�����  �    Status                           4W& 
  �  �    Instrument Handle                5 � � � �    OCP Specification High             	            ?�      �      ��                           -    It returns the result of OCP test function.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    #    It returns the result of OCP test function.

Parameter1 value:
-1 :denotes the OCP test is stop.
-2 :denotes the OCP test is ready to execute what wait for Von or other condition.
-3 :denotes the OCP test is executed.
0  : PASS  
1  : FAIL

Parameter2 value: OCP current.  (Unit = Ampere)    8�����  �    Status                           =T& 
  �  �    Instrument Handle                >
 � � �  �    OCP Result                         	               	            )    It returns the OCP setting range value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     )    It returns the OCP setting range value.   @!����  �    Status                           D�& 
  �  �    Instrument Handle                E� � � �  �    Get OCP Range                      	               	           ,    It returns the setting start curren value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     ,    It returns the setting start curren value.   F�����  �    Status                           KQ& 
  �  �    Instrument Handle                L � � �  �    Get OCP Start Current              	               	           *    It returns the setting end curren value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     *    It returns the setting end curren value.   M(����  �    Status                           Q�& 
  �  �    Instrument Handle                R� � � �  �    Get OCP End Current                	               	           *    It returns the setting step count value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     *    It returns the setting step count value.   S�����  �    Status                           XW& 
  �  �    Instrument Handle                Y � � �  �    Get OCP Step Count                 	               	            *    It returns the setting dwell time value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     *    It returns the setting dwell time value.   Z,����  �    Status                           ^�& 
  �  �    Instrument Handle                _� � � �  �    Get OCP Dwell Time                 	               	            /    It returns the setting trigger voltage value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     /    It returns the setting trigger voltage value.   `�����  �    Status                           e`& 
  �  �    Instrument Handle                f � � �  �    Get OCP Trigger Voltage            	               	           B    It returns the setting low level current of specification value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     B    It returns the setting low level current of specification value.   gR����  �    Status                           k�& 
  �  �    Instrument Handle                l� � � �  �    Get OCP Specification Low          	               	           C    It returns the setting high level current of specification value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     C    It returns the setting high level current of specification value.   n����  �    Status                           r�& 
  �  �    Instrument Handle                sh � � �  �    Get OCP Specification High         	               	           &    It executes or cancels the OPP Test.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     ?    It executes or cancels the OPP Test.

Parameter:

0:OFF
1:ON
   t�����  �    Status                           yI# 
  �  �    Instrument Handle                y� � �  �    OPP Test                           	               On 1 Off 0    &    It sets the range for OPP execution.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     A    It sets the range for OPP execution.

Parameter:

0:LOW
1:HIGH
   {1����  �    Status                           �# 
  �  �    Instrument Handle                �� � �  �    OPP Range                          	               High 1 Low 0    ,    It sets the start power for OPP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the start power for OPP test mode.

Model Range: 
63101A  
Low:0 ~ 20(W) , High:0 ~ 200(W)

63102A  
Low:0 ~ 20(W) , High:0 ~ 100(W)

63103A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63105A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63106A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63107A(R) Low:0 ~ 30(W)
63107A(L) Low:0 ~ 30(W) , High:0 ~ 250(W)

63108A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63110A
Not Support

63112A  
Low:0 ~ 120(W) , High:0 ~ 1200(W)

63123A  
Low:0 ~ 35(W) , High:0 ~ 350(W)

63113A
0 ~ 300(W)   ������  �    Status                           �}& 
  �  �    Instrument Handle                �3 � � � �    OPP Start Power                    	            ?�      �      ��                           *    It sets the end power for OPP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the end power for OPP test mode.

Model Range: 
63101A  
Low:0 ~ 20(W) , High:0 ~ 200(W)

63102A  
Low:0 ~ 20(W) , High:0 ~ 100(W)

63103A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63105A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63106A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63107A(R) Low:0 ~ 30(W)
63107A(L) Low:0 ~ 30(W) , High:0 ~ 250(W)

63108A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63110A
Not Support

63112A  
Low:0 ~ 120(W) , High:0 ~ 1200(W)

63123A  
Low:0 ~ 35(W) , High:0 ~ 350(W)

63113A
0 ~ 300(W)   �=����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � � �    OPP End Power                      	            ?�      �      ��                           +    It sets the step count for OPP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     =    It sets the step count for OPP test mode.

Range: 1 ~ 1000
   ������  �    Status                           �V& 
  �  �    Instrument Handle                � � � � �    OPP Step Count                     	               ����                 +    It sets the dwell time for OPP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     A    It sets the dwell time for OPP test mode.

Range: 1 ~ 1000(ms)
   �J����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � � �    OPP Dwell Time                     	               ����                 0    It sets the trigger voltage for OPP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It sets the trigger voltage for OPP test mode.

Model Range: 

63101A|63102A|63103A|63106A|63107A|63112A :0 ~ 80(V)

63105A|63108A :0 ~ 500(V)

63110A  
Not Support   ������  �    Status                           ��'   �  �    Instrument Handle                �W � � � �    OPP Trigger Voltage                	            ?�      �      ��                           A    It sets the low level power of specification for OPP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
        It sets the low level power of specification for OPP test mode.

Model Range: 
63101A  
Low:0 ~ 20(W) , High:0 ~ 200(W)

63102A  
Low:0 ~ 20(W) , High:0 ~ 100(W)

63103A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63105A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63106A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63107A(R) Low:0 ~ 30(W)
63107A(L) Low:0 ~ 30(W) , High:0 ~ 250(W)

63108A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63110A
Not Support

63112A  
Low:0 ~ 120(W) , High:0 ~ 1200(W)

63123A  
Low:0 ~ 35(W) , High:0 ~ 350(W)

63113A
0 ~ 300(W)   �(����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � � �    OPP Specification Low              	            ?�      �      ��                           B    It sets the high level power of specification for OPP test mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
        It sets the high level power of specification for OPP test mode.

Model Range: 
63101A  
Low:0 ~ 20(W) , High:0 ~ 200(W)

63102A  
Low:0 ~ 20(W) , High:0 ~ 100(W)

63103A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63105A  
Low:0 ~ 30(W) , High:0 ~ 300(W)

63106A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63107A(R) Low:0 ~ 30(W)
63107A(L) Low:0 ~ 30(W) , High:0 ~ 250(W)

63108A  
Low:0 ~ 60(W) , High:0 ~ 600(W)

63110A
Not Support

63112A  
Low:0 ~ 120(W) , High:0 ~ 1200(W)

63123A  
Low:0 ~ 35(W) , High:0 ~ 350(W)

63113A
0 ~ 300(W)   ������  �    Status                           �o& 
  �  �    Instrument Handle                �% � � � �    OPP Specification High             	            ?�      �      ��                           -    It returns the result of OPP test function.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    !    It returns the result of OPP test function.

Parameter1 value:
-1 :denotes the OPP test is stop.
-2 :denotes the OPP test is ready to execute what wait for Von or other condition.
-3 :denotes the OPP test is executed.
0  : PASS  
1  : FAIL

Parameter2 value: OPP power.  (Unit = Ampere)    �H����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � �  �    OPP Result                         	               	            )    It returns the OPP setting range value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     )    It returns the OPP setting range value.   ������  �    Status                           �m& 
  �  �    Instrument Handle                �# � � �  �    Get OPP Range                      	               	           +    It returns the setting start power value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     +    It returns the setting start power value.   �B����  �    Status                           ��& 
  �  �    Instrument Handle                ˥ � � �  �    Get OPP Start Power                	               	           )    It returns the setting end power value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     )    It returns the setting end power value.   ������  �    Status                           �q& 
  �  �    Instrument Handle                �' � � �  �    Get OPP End Power                  	               	           *    It returns the setting step count value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     *    It returns the setting step count value.   �E����  �    Status                           ��& 
  �  �    Instrument Handle                ب � � �  �    Get OPP Step Count                 	               	            *    It returns the setting dwell time value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     *    It returns the setting dwell time value.   ������  �    Status                           �t& 
  �  �    Instrument Handle                �* � � �  �    Get OPP Dwell Time                 	               	            /    It returns the setting trigger voltage value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     /    It returns the setting trigger voltage value.   �N����  �    Status                           ��& 
  �  �    Instrument Handle                � � � �  �    Get OPP Trigger Voltage            	               	           @    It returns the setting low level power of specification value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     @    It returns the setting low level power of specification value.   ������  �    Status                           �& 
  �  �    Instrument Handle                �N � � �  �    Get OPP Specification Low          	               	           A    It returns the setting high level power of specification value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     A    It returns the setting high level power of specification value.   �����  �    Status                           �G& 
  �  �    Instrument Handle                �� � � �  �    Get OPP Specification High         	               	           +    It sets the output voltage of LED driver.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It sets the output voltage of LED driver.
63110A
Range�G
LEDL :0.004 ~ 100(V) 
LEDH :0.02 ~ 500(V)

63113A
Range�G
LEDL :0.0012 ~ 60(V) 
LEDH :0.006 ~ 300(V)

   �4����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � � �    LED Voltage Out                    	            ?�      �      ��      ?�                   +    It sets the output current of LED driver.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It sets the output current of LED driver.

63110A:
Range�G0.00004 ~ 2(A)

63113A
LEDL@CL:0.0001 ~ 5(A)
LEDL@CH:0.0004 ~ 20(A)
LEDH:0.0001 ~ 5(A)   �M����  �    Status                           ��& 
  �  �    Instrument Handle                 � � � � �    LED Current Out                    	            ?�      �      ��      ?�                   ,    It sets the LED operating point impedance.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     =    It sets the LED operating point impedance.

Rang�G0.001 ~ 1   X����  �    Status                           & 
  �  �    Instrument Handle                � � � � �    LED Rd Coefficient                 	            ?�      �      ��      ?�                   2    It sets the Ohm of operating point impedance Rd.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It sets the Ohm of operating point impedance Rd.

Rang:

63110A:
LEDL :3 ~ 1000(ohm) 
LEDH :10 ~ 10000(ohm)

63113A
LEDL@CH : 0.05 ~ 50(ohm)
LEDL@CL : 0.8 ~ 800(ohm)
LEDH@CL : 4 ~ 4000(ohm)
   	����  �    Status                           �& 
  �  �    Instrument Handle                w � � � �    LED Rd Ohm                         	            ?�      �      ��      @Y                   "    It sets the forward bias of LED.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It sets the forward bias of LED.

Range�G

63110A:
LEDL :0.004 ~ 100(V)
LEDH :0.02 ~ 500(V)

63113A:
LEDL :0.006 ~ 60(V)
LEDH :0.03 ~ 300(V)
   C����  �    Status                           �& 
  �  �    Instrument Handle                � � � � �    LED VF                             	            ?�      �      ��      ?�                   3    It selects the parameters to be set for LED Mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     g    It selects the parameters to be set for LED Mode.

Parameter:

0:DEFAULT   
1:COEFF   
2:OHM   
3:VF
   R����  �    Status                           �& 
  �  �    Instrument Handle                � e �� �    Configure Rd Select                	                          #Default 0 Coefficient 1 Ohm 2 VF 3    d    It sets the response speed of Electronic Load to default or                         user-defined.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It sets the response speed of Electronic Load to default or                         user-defined.

Parameter:

0:SET 
1:DEFAULT   q����  �    Status                           ## 
  �  �    Instrument Handle                #� � �  �    Configure Response Select          	               Default 1 Set 0    1    It sets the response speed of Electronic Load.     �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     E    It sets the response speed of Electronic Load. 

Parameter�G 1 ~ 5    %X����  �    Status                           *& 
  �  �    Instrument Handle                *� � � � �    Configure Response Set             	               ����                 a    It sets the LED mode setting for one single channel or all                           channels.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     }    It sets the LED mode setting for one single channel or all                           channels.

Parameter:

0:SINGLE 
1:ALL   ,7����  �    Status                           0�# 
  �  �    Instrument Handle                1� � �  �    Configure Set All LED              	               All 1 Single 0    )    It sets the current range of CR mode.

    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     C    It sets the current range of CR mode.

Parameter:

0:Low 
1:High
   3����  �    Status                           7�# 
  �  �    Instrument Handle                8t � �  �    Configure Current Range            	               High 1 Low 0    )    It sets the Rr function to on or off.

    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     B    It sets the Rr function to on or off.

Parameter: 

0:OFF 
1:ON
   9�����  �    Status                           >\# 
  �  �    Instrument Handle                ? � �  �    Configure Rr                       	               On 1 Off 0    .    It sets the Rr to default or user-defined.

    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     J    It sets the Rr to default or user-defined.

Parameter:

0:SET 
1:DEFAULT   @O����  �    Status                           D�# 
  �  �    Instrument Handle                E� � �  �    Configure Rr Select                	               Default 1 Set 0    >    It sets the ripple resistance Rr to default or user-defined.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     O    It sets the ripple resistance Rr to default or user-defined.

Range�G 5 ~ 125   G����  �    Status                           K�& 
  �  �    Instrument Handle                Lo � � � �    Configure Rr Set                   	            ?�      �      ��      @                   c    It sets if enable Short function when pressing the SHORT key on                          Module.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     |    It sets if enable Short function when pressing the SHORT key on                          Module.

Parameter:

0:OFF 
1:ON
   N����  �    Status                           R�# 
  �  �    Instrument Handle                Sn � �  �    Configure Short                    	               On 1 Off 0        It returns the set Vo value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
         It returns the set Vo value.   T�����  �    Status                           Y�& 
  �  �    Instrument Handle                Z8 � � �  �    Get LED Voltage Out                	               	               It returns the set Io value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
         It returns the set Io value.   [?����  �    Status                           _�& 
  �  �    Instrument Handle                `� � � �  �    Get LED Current Out                	               	           "    It returns the set Coeff. value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     "    It returns the set Coeff. value.   a�����  �    Status                           fZ& 
  �  �    Instrument Handle                g � � �  �    Get LED Rd Coefficient             	               	               It returns the set Rd Ohm.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
         It returns the set Rd Ohm.   h����  �    Status                           l�& 
  �  �    Instrument Handle                m| � � �  �    Get LED Rd Ohm                     	               	               It returns the set Vf value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
         It returns the set Vf value.   n�����  �    Status                           s.& 
  �  �    Instrument Handle                s� � � �  �    Get LED VF                         	               	           3    It returns the response speed of Electronic Load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     3    It returns the response speed of Electronic Load.   u ����  �    Status                           y�& 
  �  �    Instrument Handle                zc � � �  �    Get Configure Response Set         	               	            9    It returns the response speed the Electronic Load uses.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     9    It returns the response speed the Electronic Load uses.   {�����  �    Status                           �G& 
  �  �    Instrument Handle                �� � � �  �    Get Configure Response Select      	               	               It returns the Rd select.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
         It returns the Rd select.   �����  �    Status                           ��& 
  �  �    Instrument Handle                � � � �  �    Get Configure Rd Select            	               	            !    It returns the all channel set.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     !    It returns the all channel set.   ������  �    Status                           �3& 
  �  �    Instrument Handle                �� � � �  �    Get Configure Set_All_LED          	               	           /    It returns the current range set for CR mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     /    It returns the current range set for CR mode.   �����  �    Status                           ��& 
  �  �    Instrument Handle                �g � � �  �    Get Configure Current Range        	               	           &    It returns if the Rr function is on.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     &    It returns if the Rr function is on.   ������  �    Status                           �4& 
  �  �    Instrument Handle                �� � � �  �    Get Configure Rr                   	               	               It returns the Rr select.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
         It returns the Rr select.   ������  �    Status                           ��& 
  �  �    Instrument Handle                �Y � � �  �    Get Configure Rr Select            	               	               It returns the Rr set value.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
         It returns the Rr set value.   �]����  �    Status                           �
& 
  �  �    Instrument Handle                �� � � �  �    Get Configure Rr Set               	               	           )    It returns if the SHORT key is enabled.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     )    It returns if the SHORT key is enabled.   ������  �    Status                           �& 
  �  �    Instrument Handle                �5 � � �  �    Get Configure Short                	               	           !    It sets the program parameters.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     ,    It sets the program number.

Range: 1 ~ 10     �    It selects the active load modules.

Range: 0 ~ 255

Channel      8   7   6   5   4   3   2   1
 -  -  -  -  -   -   -   -   -   -   -   -
Bit Weight  128  64  32  16  8   4   2   1     F    It sets the type of program file in serial execution.

Range: 0 ~ 10     ?    It sets the load on time of program file.

Range: 0 ~ 30.0(S)     @    It sets the load off time of program file.

Range: 0 ~ 30.0(S)     K    It sets the the pass/fail delay time of program file.

Range: 0 ~ 30.0(S)   �J����  �    Status                           ��& 
  �  �    Instrument Handle                �� Y s � �    Program File                     �� Y � � �    Program Active                   �� Yf � �    Program Chain                    �� � y � �    Program On Time                  �5 � � � �    Program Off Time                 �} �l � �    Program P/F Delay Time             	               ����                 ����                  ����               ?�      A�����  ��                        ?�      A�����  ��                        ?�      A�����  ��                           "    It sets the sequence parameters.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     6    It sets the sequence of program file.

Range: 1 ~ 10     P    It sets the the type of sequence.

Parameter:
0 :SKIP
1 :AUTO
2 :MANUAL
3 :EXT     �    It sets the the short channel of program file sequence.

Range: 0 ~ 255

Channel      8   7   6   5   4   3   2   1
 -  -  -  -  -   -   -   -   -   -   -   -
Bit Weight  128  64  32  16  8   4   2   1     F    It sets the short time of program file sequence.

Range: 0 ~ 30.0(S)   �x����  �    Status                           �%& 
  �  �    Instrument Handle                �� Y } � �    Program Sequence                 � Y �� �    Program Sequence Mode            �q Y� � �    Program Sequence Short Channel   �D � � � �    Program Sequence Short Time        	               ����                            SKIP 0 AUTO 1 MANUAL 2 EXT 3    ����               ?�      A�����  ��                           #    It saves the setting of program.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   ����  �    Status                           �=&   �  �    Instrument Handle                  	                   It executes the program.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     3    It executes the program.

Parameter:

0:OFF
1:ON
   ȏ����  �    Status                           �<# 
  �  �    Instrument Handle                �� � �  �    Program Run                        	               On 1 Off 0         It echoes the manual key code.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It echoes the manual key code.

Parameter:
0  :Key 0
1  :Key 1
2  :Key 2
3  :Key 3
4  :Key 4
5  :Key 5
6  :Key 6
7  :Key 7
8  :Key 8
9  :Key 9
10 :Key Up
11 :Key Down   �����  �    Status                           ӿ& 
  �  �    Instrument Handle                �u [ �� �    Program Key                        	                          fKey 0 0 Key 1 1 Key 2 2 Key 3 3 Key 4 4 Key 5 5 Key 6 6 Key 7 7 Key 8 8 Key 9 9 Key Up 10 Key Down 11    $    It returns the program parameters.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     ,    It sets the program number.

Range: 1 ~ 10     �    It selects the active load modules.

Range: 0 ~ 255

Channel      8   7   6   5   4   3   2   1
 -  -  -  -  -   -   -   -   -   -   -   -
Bit Weight  128  64  32  16  8   4   2   1     F    It sets the type of program file in serial execution.

Range: 0 ~ 10     =    It sets the load on time of program file.

Range: 0 ~ 30(S)     >    It sets the load off time of program file.

Range: 0 ~ 30(S)     I    It sets the the pass/fail delay time of program file.

Range: 0 ~ 30(S)   �u����  �    Status                           �"& 
  �  �    Instrument Handle                �� Y ) � �    Program File                     � Y � � �    Program Active                   �� Ye � �    Program Chain                    � � ) � �    Program On Time                  �^ � � � �    Program Off Time                 ݤ �e � �    Program P/F Delay Time             	               	            	            	            	           	           	           "    It sets the sequence parameters.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     6    It sets the sequence of program file.

Range: 1 ~ 10     P    It sets the the type of sequence.

Parameter:
0 :SKIP
1 :AUTO
2 :MANUAL
3 :EXT     �    It sets the the short channel of program file sequence.

Range: 0 ~ 255

Channel      8   7   6   5   4   3   2   1
 -  -  -  -  -   -   -   -   -   -   -   -
Bit Weight  128  64  32  16  8   4   2   1     F    It sets the short time of program file sequence.

Range: 0 ~ 30.0(S)   �����  �    Status                           ��& 
  �  �    Instrument Handle                � Y - � �    Program Sequence                 �� Y � �� �    Program Sequence Mode            � Yz � �    Program Sequence Short Channel   �� � � � �    Program Sequence Short Time        	               	            	            	            	           #    It returns the program run state.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     #    It returns the program run state.   ������  �    Status                           �& 
  �  �    Instrument Handle                �E � � �  �    Get Program Run                    	               	           "    It sets the specific entry mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     B    It sets the specific entry mode.

Parameter:

0:PERCENT
1:VALUE
   �U����  �    Status                           �# 
  �  �    Instrument Handle                �� � �  �    Specification Unit                 	               Value 1 Percent 0    6    It sets the voltage specification for CC,CR,CP Mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
        It sets the high voltage specification for CC,CR,CP Mode.

When Specific entry mode is VALUE
Model Range: 
63101A|63102A|63103A|63106A|63107A|63112A|63123A  :0 ~ 80(V)

63105A|63108A|63110A  :0 ~ 500(V)



When Specific entry mode is PERCENT
Range: 0 ~ 100(%)     �    It sets the center voltage specification for CC,CR,CP Mode.

Model Range: 
63101A|63102A|63103A|63106A|63107A|63112A|63123A  :0 ~ 80(V)

63105A|63108A|63110A  :0 ~ 500(V)        It sets the low voltage specification for CC,CR,CP Mode.

When Specific entry mode is VALUE
Model Range: 
63101A|63102A|63103A|63106A|63107A|63112A|63123A  :0 ~ 80(V)

63105A|63108A|63110A  :0 ~ 500(V)



When Specific entry mode is PERCENT
Range: 0 ~ 100(%)   �����  �    Status                           ��& 
  �  �    Instrument Handle                �g Y � � �    Specifcation Voltage High        �t � � � �    Specifcation Voltage Center      �( � � � �    Specifcation Voltage Low           	            ?�      �      ��                        ?�      �      ��                        ?�      �      ��                           0    It sets the current specification for CV Mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It sets the high current specification for CV Mode.

When Specific entry mode is VALUE
Model Range: 
63101A :0 ~ 40(A)

63102A :0 ~ 20(A)

63103A :0 ~ 60(A)

63105A :0 ~ 10(A)

63106A :0 ~ 120(A)

63107A(R) :0 ~ 5(A)
63107A(L) :0 ~ 40(A)

63108A :0 ~ 20(A)

63110A :0 ~ 2(A)

63112A :0 ~ 240(A)

63123A :0 ~ 70(A)

63113A :0 ~ 20(A)



When Specific entry mode is PERCENT
Range: 0 ~ 100(%)    .    It sets the center current specification for CV Mode.

Model Range: 
63101A :0 ~ 40(A)

63102A :0 ~ 20(A)

63103A :0 ~ 60(A)

63105A :0 ~ 10(A)

63106A :0 ~ 120(A)

63107A(R) :0 ~ 5(A)
63107A(L) :0 ~ 40(A)

63108A :0 ~ 20(A)

63110A :0 ~ 2(A)

63112A :0 ~ 240(A)

63123A :0 ~ 60(A)

63113A :0 ~ 20(A)    s    It sets the low current specification for CV Mode.

When Specific entry mode is VALUE
Model Range: 
63101A :0 ~ 40(A)

63102A :0 ~ 20(A)

63103A :0 ~ 60(A)

63105A :0 ~ 10(A)

63106A :0 ~ 120(A)

63107A(R) :0 ~ 5(A)
63107A(L) :0 ~ 40(A)

63108A :0 ~ 20(A)

63110A :0 ~ 2(A)

63112A :0 ~ 240(A)

63123A :0 ~ 60(A)



When Specific entry mode is PERCENT
Range: 0 ~ 100(%)    ����  �    Status                           �& 
  �  �    Instrument Handle                i Y � � �    Specifcation Current High        � � � � �    Specifcation Current Center      . � � � �    Specifcation Current Low           	            ?�      �      ��                        ?�      �      ��                        ?�      �      ��                           /    It sets the power specification for LED Mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It sets the high power specification for LED Mode.

When Specific entry mode is VALUE
Model Range: 
63110A :0 ~ 100(W)



When Specific entry mode is PERCENT
Range: 0 ~ 100(%)     X    It sets the center power specification for LED Mode.

Model Range: 
63110A :0 ~ 100(W)     �    It sets the low power specification for LED Mode.

When Specific entry mode is VALUE
Model Range: 
63110A :0 ~ 100(W)



When Specific entry mode is PERCENT
Range: 0 ~ 100(%)   z����  �    Status                           '& 
  �  �    Instrument Handle                � Y � � �    Specifcation Power High          � � � � �    Specifcation Power Center        � � � � �    Specifcation Power Low             	            ?�      �      ��                        ?�      �      ��                        ?�      �      ��                           -    It starts or closes the specification test.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     F    It starts or closes the specification test.

Parameter:

0:OFF
1:On
   }����  �    Status                           *# 
  �  �    Instrument Handle                � � �  �    Specification Test                 	               On 1 Off 0    %    It returns the specific entry mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     %    It returns the specific entry mode.   ����  �    Status                           �& 
  �  �    Instrument Handle                 { � � �  �    Get Specification Unit             	               	           B    It requests the GO-NG result refer to the voltage specification.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     B    It requests the GO-NG result refer to the voltage specification.   !�����  �    Status                           &Z& 
  �  �    Instrument Handle                ' � � �  �    Get Spec_Volt Result               	               	            B    It requests the GO-NG result refer to the current specification.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     B    It requests the GO-NG result refer to the current specification.   (_����  �    Status                           -& 
  �  �    Instrument Handle                -� � � �  �    Get Spec_Curr Result               	               	            B    It requests GO-NG result reference to all channel specification.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     B    It requests GO-NG result reference to all channel specification.   /����  �    Status                           3�& 
  �  �    Instrument Handle                4t � � �  �    Get Spec_All_Channel Result        	               	            9    It returns the voltage specification for CC,CR,CP Mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     >    It returns the high voltage specification for CC,CR,CP Mode.     @    It returns the center voltage specification for CC,CR,CP Mode.     =    It returns the low voltage specification for CC,CR,CP Mode.   5�����  �    Status                           :g& 
  �  �    Instrument Handle                ; Y � � �    Specifcation Voltage High        ;c � � � �    Specifcation Voltage Center      ;� � � � �    Specifcation Voltage Low           	               	           	           	           3    It returns the current specification for CV Mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     8    It returns the high current specification for CV Mode.     :    It returns the center current specification for CV Mode.     7    It returns the low current specification for CV Mode.   =h����  �    Status                           B& 
  �  �    Instrument Handle                B� Y � � �    Specifcation Current High        C � � � �    Specifcation Current Center      CM � � � �    Specifcation Current Low           	               	           	           	           2    It returns the power specification for LED Mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     7    It returns the high power specification for LED Mode.     9    It returns the center power specification for LED Mode.     6    It returns the low power specification for LED Mode.   E����  �    Status                           I�& 
  �  �    Instrument Handle                Jf Y � � �    Specifcation Power High          J� � � � �    Specifcation Power Center        J� � � � �    Specifcation Power Low             	               	           	           	           $    It returns the specification test.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     $    It returns the specification test.   L�����  �    Status                           Q:& 
  �  �    Instrument Handle                Q� � � �  �    Get Specification Test             	               	           L    It returns the real time voltage measured at the input of the load module.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     L    It returns the real time voltage measured at the input of the load module.   S+����  �    Status                           W�& 
  �  �    Instrument Handle                X� � � �  �    Fetch Voltage                      	               	           L    It returns the real time current measured at the input of the load module.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     L    It returns the real time current measured at the input of the load module.   Y�����  �    Status                           ^�& 
  �  �    Instrument Handle                _T � � �  �    Fetch Current                      	               	           J    It returns the real time power measured at the input of the load module.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     J    It returns the real time power measured at the input of the load module.   `�����  �    Status                           eb& 
  �  �    Instrument Handle                f � � �  �    Fetch Power                        	               	           5    It returns the real time status of the load module.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    It returns the real time status of the load module.

Bit Position             7   6   5   4   3   2   1   0
Bit Name         -   -   -   -   -   OT  RV  OP  OV  OC
Bit Weight              128  64  32  16  8   4   2   1   gb����  �    Status                           l& 
  �  �    Instrument Handle                l� � � �  �    Fetch Status                       	               	            P    It returns the real time voltage measured at the input of the all load module.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     P    It returns the real time voltage measured at the input of the all load module.   n�����  �    Status                           si& 
  �  �    Instrument Handle                t � � �  �    Fetch All Voltage                  	               	            P    It returns the real time current measured at the input of the all load module.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     P    It returns the real time current measured at the input of the all load module.   u�����  �    Status                           z7& 
  �  �    Instrument Handle                z� � � �  �    Fetch All Current                  	               	            N    It returns the real time power measured at the input of the all load module.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     N    It returns the real time power measured at the input of the all load module.   |V����  �    Status                           �& 
  �  �    Instrument Handle                �� � � �  �    Fetch All Power                    	               	            .    It returns the time measured in timing mode.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    It returns the time measured in timing mode.

Parameter1 value:
-1 :denotes the Timing Function test is stop.
-2 :denotes the Timing Function test is ready to execute what wait for Von or other condition.
-3 :denotes the Timing Function test is execute.
-4 :denotes the Timeout.
-5 :denotes the input voltage is lower than TRIGer voltage.


Parameter2 value: hr:min:sec.ms
If the parameter1 is -1 or -2, it does not return parameter2.   � ����  �    Status                           ��& 
  �  �    Instrument Handle                �c � � �  �    Fetch Time                         	               	            B    It returns the voltage measured at the input of electronic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     B    It returns the voltage measured at the input of electronic load.   �$����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � �  �    Measure Voltage                    	               	           B    It returns the current measured at the input of electronic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     B    It returns the current measured at the input of electronic load.   ������  �    Status                           ��& 
  �  �    Instrument Handle                �9 � � �  �    Measure Current                    	               	           @    It returns the power measured at the input of electronic load.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     @    It returns the power measured at the input of electronic load.   ������  �    Status                           �3& 
  �  �    Instrument Handle                �� � � �  �    Measure Power                      	               	           C    It returns the voltage measured at the input of all load modules.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     C    It returns the voltage measured at the input of all load modules.   �7����  �    Status                           ��& 
  �  �    Instrument Handle                �� � � �  �    Measure All Voltage                	               	            C    It returns the current measured at the input of all load modules.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     C    It returns the current measured at the input of all load modules.   ������  �    Status                           ��& 
  �  �    Instrument Handle                �N � � �  �    Measure All Current                	               	            A    It returns the power measured at the input of all load modules.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     A    It returns the power measured at the input of all load modules.   ������  �    Status                           �J& 
  �  �    Instrument Handle                �  � � �  �    Measure All Power                  	               	            F    It selects the input port of electronic load to measure the voltage.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     b    It selects the input port of electronic load to measure the voltage. 

Parameter:

0:LOAD
1:UUT
   �R����  �    Status                           ��# 
  �  �    Instrument Handle                �� � �  �    Measure Input                      	               UUT 1 LOAD 0    4    It sets the scanning mode of frame to load module.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     M    It sets the scanning mode of frame to load module.

Parameter:

0:OFF
1:ON
   �����  �    Status                           ��# 
  �  �    Instrument Handle                �} � �  �    Measure Scan                       	               On 1 Off 0    /    It returns the input port which has been set.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     /    It returns the input port which has been set.   ������  �    Status                           �s& 
  �  �    Instrument Handle                �) � � �  �    Get Measure Input                  	               	           ,    It returns the scanning mode of the frame.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the C63800_error_message function.  To obtain additional information about the error condition, use the C63800_GetErrorInfo and C63800_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the C6310A_init or C6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     ,    It returns the scanning mode of the frame.   �O����  �    Status                           ��& 
  �  �    Instrument Handle                ̲ � � �  �    Get Measure Scan                   	               	          &    This function resets the instrument to a known state and sends initialization commands to the instrument.  The initialization commands set instrument settings such as Headers Off, Short Command form, and Data Transfer Binary to the state necessary for the operation of the instrument driver.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0102  Reset not supported.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   ��#����  �    Status                           ӂ-   �  �    Instrument Handle                  	               Z    This function runs the instrument's self test routine and returns the test result(s). 

    &    This control contains the value returned from the instrument self test.  Zero means success.  For any other code, see the device's operator's manual.

Self-Test Code    Description
---------------------------------------
   0              Passed self test
   1              Self test failed

     �    Returns the self-test response string from the instrument. See the device's operation manual for an explanation of the string's contents.

You must pass a ViChar array with at least 256 bytes.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0103  Self-Test not supported.

BFFC0012  Error interpreting instrument response.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   � =  �  �    Self Test Result                 �F = � � ,    Self-Test Message                �#����  �    Status                           ��-   �  �    Instrument Handle                  	           	            	               `    This function returns the revision numbers of the instrument driver and instrument firmware.

     �    Returns the instrument driver software revision numbers in the form of a string.

You must pass a ViChar array with at least 256 bytes.     �    Returns the instrument firmware revision numbers in the form of a string.

You must pass a ViChar array with at least 256 bytes.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

3FFC0105  Revision Query not supported.

BFFC0012  Error interpreting instrument response.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   � = 3 �  �    Instrument Driver Revision       ި =6 �  �    Firmware Revision                �3#����  �    Status                           �"-   �  �    Instrument Handle                  	            	            	               V    This function reads an error code and a message from the instrument's error queue.

     B    Returns the error code read from the instrument's error queue.

     �    Returns the error message string read from the instrument's error message queue.

You must pass a ViChar array with at least 256 bytes.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFC0104  Error Query not supported

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

BFFC0012  Error interpreting instrument response.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �6 =  �  �    Error Code                       � = � � ,    Error Message                    �#����  �    Status                           ��-   �  �    Instrument Handle                  	            	            	               n    This function converts a status code returned by an instrument driver function into a user-readable string.      z    Pass the Status parameter that is returned from any of the instrument driver functions.

Default Value:  0  (VI_SUCCESS)     �    Returns the user-readable message string that corresponds to the status code you specify.

You must pass a ViChar array with at least 256 bytes.
    H    Reports the status of this operation.  

This function can return only three possible status codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0085  Unknown status code (warning).

BFFF000A  Invalid parameter (Error Message buffer is VI_NULL).

        The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

You can pass VI_NULL for this parameter.  This is useful when one of the initialize functions fail.

Default Value:  VI_NULL
   �) =  �  h    Error Code                       � = � � �    Error Message                    �F#����  �    Status                           �-   �  �    Instrument Handle                  0    	            	           VI_NULL   I    This function returns the error information associated with an IVI session or with the current execution thread.  If you specify a valid IVI session for the Instrument Handle parameter, this function retrieves and then clears the error information for the session.  If you pass VI_NULL for the Instrument Handle parameter, this function retrieves and then clears the error information for the current execution thread.  

The error information includes a primary error, secondary error, and an error elaboration string.  For a particular session, this information is the same as the values held in the following attributes:

CHR6310A_ATTR_PRIMARY_ERROR
CHR6310A_ATTR_SECONDARY_ERROR
CHR6310A_ATTR_ERROR_ELABORATION

The IVI Library also maintains this error information separately for each thread.  This is useful if you do not have a session handle to pass to Chr6310A_GetErrorInfo or Chr6310A_ClearErrorInfo, which
occurs when a call to Chr6310A_init or Chr6310A_InitWithOptions fails.

You can call Chr6310A_error_message to obtain a text description of the primary or secondary error value.
    �    Reports the status of this operation.

This function returns a non-zero status code only when it is unable to obtain the error information for the session or thread.  The function can return the following status codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFA0021  Unable to allocate system resource.

BFFF000E  Invalid session handle.

     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  VI_NULL
    �    The primary error code for the session or execution thread.   The value is the same as the one held by the attribute CHR6310A_ATTR_PRIMARY_ERROR.
  
A value of VI_SUCCESS (0) indicates that no error occurred,  A positive value indicates a warning.  A negative value indicates an error.  

You can call Chr6310A_error_message to get a text description of the value.

If you are not interested in this value, you can pass VI_NULL.    �    The secondary error code for the session or execution thread.  If the primary error code is non-zero, the secondary error code can further describe the error or warning condition.  The value is the same as the one held by the attribute CHR6310A_ATTR_SECONDARY_ERROR.

A value of VI_SUCCESS (0) indicates no further description.  

You can call Chr6310A_error_message to get a text description of the value.

If you are not interested in this value, you can pass VI_NULL.
    �    The error elaboration string for the session or execution thread.  If the primary error code is non-zero, the elaboration string can further describe the error or warning condition.   The value is the same as the one held by the attribute CHR6310A_ATTR_ERROR_ELABORATION.  

If you are not interested in this value, you can pass VI_NULL.  Otherwise, you must pass a ViChar array with at least 256 bytes.
   �#����  �    Status                           ��-   �  �    Instrument Handle                �q 5 R �  �    Primary Error                    �' 5A �  �    Secondary Error                  � � Q � �    Error Elaboration                  	           VI_NULL    	           	           	           �    This function clears the error information for the current execution thread and the IVI session you specify.  If you pass VI_NULL for the Instrument Handle parameter, this function clears the error information only for the current execution thread. 

The error information includes a primary error code, secondary error code, and an error elaboration string.  For a particular session, this information is the same as the values held in the following attributes:

CHR6310A_ATTR_PRIMARY_ERROR
CHR6310A_ATTR_SECONDARY_ERROR
CHR6310A_ATTR_ERROR_ELABORATION

This function sets the primary and secondary error codes to VI_SUCCESS (0), and sets the error elaboration string to "".

The IVI Library also maintains this error information separately for each thread.  This is useful if you do not have a session handle to pass to Chr6310A_ClearErrorInfo or Chr6310A_GetErrorInfo, which occurs when a call to Chr6310A_init or Chr6310A_InitWithOptions fails.

    w    Reports the status of this operation.

This function returns a non-zero status code only when it is unable to clear the error information for the session or thread.  The function can return the following status codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFF000E  Invalid session handle.

     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   �#����  �    Status                           )-   �  �    Instrument Handle                  	              �    This function returns the coercion information associated with the IVI session.  This function retrieves and clears the oldest instance in which the instrument driver coerced a value you specified to another value.

If you set the CHR6310A_ATTR_RECORD_COERCIONS attribute to VI_TRUE, the instrument driver keeps a list of all coercions it makes on ViInt32 or ViReal64 values you pass to instrument driver functions.  You use this function to retrieve information from that list.

If the next coercion record string, including the terminating NUL byte, contains more bytes than you indicate in this parameter, the function copies Buffer Size - 1 bytes into the buffer, places an ASCII NUL byte at the end of the buffer, and returns the buffer size you must pass to get the entire value.  For example, if the value is "123456" and the Buffer Size is 4, the function places "123" into the buffer and returns 7.

If you pass a negative number, the function copies the value to the buffer regardless of the number of bytes in the value.

If you pass 0, you can pass VI_NULL for the Coercion Record buffer parameter.

The function returns an empty string in the Coercion Record parameter if no coercion records remain for the session.

        Reports the status of this operation.

If the function succeeds and the buffer you pass is large enough to hold the entire coercion record string, the function returns 0.

If the next coercion record string, including the terminating NUL byte, is larger than the size you indicate in the Buffer Size parameter, the function copies Buffer Size - 1 bytes into the buffer, places an ASCII NUL byte at the end of the buffer, and returns the buffer size you must pass to get the entire value.  For example, if the value is "123456" and the Buffer Size is 4, the function places "123" into the buffer and returns 7.

If the function fails for some other reason, it returns a negative error code.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the Chr6310A_init function.  The handle identifies a particular instrument session.

Default Value:  None    �    Returns the next coercion record for the IVI session.  If there are no coercion records, the function returns an empty string.

The buffer must contain at least as many elements as the value you specify with the Buffer Size parameter.  If the next coercion record string, including the terminating NUL byte, contains more bytes than you indicate with the Buffer Size parameter, the function copies Buffer Size - 1 bytes into the buffer, places an ASCII NUL byte at the end of the buffer, and returns the buffer size you must pass to get the entire value.  For example, if the value is "123456" and the Buffer Size is 4, the function places "123" into the buffer and returns 7.

This parameter returns an empty string if no coercion records remain for the session.
    �    Pass the number of bytes in the ViChar array you specify for the Coercion Record parameter.

If the next coercion record string, including the terminating NUL byte, contains more bytes than you indicate in this parameter, the function copies Buffer Size - 1 bytes into the buffer, places an ASCII NUL byte at the end of the buffer, and returns the buffer size you must pass to get the entire value.  For example, if the value is "123456" and the Buffer Size is 4, the function places "123" into the buffer and returns 7.

If you pass a negative number, the function copies the value to the buffer regardless of the number of bytes in the value.

If you pass 0, you can pass VI_NULL for the Coercion Record buffer parameter.

Default Value:  None

   5#����  �    Status                           K-   �  �    Instrument Handle                � � Q � �    Coercion Record                  � = � �  �    Buffer Size                        	               	               :    This function obtains a multithread lock on the instrument session.  Before it does so, it waits until all other execution threads have released their locks on the instrument session.

Other threads might have obtained a lock on this session in the following ways:

- The user's application called Chr6310A_LockSession.

- A call to the instrument driver locked the session.

- A call to the IVI engine locked the session.

After your call to Chr6310A_LockSession returns successfully, no other threads can access the instrument session until you call Chr6310A_UnlockSession.

Use Chr6310A_LockSession and Chr6310A_UnlockSession around a sequence of calls to instrument driver functions if you require that the instrument retain its settings through the end of the sequence.

You can safely make nested calls to Chr6310A_LockSession within the same thread.  To completely unlock the session, you must balance each call to Chr6310A_LockSession with a call to Chr6310A_UnlockSession.  If, however, you use the Caller Has Lock parameter in all calls to Chr6310A_LockSession and Chr6310A_UnlockSession within a function, the IVI Library locks the session only once within the function regardless of the number of calls you make to Chr6310A_LockSession.  This allows you to call Chr6310A_UnlockSession just once at the end of the function. 
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    This parameter serves as a convenience.  If you do not want to use this parameter, pass VI_NULL. 

Use this parameter in complex functions to keep track of whether you obtain a lock and therefore need to unlock the session.  Pass the address of a local ViBoolean variable.  In the declaration of the local variable, initialize it to VI_FALSE.  Pass the address of the same local variable to any other calls you make to Chr6310A_LockSession or Chr6310A_UnlockSession in the same function.

The parameter is an input/output parameter.  Chr6310A_LockSession and Chr6310A_UnlockSession each inspect the current value and take the following actions:

- If the value is VI_TRUE, Chr6310A_LockSession does not lock the session again.  If the value is VI_FALSE, Chr6310A_LockSession obtains the lock and sets the value of the parameter to VI_TRUE.

- If the value is VI_FALSE, Chr6310A_UnlockSession does not attempt to unlock the session.  If the value is VI_TRUE, Chr6310A_UnlockSession releases the lock and sets the value of the parameter to VI_FALSE.
 
Thus, you can, call Chr6310A_UnlockSession at the end of your function without worrying about whether you actually have the lock.  

Example:

ViStatus TestFunc (ViSession vi, ViInt32 flags)
{
    ViStatus error = VI_SUCCESS;
    ViBoolean haveLock = VI_FALSE;

    if (flags & BIT_1)
        {
        viCheckErr( Chr6310A_LockSession(vi, &haveLock));
        viCheckErr( TakeAction1(vi));
        if (flags & BIT_2)
            {
            viCheckErr( Chr6310A_UnlockSession(vi, &haveLock));
            viCheckErr( TakeAction2(vi));
            viCheckErr( Chr6310A_LockSession(vi, &haveLock);
            } 
        if (flags & BIT_3)
            viCheckErr( TakeAction3(vi));
        }

Error:
    /* 
       At this point, you cannot really be sure that 
       you have the lock.  Fortunately, the haveLock 
       variable takes care of that for you.          
    */
    Chr6310A_UnlockSession(vi, &haveLock);
    return error;
}   #����  �    Status                           "�-   �  �    Instrument Handle                #f H � �  �    Caller Has Lock                    	               	            �    This function releases a lock that you acquired on an instrument session using Chr6310A_LockSession.  Refer to Chr6310A_LockSession for additional information on session locks.
    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
    �    This parameter serves as a convenience.  If you do not want to use this parameter, pass VI_NULL. 

Use this parameter in complex functions to keep track of whether you obtain a lock and therefore need to unlock the session. 
Pass the address of a local ViBoolean variable.  In the declaration of the local variable, initialize it to VI_FALSE.  Pass the address of the same local variable to any other calls you make to Chr6310A_LockSession or Chr6310A_UnlockSession in the same function.

The parameter is an input/output parameter.  Chr6310A_LockSession and Chr6310A_UnlockSession each inspect the current value and take the following actions:

- If the value is VI_TRUE, Chr6310A_LockSession does not lock the session again.  If the value is VI_FALSE, Chr6310A_LockSession obtains the lock and sets the value of the parameter to VI_TRUE.

- If the value is VI_FALSE, Chr6310A_UnlockSession does not attempt to unlock the session.  If the value is VI_TRUE, Chr6310A_UnlockSession releases the lock and sets the value of the parameter to VI_FALSE.
 
Thus, you can, call Chr6310A_UnlockSession at the end of your function without worrying about whether you actually have the lock.  

Example:

ViStatus TestFunc (ViSession vi, ViInt32 flags)
{
    ViStatus error = VI_SUCCESS;
    ViBoolean haveLock = VI_FALSE;

    if (flags & BIT_1)
        {
        viCheckErr( Chr6310A_LockSession(vi, &haveLock));
        viCheckErr( TakeAction1(vi));
        if (flags & BIT_2)
            {
            viCheckErr( Chr6310A_UnlockSession(vi, &haveLock));
            viCheckErr( TakeAction2(vi));
            viCheckErr( Chr6310A_LockSession(vi, &haveLock);
            } 
        if (flags & BIT_3)
            viCheckErr( TakeAction3(vi));
        }

Error:
    /* 
       At this point, you cannot really be sure that 
       you have the lock.  Fortunately, the haveLock 
       variable takes care of that for you.          
    */
    Chr6310A_UnlockSession(vi, &haveLock);
    return error;
}   ,�#����  �    Status                           1?-   �  �    Instrument Handle                1� H � �  �    Caller Has Lock                    	               	            �    This function writes a user-specified string to the instrument.

Note:  This function bypasses IVI attribute state caching.  Therefore, when you call this function, the cached values for all attributes will be invalidated.     2    Pass the string to be written to the instrument.         Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   ;l A � �  �    Write Buffer                     ;�#����  �    Status                           ?�-   �  �    Instrument Handle                      	               /    This function reads data from the instrument.     c    After this function executes, this parameter contains the data that was read from the instrument.    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
     �    Pass the maximum number of bytes to read from the instruments.  

Valid Range:  0 to the number of elements in the Read Buffer.

Default:  0

     ^    Returns the number of bytes actually read from the instrument and stored in the Read Buffer.   Ar @ �  �    Read Buffer                      A�#����  �    Status                           Fp-   �  �    Instrument Handle                G* @ C �  �    Number of Bytes To Read          G� �' �  �    Num Bytes Read                     	            	               0    	           �    This function performs the following operations:

- Closes the instrument I/O session.

- Destroys the instrument driver session and all of its attributes.

- Deallocates any memory resources the driver uses.

Notes:

(1) You must unlock the session before calling Chr6310A_close.

(2) After calling Chr6310A_close, you cannot use the instrument driver again until you call Chr6310A_init or Chr6310A_InitWithOptions.

    �    Reports the status of this operation.

To obtain a text description of the status code, or if the status code is not listed below, call the Chr6310A_error_message function.  To obtain additional information about the error condition, use the Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo functions.

Status Codes:

Status    Description
-------------------------------------------------
       0  No error (the call was successful).

3FFF0005  The specified termination character was read.
3FFF0006  The specified number of bytes was read.

BFFF0000  Miscellaneous or system error occurred.
BFFF000E  Invalid session handle.
BFFF0015  Timeout occurred before operation could complete.
BFFF0034  Violation of raw write protocol occurred.
BFFF0035  Violation of raw read protocol occurred.
BFFF0036  Device reported an output protocol error.
BFFF0037  Device reported an input protocol error.
BFFF0038  Bus error occurred during transfer.
BFFF003A  Invalid setup (attributes are not consistent).
BFFF005F  A "no listeners" condition was detected.
BFFF0060  This interface is not the controller-in-charge.
BFFF0067  Operation is not supported on this session.
     �    The ViSession handle that you obtain from the Chr6310A_init or Chr6310A_InitWithOptions function.  The handle identifies a particular instrument session.

Default Value:  None
   K
#����  �    Status                           O�-   �  �    Instrument Handle                  	            ����         
  %�     K.    init                            ����         '�  K 	    K.    InitInterface                   ����         NG  rH     K.    InitWithOptions                 ����         t�  }T     K.    Set_Configure_Von               ����         ~.  ��     K.    Set_Configure_Voltage_Range     ����         ��  ��     K.    Set_Configure_Voltage_Latch     ����         �D  ��     K.    Set_Configure_Von_Latch_Reset   ����         �D  �V     K.    Set_Configure_Auto_Load         ����         �  �	     K.    Set_Configure_Auto_Mode         ����         ��  ��     K.    Set_Configure_Sound             ����         ��  ��     K.    Set_Configure_Remote            ����         �P  ��     K.    Set_Configure_Save              ����         �c  �     K.    Set_Configure_Load              ����         ��  ��     K.    Set_Configure_Timing_State      ����         �c  �E     K.    Set_Configure_Timing_Trigger    ����         �  �     K.    Set_Configure_Timing_Timeout    ����         ��  ֲ     K.    Set_Configure_Voff_State        ����         �o  ��     K.    Set_Configure_Voff_Final_V      ����         �  �     K.    Set_Configure_Measure_Average   ����         �g  �?     K.    Set_Configure_Digital_IO        ����         ��  �
     K.    Set_Configure_Key               ����         ��  ��     K.    Set_Configure_Echo              ����       ���� T     K.    Set_LEDLCRL_Range               ����         �     K.    Get_Configure_Von               ����        � 9     K.    Get_Configure_Voltage_Range     ����        � �     K.    Get_Configure_Voltage_Latch     ����        j -     K.    Get_Configure_Auto_Load         ����        � !�     K.    Get_Configure_Auto_Mode         ����        "v (o     K.    Get_Configure_Sound             ����        )* /&     K.    Get_Configure_Load              ����        /� 5�     K.    Get_Configure_Timing_State      ����        6� <P     K.    Get_Configure_Timing_Trigger    ����        = B�     K.    Get_Configure_Timing_Timeout    ����        Cs IU     K.    Get_Configure_Voff_State        ����        J O�     K.    Get_Configure_Voff_Final_V      ����        P� VM     K.    Get_Configure_Measure_Average   ����        W \�     K.    Get_Configure_Digital_IO        ����        ]� cM     K.    Get_Configure_Key               ����        d i�     K.    Get_Configure_Echo              ����       ���� p1     K.    Get_Configure_LEDLCRL_Range     ����        p� w`     K.    Set_Channel_Load                ����        x& ~     K.    Set_Channel_Active              ����        ~� ��     K.    Set_Channel_Synchronized        ����        �� �     K.    Get_Channel_Load                ����        �: �	     K.    Get_Min_of_Channel_Load         ����        �� ��     K.    Get_Max_of_Channel_Load         ����        �N �c     K.    Get_Channel_Synchronized        ����        � ��     K.    Get_Channel_ID                  ����        �� ��     K.    Set_Load_State                  ����        �� ��     K.    Set_Load_Short_State            ����        �O �N     K.    Set_Load_Short_Key              ����        � ��     K.    Set_Load_Protection_Clear       ����        �4 ��     K.    Set_Load_Clear                  ����        �G ��     K.    Set_Load_Save                   ����        �S �"     K.    Get_Load_State                  ����        �� ٲ     K.    Get_Load_Short_State            ����        �m �T     K.    Get_Load_Short_Key              ����        � �y     K.    Get_Load_Protection_Status      ����        �4 ��     K.    RUN                             ����        �@ ��     K.    ABORT                           ����        �M ��     K.    Show_Display                    ����        �s �     K.    CLS                             ����        Q I     K.    Set_ESE                         ����         (     K.    Set_OPC                         ����        � �     K.    RCL                             ����        H  �     K.    RST                             ����        !j (7     K.    SAV                             ����        (� 0     K.    Set_SRE                         ����        0� 7x     K.    Get_ESR                         ����        83 >D     K.    Get_Identification_String       ����        >� E,     K.    Get_OPC                         ����        E� L�     K.    Get_RDT                         ����        M~ V�     K.    Get_STB                         ����        W` m     K.    SetAttributeViInt32             ����        n� �b     K.    SetAttributeViReal64            ����        �� ��     K.    SetAttributeViString            ����        �J ��     K.    SetAttributeViBoolean           ����        �� �O     K.    SetAttributeViSession           ����        �� ݂     K.    GetAttributeViInt32             ����        ޿ �Z     K.    GetAttributeViReal64            ����        � �     K.    GetAttributeViString            ����        p "     K.    GetAttributeViBoolean           ����        #L 4�     K.    GetAttributeViSession           ����        6( Gp     K.    CheckAttributeViInt32           ����        I ZV     K.    CheckAttributeViReal64          ����        [� m<     K.    CheckAttributeViString          ����        n� �%     K.    CheckAttributeViBoolean         ����        �� �     K.    CheckAttributeViSession         ����        �� �	     K.    Set_Mode                        ����        � ��     K.    Get_Mode                        ����        �� ��     K.    Set_CC_Static_L1                ����        �� ��     K.    Set_CC_Static_L2                ����        �\ ��     K.    Set_CC_Static_Rise_Slew_Rate    ����        �l ��     K.    Set_CC_Static_Fall_Slew_Rate    ����        ɵ ѿ     K.    Set_CC_Dynamic_L1               ����        ҙ ��     K.    Set_CC_Dynamic_L2               ����        ۧ ��     K.    Set_CC_Dynamic_Rise_Slew_Rate   ����        � ��     K.    Set_CC_Dynamic_Fall_Slew_Rate   ����        �� ��     K.    Set_CC_Dynamic_T1               ����        �� ��     K.    Set_CC_Dynamic_T2               ����        �� q     K.    Get_CC_Static_L1                ����        , 
	     K.    Get_CC_Static_L2                ����        
� �     K.    Get_CC_Static_Rise_Slew_Rate    ����        f M     K.    Get_CC_Static_Fall_Slew_Rate    ����         �     K.    Get_CC_Dynamic_L1               ����        � $�     K.    Get_CC_Dynamic_L2               ����        %` +I     K.    Get_CC_Dynamic_Rise_Slew_Rate   ����        , 1�     K.    Get_CC_Dynamic_Fall_Slew_Rate   ����        2� 8�     K.    Get_CC_Dynamic_T1               ����        9J ?1     K.    Get_CC_Dynamic_T2               ����        ?� H�     K.    Set_CR_Static_L1                ����        I� R�     K.    Set_CR_Static_L2                ����        S� Z�     K.    Set_CR_Static_Rise_Slew_Rate    ����        [� c     K.    Set_CR_Static_Fall_Slew_Rate    ����        c� i�     K.    Get_CR_Static_L1                ����        j� pu     K.    Get_CR_Static_L2                ����        q0 v�     K.    Get_CR_Static_Rise_Slew_Rate    ����        w� }]     K.    Get_CR_Static_Fall_Slew_Rate    ����        ~ ��     K.    Set_CV_Static_L1                ����        �l ��     K.    Set_CV_Static_L2                ����        �� ��     K.    Set_CV_Current_Limit            ����        �n �M     K.    Set_CV_Response_Speed           ����        � ��     K.    Get_CV_Static_L1                ����        �� ��     K.    Get_CV_Static_L2                ����        �K �0     K.    Get_CV_Current_Limit            ����        �� ��     K.    Get_CV_Response_Speed           ����        �o �     K.    Set_CP_Static_L1                ����        �� ŋ     K.    Set_CP_Static_L2                ����        �e �S     K.    Set_CP_Static_Rise_Slew_Rate    ����        �- �     K.    Set_CP_Static_Fall_Slew_Rate    ����        �� ��     K.    Get_CP_Static_L1                ����        މ �d     K.    Get_CP_Static_L2                ����        � ��     K.    Get_CP_Static_Rise_Slew_Rate    ����        � �L     K.    Get_CP_Static_Fall_Slew_Rate    ����        � ��     K.    Set_OCP_Test                    ����        �� �v     K.    Set_OCP_Range                   ����        �5 C     K.    Set_OCP_Start_Current           ����         '     K.    Set_OCP_End_Current             ����         �     K.    Set_OCP_Step_Count              ����        � �     K.    Set_OCP_Dwell_Time              ����        G %r     K.    Set_OCP_Trigger_Voltage         ����        &L .�     K.    Set_OCP_Specification_Low       ����        /^ 7�     K.    Set_OCP_Specification_High      ����        8r ?5     K.    Query_OCP_Result                ����        ?� E�     K.    Get_OCP_Range                   ����        Fp L;     K.    Get_OCP_Start_Current           ����        L� R�     K.    Get_OCP_End_Current             ����        Sx Y?     K.    Get_OCP_Step_Count              ����        Y� _�     K.    Get_OCP_Dwell_Time              ����        `| fM     K.    Get_OCP_Trigger_Voltage         ����        g l�     K.    Get_OCP_Specification_Low       ����        m� s�     K.    Get_OCP_Specification_High      ����        tn zF     K.    Set_OPP_Test                    ����        { ��     K.    Set_OPP_Range                   ����        �� �1     K.    Set_OPP_Start_Power             ����        � ��     K.    Set_OPP_End_Power               ����        �v �Q     K.    Set_OPP_Step_Count              ����        � ��     K.    Set_OPP_Dwell_Time              ����        �� �     K.    Set_OPP_Trigger_Voltage         ����        �� ��     K.    Set_OPP_Specification_Low       ����        �x �9     K.    Set_OPP_Specification_High      ����        � ��     K.    Query_OPP_Result                ����        �� �T     K.    Get_OPP_Range                   ����        � ��     K.    Get_OPP_Start_Power             ����        ̓ �X     K.    Get_OPP_End_Power               ����        � ��     K.    Get_OPP_Step_Count              ����        ٕ �\     K.    Get_OPP_Dwell_Time              ����        � ��     K.    Get_OPP_Trigger_Voltage         ����        � �     K.    Get_OPP_Specification_Low       ����        �Q �F     K.    Get_OPP_Specification_High      ����        � �@     K.    Set_LED_Voltage_Out             ����        � J     K.    Set_LED_Current_Out             ����        $       K.    Set_LED_Rd_Coefficient          ����        � ?     K.    Set_LED_Rd_Ohm                  ����         =     K.    Set_LED_VF                      ����         $     K.    Set_Configure_Rd_Select         ����         $]     K.    Set_Configure_Response_Select   ����        % +     K.    Set_Configure_Response_Set      ����        +� 2     K.    Set_Configure_Set_All_LED       ����        2� 8�     K.    Set_Configure_Current_Range     ����        9~ ?\     K.    Set_Configure_Rr                ����        @ F     K.    Set_Configure_Rr_Select         ����        F� L�     K.    Set_Configure_Rr_Set            ����        M� S�     K.    Set_Configure_Short             ����        T� Z^     K.    Get_LED_Voltage_Out             ����        [ `�     K.    Get_LED_Current_Out             ����        a� g:     K.    Get_LED_Rd_Coefficient          ����        g� m�     K.    Get_LED_Rd_Ohm                  ����        n[ t
     K.    Get_LED_VF                      ����        t� z�     K.    Get_Configure_Response_Set      ����        {Y �>     K.    Get_Configure_Response_Select   ����        �� ��     K.    Get_Configure_Rd_Select         ����        �] �     K.    Get_Configure_Set_All_LED       ����        �� ��     K.    Get_Configure_Current_Range     ����        �Y �     K.    Get_Configure_Rr                ����        �� �|     K.    Get_Configure_Rr_Select         ����        �7 ��     K.    Get_Configure_Rr_Set            ����        �� �f     K.    Get_Configure_Short             ����        �! ��     K.    Set_Program_Parameters          ����        �N ��     K.    Set_Sequence_Parameters         ����        �e ��     K.    Set_Program_Save                ����        �m �-     K.    Set_Program_Run                 ����        �� �%     K.    Set_Program_Key                 ����        �I ��     K.    Get_Program_Parameters          ����        �� �9     K.    Get_Sequence_Parameters         ����        � �p     K.    Get_Program_Run                 ����        �+ �     K.    Set_Specification_Unit          ����        �� �4     K.    Set_Specification_Voltage       ����        �� 	�     K.    Set_Specification_Current       ����        C �     K.    Set_Specification_Power         ����        H .     K.    Set_Specification_Test          ����        �  �     K.    Get_Specification_Unit          ����        !c 'Z     K.    Get_Spec_Volt_Result            ����        ( .     K.    Get_Spec_Curr_Result            ����        .� 4�     K.    Get_Spec_All_Channel_Result     ����        5y ;�     K.    Get_Specification_Voltage       ����        =- C�     K.    Get_Specification_Current       ����        D� K$     K.    Get_Specification_Power         ����        La R     K.    Get_Specification_Test          ����        R� X�     K.    Fetch_Voltage                   ����        Y� _�     K.    Fetch_Current                   ����        `c fj     K.    Fetch_Power                     ����        g% m�     K.    Fetch_Status                    ����        nd tw     K.    Fetch_All_Voltage               ����        u2 {E     K.    Fetch_All_Current               ����        |  �     K.    Fetch_All_Power                 ����        �� �     K.    Fetch_Time                      ����        �� ��     K.    Measure_Voltage                 ����        �� ��     K.    Measure_Current                 ����        �> �1     K.    Measure_Power                   ����        �� ��     K.    Measure_All_Voltage             ����        �� ��     K.    Measure_All_Current             ����        �T �I     K.    Measure_All_Power               ����        � �     K.    Set_Measure_Input               ����        �� ��     K.    Set_Measure_Scan                ����        �� �`     K.    Get_Measure_Input               ����        � ��     K.    Get_Measure_Scan                ����        ͡ �<     K.    reset                           ����        Զ ܳ     K.    self_test                       ����        ݯ ��     K.    revision_query                  ����        �� �     K.    error_query                     ����        �� �     K.    error_message                   ����        � ��     K.    GetErrorInfo                    ����        �� �     K.    ClearErrorInfo                  ����        ] �     K.    GetNextCoercionRecord           ����        � +6     K.    LockSession                     ����        +� 9�     K.    UnlockSession                   ����        :� @�     K.    WriteInstrData                  ����        A; H(     K.    ReadInstrData                   ����        I^ PW     K.    close                                 �                                     DInitialize                           DInit Interface                       DInitialize With Options             �Application Functions             ����Configure Function                   DSet Configure Von                    DSet Configure Voltage Range          DSet Configure Voltage Latch          DSet Configure Von Latch Reset        DSet Configure Auto Load              DSet Configure Auto Mode              DSet Configure Sound                  DSet Configure Remote(For RS232)      DSet Configure Save                   DSet Configure LOADON                 DSet Configure Timing State           DSet Configure Timing Trigger         DSet Configure Timing Timeout         DSet Configure Voff State             DSet Configure Voff Final V           DSet Configure Measure Average        DSet Configure Digital IO             DSet Configure Key                    DSet Configure Echo                   DSet Configure LEDLCRL I Range     ����Query                                DGet Configure Von                    DGet Configure Voltage Range          DGet Configure Voltage Latch          DGet Configure Auto Load              DGet Configure Auto Mode              DGet Configure Sound                  DGet Configure LOADON                 DGet Configure Timing State           DGet Configure Timing Trigger         DGet Configure Timing Timeout         DGet Configure Voff State             DGet Configure Voff Final V           DGet Configure Measure Average        DGet Configure Digital IO             DGet Configure Key                    DGet Configure Echo                   DGet Configure LEDLCRL I Range     ����Channel Function                     DSet Channel Load                     DSet Channel Active                   DSet Channel Synchronized          ����Query                                DGet Channel Load                     DGet Min of Channel Load              DGet Max of Channel Load              DGet Channel Synchronized             DGet Channel ID                    ����Load Function                        DSet Load State                       DSet Load Short State                 DSet Load Short Key                   DSet Load Protection Clear            DSet Load Clear                       DSet Load Save                     ����Query                                DGet Load State                       DGet Load Short State                 DGet Load Short Key                   DGet Load Protection Status        ����All Run                              DRUN                               ����All Abort                            DABORT                             ����Show Display                         DShow Display                         DCLS                                  DSet ESE                              DSet OPC                              DRCL(Restore Memory)                  DRST                                  DSAV(Store Memory)                    DSet SRE                           ����Query                                DGet ESR                              DGet Identification String            DGet OPC                              DGet RDT                              DGet STB                             jConfiguration Functions             kSet/Get/Check Attribute             �Set Attribute                        DSet Attribute ViInt32                DSet Attribute ViReal64               DSet Attribute ViString               DSet Attribute ViBoolean              DSet Attribute ViSession             PGet Attribute                        DGet Attribute ViInt32                DGet Attribute ViReal64               DGet Attribute ViString               DGet Attribute ViBoolean              DGet Attribute ViSession             �Check Attribute                      DCheck Attribute ViInt32              DCheck Attribute ViReal64             DCheck Attribute ViString             DCheck Attribute ViBoolean            DCheck Attribute ViSession           tAction/Status Functions           ����Load Mode                            DSet Mode                          ����Query                                DGet Mode                          ����CC&CCD Mode                          DSet CC Static L1                     DSet CC Static L2                     DSet CC Static Rise Slew Rate         DSet CC Static Fall Slew Rate         DSet CC Dynamic L1                    DSet CC Dynamic L2                    DSet CC Dynamic Rise Slew Rate        DSet CC Dynamic Fall Slew Rate        DSet CC Dynamic T1                    DSet CC Dynamic T2                 ����Query                                DGet CC Static L1                     DGet CC Static L2                     DGet CC Static Rise Slew Rate         DGet CC Static Fall Slew Rate         DGet CC Dynamic L1                    DGet CC Dynamic L2                    DGet CC Dynamic Rise Slew Rate        DGet CC Dynamic Fall Slew Rate        DGet CC Dynamic T1                    DGet CC Dynamic T2                 ����CR Mode                              DSet CR Static L1                     DSet CR Static L2                     DSet CR Static Rise Slew Rate         DSet CR Static Fall Slew Rate      ����Query                                DGet CR Static L1                     DGet CR Static L2                     DGet CR Static Rise Slew Rate         DGet CR Static Fall Slew Rate      ����CV Mode                              DSet CV Static L1                     DSet CV Static L2                     DSet CV Current Limit                 DSet CV Response Speed             ����Query                                DGet CV Static L1                     DGet CV Static L2                     DGet CV Current Limit                 DGet CV Response Speed             ����CP Mode                              DSet CP Static L1                     DSet CP Static L2                     DSet CP Static Rise Slew Rate         DSet CP Static Fall Slew Rate      ����Query                                DGet CP Static L1                     DGet CP Static L2                     DGet CP Static Rise Slew Rate         DGet CP Static Fall Slew Rate      ����OCP Mode(63110A not support)         DSet OCP Test                         DSet OCP Range                        DSet OCP Start Current                DSet OCP End Current                  DSet OCP Step Count                   DSet OCP Dwell Time                   DSet OCP Trigger Voltage              DSet OCP Specification Low            DSet OCP Specification High           DQuery OCP Result                  ����Query                                DGet OCP Range                        DGet OCP Start Current                DGet OCP End Current                  DGet OCP Step Count                   DGet OCP Dwell Time                   DGet OCP Trigger Voltage              DGet OCP Specification Low            DGet OCP Specification High        ����OPP Mode(63110A not support)         DSet OPP Test                         DSet OPP Range                        DSet OPP Start Power                  DSet OPP End Power                    DSet OPP Step Count                   DSet OPP Dwell Time                   DSet OPP Trigger Voltage              DSet OPP Specification Low            DSet OPP Specification High           DQuery OPP Result                  ����Query                                DGet OPP Range                        DGet OPP Start Power                  DGet OPP End Power                    DGet OPP Step Count                   DGet OPP Dwell Time                   DGet OPP Trigger Voltage              DGet OPP Specification Low            DGet OPP Specification High        ����LED Mode(For 63110A or 63113A)       DSet LED Voltage Out                  DSet LED Current Out                  DSet LED Rd Coefficient               DSet LED Rd Ohm                       DSet LED VF                           DSet Configure Rd Select              DSet Configure Response Select        DSet Configure Response Set           DSet Configure Set All LED         ����Only for 63110A                      DSet Configure Current Range          DSet Configure Rr                     DSet Configure Rr Select              DSet Configure Rr Set                 DSet Configure Short               ����Query                                DGet LED Voltage Out                  DGet LED Current Out                  DGet LED Rd Coefficient               DGet LED Rd Ohm                       DGet LED VF                           DGet Configure Response Set           DGet Configure Response Select        DGet Configure Rd Select              DGet Configure Set All LED         ����Only for 63110A                      DGet Configure Current Range          DGet Configure Rr                     DGet Configure Rr Select              DGet Configure Rr Set                 DGet Configure Short               ����Program(63110A not support)          DSet Program Parameters               DSet Sequence Parameters              DSet Program Save                     DSet Program Run                      DSet Program Key                   ����Query                                DGet Program Parameters               DGet Sequence Parameters              DGet Program Run                   ����Specification                        DSet Specification Unit               DSet Specification Voltage            DSet Specification Current            DSet Specification Power(LED)         DSet Specification Test            ����Query                                DGet Specification Unit               DGet Spec_Volt Result                 DGet Spec_Curr Result                 DGet Spec_All_Channel Result          DGet Specification Voltage            DGet Specification Current            DGet Specification Power(LED)         DGet Specification Test              �Data Functions                    ����Fetch Function                       DFetch Voltage                        DFetch Current                        DFetch Power                          DFetch Status                         DFetch All Voltage                    DFetch All Current                    DFetch All Power                      DFetch Time                        ����Measure Function                     DMeasure Voltage                      DMeasure Current                      DMeasure Power                        DMeasure All Voltage                  DMeasure All Current                  DMeasure All Power                    DSet Measure Input                    DSet Measure Scan                  ����Query                                DGet Measure Input                    DGet Measure Scan                    PUtility Functions                    DReset                                DSelf-Test                            DRevision Query                       DError-Query                          DError Message                       �Error Info                           DGet Error Info                       DClear Error Info                    	Coercion Info                        DGet Next Coercion Record            	XLocking                              DLock Session                         DUnlock Session                      	�Instrument I/O                       DWrite Instrument Data                DRead Instrument Data                 DClose                           