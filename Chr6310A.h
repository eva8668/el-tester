/****************************************************************************
 *                       Programmable DC Electronic Load                           
 *---------------------------------------------------------------------------
 *   Copyright (c) National Instruments 1998.  All Rights Reserved.         
 *---------------------------------------------------------------------------
 *                                                                          
 * Title:    Chr6310A.h                                        
 * Purpose:  Programmable DC Electronic Load                                       
 *           instrument driver declarations.                                
 *                                                                          
 ****************************************************************************/

#ifndef __CHR6310A_HEADER
#define __CHR6310A_HEADER

#include <ivi.h>

#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif

/****************************************************************************
 *----------------- Instrument Driver Revision Information -----------------*
 ****************************************************************************/
#define CHR6310A_MAJOR_VERSION                1     /* Instrument driver major version   */
#define CHR6310A_MINOR_VERSION                0     /* Instrument driver minor version   */
                                                                
#define CHR6310A_CLASS_SPEC_MAJOR_VERSION     2     /* Class specification major version */
#define CHR6310A_CLASS_SPEC_MINOR_VERSION     0     /* Class specification minor version */


#define CHR6310A_SUPPORTED_INSTRUMENT_MODELS  "63101A,63102A,63103A,63104A,63106A,63107A,63108A,63110A,63112A,63123A"
#define CHR6310A_DRIVER_VENDOR                "Chroma ATE INC."
#define CHR6310A_DRIVER_DESCRIPTION           "Programmable DC Electronic Load 6310A Series Instrument Driver"
                    



/**************************************************************************** 
 *------------------------------ Useful Macros -----------------------------* 
 ****************************************************************************/


/**************************************************************************** 
 *---------------------------- Attribute Defines ---------------------------* 
 ****************************************************************************/

    /*- IVI Inherent Instrument Attributes ---------------------------------*/    

        /* User Options */
#define CHR6310A_ATTR_RANGE_CHECK                   IVI_ATTR_RANGE_CHECK                    /* ViBoolean */
#define CHR6310A_ATTR_QUERY_INSTR_STATUS            IVI_ATTR_QUERY_INSTR_STATUS             /* ViBoolean */
#define CHR6310A_ATTR_CACHE                         IVI_ATTR_CACHE                          /* ViBoolean */
#define CHR6310A_ATTR_SIMULATE                      IVI_ATTR_SIMULATE                       /* ViBoolean */
#define CHR6310A_ATTR_RECORD_COERCIONS              IVI_ATTR_RECORD_COERCIONS               /* ViBoolean */

        /* Instrument Capabilities */
#define CHR6310A_ATTR_NUM_CHANNELS                  IVI_ATTR_NUM_CHANNELS                   /* ViInt32,  read-only  */

        /* Driver Information  */
#define CHR6310A_ATTR_SPECIFIC_DRIVER_PREFIX        IVI_ATTR_SPECIFIC_DRIVER_PREFIX         /* ViString, read-only  */
#define CHR6310A_ATTR_SUPPORTED_INSTRUMENT_MODELS   IVI_ATTR_SUPPORTED_INSTRUMENT_MODELS    /* ViString, read-only  */
#define CHR6310A_ATTR_GROUP_CAPABILITIES            IVI_ATTR_GROUP_CAPABILITIES             /* ViString, read-only  */
#define CHR6310A_ATTR_INSTRUMENT_MANUFACTURER       IVI_ATTR_INSTRUMENT_MANUFACTURER        /* ViString, read-only  */
#define CHR6310A_ATTR_INSTRUMENT_MODEL              IVI_ATTR_INSTRUMENT_MODEL               /* ViString, read-only  */
#define CHR6310A_ATTR_INSTRUMENT_FIRMWARE_REVISION  IVI_ATTR_INSTRUMENT_FIRMWARE_REVISION              /* ViString, read-only  */
#define CHR6310A_ATTR_SPECIFIC_DRIVER_MAJOR_VERSION IVI_ATTR_SPECIFIC_DRIVER_MAJOR_VERSION  /* ViInt32,  read-only  */
#define CHR6310A_ATTR_SPECIFIC_DRIVER_MINOR_VERSION IVI_ATTR_SPECIFIC_DRIVER_MINOR_VERSION  /* ViInt32,  read-only  */
#define CHR6310A_ATTR_SPECIFIC_DRIVER_REVISION      IVI_ATTR_SPECIFIC_DRIVER_REVISION       /* ViString, read-only  */
#define CHR6310A_ATTR_SPECIFIC_DRIVER_VENDOR        IVI_ATTR_SPECIFIC_DRIVER_VENDOR         /* ViString, read-only  */
#define CHR6310A_ATTR_SPECIFIC_DRIVER_DESCRIPTION   IVI_ATTR_SPECIFIC_DRIVER_DESCRIPTION    /* ViString, read-only  */
#define CHR6310A_ATTR_SPECIFIC_DRIVER_CLASS_SPEC_MAJOR_VERSION IVI_ATTR_SPECIFIC_DRIVER_CLASS_SPEC_MAJOR_VERSION /* ViInt32, read-only */
#define CHR6310A_ATTR_SPECIFIC_DRIVER_CLASS_SPEC_MINOR_VERSION IVI_ATTR_SPECIFIC_DRIVER_CLASS_SPEC_MINOR_VERSION /* ViInt32, read-only */

        /* Error Info */
#define CHR6310A_ATTR_PRIMARY_ERROR                 IVI_ATTR_PRIMARY_ERROR                  /* ViInt32   */
#define CHR6310A_ATTR_SECONDARY_ERROR               IVI_ATTR_SECONDARY_ERROR                /* ViInt32   */
#define CHR6310A_ATTR_ERROR_ELABORATION             IVI_ATTR_ERROR_ELABORATION              /* ViString  */

        /* Advanced Session Information */
#define CHR6310A_ATTR_LOGICAL_NAME                  IVI_ATTR_LOGICAL_NAME                   /* ViString, read-only  */
#define CHR6310A_ATTR_RESOURCE_DESCRIPTOR           IVI_ATTR_RESOURCE_DESCRIPTOR            /* ViString, read-only  */
#define CHR6310A_ATTR_IO_SESSION_TYPE               IVI_ATTR_IO_SESSION_TYPE                /* ViString, read-only  */
#define CHR6310A_ATTR_IO_SESSION                    IVI_ATTR_IO_SESSION                     /* ViSession, read-only */
    

    /*- Instrument-Specific Attributes -------------------------------------*/

#define CHR6310A_ATTR_ID_QUERY_RESPONSE      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 1L)     /* ViString (Read Only) */
#define CHR6310A_ATTR_STANDARD_EVENT_STATUS_ENABLE (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 2L)
#define CHR6310A_ATTR_STANDARD_EVENT_STATUS_REGISTER (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 3L)
#define CHR6310A_ATTR_RECALL_INSTRUMENT_STATE (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 4L)
#define CHR6310A_ATTR_SAVE_INSTRUMENT_STATE (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 5L)
#define CHR6310A_ATTR_SERVICE_REQUEST_ENABLE (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 6L)
#define CHR6310A_ATTR_CHANNEL_LOAD          (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 7L)
#define CHR6310A_ATTR_QUERY_MIN_CHANNEL_LOAD (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 8L)
#define CHR6310A_ATTR_QUERY_MAX_CHANNEL_LOAD (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 9L)
#define CHR6310A_ATTR_CHANNEL_ACTIVE        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 10L)
#define CHR6310A_ATTR_CHANNEL_SYNCHRONIZED  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 11L)
#define CHR6310A_ATTR_CONFIGURE_VON         (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 12L)
#define CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 13L)
#define CHR6310A_ATTR_GET_CONFIGURE_VOLTAGE_RANGE (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 14L)
#define CHR6310A_ATTR_CONFIGURE_VOLTAGE_LATCH (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 15L)
#define CHR6310A_ATTR_CONFIGURE_AUTO_LOAD (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 16L)
#define CHR6310A_ATTR_CONFIGURE_AUTO_MODE   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 17L)
#define CHR6310A_ATTR_CONFIGURE_SOUND       (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 18L)
#define CHR6310A_ATTR_CONFIGURE_REMOTE      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 19L)
#define CHR6310A_ATTR_CONFIGURE_LOAD        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 20L)
#define CHR6310A_ATTR_CONFIGURE_TIMING_STATE (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 21L)
#define CHR6310A_ATTR_CONFIGURE_TIMING_TRIGGER (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 22L)
#define CHR6310A_ATTR_CONFIGURE_TIMING_TIMEOUT (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 23L)
#define CHR6310A_ATTR_CONFIGURE_VOFF_STATE  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 24L)
#define CHR6310A_ATTR_CONFIGURE_VOFF_FINAL_V (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 25L)
#define CHR6310A_ATTR_CONFIGURE_MEASURE_AVERAGE (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 26L)
#define CHR6310A_ATTR_CONFIGURE_DIGITAL_IO  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 27L)
#define CHR6310A_ATTR_CONFIGURE_KEY         (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 28L)
#define CHR6310A_ATTR_CONFIGURE_ECHO        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 29L)
#define CHR6310A_ATTR_CURRENT_STATIC_L1     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 30L)
#define CHR6310A_ATTR_MODE                  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 31L)
#define CHR6310A_ATTR_CURRENT_STATIC_L2     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 32L)
#define CHR6310A_ATTR_CC_STATIC_MIN_L1 (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 33L)
#define CHR6310A_ATTR_CC_STATIC_MAX_L1 (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 34L)
#define CHR6310A_ATTR_CC_RISE_SLEWRATE_MIN  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 43L)
#define CHR6310A_ATTR_CC_RISE_SLEWRATE_MAX  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 44L)
#define CHR6310A_ATTR_CC_FALL_SLEWRATE_MIN  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 45L)
#define CHR6310A_ATTR_CC_FALL_SLEWRATE_MAX  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 46L)
#define CHR6310A_ATTR_CURRENT_STATIC_RISE_SLEW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 47L)
#define CHR6310A_ATTR_CURRENT_STATIC_FALL_SLEW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 48L)
#define CHR6310A_ATTR_CC_DYNAMIC_MIN_L1 (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 49L)
#define CHR6310A_ATTR_CC_DYNAMIC_MAX_L1 (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 50L)
#define CHR6310A_ATTR_CURRENT_DYNAMIC_L1    (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 51L)
#define CHR6310A_ATTR_CURRENT_DYNAMIC_L2    (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 52L)
#define CHR6310A_ATTR_CC_STATIC_MAX_L2      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 53L)
#define CHR6310A_ATTR_CC_STATIC_MIN_L2      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 54L)
#define CHR6310A_ATTR_CC_DYNAMIC_MIN_L2     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 55L)
#define CHR6310A_ATTR_CC_DYNAMIC_MAX_L2     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 56L)
#define CHR6310A_ATTR_CC_DYNAMIC_RISE_MIN   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 57L)
#define CHR6310A_ATTR_CC_DYNAMIC_RISE_MAX   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 58L)
#define CHR6310A_ATTR_CC_DYNAMIC_FALL_MIN   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 59L)
#define CHR6310A_ATTR_CC_DYNAMIC_FALL_MAX   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 60L)
#define CHR6310A_ATTR_CURRENT_DYNAMIC_RISE_SLEW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 61L)
#define CHR6310A_ATTR_CURRENT_DYNAMIC_FALL_SLEW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 62L)
#define CHR6310A_ATTR_CC_DYNAMIC_T1_MIN     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 63L)
#define CHR6310A_ATTR_CC_DYNAMIC_T1_MAX     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 64L)
#define CHR6310A_ATTR_CC_DYNAMIC_T2_MIN     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 65L)
#define CHR6310A_ATTR_CC_DYNAMIC_T2_MAX     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 66L)
#define CHR6310A_ATTR_CURRENT_DYNAMIC_T1    (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 67L)
#define CHR6310A_ATTR_CURRENT_DYNAMIC_T2    (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 68L)
#define CHR6310A_ATTR_LOAD_STATE            (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 69L)
#define CHR6310A_ATTR_LOAD_SHORT_STATE      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 70L)
#define CHR6310A_ATTR_LOAD_SHORT_KEY        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 71L)
#define CHR6310A_ATTR_MEASURE_INPUT         (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 72L)
#define CHR6310A_ATTR_MEASURE_SCAN          (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 73L)
#define CHR6310A_ATTR_PROGRAM_FILE          (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 74L)
#define CHR6310A_ATTR_PROGRAM_ACTIVE        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 75L)
#define CHR6310A_ATTR_PROGRAM_CHAIN         (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 76L)
#define CHR6310A_ATTR_PROGRAM_ON_TIME_MIN   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 77L)
#define CHR6310A_ATTR_PROGRAM_ON_TIME_MAX   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 78L)
#define CHR6310A_ATTR_PROGRAM_OFF_TIME_MIN  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 79L)
#define CHR6310A_ATTR_PROGRAM_OFF_TIME_MAX  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 80L)
#define CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 81L)
#define CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 82L)
#define CHR6310A_ATTR_PROGRAM_ON_TIME       (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 83L)
#define CHR6310A_ATTR_PROGRAM_OFF_TIME      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 84L)
#define CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 85L)
#define CHR6310A_ATTR_SEQUENCE              (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 86L)
#define CHR6310A_ATTR_SEQUENCE_MODE         (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 87L)
#define CHR6310A_ATTR_SEQUENCE_SHORT_CHANNEL (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 88L)
#define CHR6310A_ATTR_SEQUENCE_SHORT_TIME   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 89L)
#define CHR6310A_ATTR_PROGRAM_RUN           (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 90L)
#define CHR6310A_ATTR_PROGRAM_KEY           (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 91L)
#define CHR6310A_ATTR_CR_STATIC_MIN_L1      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 92L)
#define CHR6310A_ATTR_CR_STATIC_MAX_L1      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 93L)
#define CHR6310A_ATTR_CR_STATIC_MIN_L2      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 94L)
#define CHR6310A_ATTR_CR_STATIC_MAX_L2      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 95L)
#define CHR6310A_ATTR_CR_RISE_SLEW_MIN      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 96L)
#define CHR6310A_ATTR_CR_RISE_SLEW_MAX      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 97L)
#define CHR6310A_ATTR_CR_FALL_SLEW_MIN      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 98L)
#define CHR6310A_ATTR_CR_FALL_SLEW_MAX      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 99L)
#define CHR6310A_ATTR_RESISTANCE_STATIC_L1  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 100L)
#define CHR6310A_ATTR_RESISTANCE_STATIC_L2  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 101L)
#define CHR6310A_ATTR_RESISTANCE_STATIC_RISE_SLEW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 102L)
#define CHR6310A_ATTR_RESISTANCE_STATIC_FALL_SLEW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 103L)
#define CHR6310A_ATTR_SHOW_DISPLAY          (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 104L)
#define CHR6310A_ATTR_SPECIFICATION_UNIT    (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 105L)
#define CHR6310A_ATTR_SPECIFICATION_VOLTAGE_HIGH (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 106L)
#define CHR6310A_ATTR_SPECIFICATION_VOLTAGE_CENTER (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 107L)
#define CHR6310A_ATTR_SPECIFICATION_VOLTAGE_LOW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 108L)
#define CHR6310A_ATTR_SPECIFICATION_CURRENT_HIGH (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 109L)
#define CHR6310A_ATTR_SPECIFICATION_CURRENT_CENTER (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 110L)
#define CHR6310A_ATTR_SPECIFICATION_CURRENT_LOW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 111L)
#define CHR6310A_ATTR_SPECIFICATION_TEST    (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 112L)
#define CHR6310A_ATTR_CV_STATIC_MIN_L1      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 113L)
#define CHR6310A_ATTR_CV_STATIC_MAX_L1      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 114L)
#define CHR6310A_ATTR_CV_CURRENT_MIN        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 115L)
#define CHR6310A_ATTR_CV_CURRENT_MAX        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 116L)
#define CHR6310A_ATTR_CV_STATIC_MAX_L2      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 117L)
#define CHR6310A_ATTR_CV_STATIC_MIN_L2      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 118L)
#define CHR6310A_ATTR_VOLTAGE_STATIC_L1 (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 119L)
#define CHR6310A_ATTR_VOLTAGE_STATIC_L2 (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 120L)
#define CHR6310A_ATTR_VOLTAGE_CURRENT_LIMIT (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 121L)
#define CHR6310A_ATTR_VOLTAGE_RESPONSE_SPEED (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 122L)
#define CHR6310A_ATTR_CP_STATIC_MIN_L1      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 123L)
#define CHR6310A_ATTR_CP_STATIC_MAX_L1      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 124L)
#define CHR6310A_ATTR_CP_STATIC_MIN_L2      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 125L)
#define CHR6310A_ATTR_CP_STATIC_MAX_L2      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 126L)
#define CHR6310A_ATTR_CP_RISE_SLEW_MIN      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 127L)
#define CHR6310A_ATTR_CP_RISE_SLEW_MAX      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 128L)
#define CHR6310A_ATTR_CP_FALL_SLEW_MIN      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 129L)
#define CHR6310A_ATTR_CP_FALL_SLEW_MAX      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 130L)
#define CHR6310A_ATTR_POWER_STATIC_L1       (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 131L)
#define CHR6310A_ATTR_POWER_STATIC_L2       (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 132L)
#define CHR6310A_ATTR_POWER_STATIC_RISE_SLEW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 133L)
#define CHR6310A_ATTR_POWER_STATIC_FALL_SLEW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 134L)
#define CHR6310A_ATTR_OCP_TEST              (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 135L)
#define CHR6310A_ATTR_OCP_RANGE             (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 136L)
#define CHR6310A_ATTR_OCP_START_CURRENT_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 137L)
#define CHR6310A_ATTR_OCP_START_CURRENT_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 138L)
#define CHR6310A_ATTR_OCP_END_CURRENT_MIN   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 139L)
#define CHR6310A_ATTR_OCP_END_CURRENT_MAX   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 140L)
#define CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 141L)
#define CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 142L)
#define CHR6310A_ATTR_OCP_SPECIFICATION_L_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 143L)
#define CHR6310A_ATTR_OCP_SPECIFICATION_L_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 144L)
#define CHR6310A_ATTR_OCP_SPECIFICATION_H_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 145L)
#define CHR6310A_ATTR_OCP_SPECIFICATION_H_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 146L)
#define CHR6310A_ATTR_OCP_START_CURRENT     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 147L)
#define CHR6310A_ATTR_OCP_END_CURRENT       (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 148L)
#define CHR6310A_ATTR_OCP_STEP_COUNT        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 149L)
#define CHR6310A_ATTR_OCP_DWELL_TIME        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 150L)
#define CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 151L)
#define CHR6310A_ATTR_OCP_SPECIFICATION_LOW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 152L)
#define CHR6310A_ATTR_OCP_SPECIFICATION_HIGH (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 153L)
#define CHR6310A_ATTR_OPP_START_POWER_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 154L)
#define CHR6310A_ATTR_OPP_START_POWER_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 155L)
#define CHR6310A_ATTR_OPP_END_POWER_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 156L)
#define CHR6310A_ATTR_OPP_END_POWER_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 157L)
#define CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 158L)
#define CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 159L)
#define CHR6310A_ATTR_OPP_SPECIFICATION_L_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 160L)
#define CHR6310A_ATTR_OPP_SPECIFICATION_L_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 161L)
#define CHR6310A_ATTR_OPP_SPECIFICATION_H_MIN (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 162L)
#define CHR6310A_ATTR_OPP_SPECIFICATION_H_MAX (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 163L)
#define CHR6310A_ATTR_OPP_TEST              (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 164L)
#define CHR6310A_ATTR_OPP_RANGE             (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 165L)
#define CHR6310A_ATTR_OPP_START_POWER (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 166L)
#define CHR6310A_ATTR_OPP_END_POWER         (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 167L)
#define CHR6310A_ATTR_OPP_STEP_COUNT        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 168L)
#define CHR6310A_ATTR_OPP_DWELL_TIME        (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 169L)
#define CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 170L)
#define CHR6310A_ATTR_OPP_SPECIFICATION_LOW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 171L)
#define CHR6310A_ATTR_OPP_SPECIFICATION_HIGH (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 172L)
#define CHR6310A_ATTR_LED_VOLTAGE_OUT       (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 173L)
#define CHR6310A_ATTR_LED_CURRENT_OUT       (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 174L)
#define CHR6310A_ATTR_LED_RD_COEFFICIENT    (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 175L)
#define CHR6310A_ATTR_LED_RD_OHM            (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 176L)
#define CHR6310A_ATTR_LED_VF                (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 177L)
#define CHR6310A_ATTR_LED_CONFIGURE_CURRENT_RANGE (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 178L)
#define CHR6310A_ATTR_LED_CONFIGURE_RD_SELECT (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 179L)
#define CHR6310A_ATTR_LED_CONFIGURE_RR      (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 180L)
#define CHR6310A_ATTR_LED_CONFIGURE_RR_SELECT (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 181L)
#define CHR6310A_ATTR_LED_CONFIGURE_RR_SET  (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 182L)
#define CHR6310A_ATTR_LED_CONFIGURE_SHORT   (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 183L)
#define CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SELECT (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 184L)
#define CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SET (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 185L)
#define CHR6310A_ATTR_LED_CONFIGURE_SET_ALL_LED (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 186L)
#define CHR6310A_ATTR_SPECIFICATION_POWER_HIGH (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 187L)
#define CHR6310A_ATTR_SPECIFICATION_POWER_CENTER (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 188L)
#define CHR6310A_ATTR_SPECIFICATION_POWER_LOW (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 189L)
#define CHR6310A_ATTR_CONFIGURE_LEDLCRL     (IVI_SPECIFIC_PUBLIC_ATTR_BASE + 190L)

    
/**************************************************************************** 
 *------------------------ Attribute Value Defines -------------------------* 
 ****************************************************************************/

        /* Instrument specific attribute value definitions */

/**************************************************************************** 
 *---------------- Instrument Driver Function Declarations -----------------* 
 ****************************************************************************/

    /*- Init and Close Functions -------------------------------------------*/
ViStatus _VI_FUNC  Chr6310A_init (ViRsrc resourceName, ViBoolean IDQuery,
                                  ViBoolean resetDevice, ViSession *vi);

ViStatus _VI_FUNC Chr6310A_InitInterface (ViRsrc resourceName, ViBoolean IDQuery,
                                          ViBoolean resetDevice,
                                          ViSession *instrumentHandle,
                                          ViInt32 baudRate, ViChar IDString[],
                                          ViString optionString);

ViStatus _VI_FUNC  Chr6310A_InitWithOptions (ViRsrc resourceName, ViBoolean IDQuery,
                                             ViBoolean resetDevice, ViString optionString, 
                                             ViSession *newVi);
ViStatus _VI_FUNC  Chr6310A_close (ViSession vi);   

    /*- Coercion Info Functions --------------------------------------------*/
ViStatus _VI_FUNC  Chr6310A_GetNextCoercionRecord (ViSession vi,
                                                   ViInt32 bufferSize,
                                                   ViChar record[]);

    /*- Locking Functions --------------------------------------------------*/
ViStatus _VI_FUNC  Chr6310A_LockSession (ViSession vi, ViBoolean *callerHasLock);   
ViStatus _VI_FUNC  Chr6310A_UnlockSession (ViSession vi, ViBoolean *callerHasLock);
    
    /*- Error Functions ----------------------------------------------------*/
ViStatus _VI_FUNC  Chr6310A_error_query (ViSession vi, ViInt32 *errorCode,
                                         ViChar errorMessage[]);
ViStatus _VI_FUNC  Chr6310A_GetErrorInfo (ViSession vi, ViStatus *primaryError, 
                                          ViStatus *secondaryError, 
                                          ViChar errorElaboration[256]);
ViStatus _VI_FUNC  Chr6310A_ClearErrorInfo (ViSession vi);
ViStatus _VI_FUNC  Chr6310A_error_message (ViSession vi, ViStatus errorCode,
                                           ViChar errorMessage[256]);

ViStatus _VI_FUNC Chr6310A_Set_LED_Voltage_Out (ViSession instrumentHandle,
                                                ViReal64 LEDVoltageOut);

ViStatus _VI_FUNC Chr6310A_Set_LED_Current_Out (ViSession instrumentHandle,
                                                ViReal64 LEDCurrentOut);

ViStatus _VI_FUNC Chr6310A_Set_LED_Rd_Coefficient (ViSession instrumentHandle,
                                                   ViReal64 LEDRdCoefficient);

ViStatus _VI_FUNC Chr6310A_Set_LED_Rd_Ohm (ViSession instrumentHandle,
                                           ViReal64 LEDRdOhm);

ViStatus _VI_FUNC Chr6310A_Set_LED_VF (ViSession instrumentHandle,
                                       ViReal64 LEDVF);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Current_Range (ViSession instrumentHandle,
                                                        ViBoolean configureCurrentRange);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Rd_Select (ViSession instrumentHandle,
                                                    ViInt32 configureRdSelect);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Rr (ViSession instrumentHandle,
                                             ViBoolean configureRr);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Rr_Select (ViSession instrumentHandle,
                                                    ViBoolean configureRrSelect);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Rr_Set (ViSession instrumentHandle,
                                                 ViReal64 configureRrSet);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Short (ViSession instrumentHandle,
                                                ViBoolean configureShort);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Response_Select (ViSession instrumentHandle,
                                                          ViBoolean configureResponseSelect);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Response_Set (ViSession instrumentHandle,
                                                       ViInt32 configureResponseSet);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Set_All_LED (ViSession instrumentHandle,
                                                      ViBoolean configureSetAllLED);

ViStatus _VI_FUNC Chr6310A_Get_LED_Voltage_Out (ViSession instrumentHandle,
                                                ViReal64 *getLEDVoltageOut);

ViStatus _VI_FUNC Chr6310A_Get_LED_Current_Out (ViSession instrumentHandle,
                                                ViReal64 *getLEDCurrentOut);

ViStatus _VI_FUNC Chr6310A_Get_LED_Rd_Coefficient (ViSession instrumentHandle,
                                                   ViReal64 *getLEDRdCoefficient);

ViStatus _VI_FUNC Chr6310A_Get_LED_Rd_Ohm (ViSession instrumentHandle,
                                           ViReal64 *getLEDRdOhm);

ViStatus _VI_FUNC Chr6310A_Get_LED_VF (ViSession instrumentHandle,
                                       ViReal64 *getLEDVF);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Current_Range (ViSession instrumentHandle,
                                                        ViBoolean *getConfigureCurrentRange);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Rd_Select (ViSession instrumentHandle,
                                                    ViInt32 *getConfigureRdSelect);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Rr (ViSession instrumentHandle,
                                             ViBoolean *getConfigureRr);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Rr_Select (ViSession instrumentHandle,
                                                    ViBoolean *getConfigureRrSelect);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Rr_Set (ViSession instrumentHandle,
                                                 ViReal64 *getConfigureRrSet);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Short (ViSession instrumentHandle,
                                                ViBoolean *getConfigureShort);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Response_Select (ViSession instrumentHandle,
                                                          ViBoolean *getConfigureResponseSelect);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Response_Set (ViSession instrumentHandle,
                                                       ViInt32 *getConfigureResponseSet);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Set_All_LED (ViSession instrumentHandle,
                                                      ViBoolean *getConfigureSet_All_LED);

ViStatus _VI_FUNC Chr6310A_Set_Program_Parameters (ViSession instrumentHandle,
                                                   ViInt32 programFile,
                                                   ViInt32 programActive,
                                                   ViInt32 programChain,
                                                   ViReal64 programOnTime,
                                                   ViReal64 programOffTime,
                                                   ViReal64 programPFDelayTime);

ViStatus _VI_FUNC Chr6310A_Set_Sequence_Parameters (ViSession instrumentHandle,
                                                    ViInt32 programSequence,
                                                    ViInt32 programSequenceMode,
                                                    ViInt32 programSequenceShortChannel,
                                                    ViReal64 programSequenceShortTime);

ViStatus _VI_FUNC Chr6310A_Set_Program_Save (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Set_Program_Run (ViSession instrumentHandle,
                                            ViBoolean programRun);

ViStatus _VI_FUNC Chr6310A_Set_Program_Key (ViSession instrumentHandle,
                                            ViInt32 programKey);

ViStatus _VI_FUNC Chr6310A_Get_Program_Parameters (ViSession instrumentHandle,
                                                   ViInt32 *programFile,
                                                   ViInt32 *programActive,
                                                   ViInt32 *programChain,
                                                   ViReal64 *programOnTime,
                                                   ViReal64 *programOffTime,
                                                   ViReal64 *programPFDelayTime);

ViStatus _VI_FUNC Chr6310A_Get_Sequence_Parameters (ViSession instrumentHandle,
                                                    ViInt32 *programSequence,
                                                    ViChar programSequenceMode[],
                                                    ViInt32 *programSequenceShortChannel,
                                                    ViReal64 *programSequenceShortTime);

ViStatus _VI_FUNC Chr6310A_Get_Program_Run (ViSession instrumentHandle,
                                            ViBoolean *getProgramRun);

ViStatus _VI_FUNC Chr6310A_Set_Specification_Unit (ViSession instrumentHandle,
                                                   ViBoolean specificationUnit);

ViStatus _VI_FUNC Chr6310A_Set_Specification_Voltage (ViSession instrumentHandle,
                                                      ViReal64 specifcationVoltageHigh,
                                                      ViReal64 specifcationVoltageCenter,
                                                      ViReal64 specifcationVoltageLow);

ViStatus _VI_FUNC Chr6310A_Set_Specification_Current (ViSession instrumentHandle,
                                                      ViReal64 specifcationCurrentHigh,
                                                      ViReal64 specifcationCurrentCenter,
                                                      ViReal64 specifcationCurrentLow);

ViStatus _VI_FUNC Chr6310A_Set_Specification_Power (ViSession instrumentHandle,
                                                    ViReal64 specifcationPowerHigh,
                                                    ViReal64 specifcationPowerCenter,
                                                    ViReal64 specifcationPowerLow);

ViStatus _VI_FUNC Chr6310A_Set_Specification_Test (ViSession instrumentHandle,
                                                   ViBoolean specificationTest);

ViStatus _VI_FUNC Chr6310A_Get_Specification_Unit (ViSession instrumentHandle,
                                                   ViBoolean *getSpecificationUnit);

ViStatus _VI_FUNC Chr6310A_Get_Spec_Volt_Result (ViSession instrumentHandle,
                                                 ViChar getSpec_VoltResult[]);

ViStatus _VI_FUNC Chr6310A_Get_Spec_Curr_Result (ViSession instrumentHandle,
                                                 ViChar getSpec_CurrResult[]);

ViStatus _VI_FUNC Chr6310A_Get_Spec_All_Channel_Result (ViSession instrumentHandle,
                                                        ViChar getSpec_All_ChannelResult[]);

ViStatus _VI_FUNC Chr6310A_Get_Specification_Voltage (ViSession instrumentHandle,
                                                      ViReal64 *specifcationVoltageHigh,
                                                      ViReal64 *specifcationVoltageCenter,
                                                      ViReal64 *specifcationVoltageLow);

ViStatus _VI_FUNC Chr6310A_Get_Specification_Current (ViSession instrumentHandle,
                                                      ViReal64 *specifcationCurrentHigh,
                                                      ViReal64 *specifcationCurrentCenter,
                                                      ViReal64 *specifcationCurrentLow);

ViStatus _VI_FUNC Chr6310A_Get_Specification_Power (ViSession instrumentHandle,
                                                    ViReal64 *specifcationPowerHigh,
                                                    ViReal64 *specifcationPowerCenter,
                                                    ViReal64 *specifcationPowerLow);

ViStatus _VI_FUNC Chr6310A_Get_Specification_Test (ViSession instrumentHandle,
                                                   ViBoolean *getSpecificationTest);

   
ViStatus _VI_FUNC Chr6310A_Fetch_Voltage (ViSession instrumentHandle,
                                          ViReal64 *fetchVoltage);

ViStatus _VI_FUNC Chr6310A_Fetch_Current (ViSession instrumentHandle,
                                          ViReal64 *fetchCurrent);

ViStatus _VI_FUNC Chr6310A_Fetch_Power (ViSession instrumentHandle,
                                        ViReal64 *fetchPower);

ViStatus _VI_FUNC Chr6310A_Fetch_Status (ViSession instrumentHandle,
                                         ViInt32 *fetchStatus);

ViStatus _VI_FUNC Chr6310A_Fetch_All_Voltage (ViSession instrumentHandle,
                                              ViChar fetchAllVoltage[]);

ViStatus _VI_FUNC Chr6310A_Fetch_All_Current (ViSession instrumentHandle,
                                              ViChar fetchAllCurrent[]);

ViStatus _VI_FUNC Chr6310A_Fetch_All_Power (ViSession instrumentHandle,
                                            ViChar fetchAllPower[]);

ViStatus _VI_FUNC Chr6310A_Fetch_Time (ViSession instrumentHandle,
                                       ViChar fetchTime[]);

ViStatus _VI_FUNC Chr6310A_Set_Measure_Input (ViSession instrumentHandle,
                                              ViBoolean measureInput);

ViStatus _VI_FUNC Chr6310A_Set_Measure_Scan (ViSession instrumentHandle,
                                             ViBoolean measureScan);

ViStatus _VI_FUNC Chr6310A_Get_Measure_Input (ViSession instrumentHandle,
                                              ViBoolean *getMeasureInput);

ViStatus _VI_FUNC Chr6310A_Get_Measure_Scan (ViSession instrumentHandle,
                                             ViBoolean *getMeasureScan);

ViStatus _VI_FUNC Chr6310A_Measure_Voltage (ViSession instrumentHandle,
                                            ViReal64 *measureVoltage);

ViStatus _VI_FUNC Chr6310A_Measure_Current (ViSession instrumentHandle,
                                            ViReal64 *measureCurrent);

ViStatus _VI_FUNC Chr6310A_Measure_Power (ViSession instrumentHandle,
                                          ViReal64 *measurePower);

ViStatus _VI_FUNC Chr6310A_Measure_All_Voltage (ViSession instrumentHandle,
                                                ViChar measureAllVoltage[]);

ViStatus _VI_FUNC Chr6310A_Measure_All_Current (ViSession instrumentHandle,
                                                ViChar measureAllCurrent[]);

ViStatus _VI_FUNC Chr6310A_Measure_All_Power (ViSession instrumentHandle,
                                              ViChar measureAllPower[]);

    /*- Utility Functions --------------------------------------------------*/
ViStatus _VI_FUNC  Chr6310A_reset (ViSession vi);
ViStatus _VI_FUNC  Chr6310A_self_test (ViSession vi, ViInt16 *selfTestResult,
                                       ViChar selfTestMessage[]);
ViStatus _VI_FUNC  Chr6310A_revision_query (ViSession vi, 
                                            ViChar instrumentDriverRevision[],
                                            ViChar firmwareRevision[]);
ViStatus _VI_FUNC  Chr6310A_WriteInstrData (ViSession vi, ViConstString writeBuffer); 
ViStatus _VI_FUNC  Chr6310A_ReadInstrData  (ViSession vi, ViInt32 numBytes, 
                                            ViChar rdBuf[], ViInt32 *bytesRead);

    /*- Set, Get, and Check Attribute Functions ----------------------------*/
ViStatus _VI_FUNC  Chr6310A_GetAttributeViInt32 (ViSession vi, ViConstString channelName, ViAttr attribute, ViInt32 *value);
ViStatus _VI_FUNC  Chr6310A_GetAttributeViReal64 (ViSession vi, ViConstString channelName, ViAttr attribute, ViReal64 *value);
ViStatus _VI_FUNC  Chr6310A_GetAttributeViString (ViSession vi, ViConstString channelName, ViAttr attribute, ViInt32 bufSize, ViChar value[]); 
ViStatus _VI_FUNC  Chr6310A_GetAttributeViSession (ViSession vi, ViConstString channelName, ViAttr attribute, ViSession *value);
ViStatus _VI_FUNC  Chr6310A_GetAttributeViBoolean (ViSession vi, ViConstString channelName, ViAttr attribute, ViBoolean *value);

ViStatus _VI_FUNC Chr6310A_CLS (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Set_ESE (ViSession instrumentHandle, ViInt32 setESE);

ViStatus _VI_FUNC Chr6310A_Get_ESR (ViSession instrumentHandle, ViInt32 *getESR);

ViStatus _VI_FUNC Chr6310A_Get_Identification_String (ViSession instrumentHandle,
                                                      ViChar getIdentificationString[]);

ViStatus _VI_FUNC Chr6310A_Set_OPC (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Get_OPC (ViSession instrumentHandle, ViInt32 *getOPC);

ViStatus _VI_FUNC Chr6310A_RCL (ViSession instrumentHandle, ViInt32 setRCL);

ViStatus _VI_FUNC Chr6310A_Get_RDT (ViSession instrumentHandle, ViChar getRDT[]);

ViStatus _VI_FUNC Chr6310A_RST (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_SAV (ViSession instrumentHandle, ViInt32 setSAV);

ViStatus _VI_FUNC Chr6310A_Set_SRE (ViSession instrumentHandle, ViInt32 setSRE);

ViStatus _VI_FUNC Chr6310A_Get_STB (ViSession instrumentHandle, ViInt32 *getSTB);

ViStatus _VI_FUNC Chr6310A_ABORT (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Get_Configure_LEDLCRL_Range (ViSession instrumentHandle,
                                                        ViBoolean *LEDLCRLRange);

ViStatus _VI_FUNC Chr6310A_Set_Channel_Load (ViSession instrumentHandle,
                                             ViInt32 channel);

ViStatus _VI_FUNC Chr6310A_Set_Channel_Active (ViSession instrumentHandle,
                                               ViBoolean channelActive);

ViStatus _VI_FUNC Chr6310A_Set_Channel_Synchronized (ViSession instrumentHandle,
                                                     ViBoolean channelSynchronized);

ViStatus _VI_FUNC Chr6310A_Get_Channel_Load (ViSession instrumentHandle,
                                             ViInt32 *getChannelLoad);

ViStatus _VI_FUNC Chr6310A_Get_Min_of_Channel_Load (ViSession instrumentHandle,
                                                    ViInt32 *getMin_ofChannelLoad);

ViStatus _VI_FUNC Chr6310A_Get_Max_of_Channel_Load (ViSession instrumentHandle,
                                                    ViInt32 *getMax_ofChannelLoad);

ViStatus _VI_FUNC Chr6310A_Get_Channel_Synchronized (ViSession instrumentHandle,
                                                     ViBoolean *getChannelSynchronized);

ViStatus _VI_FUNC Chr6310A_Get_Channel_ID (ViSession instrumentHandle,
                                           ViChar getChannelID[]);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Von (ViSession instrumentHandle,
                                              ViReal64 configureVon);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Voltage_Range (ViSession instrumentHandle,
                                                        ViReal64 voltageRange);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Voltage_Latch (ViSession instrumentHandle,
                                                        ViBoolean voltageLatch);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Von_Latch_Reset (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Auto_Load (ViSession instrumentHandle,
                                                    ViBoolean autoLoad);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Auto_Mode (ViSession instrumentHandle,
                                                    ViBoolean autoMode);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Sound (ViSession instrumentHandle,
                                                ViBoolean sound);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Remote (ViSession instrumentHandle,
                                                 ViBoolean remote);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Save (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Load (ViSession instrumentHandle,
                                               ViBoolean load);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Timing_State (ViSession instrumentHandle,
                                                       ViBoolean timingState);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Timing_Trigger (ViSession instrumentHandle,
                                                         ViReal64 timingTrigger);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Timing_Timeout (ViSession instrumentHandle,
                                                         ViInt32 timingTimeout);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Voff_State (ViSession instrumentHandle,
                                                     ViBoolean voffState);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Voff_Final_V (ViSession instrumentHandle,
                                                       ViReal64 configureVoffFinalVoltage);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Measure_Average (ViSession instrumentHandle,
                                                          ViInt32 measureAverage);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Digital_IO (ViSession instrumentHandle,
                                                     ViBoolean digitalIO);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Key (ViSession instrumentHandle,
                                              ViBoolean key);

ViStatus _VI_FUNC Chr6310A_Set_Configure_Echo (ViSession instrumentHandle,
                                               ViBoolean echo);

ViStatus _VI_FUNC Chr6310A_Set_LEDLCRL_Range (ViBoolean LEDLCRLRange,
                                              ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Von (ViSession instrumentHandle,
                                              ViReal64 *getConfigureVon);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Voltage_Range (ViSession instrumentHandle,
                                                        ViReal64 *get_Configure_Voltage_Range);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Voltage_Latch (ViSession instrumentHandle,
                                                        ViBoolean *getConfigureVoltageLatch);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Auto_Load (ViSession instrumentHandle,
                                                    ViBoolean *getConfigureAutoLoad);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Auto_Mode (ViSession instrumentHandle,
                                                    ViBoolean *getConfigureAutoMode);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Sound (ViSession instrumentHandle,
                                                ViBoolean *getConfigureSound);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Load (ViSession instrumentHandle,
                                               ViBoolean *getConfigureLoad);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Timing_State (ViSession instrumentHandle,
                                                       ViBoolean *getConfigureTimingState);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Timing_Trigger (ViSession instrumentHandle,
                                                         ViReal64 *getConfigureTimingTrigger);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Timing_Timeout (ViSession instrumentHandle,
                                                         ViInt32 *getConfigureTimingTimeout);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Voff_State (ViSession instrumentHandle,
                                                     ViBoolean *getConfigureVoffState);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Voff_Final_V (ViSession instrumentHandle,
                                                       ViReal64 *getConfigureVoffFinalV);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Measure_Average (ViSession instrumentHandle,
                                                          ViInt32 *getConfigureMeasureAverage);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Digital_IO (ViSession instrumentHandle,
                                                     ViBoolean *getConfigureDigitalIO);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Key (ViSession instrumentHandle,
                                              ViBoolean *getConfigureKey);

ViStatus _VI_FUNC Chr6310A_Get_Configure_Echo (ViSession instrumentHandle,
                                               ViBoolean *getConfigureEcho);

ViStatus _VI_FUNC Chr6310A_Set_CC_Static_L1 (ViSession instrumentHandle,
                                             ViReal64 CCStaticL1);

ViStatus _VI_FUNC Chr6310A_Set_CC_Static_L2 (ViSession instrumentHandle,
                                                  ViReal64 CCStaticL2);

ViStatus _VI_FUNC Chr6310A_Set_CC_Static_Rise_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 CCStaticRiseSlewRate);

ViStatus _VI_FUNC Chr6310A_Set_CC_Static_Fall_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 CCStaticFallSlewRate);

ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_L1 (ViSession instrumentHandle,
                                                   ViReal64 CCDynamicL1);

ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_L2 (ViSession instrumentHandle,
                                                   ViReal64 CCDynamicL2);

ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_Rise_Slew_Rate (ViSession instrumentHandle,
                                                          ViReal64 CCDynamicRiseSlewRate);

ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_Fall_Slew_Rate (ViSession instrumentHandle,
                                                          ViReal64 CCDynamicFallSlewRate);

ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_T1 (ViSession instrumentHandle,
                                                   ViReal64 CCDynamicT1);

ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_T2 (ViSession instrumentHandle,
                                                   ViReal64 CCDynamicT2);

ViStatus _VI_FUNC Chr6310A_Get_CC_Static_L1 (ViSession instrumentHandle,
                                                  ViReal64 *getCCStaticL1);

ViStatus _VI_FUNC Chr6310A_Get_CC_Static_L2 (ViSession instrumentHandle,
                                                  ViReal64 *getCCStaticL2);

ViStatus _VI_FUNC Chr6310A_Get_CC_Static_Rise_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 *getCCStaticRiseSlewRate);

ViStatus _VI_FUNC Chr6310A_Get_CC_Static_Fall_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 *getCCStaticFallSlew);

ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_L1 (ViSession instrumentHandle,
                                                   ViReal64 *getCCDynamicL1);

ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_L2 (ViSession instrumentHandle,
                                                   ViReal64 *getCCDynamicL2);

ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_Rise_Slew_Rate (ViSession instrumentHandle,
                                                          ViReal64 *getCCDynamicRiseSlew);

ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_Fall_Slew_Rate (ViSession instrumentHandle,
                                                          ViReal64 *getCCDynamicFallSlew);

ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_T1 (ViSession instrumentHandle,
                                                   ViReal64 *getCCDynamicT1);

ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_T2 (ViSession instrumentHandle,
                                                   ViReal64 *getCCDynamicT2);

ViStatus _VI_FUNC Chr6310A_Set_CR_Static_L1 (ViSession instrumentHandle,
                                             ViReal64 CRStaticL1);

ViStatus _VI_FUNC Chr6310A_Set_CR_Static_L2 (ViSession instrumentHandle,
                                             ViReal64 CRStaticL2);

ViStatus _VI_FUNC Chr6310A_Set_CR_Static_Rise_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 CRStaticRiseSlewRate);

ViStatus _VI_FUNC Chr6310A_Set_CR_Static_Fall_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 CRStaticFallSlewRate);

ViStatus _VI_FUNC Chr6310A_Get_CR_Static_L1 (ViSession instrumentHandle,
                                             ViReal64 *getCRStaticL1);

ViStatus _VI_FUNC Chr6310A_Get_CR_Static_L2 (ViSession instrumentHandle,
                                             ViReal64 *getCRStaticL1);

ViStatus _VI_FUNC Chr6310A_Get_CR_Static_Rise_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 *getCRStaticRiseSlewRate);

ViStatus _VI_FUNC Chr6310A_Get_CR_Static_Fall_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 *getCRStaticFallSlewRate);

ViStatus _VI_FUNC Chr6310A_Set_CV_Static_L1 (ViSession instrumentHandle,
                                             ViReal64 CVStaticL1);

ViStatus _VI_FUNC Chr6310A_Set_CV_Static_L2 (ViSession instrumentHandle,
                                             ViReal64 CVStaticL2);

ViStatus _VI_FUNC Chr6310A_Set_CV_Current_Limit (ViSession instrumentHandle,
                                                 ViReal64 CVCurrentLimit);

ViStatus _VI_FUNC Chr6310A_Set_CV_Response_Speed (ViSession instrumentHandle,
                                                  ViBoolean CVResponseSpeed);

ViStatus _VI_FUNC Chr6310A_Get_CV_Static_L1 (ViSession instrumentHandle,
                                             ViReal64 *getCRStaticL1);

ViStatus _VI_FUNC Chr6310A_Get_CV_Static_L2 (ViSession instrumentHandle,
                                             ViReal64 *getCVStaticL2);

ViStatus _VI_FUNC Chr6310A_Get_CV_Current_Limit (ViSession instrumentHandle,
                                                 ViReal64 *getCVCurrentLimit);

ViStatus _VI_FUNC Chr6310A_Get_CV_Response_Speed (ViSession instrumentHandle,
                                                  ViBoolean *getCVResponseSpeed);

ViStatus _VI_FUNC Chr6310A_Set_CP_Static_L1 (ViSession instrumentHandle,
                                             ViReal64 CPStaticL1);

ViStatus _VI_FUNC Chr6310A_Set_CP_Static_L2 (ViSession instrumentHandle,
                                             ViReal64 CPStaticL2);

ViStatus _VI_FUNC Chr6310A_Set_CP_Static_Rise_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 CPStaticRiseSlewRate);

ViStatus _VI_FUNC Chr6310A_Set_CP_Static_Fall_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 CPStaticFallSlewRate);

ViStatus _VI_FUNC Chr6310A_Get_CP_Static_L1 (ViSession instrumentHandle,
                                             ViReal64 *getCPStaticL1);

ViStatus _VI_FUNC Chr6310A_Get_CP_Static_L2 (ViSession instrumentHandle,
                                             ViReal64 *getCPStaticL2);

ViStatus _VI_FUNC Chr6310A_Get_CP_Static_Rise_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 *getCPStaticRiseSlewRate);

ViStatus _VI_FUNC Chr6310A_Get_CP_Static_Fall_Slew_Rate (ViSession instrumentHandle,
                                                         ViReal64 *getCPStaticFallSlewRate);

ViStatus _VI_FUNC Chr6310A_Set_OCP_Test (ViSession instrumentHandle,
                                         ViBoolean OCPTest);

ViStatus _VI_FUNC Chr6310A_Set_OCP_Range (ViSession instrumentHandle,
                                          ViBoolean OCPRange);

ViStatus _VI_FUNC Chr6310A_Set_OCP_Start_Current (ViSession instrumentHandle,
                                                 ViReal64 OCPStartCurrent);

ViStatus _VI_FUNC Chr6310A_Set_OCP_End_Current (ViSession instrumentHandle,
                                               ViReal64 OCPEndCurrent);

ViStatus _VI_FUNC Chr6310A_Set_OCP_Step_Count (ViSession instrumentHandle,
                                               ViInt32 OCPStepCount);

ViStatus _VI_FUNC Chr6310A_Set_OCP_Dwell_Time (ViSession instrumentHandle,
                                               ViInt32 OCPDwellTime);

ViStatus _VI_FUNC Chr6310A_Set_OCP_Trigger_Voltage (ViSession instrumentHandle,
                                                    ViReal64 OCPTriggerVoltage);

ViStatus _VI_FUNC Chr6310A_Set_OCP_Specification_Low (ViSession instrumentHandle,
                                                      ViReal64 OCPSpecificationLow);

ViStatus _VI_FUNC Chr6310A_Set_OCP_Specification_High (ViSession instrumentHandle,
                                                       ViReal64 OCPSpecificationHigh);

ViStatus _VI_FUNC Chr6310A_Query_OCP_Result (ViSession instrumentHandle,
                                             ViChar OCPResult[]);

ViStatus _VI_FUNC Chr6310A_Get_OCP_Range (ViSession instrumentHandle,
                                          ViBoolean *getOCPRange);

ViStatus _VI_FUNC Chr6310A_Get_OCP_Start_Current (ViSession instrumentHandle,
                                                  ViReal64 *getOCPStartCurrent);

ViStatus _VI_FUNC Chr6310A_Get_OCP_End_Current (ViSession instrumentHandle,
                                                ViReal64 *getOCPEndCurrent);

ViStatus _VI_FUNC Chr6310A_Get_OCP_Step_Count (ViSession instrumentHandle,
                                               ViInt32 *getOCPStepCount);

ViStatus _VI_FUNC Chr6310A_Get_OCP_Dwell_Time (ViSession instrumentHandle,
                                               ViInt32 *getOCPDwellTime);

ViStatus _VI_FUNC Chr6310A_Get_OCP_Trigger_Voltage (ViSession instrumentHandle,
                                                    ViReal64 *getOCPTriggerVoltage);

ViStatus _VI_FUNC Chr6310A_Get_OCP_Specification_Low (ViSession instrumentHandle,
                                                      ViReal64 *getOCPSpecificationLow);

ViStatus _VI_FUNC Chr6310A_Get_OCP_Specification_High (ViSession instrumentHandle,
                                                       ViReal64 *getOCPSpecificationHigh);

ViStatus _VI_FUNC Chr6310A_Set_OPP_Test (ViSession instrumentHandle,
                                         ViBoolean OPPTest);

ViStatus _VI_FUNC Chr6310A_Set_OPP_Range (ViSession instrumentHandle,
                                          ViBoolean OPPRange);

ViStatus _VI_FUNC Chr6310A_Set_OPP_Start_Power (ViSession instrumentHandle,
                                                ViReal64 OPPStartPower);

ViStatus _VI_FUNC Chr6310A_Set_OPP_End_Power (ViSession instrumentHandle,
                                              ViReal64 OPPEndPower);

ViStatus _VI_FUNC Chr6310A_Set_OPP_Step_Count (ViSession instrumentHandle,
                                               ViInt32 OPPStepCount);

ViStatus _VI_FUNC Chr6310A_Set_OPP_Dwell_Time (ViSession instrumentHandle,
                                               ViInt32 OPPDwellTime);

ViStatus _VI_FUNC Chr6310A_Set_OPP_Trigger_Voltage (ViSession instrumentHandle,
                                                    ViReal64 OPPTriggerVoltage);

ViStatus _VI_FUNC Chr6310A_Set_OPP_Specification_Low (ViSession instrumentHandle,
                                                      ViReal64 OPPSpecificationLow);

ViStatus _VI_FUNC Chr6310A_Set_OPP_Specification_High (ViSession instrumentHandle,
                                                       ViReal64 OPPSpecificationHigh);

ViStatus _VI_FUNC Chr6310A_Query_OPP_Result (ViSession instrumentHandle,
                                             ViChar OPPResult[]);

ViStatus _VI_FUNC Chr6310A_Get_OPP_Range (ViSession instrumentHandle,
                                          ViBoolean *getOPPRange);

ViStatus _VI_FUNC Chr6310A_Get_OPP_Start_Power (ViSession instrumentHandle,
                                                ViReal64 *getOPPStartPower);

ViStatus _VI_FUNC Chr6310A_Get_OPP_End_Power (ViSession instrumentHandle,
                                              ViReal64 *getOPPEndPower);

ViStatus _VI_FUNC Chr6310A_Get_OPP_Step_Count (ViSession instrumentHandle,
                                               ViInt32 *getOPPStepCount);

ViStatus _VI_FUNC Chr6310A_Get_OPP_Dwell_Time (ViSession instrumentHandle,
                                               ViInt32 *getOPPDwellTime);

ViStatus _VI_FUNC Chr6310A_Get_OPP_Trigger_Voltage (ViSession instrumentHandle,
                                                    ViReal64 *getOPPTriggerVoltage);

ViStatus _VI_FUNC Chr6310A_Get_OPP_Specification_Low (ViSession instrumentHandle,
                                                      ViReal64 *getOPPSpecificationLow);

ViStatus _VI_FUNC Chr6310A_Get_OPP_Specification_High (ViSession instrumentHandle,
                                                       ViReal64 *getOPPSpecificationHigh);

ViStatus _VI_FUNC Chr6310A_Set_Mode (ViSession instrumentHandle, ViInt32 mode);

ViStatus _VI_FUNC Chr6310A_Get_Mode (ViSession instrumentHandle,
                                     ViChar getMode[]);

ViStatus _VI_FUNC Chr6310A_Set_Load_State (ViSession instrumentHandle,
                                           ViBoolean loadState);

ViStatus _VI_FUNC Chr6310A_Set_Load_Short_State (ViSession instrumentHandle,
                                                 ViBoolean loadShortState);

ViStatus _VI_FUNC Chr6310A_Set_Load_Short_Key (ViSession instrumentHandle,
                                               ViBoolean loadShortKey);

ViStatus _VI_FUNC Chr6310A_Set_Load_Protection_Clear (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Set_Load_Clear (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Set_Load_Save (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Get_Load_State (ViSession instrumentHandle,
                                           ViBoolean *getLoadState);

ViStatus _VI_FUNC Chr6310A_Get_Load_Short_State (ViSession instrumentHandle,
                                                 ViBoolean *getLoadShortState);

ViStatus _VI_FUNC Chr6310A_Get_Load_Short_Key (ViSession instrumentHandle,
                                               ViBoolean *getLoadShortKey);

ViStatus _VI_FUNC Chr6310A_Get_Load_Protection_Status (ViSession instrumentHandle,
                                                       ViInt32 *loadProtectionStatus);

ViStatus _VI_FUNC Chr6310A_RUN (ViSession instrumentHandle);

ViStatus _VI_FUNC Chr6310A_Show_Display (ViSession instrumentHandle,
                                         ViInt32 display);

ViStatus _VI_FUNC  Chr6310A_SetAttributeViInt32 (ViSession vi, ViConstString channelName, ViAttr attribute, ViInt32 value);
ViStatus _VI_FUNC  Chr6310A_SetAttributeViReal64 (ViSession vi, ViConstString channelName, ViAttr attribute, ViReal64 value);
ViStatus _VI_FUNC  Chr6310A_SetAttributeViString (ViSession vi, ViConstString channelName, ViAttr attribute, ViConstString value); 
ViStatus _VI_FUNC  Chr6310A_SetAttributeViSession (ViSession vi, ViConstString channelName, ViAttr attribute, ViSession value);
ViStatus _VI_FUNC  Chr6310A_SetAttributeViBoolean (ViSession vi, ViConstString channelName, ViAttr attribute, ViBoolean value);

ViStatus _VI_FUNC  Chr6310A_CheckAttributeViInt32 (ViSession vi, ViConstString channelName, ViAttr attribute, ViInt32 value);
ViStatus _VI_FUNC  Chr6310A_CheckAttributeViReal64 (ViSession vi, ViConstString channelName, ViAttr attribute, ViReal64 value);
ViStatus _VI_FUNC  Chr6310A_CheckAttributeViString (ViSession vi, ViConstString channelName, ViAttr attribute, ViConstString value); 
ViStatus _VI_FUNC  Chr6310A_CheckAttributeViSession (ViSession vi, ViConstString channelName, ViAttr attribute, ViSession value);
ViStatus _VI_FUNC  Chr6310A_CheckAttributeViBoolean (ViSession vi, ViConstString channelName, ViAttr attribute, ViBoolean value);

    /*********************************************************
        Functions reserved for class driver use only.
        End-users should not call these functions.  
     *********************************************************/
ViStatus _VI_FUNC  Chr6310A_IviInit (ViRsrc resourceName, ViBoolean IDQuery, 
                                     ViBoolean reset, ViSession vi);
ViStatus _VI_FUNC  Chr6310A_IviClose (ViSession vi);   

/****************************************************************************
 *------------------------ Error And Completion Codes ----------------------*
 ****************************************************************************/
#define CHR6310A_ERROR_MODEL_NOT_SUPPORT  (IVI_SPECIFIC_ERROR_BASE + 1) 
#define CHR6310A_ERROR_INVALID_MODEL	  (IVI_SPECIFIC_ERROR_BASE + 2)
#define CHR6310A_ERROR_INVALID_FUNCTION	  (IVI_SPECIFIC_ERROR_BASE + 3)
#define CHR6310A_ERROR_LOAD_STATE         (IVI_SPECIFIC_ERROR_BASE + 4)
#define CHR6310A_ERROR_LOAD_SHORT_STATE   (IVI_SPECIFIC_ERROR_BASE + 5)
#define CHR6310A_ERROR_SPEC_VALUE		  (IVI_SPECIFIC_ERROR_BASE + 6)
#define CHR6310A_ERROR_INVALID_MODEL_OCP  (IVI_SPECIFIC_ERROR_BASE + 7)
#define CHR6310A_ERROR_INVALID_MODEL_OPP  (IVI_SPECIFIC_ERROR_BASE + 8)  
#define CHR6310A_ERROR_INVALID_MODE		  (IVI_SPECIFIC_ERROR_BASE + 9)
#define CHR6310A_ERROR_INVALID_VALUE	  (IVI_SPECIFIC_ERROR_BASE + 10)
/**************************************************************************** 
 *---------------------------- End Include File ----------------------------* 
 ****************************************************************************/

#define CHR6310A_VAL_CCL                                            0
#define CHR6310A_VAL_CCH                                            1
#define CHR6310A_VAL_CCDL                                           2
#define CHR6310A_VAL_CCDH                                           3
#define CHR6310A_VAL_CRL                                            4
#define CHR6310A_VAL_CRH                                            5
#define CHR6310A_VAL_CV                                             6
#define CHR6310A_VAL_CPL                                            7
#define CHR6310A_VAL_CPH                                            8

#define CHR6310A_VAL_LEDL                                           9
#define CHR6310A_VAL_LEDH                                           10


#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif
#endif /* __CHR6310A_HEADER */




