#include <cvirte.h>		
#include <userint.h>
#include "chr6310A_example.h"

static int panelHandle;

/***************************************************************************** 
 *  Copyright 1998 National Instruments Corporation.  All Rights Reserved.   * 
 *****************************************************************************/
/*****************************************************************************
 *  Programmable DC Electronic Load Instrument Driver                               
 *  LabWindows/CVI 6.0 Instrument Driver                                     
 *  Original Release: 2010年05月05日                                  
 *  By: Victor, Chroma                              
 *      PH.    Fax                               
 *                                                                           
 *  Modification History:                                                    
 *                                                                           
 *       2010年05月05日 - Instrument Driver Created.                  
 *                                                                           
 *****************************************************************************/

#include <string.h>
#include <stdio.h>  
#include <formatio.h>
#include "Chr6310A.h"
#include <utility.h>
#include <ansi_c.h>

/*****************************************************************************
 *--------------------- Hidden Attribute Declarations -----------------------*
 *****************************************************************************/

#define CHR6310A_ATTR_OPC_TIMEOUT      (IVI_SPECIFIC_PRIVATE_ATTR_BASE + 1L)   /* ViInt32 */

/*****************************************************************************
 *---------------------------- Useful Macros --------------------------------*
 *****************************************************************************/

    /*- I/O buffer size -----------------------------------------------------*/
#define BUFFER_SIZE                             512L        
#define CHR6310A_IO_SESSION_TYPE   IVI_VAL_VISA_SESSION_TYPE

    /*- 488.2 Event Status Register (ESR) Bits ------------------------------*/
#define IEEE_488_2_QUERY_ERROR_BIT              0x04
#define IEEE_488_2_DEVICE_DEPENDENT_ERROR_BIT   0x08
#define IEEE_488_2_EXECUTION_ERROR_BIT          0x10
#define IEEE_488_2_COMMAND_ERROR_BIT            0x20


    /*- List of channels passed to the Ivi_BuildChannelTable function -------*/ 
#define CHANNEL_LIST                            "1"

static IviRangeTableEntry attrSpecificationPowerLowRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSpecificationPowerLowRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSpecificationPowerLowRangeTableEntries,
	};

static IviRangeTableEntry attrSpecificationPowerCenterRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSpecificationPowerCenterRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSpecificationPowerCenterRangeTableEntries,
	};

static IviRangeTableEntry attrSpecificationPowHighRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSpecificationPowHighRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSpecificationPowHighRangeTableEntries,
	};

static IviRangeTableEntry attrLedConfigureResponseSetRangeTableEntries[] =
	{
		{1, 5, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrLedConfigureResponseSetRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrLedConfigureResponseSetRangeTableEntries,
	};

static IviRangeTableEntry attrLedConfigureRrSetRangeTableEntries[] =
	{
		{5, 125, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrLedConfigureRrSetRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrLedConfigureRrSetRangeTableEntries,
	};

static IviRangeTableEntry attrConfigureRdSelectRangeTableEntries[] =
	{
		{0, 3, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrConfigureRdSelectRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrConfigureRdSelectRangeTableEntries,
	};

static IviRangeTableEntry attrLedVfRangeTableEntries[] =
	{
		{0, 500, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrLedVfRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrLedVfRangeTableEntries,
	};

static IviRangeTableEntry attrLedRdOhmRangeTableEntries[] =
	{
		{0, 10000, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrLedRdOhmRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrLedRdOhmRangeTableEntries,
	};

static IviRangeTableEntry attrLedRdCoefficientRangeTableEntries[] =
	{
		{0.001, 1, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrLedRdCoefficientRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrLedRdCoefficientRangeTableEntries,
	};

static IviRangeTableEntry attrLedCurrentOutRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrLedCurrentOutRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrLedCurrentOutRangeTableEntries,
	};

static IviRangeTableEntry attrLedVoltageOutRangeTableEntries[] =
	{
		{0, 500, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrLedVoltageOutRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrLedVoltageOutRangeTableEntries,
	};

static IviRangeTableEntry attrOppSpecificationHighRangeTableEntries[] =
	{
		{0, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOppSpecificationHighRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOppSpecificationHighRangeTableEntries,
	};

static IviRangeTableEntry attrOppSpecificationLowRangeTableEntries[] =
	{
		{0, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOppSpecificationLowRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOppSpecificationLowRangeTableEntries,
	};

static IviRangeTableEntry attrOppTriggerVoltageRangeTableEntries[] =
	{
		{0, 80, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOppTriggerVoltageRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOppTriggerVoltageRangeTableEntries,
	};

static IviRangeTableEntry attrOppDwellTimeRangeTableEntries[] =
	{
		{1, 1000, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOppDwellTimeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOppDwellTimeRangeTableEntries,
	};

static IviRangeTableEntry attrOppStepCountRangeTableEntries[] =
	{
		{1, 1000, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOppStepCountRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOppStepCountRangeTableEntries,
	};

static IviRangeTableEntry attrOppStartPowerRangeTableEntries[] =
	{
		{0, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOppStartPowerRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOppStartPowerRangeTableEntries,
	};

static IviRangeTableEntry attrOcpStartPowerRangeTableEntries[] =
	{
		{0, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOcpStartPowerRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOcpStartPowerRangeTableEntries,
	};

static IviRangeTableEntry attrOcpSpecificationHighRangeTableEntries[] =
	{
		{0, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOcpSpecificationHighRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOcpSpecificationHighRangeTableEntries,
	};

static IviRangeTableEntry attrOcpSpecificationLowRangeTableEntries[] =
	{
		{0, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOcpSpecificationLowRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOcpSpecificationLowRangeTableEntries,
	};

static IviRangeTableEntry attrOcpTriggerVoltageRangeTableEntries[] =
	{
		{0, 80, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOcpTriggerVoltageRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOcpTriggerVoltageRangeTableEntries,
	};

static IviRangeTableEntry attrOcpDwellTimeRangeTableEntries[] =
	{
		{1, 1000, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOcpDwellTimeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOcpDwellTimeRangeTableEntries,
	};

static IviRangeTableEntry attrOcpStepCountRangeTableEntries[] =
	{
		{1, 1000, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOcpStepCountRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOcpStepCountRangeTableEntries,
	};

static IviRangeTableEntry attrOcpEndCurrentRangeTableEntries[] =
	{
		{0, 20, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOcpEndCurrentRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOcpEndCurrentRangeTableEntries,
	};

static IviRangeTableEntry attrOcpStartCurrentRangeTableEntries[] =
	{
		{0, 20, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrOcpStartCurrentRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrOcpStartCurrentRangeTableEntries,
	};

static IviRangeTableEntry attrPowerRiseSlewRangeTableEntries[] =
	{
		{0, 4, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrPowerRiseSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrPowerRiseSlewRangeTableEntries,
	};

static IviRangeTableEntry attrPowerStaticFallSlewRangeTableEntries[] =
	{
		{0, 4, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrPowerStaticFallSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrPowerStaticFallSlewRangeTableEntries,
	};

static IviRangeTableEntry attrPowerStaticL1RangeTableEntries[] =
	{
		{0, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrPowerStaticL1RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrPowerStaticL1RangeTableEntries,
	};

static IviRangeTableEntry attrPowerStaticL2RangeTableEntries[] =
	{
		{0, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrPowerStaticL2RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrPowerStaticL2RangeTableEntries,
	};

static IviRangeTableEntry attrVoltageCurrentLimitRangeTableEntries[] =
	{
		{0, 20, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVoltageCurrentLimitRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVoltageCurrentLimitRangeTableEntries,
	};

static IviRangeTableEntry attrVoltageL2RangeTableEntries[] =
	{
		{0, 80, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVoltageL2RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVoltageL2RangeTableEntries,
	};

static IviRangeTableEntry attrVoltageL1RangeTableEntries[] =
	{
		{0, 80, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrVoltageL1RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrVoltageL1RangeTableEntries,
	};

static IviRangeTableEntry attrSpecificationCurrentHighRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSpecificationCurrentHighRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSpecificationCurrentHighRangeTableEntries,
	};

static IviRangeTableEntry attrSpecificationCurrentCenterRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSpecificationCurrentCenterRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSpecificationCurrentCenterRangeTableEntries,
	};

static IviRangeTableEntry attrSpecificationCurrentLowRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSpecificationCurrentLowRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSpecificationCurrentLowRangeTableEntries,
	};

static IviRangeTableEntry attrSpecificationVoltageHighRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSpecificationVoltageHighRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSpecificationVoltageHighRangeTableEntries,
	};

static IviRangeTableEntry attrSpecificationVoltageCenterRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSpecificationVoltageCenterRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSpecificationVoltageCenterRangeTableEntries,
	};

static IviRangeTableEntry attrSpecificationVoltageLowRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSpecificationVoltageLowRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSpecificationVoltageLowRangeTableEntries,
	};

static IviRangeTableEntry attrShowDisplayRangeTableEntries[] =
	{
		{0, 0, 0, "L", 0},
		{1, 0, 0, "LPI", 0},
		{2, 0, 0, "LVP", 0},
		{3, 0, 0, "R", 0},
		{4, 0, 0, "RPI", 0},
		{5, 0, 0, "RVP", 0},
		{6, 0, 0, "LRV", 0},
		{7, 0, 0, "LRI", 0},
		{8, 0, 0, "LRP", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrShowDisplayRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrShowDisplayRangeTableEntries,
	};

static IviRangeTableEntry attrResistanceStaticFallSlewRangeTableEntries[] =
	{
		{0, 0.8, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrResistanceStaticFallSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrResistanceStaticFallSlewRangeTableEntries,
	};

static IviRangeTableEntry attrResistanceStaticRiseSlewRangeTableEntries[] =
	{
		{0, 0.8, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrResistanceStaticRiseSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrResistanceStaticRiseSlewRangeTableEntries,
	};

static IviRangeTableEntry attrResistanceStaticL1RangeTableEntries[] =
	{
		{0, 15000, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrResistanceStaticL1RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrResistanceStaticL1RangeTableEntries,
	};

static IviRangeTableEntry attrCrStaticL1RangeTableEntries[] =
	{
		{0, 15000, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCrStaticL1RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCrStaticL1RangeTableEntries,
	};

static IviRangeTableEntry attrProgramKeyRangeTableEntries[] =
	{
		{0, 11, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgramKeyRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgramKeyRangeTableEntries,
	};

static IviRangeTableEntry attrSequenceShortTimeRangeTableEntries[] =
	{
		{0, 30, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSequenceShortTimeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSequenceShortTimeRangeTableEntries,
	};

static IviRangeTableEntry attrSequenceShortChannelRangeTableEntries[] =
	{
		{0, 255, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSequenceShortChannelRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSequenceShortChannelRangeTableEntries,
	};

static IviRangeTableEntry attrSequenceModeRangeTableEntries[] =
	{
		{0, 0, 0, "SKIP", 0},
		{1, 0, 0, "AUTO", 0},
		{2, 0, 0, "MANUAL", 0},
		{3, 0, 0, "EXT", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSequenceModeRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSequenceModeRangeTableEntries,
	};

static IviRangeTableEntry attrSequenceRangeTableEntries[] =
	{
		{1, 10, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSequenceRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSequenceRangeTableEntries,
	};

static IviRangeTableEntry attrProgramOffTimeRangeTableEntries[] =
	{
		{0, 30, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgramOffTimeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgramOffTimeRangeTableEntries,
	};

static IviRangeTableEntry attrProgramPfDelayTimeRangeTableEntries[] =
	{
		{0, 30, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgramPfDelayTimeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgramPfDelayTimeRangeTableEntries,
	};

static IviRangeTableEntry attrProgramOnTimeRangeTableEntries[] =
	{
		{0, 30, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgramOnTimeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgramOnTimeRangeTableEntries,
	};

static IviRangeTableEntry attrProgramChainRangeTableEntries[] =
	{
		{0, 10, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgramChainRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgramChainRangeTableEntries,
	};

static IviRangeTableEntry attrProgramActiveRangeTableEntries[] =
	{
		{0, 255, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgramActiveRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgramActiveRangeTableEntries,
	};

static IviRangeTableEntry attrProgramFileRangeTableEntries[] =
	{
		{1, 10, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrProgramFileRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrProgramFileRangeTableEntries,
	};

static IviRangeTableEntry attrCurrentDynamicT1RangeTableEntries[] =
	{
		{0, 50, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentDynamicT1RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentDynamicT1RangeTableEntries,
	};

static IviRangeTableEntry attrCurrentDynamicT2RangeTableEntries[] =
	{
		{0, 50, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentDynamicT2RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentDynamicT2RangeTableEntries,
	};

static IviRangeTableEntry attrCurrentDynamicFallSlewRangeTableEntries[] =
	{
		{0, 0.8, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentDynamicFallSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentDynamicFallSlewRangeTableEntries,
	};

static IviRangeTableEntry attrCurrentDynamicRiseSlewRangeTableEntries[] =
	{
		{0, 0.8, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentDynamicRiseSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentDynamicRiseSlewRangeTableEntries,
	};

static IviRangeTableEntry attrCurrentDynamicL2RangeTableEntries[] =
	{
		{0, 20, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentDynamicL2RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentDynamicL2RangeTableEntries,
	};

static IviRangeTableEntry attrCurrentDynamicL1RangeTableEntries[] =
	{
		{0, 20, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentDynamicL1RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentDynamicL1RangeTableEntries,
	};

static IviRangeTableEntry attrCurrentStaticRiseSlewRangeTableEntries[] =
	{
		{0, 0.8, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentStaticRiseSlewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentStaticRiseSlewRangeTableEntries,
	};

static IviRangeTableEntry attrCurrentStaticRiseslewRangeTableEntries[] =
	{
		{0, 0.8, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentStaticRiseslewRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentStaticRiseslewRangeTableEntries,
	};

static IviRangeTableEntry attrCurrentStaticL2RangeTableEntries[] =
	{
		{0, 20, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentStaticL2RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentStaticL2RangeTableEntries,
	};

static IviRangeTableEntry attrModeRangeTableEntries[] =
	{
		{CHR6310A_VAL_CCL, 0, 0, "CCL", 0},
		{CHR6310A_VAL_CCH, 0, 0, "CCH", 0},
		{CHR6310A_VAL_CCDL, 0, 0, "CCDL", 0},
		{CHR6310A_VAL_CCDH, 0, 0, "CCDH", 0},
		{CHR6310A_VAL_CRL, 0, 0, "CRL", 0},
		{CHR6310A_VAL_CRH, 0, 0, "CRH", 0},
		{CHR6310A_VAL_CV, 0, 0, "CV", 0},
		{CHR6310A_VAL_CPL, 0, 0, "CPL", 0},
		{CHR6310A_VAL_CPH, 0, 0, "CPH", 0},
		{CHR6310A_VAL_LEDL, 0, 0, "LEDL", 0},
		{CHR6310A_VAL_LEDH, 0, 0, "LEDH", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrModeRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrModeRangeTableEntries,
	};

static IviRangeTableEntry attrCurrentStaticL1RangeTableEntries[] =
	{
		{0, 20, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrCurrentStaticL1RangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrCurrentStaticL1RangeTableEntries,
	};

static IviRangeTableEntry attrConfigureMeasureAverageRangeTableEntries[] =
	{
		{1, 64, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrConfigureMeasureAverageRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrConfigureMeasureAverageRangeTableEntries,
	};

static IviRangeTableEntry attrConfigureVoffFinalVRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrConfigureVoffFinalVRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrConfigureVoffFinalVRangeTableEntries,
	};

static IviRangeTableEntry attrConfigureTimingTimeoutRangeTableEntries[] =
	{
		{1, 86400000, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrConfigureTimingTimeoutRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrConfigureTimingTimeoutRangeTableEntries,
	};

static IviRangeTableEntry attrConfigureTimingTriggerRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrConfigureTimingTriggerRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrConfigureTimingTriggerRangeTableEntries,
	};

static IviRangeTableEntry attrModelNameRangeTableEntries[] =
	{
		{0, 0, 0, "NONE", 0},
		{1, 0, 0, "63101A", 0},
		{2, 0, 0, "63102A", 0},
		{3, 0, 0, "63103A", 0},
		{4, 0, 0, "63105A", 0},
		{5, 0, 0, "63106A", 0},
		{6, 0, 0, "63107AL", 0},
		{7, 0, 0, "63107AR", 0},
		{8, 0, 0, "63108A", 0},
		{9, 0, 0, "63110A", 0},
		{10, 0, 0, "63112A", 0},
		{11, 0, 0, "63123A", 0},
		{12, 0, 0, "63113A", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrModelNameRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrModelNameRangeTableEntries,
	};

static IviRangeTableEntry attrConfigureVoltageRangeRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrConfigureVoltageRangeRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrConfigureVoltageRangeRangeTableEntries,
	};

static IviRangeTableEntry attrConfigureVonRangeTableEntries[] =
	{
		{0, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrConfigureVonRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrConfigureVonRangeTableEntries,
	};



static IviRangeTableEntry attrChannelListRangeTableEntries[] =
	{
		{1, 0, 0, "", 0},
		{2, 0, 0, "", 0},
		{3, 0, 0, "", 0},
		{4, 0, 0, "", 0},
		{5, 0, 0, "", 0},
		{6, 0, 0, "", 0},
		{7, 0, 0, "", 0},
		{8, 0, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrChannelListRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrChannelListRangeTableEntries,
	};

static IviRangeTableEntry attrChannelLoadRangeTableEntries[] =
	{
		{0, 0, 0, "63102A", 0},
		{0, 0, 0, "63102A", 0},
		{0, 0, 0, "63102A", 0},
		{0, 0, 0, "63102A", 0},
		{0, 0, 0, "63102A", 0},
		{0, 0, 0, "63102A", 0},
		{0, 0, 0, "63102A", 0},
		{0, 0, 0, "63102A", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrChannelLoadRangeTable =
	{
		IVI_VAL_DISCRETE,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrChannelLoadRangeTableEntries,
	};

static IviRangeTableEntry attrServiceRequestEnableRangeTableEntries[] =
	{
		{0, 255, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrServiceRequestEnableRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrServiceRequestEnableRangeTableEntries,
	};

static IviRangeTableEntry attrSaveInstrumentStateRangeTableEntries[] =
	{
		{1, 100, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrSaveInstrumentStateRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrSaveInstrumentStateRangeTableEntries,
	};

static IviRangeTableEntry attrRecallInstrumentStateRangeTableEntries[] =
	{
		{1, 101, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrRecallInstrumentStateRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrRecallInstrumentStateRangeTableEntries,
	};

static IviRangeTableEntry attrStandardEventStatusEnableRangeTableEntries[] =
	{
		{0, 255, 0, "", 0},
		{IVI_RANGE_TABLE_LAST_ENTRY}
	};

static IviRangeTable attrStandardEventStatusEnableRangeTable =
	{
		IVI_VAL_RANGED,
        VI_TRUE,
        VI_TRUE,
        VI_NULL,
        attrStandardEventStatusEnableRangeTableEntries,
	};

/*****************************************************************************
 *-------------- Utility Function Declarations (Non-Exported) ---------------*
 *****************************************************************************/
static ViStatus Chr6310A_InitAttributes (ViSession vi);
static ViStatus Chr6310A_DefaultInstrSetup (ViSession openInstrSession);
static ViStatus Chr6310A_CheckStatus (ViSession vi);
static ViStatus Chr6310A_WaitForOPC (ViSession vi, ViInt32 maxTime);

    /*- File I/O Utility Functions -*/
static ViStatus Chr6310A_ReadToFile (ViSession vi, ViConstString filename, 
                                     ViInt32 maxBytesToRead, ViInt32 fileAction, ViInt32 *totalBytesWritten);
static ViStatus Chr6310A_WriteFromFile (ViSession vi, ViConstString filename, 
                                        ViInt32 maxBytesToWrite, ViInt32 byteOffset, 
                                        ViInt32 *totalBytesWritten);

/*****************************************************************************
 *----------------- Callback Declarations (Non-Exported) --------------------*
 *****************************************************************************/

    /*- Global Session Callbacks --------------------------------------------*/
    
static ViStatus _VI_FUNC Chr6310A_CheckStatusCallback (ViSession vi, ViSession io);
static ViStatus _VI_FUNC Chr6310A_WaitForOPCCallback (ViSession vi, ViSession io);

    /*- Attribute callbacks -------------------------------------------------*/

static ViStatus _VI_FUNC Chr6310AAttrDriverRevision_ReadCallback (ViSession vi,
                                                                  ViSession io, 
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId, 
                                                                  const ViConstString cacheValue);
static ViStatus _VI_FUNC Chr6310AAttrInstrumentManufacturer_ReadCallback (ViSession vi, 
                                                                          ViSession io,
                                                                          ViConstString channelName, 
                                                                          ViAttr attributeId,
                                                                          const ViConstString cacheValue);
static ViStatus _VI_FUNC Chr6310AAttrInstrumentModel_ReadCallback (ViSession vi, 
                                                                   ViSession io,
                                                                   ViConstString channelName, 
                                                                   ViAttr attributeId,
                                                                   const ViConstString cacheValue);

static ViStatus _VI_FUNC Chr6310AAttrIdQueryResponse_ReadCallback (ViSession vi,
                                                                   ViSession io, 
                                                                   ViConstString channelName, 
                                                                   ViAttr attributeId, 
                                                                   const ViConstString cacheValue);

static ViStatus _VI_FUNC Chr6310AAttrStandardEventStatusEnable_ReadCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrStandardEventStatusEnable_WriteCallback (ViSession vi,
                                                                              ViSession io,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrRecallInstrumentState_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrSaveInstrumentState_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrServiceRequestEnable_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrServiceRequestEnable_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrChannelLoad_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrChannelLoad_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrQueryMinChannelLoad_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrQueryMaxChannelLoad_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrChannelActive_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrChannelSynchronized_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrChannelSynchronized_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVon_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVon_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVon_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoltageRange_RangeTableCallback (ViSession vi,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               IviRangeTablePtr *rangeTablePtr);


static ViStatus _VI_FUNC Chr6310AAttrConfigureVoltageRange_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrGetConfigureVoltageRange_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoltageLatch_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoltageLatch_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureAutoLoad_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureAutoLoad_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureAutoMode_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureAutoMode_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureSound_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureSound_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViBoolean value);


static ViStatus _VI_FUNC Chr6310AAttrConfigureRemote_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureLoad_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureLoad_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingState_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingState_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTrigger_RangeTableCallback (ViSession vi,
                                                                                ViConstString channelName,
                                                                                ViAttr attributeId,
                                                                                IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTrigger_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTrigger_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTimeout_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTimeout_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffState_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffState_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffFinalV_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffFinalV_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffFinalV_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureMeasureAverage_ReadCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureMeasureAverage_WriteCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureDigitalIo_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureDigitalIo_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureKey_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureKey_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureEcho_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureEcho_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL1_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL1_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL1_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrMode_ReadCallback (ViSession vi,
                                                        ViSession io,
                                                        ViConstString channelName,
                                                        ViAttr attributeId,
                                                        ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrMode_WriteCallback (ViSession vi,
                                                         ViSession io,
                                                         ViConstString channelName,
                                                         ViAttr attributeId,
                                                         ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL2_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL2_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL2_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrCcStaticMinL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcStaticMaxL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcRiseSlewrateMax_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcRiseSlewrateMin_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcFallSlewrateMax_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcFallSlewrateMin_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticRiseSlew_RangeTableCallback (ViSession vi,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticRiseSlew_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticRiseSlew_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticFallSlew_RangeTableCallback (ViSession vi,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticFallSlew_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticFallSlew_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicMaxL1_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicMinL1_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL1_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL1_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL1_RangeTableCallback (ViSession vi,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL2_RangeTableCallback (ViSession vi,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL2_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL2_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicMaxL2_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicMinL2_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcStaticMinL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcStaticMaxL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicFallMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicFallMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicRiseMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicRiseMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicRiseSlew_RangeTableCallback (ViSession vi,
                                                                                ViConstString channelName,
                                                                                ViAttr attributeId,
                                                                                IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicRiseSlew_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicRiseSlew_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicFallSlew_RangeTableCallback (ViSession vi,
                                                                                ViConstString channelName,
                                                                                ViAttr attributeId,
                                                                                IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicFallSlew_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicFallSlew_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicT1Max_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicT1Min_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicT2Max_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicT2Min_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT1_RangeTableCallback (ViSession vi,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT1_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT1_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT2_RangeTableCallback (ViSession vi,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT2_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT2_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrLoadState_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrLoadState_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrLoadShortState_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrLoadShortState_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrLoadShortKey_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrLoadShortKey_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrMeasureInput_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrMeasureInput_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrMeasureScan_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrMeasureScan_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrProgramFile_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramFile_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrProgramActive_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramActive_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrProgramChain_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramChain_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTimeMax_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTimeMin_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTimeMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTimeMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTimeMax_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTimeMin_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTime_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTime_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTime_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTime_RangeTableCallback (ViSession vi,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTime_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTime_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTime_RangeTableCallback (ViSession vi,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTime_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTime_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSequence_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrSequence_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrSequenceMode_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrSequenceMode_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrSequenceShortChannel_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrSequenceShortChannel_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrSequenceShortTime_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSequenceShortTime_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrProgramRun_ReadCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrProgramRun_WriteCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViBoolean value);


static ViStatus _VI_FUNC Chr6310AAttrProgramKey_WriteCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrCrRiseSlewMin_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCrRiseSlewMax_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCrStaticMaxL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCrStaticMaxL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCrStaticMinL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCrStaticMinL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCrFallSlewMax_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCrFallSlewMin_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL1_RangeTableCallback (ViSession vi,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL1_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL1_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL2_RangeTableCallback (ViSession vi,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL2_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL2_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 value);


static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticRiseSlew_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticRiseSlew_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticRiseSlew_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticFallSlew_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticFallSlew_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticFallSlew_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value);


static ViStatus _VI_FUNC Chr6310AAttrShowDisplay_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationUnit_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationUnit_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageCenter_RangeTableCallback (ViSession vi,
                                                                                    ViConstString channelName,
                                                                                    ViAttr attributeId,
                                                                                    IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageCenter_ReadCallback (ViSession vi,
                                                                              ViSession io,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageCenter_WriteCallback (ViSession vi,
                                                                               ViSession io,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageHigh_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageHigh_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageHigh_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageLow_RangeTableCallback (ViSession vi,
                                                                                 ViConstString channelName,
                                                                                 ViAttr attributeId,
                                                                                 IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageLow_ReadCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageLow_WriteCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentCenter_RangeTableCallback (ViSession vi,
                                                                                    ViConstString channelName,
                                                                                    ViAttr attributeId,
                                                                                    IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentCenter_ReadCallback (ViSession vi,
                                                                              ViSession io,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentCenter_WriteCallback (ViSession vi,
                                                                               ViSession io,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentHigh_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentHigh_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentHigh_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentLow_RangeTableCallback (ViSession vi,
                                                                                 ViConstString channelName,
                                                                                 ViAttr attributeId,
                                                                                 IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentLow_ReadCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentLow_WriteCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationTest_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationTest_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrCvCurrentMax_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCvCurrentMin_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCvStaticMaxL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCvStaticMinL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCvStaticMaxL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCvStaticMinL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL1_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL1_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL1_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL2_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL2_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL2_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrVoltageCurrentLimit_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrVoltageCurrentLimit_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrVoltageCurrentLimit_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrVoltageResponseSpeed_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrVoltageResponseSpeed_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrCpFallSlewMax_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCpFallSlewMin_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCpRiseSlewMax_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCpRiseSlewMin_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCpStaticMaxL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCpStaticMaxL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCpStaticMinL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrCpStaticMinL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL1_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL1_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL2_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL2_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticFallSlew_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticFallSlew_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticFallSlew_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticRiseSlew_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticRiseSlew_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticRiseSlew_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value);


static ViStatus _VI_FUNC Chr6310AAttrOcpTest_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrOcpRange_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpRange_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrentMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrentMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrentMax_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrentMin_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltageMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltageMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrent_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrent_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrent_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrent_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrent_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrent_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrOcpStepCount_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpStepCount_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrOcpDwellTime_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpDwellTime_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltage_RangeTableCallback (ViSession vi,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltage_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltage_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLow_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLow_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLow_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHigh_RangeTableCallback (ViSession vi,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHigh_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHigh_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrOppEndPowerMax_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppEndPowerMin_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppStartPowerMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppStartPowerMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltageMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltageMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppTest_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrOppRange_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrOppRange_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrOppStartPower_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOppStartPower_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppStartPower_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrOppEndPower_RangeTableCallback (ViSession vi,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOppEndPower_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppEndPower_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrOppStepCount_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppStepCount_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrOppDwellTime_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppDwellTime_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltage_RangeTableCallback (ViSession vi,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltage_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltage_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLow_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLow_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLow_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHigh_RangeTableCallback (ViSession vi,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHigh_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHigh_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrLedVoltageOut_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrLedVoltageOut_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrLedVoltageOut_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrLedCurrentOut_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrLedCurrentOut_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrLedRdCoefficient_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrLedRdCoefficient_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrLedRdOhm_RangeTableCallback (ViSession vi,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrLedRdOhm_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrLedRdOhm_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrLedVf_RangeTableCallback (ViSession vi,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrLedVf_ReadCallback (ViSession vi,
                                                         ViSession io,
                                                         ViConstString channelName,
                                                         ViAttr attributeId,
                                                         ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrLedVf_WriteCallback (ViSession vi,
                                                          ViSession io,
                                                          ViConstString channelName,
                                                          ViAttr attributeId,
                                                          ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureCurrentRange_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureCurrentRange_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRdSelect_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRdSelect_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRr_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRr_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRrSelect_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRrSelect_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRrSet_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRrSet_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureShort_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureShort_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureResponseSelect_ReadCallback (ViSession vi,
                                                                              ViSession io,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureResponseSelect_WriteCallback (ViSession vi,
                                                                               ViSession io,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               ViBoolean value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureResponseSet_ReadCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViInt32 *value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureResponseSet_WriteCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViInt32 value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureSetAllLed_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureSetAllLed_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViBoolean value);


static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerHigh_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerHigh_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerCenter_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerCenter_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerCenter_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerLow_RangeTableCallback (ViSession vi,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerLow_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerLow_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 value);

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerHigh_RangeTableCallback (ViSession vi,
                                                                                ViConstString channelName,
                                                                                ViAttr attributeId,
                                                                                IviRangeTablePtr *rangeTablePtr);

static ViStatus _VI_FUNC Chr6310AAttrConfigureLedlcrl_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViBoolean *value);

static ViStatus _VI_FUNC Chr6310AAttrConfigureLedlcrl_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean value);








/***************************/
ViInt32 g_nBaudRate; 
ViInt32 g_nParity;  
/*****************************************************************************
 *------------ User-Callable Functions (Exportable Functions) ---------------*
 *****************************************************************************/

/*****************************************************************************
 * Function: Chr6310A_init   
 * Purpose:  VXIplug&play required function.  Calls the   
 *           Chr6310A_InitWithOptions function.   
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_init (ViRsrc resourceName, ViBoolean IDQuery,
                                 ViBoolean resetDevice, ViSession *newVi)
{   
    ViStatus    error = VI_SUCCESS;
   
    if (newVi == VI_NULL)
        {
        Ivi_SetErrorInfo (VI_NULL, VI_FALSE, IVI_ERROR_INVALID_PARAMETER, 
                          VI_ERROR_PARAMETER4, "Null address for Instrument Handle");
        checkErr( IVI_ERROR_INVALID_PARAMETER);
        }

    checkErr( Chr6310A_InitWithOptions (resourceName, IDQuery, resetDevice, 
                                        "", newVi));

Error:  
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_InitInterface   
 * Purpose:  Chr6310A_Initialize the GPIB or Serial interface.       
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_InitInterface (ViRsrc resourceName, ViBoolean IDQuery,
                                          ViBoolean resetDevice,
                                          ViSession *newVi,
                                          ViInt32 baudRate, ViChar IDString[],
                                          ViString optionString)
{
	ViStatus	error = VI_SUCCESS;
	ViChar      rdBuffer[BUFFER_SIZE];
    ViSession   vi= VI_NULL; 
    
    g_nBaudRate=baudRate;

    if (newVi == VI_NULL)
        {
        Ivi_SetErrorInfo (VI_NULL, VI_FALSE, IVI_ERROR_INVALID_PARAMETER, 
                          VI_ERROR_PARAMETER4, "Null address for Instrument Handle");
        checkErr( IVI_ERROR_INVALID_PARAMETER);
        }

    checkErr( Chr6310A_InitWithOptions (resourceName, IDQuery, resetDevice, 
                                        optionString, newVi));
                                        
    checkErr( Ivi_GetAttributeViString (*newVi, VI_NULL, CHR6310A_ATTR_ID_QUERY_RESPONSE, 
	        	                                0, BUFFER_SIZE, IDString));

Error:
	return error;
}

/*****************************************************************************
 *------------------------------ 建立空殼子 ---------------------------------*
 *****************************************************************************/
void _VI_FUNC CreateIviIniIfNeeded(void) 
{
	ViStatus    error = VI_SUCCESS;
	FILE *fIviIni = NULL;
	int iTmp=0;
	char sTmp[IVI_MAX_PATHNAME_LEN]={0};

	//checkErr( Ivi_GetIviIniDir(sTmp));  // if IVI engine is greater than 1.83, user can remark this function to prevent CVI debug run-time error, Bill
	if (SetDir (sTmp)!=0)
		MakeDir(sTmp);
	if(sTmp[strlen(sTmp)-1] == '\\') sTmp[strlen(sTmp)-1]=0;
	strcat(sTmp, "\\ivi.ini");
//	MessagePopup("Before checking", sTmp);
	if(GetFileDate(sTmp, &iTmp, &iTmp, &iTmp) != -1)
		return;
	fIviIni = fopen (sTmp, "a");
//	if(fIviIni==NULL)
//		MessagePopup("File Open Error", sTmp);
	if(fIviIni != NULL)
		fclose(fIviIni);
Error:
	error = error;
}	

/*****************************************************************************
 * Function: Chr6310A_InitWithOptions                                       
 * Purpose:  This function creates a new IVI session and calls the 
 *           IviInit function.                                     
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_InitWithOptions (ViRsrc resourceName, ViBoolean IDQuery,
                                            ViBoolean resetDevice, ViString optionString, 
                                            ViSession *newVi)
{   
    ViStatus    error = VI_SUCCESS;
    ViSession   vi = VI_NULL;
    ViChar      newResourceName[IVI_MAX_MESSAGE_BUF_SIZE];
    ViChar      newOptionString[IVI_MAX_MESSAGE_BUF_SIZE];
    ViBoolean   isLogicalName;
    
    if (newVi == VI_NULL)
        {
        Ivi_SetErrorInfo (VI_NULL, VI_FALSE, IVI_ERROR_INVALID_PARAMETER, 
                          VI_ERROR_PARAMETER5, "Null address for Instrument Handle");
        checkErr( IVI_ERROR_INVALID_PARAMETER);
        }

    *newVi = VI_NULL;
    
   //CreateIviIniIfNeeded(); // Added by bill. Note: Not work in simulation mode (by Lawrence)
    
    checkErr( Ivi_GetInfoFromResourceName (resourceName, optionString, newResourceName,
                                           newOptionString, &isLogicalName));
    
        /* create a new interchangeable driver */
    checkErr( Ivi_SpecificDriverNew ("Chr6310A", newOptionString, &vi));  
    
        /* init the driver */
    checkErr( Chr6310A_IviInit (newResourceName, IDQuery, resetDevice, vi)); 
    if (isLogicalName)
        checkErr( Ivi_ApplyDefaultSetup (vi));
    *newVi = vi;
    
Error:
    if (error < VI_SUCCESS) 
        Ivi_Dispose (vi);
        
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_IviInit                                                       
 * Purpose:  This function is called by Chr6310A_InitWithOptions  
 *           or by an IVI class driver.  This function initializes the I/O 
 *           interface, optionally resets the device, optionally performs an
 *           ID query, and sends a default setup to the instrument.                
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_IviInit (ViRsrc resourceName, ViBoolean IDQuery,
                                    ViBoolean reset, ViSession vi)
{
    ViStatus    error = VI_SUCCESS;
    ViSession   io = VI_NULL;
    ViChar		Exist_CHAN[BUFFER_SIZE]={0},seps[BUFFER_SIZE/2]= ",\r",sChannelName[BUFFER_SIZE];
    ViString    Model,Channel,Token;
    ViInt32     i=0,j=0,k=0,l=0,m=0,nCount=0,length;
    
    for(k=1;k<=10;k++)
	{
		if (k==10)
			nCount+= sprintf(sChannelName+nCount,"%d",k);
		else
			nCount+= sprintf(sChannelName+nCount,"%d,",k); 
		
	}
    checkErr( Ivi_BuildChannelTable (vi, sChannelName, VI_FALSE, VI_NULL));
    
     /* Add attributes */
    checkErr( Chr6310A_InitAttributes (vi));

    if (!Ivi_Simulating(vi))
        {
        ViSession   rmSession = VI_NULL;

        /* Open instrument session */
        checkErr( Ivi_GetAttributeViSession (vi, VI_NULL, IVI_ATTR_VISA_RM_SESSION, 0,
                                             &rmSession)); 
        viCheckErr( viOpen (rmSession, resourceName, VI_NULL, VI_NULL, &io));
        /* io session owned by driver now */
        checkErr( Ivi_SetAttributeViSession (vi, VI_NULL, IVI_ATTR_IO_SESSION, 0, io));  

		/* Configure VISA Formatted I/O */
		if (!strncmp(resourceName,"ASRL",4))
		{
		viCheckErr( viFlush (io, VI_ASRL_IN_BUF|VI_ASRL_OUT_BUF|VI_WRITE_BUF_DISCARD|VI_READ_BUF_DISCARD));

		viCheckErr( viSetBuf (io, VI_ASRL_IN_BUF | VI_ASRL_OUT_BUF, 512));
		
		viCheckErr( viSetAttribute (io, VI_ATTR_TERMCHAR, 0x0A));
		viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_END_IN, VI_ASRL_END_TERMCHAR)); // VI_ASRL_END_NONE or  VI_ASRL_END_TERMCHAR
		viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_END_OUT, VI_ASRL_END_TERMCHAR)); // VI_ASRL_END_NONE or  VI_ASRL_END_TERMCHAR
		viCheckErr( viSetAttribute (io, VI_ATTR_TERMCHAR_EN, VI_TRUE));			

		viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_FLOW_CNTRL, VI_ASRL_FLOW_NONE));
		
		viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_BAUD, g_nBaudRate));   
		viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_DATA_BITS, 8));
		viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_PARITY,0));
		viCheckErr( viSetAttribute (io, VI_ATTR_ASRL_STOP_BITS,10));
		
		}
		
		/* Configure VISA Formatted I/O ==> GPIB */       		
		viCheckErr( viSetAttribute (io, VI_ATTR_TMO_VALUE, 5000 ));
		viCheckErr( viSetBuf (io, VI_READ_BUF | VI_WRITE_BUF, 10000));
		viCheckErr( viSetAttribute (io, VI_ATTR_WR_BUF_OPER_MODE, VI_FLUSH_ON_ACCESS));
		viCheckErr( viSetAttribute (io, VI_ATTR_RD_BUF_OPER_MODE, VI_FLUSH_ON_ACCESS));
		
    /*- Reset instrument ----------------------------------------------------*/
    if (reset) 
        checkErr( Chr6310A_reset (vi));
    else  /*- Send Default Instrument Setup ---------------------------------*/
        checkErr( Chr6310A_DefaultInstrSetup (vi));
	
	/*- Identification Query ------------------------------------------------*/
	if (IDQuery)                               
	    {
	    ViChar rdBuffer[BUFFER_SIZE];
	
	    #define VALID_RESPONSE_STRING_START        "Chroma,631"
	
	    checkErr( Ivi_GetAttributeViString (vi, VI_NULL, CHR6310A_ATTR_ID_QUERY_RESPONSE, 
	                                        0, BUFFER_SIZE, rdBuffer));
	    
	    if (strncmp (rdBuffer, VALID_RESPONSE_STRING_START, 
	                 strlen(VALID_RESPONSE_STRING_START)) != 0)
	        {             
	        viCheckErr( VI_ERROR_FAIL_ID_QUERY);
	        }
	    }

        /***  Query All Module State  ***/
 	    viCheckErr( viPrintf (io, "*RDT?\n"));
	    viCheckErr( viScanf (io, "%s",Exist_CHAN));
		Channel = strtok( Exist_CHAN, seps );
		while( Channel != NULL )		 
		{
		 	 length=strlen(Channel);
		 	 Token = Channel;
	 	 
		 	 if (!strcmp(Token,"0"))
		 	     m=0;
		 	 else if (!strcmp(Token,"63101A"))
		 	     m=1;
		 	 else if (!strcmp(Token,"63102A"))
		 	     m=2;
		 	 else if (!strcmp(Token,"63103A"))
		 	     m=3;
		 	 else if (!strcmp(Token,"63105A"))
		 	     m=4;
		 	 else if (!strcmp(Token,"63106A"))
		 	     m=5;
		 	 else if (!strcmp(Token,"63107AL"))
		 	     m=6;
		 	 else if (!strcmp(Token,"63107AR"))
		 	     m=7;
		 	 else if (!strcmp(Token,"63108A"))
		 	     m=8;
		 	 else if (!strcmp(Token,"63110A"))//LED Module
		 	     m=9;
		 	     
		 	 else if (!strcmp(Token,"63112A"))
		 	     m=10;
		 	 else if (!strcmp(Token,"63123A"))
		 	     m=11;
		 	 else if (!strcmp(Token,"63113A"))//LED Module
		 	     m=12;
		 	     
		 	 else
		 	 checkErr ( error = CHR6310A_ERROR_INVALID_MODEL);   
		 	 
		 	 viCheckErr(Ivi_GetViInt32EntryFromValue (m, &attrModelNameRangeTable , VI_NULL, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL)); 
             viCheckErr(Ivi_SetRangeTableEntry (&attrChannelLoadRangeTable, i, VI_NULL, VI_NULL, VI_NULL, Model, VI_NULL));
             
                if (*Token != '0'){
                	viCheckErr(Ivi_SetRangeTableEnd (&attrChannelListRangeTable, 8));//6314A frame max = 8 channel , 6312A frame max = 4 channel 
                    
                    viCheckErr(Ivi_SetRangeTableEntry (&attrChannelListRangeTable, j, i+1,VI_NULL, VI_NULL, VI_NULL, VI_NULL));
                    j++;
                        }
                        
			 Channel = strtok( NULL, seps );  
	 		 i++;
             memset(Token,0,length);
		}	    
		
         viCheckErr(Ivi_SetRangeTableEnd (&attrChannelListRangeTable, j));   
		
        }

//    checkErr(Ivi_SetNeedToCheckStatus(vi,VI_TRUE));   
    checkErr( Chr6310A_CheckStatus (vi));

Error:
    if (error < VI_SUCCESS)
        {
        if (!Ivi_Simulating (vi) && io)
            viClose (io);
        }
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_close                                                           
 * Purpose:  This function closes the instrument.                            
 *
 *           Note:  This function must unlock the session before calling
 *           Ivi_Dispose.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_close (ViSession vi)
{
    ViStatus    error = VI_SUCCESS;
    
    checkErr( Ivi_LockSession (vi, VI_NULL));
    
    checkErr( Chr6310A_IviClose (vi));

Error:    
    Ivi_UnlockSession (vi, VI_NULL);
    Ivi_Dispose (vi);  

    return error;
}

/*****************************************************************************
 * Function: Chr6310A_IviClose                                                        
 * Purpose:  This function performs all of the drivers clean-up operations   
 *           except for closing the IVI session.  This function is called by 
 *           Chr6310A_close or by an IVI class driver. 
 *
 *           Note:  This function must close the I/O session and set 
 *           IVI_ATTR_IO_SESSION to 0.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_IviClose (ViSession vi)
{
    ViStatus error = VI_SUCCESS;
    ViSession io = VI_NULL;

        /* Do not lock here.  The caller manages the lock. */

    checkErr( Ivi_GetAttributeViSession (vi, VI_NULL, IVI_ATTR_IO_SESSION, 0, &io));

Error:
    Ivi_SetAttributeViSession (vi, VI_NULL, IVI_ATTR_IO_SESSION, 0, VI_NULL);
    if(io)                                                      
        {
        viClose (io);
        }
    return error;   
}

/*****************************************************************************
 * Function: Chr6310A_Set_LED_Voltage_Out                                                         
 * Purpose:  It sets the output voltage of LED driver.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_LED_Voltage_Out (ViSession vi,
                                                ViReal64 LEDVoltageOut)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_VOLTAGE_OUT, 
												IVI_VAL_DIRECT_USER_CALL, LEDVoltageOut), 2, "LEDVoltageOut" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_LED_Current_Out                                                         
 * Purpose:  It sets the output current of LED driver.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_LED_Current_Out (ViSession vi,
                                                ViReal64 LEDCurrentOut)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_CURRENT_OUT, 
												IVI_VAL_DIRECT_USER_CALL, LEDCurrentOut), 2, "LEDCurrentOut" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_LED_Rd_Coefficient                                                         
 * Purpose:   It sets the LED operating point impedance.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_LED_Rd_Coefficient (ViSession vi,
                                                   ViReal64 LEDRdCoefficient)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_RD_COEFFICIENT, 
												IVI_VAL_DIRECT_USER_CALL, LEDRdCoefficient), 2, "LEDRdCoefficient" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_LED_Rd_Ohm                                                         
 * Purpose:  It sets the Ohm of operating point impedance Rd.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_LED_Rd_Ohm (ViSession vi,
                                           ViReal64 LEDRdOhm)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_RD_OHM, 
												IVI_VAL_DIRECT_USER_CALL, LEDRdOhm), 2, "LEDRdOhm" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_LED_VF                                                         
 * Purpose:  It sets the forward bias of LED.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_LED_VF (ViSession vi,
                                       ViReal64 LEDVF)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_VF, 
												IVI_VAL_DIRECT_USER_CALL, LEDVF), 2, "LEDVF" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Current_Range                                                         
 * Purpose:  It sets the current range of CR mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Current_Range (ViSession vi,
                                                        ViBoolean configureCurrentRange)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
    if ((!strcmp(Model,"63110A")))
	{
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_CURRENT_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, configureCurrentRange), 2, "configureCurrentRange" );
	} 
	
	else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}

	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Rd_Select                                                         
 * Purpose:  It selects the parameters to be set for LED Mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Rd_Select (ViSession vi,
                                                    ViInt32 configureRdSelect)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RD_SELECT, 
												IVI_VAL_DIRECT_USER_CALL, configureRdSelect), 2, "configureRdSelect" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Rr                                                         
 * Purpose:  It sets the Rr function to on or off.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Rr (ViSession vi,
                                             ViBoolean configureRr)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
     if ((!strcmp(Model,"63110A")))
	{
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RR, 
												IVI_VAL_DIRECT_USER_CALL, configureRr), 2, "configureRr" );
	}
	else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Rr_Select                                                         
 * Purpose:  It sets the Rr to default or user-defined.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Rr_Select (ViSession vi,
                                                    ViBoolean configureRrSelect)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN); 
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
    if ((!strcmp(Model,"63110A")))
    {
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RR_SELECT, 
												IVI_VAL_DIRECT_USER_CALL, configureRrSelect), 2, "configureRrSelect" );
	}
	else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}

	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Rr_Set                                                         
 * Purpose:  It sets the ripple resistance Rr to default or user-defined.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Rr_Set (ViSession vi,
                                                 ViReal64 configureRrSet)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN); 
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
    if ((!strcmp(Model,"63110A")))
    {
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RR_SET, 
												IVI_VAL_DIRECT_USER_CALL, configureRrSet), 2, "configureRrSet" );
	}
	else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Short                                                         
 * Purpose:  It sets if enable Short function when pressing the SHORT key on
 *           Module.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Short (ViSession vi,
                                                ViBoolean configureShort)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
    if ((!strcmp(Model,"63110A")))
    {
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_SHORT, 
												IVI_VAL_DIRECT_USER_CALL, configureShort), 2, "configureShort" );
	}
	else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Response_Select                                                         
 * Purpose:  It sets the response speed of Electronic Load to default or
 *           user-defined.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Response_Select (ViSession vi,
                                                          ViBoolean configureResponseSelect)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SELECT, 
												IVI_VAL_DIRECT_USER_CALL, configureResponseSelect), 2, "configureResponseSelect" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Response_Set                                                         
 * Purpose:  It sets the response speed of Electronic Load.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Response_Set (ViSession vi,
                                                       ViInt32 configureResponseSet)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SET, 
												IVI_VAL_DIRECT_USER_CALL, configureResponseSet), 2, "configureResponseSet" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Set_All_LED                                                         
 * Purpose:  It sets the LED mode setting for one single channel or all
 *           channels.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Set_All_LED (ViSession vi,
                                                      ViBoolean configureSetAllLED)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_SET_ALL_LED, 
												IVI_VAL_DIRECT_USER_CALL, configureSetAllLED), 2, "configureSetAllLED" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_LED_Voltage_Out                                                         
 * Purpose:  It returns the set Vo value.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_LED_Voltage_Out (ViSession vi,
                                                ViReal64 *getLEDVoltageOut)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getLEDVoltageOut == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get LED Voltage Out");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_VOLTAGE_OUT, 
												IVI_VAL_DIRECT_USER_CALL, getLEDVoltageOut));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_LED_Current_Out                                                         
 * Purpose:  It returns the set Io value.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_LED_Current_Out (ViSession vi,
                                                ViReal64 *getLEDCurrentOut)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getLEDCurrentOut == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get LED Current Out");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_CURRENT_OUT, 
												IVI_VAL_DIRECT_USER_CALL, getLEDCurrentOut));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_LED_Rd_Coefficient                                                         
 * Purpose:  It returns the set Coeff. value.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_LED_Rd_Coefficient (ViSession vi,
                                                   ViReal64 *getLEDRdCoefficient)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getLEDRdCoefficient == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get LED Rd Coefficient");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_RD_COEFFICIENT, 
												IVI_VAL_DIRECT_USER_CALL, getLEDRdCoefficient));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_LED_Rd_Ohm                                                         
 * Purpose:  It returns the set Rd Ohm.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_LED_Rd_Ohm (ViSession vi,
                                           ViReal64 *getLEDRdOhm)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getLEDRdOhm == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get LED Rd Ohm");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_RD_OHM, 
												IVI_VAL_DIRECT_USER_CALL, getLEDRdOhm));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_LED_VF                                                         
 * Purpose:  It returns the set Vf value.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_LED_VF (ViSession vi,
                                       ViReal64 *getLEDVF)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getLEDVF == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get LED VF");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_VF, 
												IVI_VAL_DIRECT_USER_CALL, getLEDVF));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_LED_VF                                                         
 * Purpose:  It returns the current range set for CR mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Current_Range (ViSession vi,
                                                        ViBoolean *getConfigureCurrentRange)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	ViString    Model="";
	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureCurrentRange == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Current Range");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
    if ((!strcmp(Model,"63110A")))
    {
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_CURRENT_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureCurrentRange));
	}
	 else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Rd_Select                                                         
 * Purpose:  It returns the Rd select.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Rd_Select (ViSession vi,
                                                    ViInt32 *getConfigureRdSelect)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureRdSelect == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Rd Select");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RD_SELECT, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureRdSelect));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Rr                                                         
 * Purpose:  It returns if the Rr function is on.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Rr (ViSession vi,
                                             ViBoolean *getConfigureRr)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureRr == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Rr");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
    if ((!strcmp(Model,"63110A")))
    {
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RR, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureRr));
	}
	else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Rr                                                         
 * Purpose:  It returns the Rr select.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Rr_Select (ViSession vi,
                                                    ViBoolean *getConfigureRrSelect)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureRrSelect == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Rr Select");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
    if ((!strcmp(Model,"63110A")))
    {
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RR_SELECT, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureRrSelect));
	}
	else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}
	
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Rr_Set                                                         
 * Purpose:  It returns the Rr set value.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Rr_Set (ViSession vi,
                                                 ViReal64 *getConfigureRrSet)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureRrSet == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Rr Set");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
    if ((!strcmp(Model,"63110A")))
    {
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RR_SET, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureRrSet));
	}
	else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}
	
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Short                                                         
 * Purpose:  It returns if the SHORT key is enabled.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Short (ViSession vi,
                                                ViBoolean *getConfigureShort)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureShort == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Short");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN); 
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
    if ((!strcmp(Model,"63110A")))
    {
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_SHORT, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureShort));
	}
	else
	{
	checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	}
	
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Response_Select                                                         
 * Purpose:  It returns the response speed the Electronic Load uses.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Response_Select (ViSession vi,
                                                          ViBoolean *getConfigureResponseSelect)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureResponseSelect == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Response Select");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SELECT, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureResponseSelect));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Response_Set                                                         
 * Purpose:  It returns the response speed of Electronic Load.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Response_Set (ViSession vi,
                                                       ViInt32 *getConfigureResponseSet)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureResponseSet == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Response Ser");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SET, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureResponseSet));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Set_All_LED                                                         
 * Purpose:  It returns the all channel set.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Set_All_LED (ViSession vi,
                                                      ViBoolean *getConfigureSet_All_LED)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureSet_All_LED == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Set All LED");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LED_CONFIGURE_SET_ALL_LED, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureSet_All_LED));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Program_Parameters                                                         
 * Purpose:  It sets the program parameters.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Program_Parameters (ViSession vi,
                                                   ViInt32 programFile,
                                                   ViInt32 programActive,
                                                   ViInt32 programChain,
                                                   ViReal64 programOnTime,
                                                   ViReal64 programOffTime,
                                                   ViReal64 programPFDelayTime)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_FILE, 
												IVI_VAL_DIRECT_USER_CALL, programFile), 2, "programFile" );
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_ACTIVE, 
												IVI_VAL_DIRECT_USER_CALL, programActive), 3, "programActive" );
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_CHAIN, 
												IVI_VAL_DIRECT_USER_CALL, programChain), 4, "programChain" );

    
	viCheckParm( Ivi_SetAttributeViReal64( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_ON_TIME, 
												IVI_VAL_DIRECT_USER_CALL, programOnTime), 5, "programOnTime" );
	viCheckParm( Ivi_SetAttributeViReal64( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_OFF_TIME, 
												IVI_VAL_DIRECT_USER_CALL, programOffTime), 6, "programOffTime" );
	viCheckParm( Ivi_SetAttributeViReal64( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME, 
												IVI_VAL_DIRECT_USER_CALL, programPFDelayTime), 7, "programPFDelayTime" );
												
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Sequence_Parameters                                                         
 * Purpose:  It sets the sequence parameters.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Sequence_Parameters (ViSession vi,
                                                    ViInt32 programSequence,
                                                    ViInt32 programSequenceMode,
                                                    ViInt32 programSequenceShortChannel,
                                                    ViReal64 programSequenceShortTime)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_SEQUENCE, 
												IVI_VAL_DIRECT_USER_CALL, programSequence), 2, "programSequence" );
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_SEQUENCE_MODE, 
												IVI_VAL_DIRECT_USER_CALL, programSequenceMode), 3, "programSequenceMode" );
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_SEQUENCE_SHORT_CHANNEL, 
												IVI_VAL_DIRECT_USER_CALL, programSequenceShortChannel), 4, "programSequenceShortChannel" );

    
	viCheckParm( Ivi_SetAttributeViReal64( vi, VI_NULL, CHR6310A_ATTR_SEQUENCE_SHORT_TIME, 
												IVI_VAL_DIRECT_USER_CALL, programSequenceShortTime), 5, "programSequenceShortTime" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Program_Save                                                         
 * Purpose:  It saves the setting of program.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Program_Save (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "PROG:SAV\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Program_Run                                                         
 * Purpose:  It executes the program.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Program_Run (ViSession vi,
                                            ViBoolean programRun)
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean   Pro_Run;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_RUN, 
												IVI_VAL_DIRECT_USER_CALL, programRun), 2, "programRun" );
    
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Program_Key                                                         
 * Purpose:  It echoes the manual key code.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Program_Key (ViSession vi,
                                            ViInt32 programKey)
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean   Pro_Run;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_KEY, 
												IVI_VAL_DIRECT_USER_CALL, programKey), 2, "programKey" );
    
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Program_Parameters                                                         
 * Purpose:  It returns the program parameters.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Program_Parameters (ViSession vi,
                                                   ViInt32 *programFile,
                                                   ViInt32 *programActive,
                                                   ViInt32 *programChain,
                                                   ViReal64 *programOnTime,
                                                   ViReal64 *programOffTime,
                                                   ViReal64 *programPFDelayTime)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( programFile == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Program File");
    	
	if ( programActive == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Program Active");
    	
	if ( programChain == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 4, "Null address for Program Chain");
    	
	if ( programOnTime == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 5, "Null address for Program On Time");
    	
	if ( programOffTime == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 6, "Null address for Program Off Time");
    	
	if ( programPFDelayTime == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 7, "Null address for Program Pass/Fail Delay Time");
    	
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_FILE, 
												IVI_VAL_DIRECT_USER_CALL, programFile));
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_ACTIVE, 
												IVI_VAL_DIRECT_USER_CALL, programActive));
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_CHAIN, 
												IVI_VAL_DIRECT_USER_CALL, programChain));

    
	viCheckErr( Ivi_GetAttributeViReal64( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_ON_TIME, 
												IVI_VAL_DIRECT_USER_CALL, programOnTime));
	viCheckErr( Ivi_GetAttributeViReal64( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_OFF_TIME, 
												IVI_VAL_DIRECT_USER_CALL, programOffTime));
	viCheckErr( Ivi_GetAttributeViReal64( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME, 
												IVI_VAL_DIRECT_USER_CALL, programPFDelayTime));
												
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Sequence_Parameters                                                         
 * Purpose:  It returns the sequence parameters.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Sequence_Parameters (ViSession vi,
                                                    ViInt32 *programSequence,
                                                    ViChar programSequenceMode[],
                                                    ViInt32 *programSequenceShortChannel,
                                                    ViReal64 *programSequenceShortTime)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     Seq_Mode;
	ViString    cmd;  	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( programSequence == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Program Sequence");
    	
	if ( programSequenceMode == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Program Sequence Mode");
    	
	if ( programSequenceShortChannel == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 4, "Null address for Program Sequence Short Channel");
    	
	if ( programSequenceShortTime == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 5, "Null address for Program Sequence Short Time");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_SEQUENCE, 
												IVI_VAL_DIRECT_USER_CALL, programSequence));
												
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_SEQUENCE_MODE, 
												IVI_VAL_DIRECT_USER_CALL, &Seq_Mode));
	viCheckErr( Ivi_GetViInt32EntryFromIndex (Seq_Mode, &attrSequenceModeRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &cmd, VI_NULL));											
	Fmt	(programSequenceMode, "%s", cmd);
												
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_SEQUENCE_SHORT_CHANNEL, 
												IVI_VAL_DIRECT_USER_CALL, programSequenceShortChannel));
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, VI_NULL, CHR6310A_ATTR_SEQUENCE_SHORT_TIME, 
												IVI_VAL_DIRECT_USER_CALL, programSequenceShortTime));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Program_Run                                                         
 * Purpose:  It returns the program run state.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Program_Run (ViSession vi,
                                            ViBoolean *getProgramRun)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getProgramRun == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Program Run");
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_PROGRAM_RUN, 
												IVI_VAL_DIRECT_USER_CALL, getProgramRun));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Specification_Unit                                                         
 * Purpose:  It sets the specific entry mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Specification_Unit (ViSession vi,
                                                   ViBoolean specificationUnit)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];  
	ViInt32		ID_NUMS=6,ID_INDEX=0;	
	ViAttr		ID[6]={CHR6310A_ATTR_SPECIFICATION_VOLTAGE_HIGH, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_CENTER, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_LOW,
	                   CHR6310A_ATTR_SPECIFICATION_CURRENT_HIGH, CHR6310A_ATTR_SPECIFICATION_CURRENT_CENTER, CHR6310A_ATTR_SPECIFICATION_CURRENT_LOW
						};
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
   	for (ID_INDEX=0;ID_INDEX<ID_NUMS;ID_INDEX++)
    viCheckErr(Ivi_InvalidateAttribute (vi, Multi_Chan, ID[ID_INDEX]));

    viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_SPECIFICATION_UNIT, 
												IVI_VAL_DIRECT_USER_CALL, specificationUnit), 2, "specificationUnit" );
            
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Specification_Voltage                                                         
 * Purpose:  It sets the voltage specification for CC,CR,CP Mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Specification_Voltage (ViSession vi,
                                                      ViReal64 specifcationVoltageHigh,
                                                      ViReal64 specifcationVoltageCenter,
                                                      ViReal64 specifcationVoltageLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
    if (( specifcationVoltageHigh >= specifcationVoltageCenter ) && ( specifcationVoltageCenter >= specifcationVoltageLow )){ 
    
     viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, specifcationVoltageHigh), 2, "specifcationVoltageHigh" );
												
     viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_CENTER, 
												IVI_VAL_DIRECT_USER_CALL, specifcationVoltageCenter), 3, "specifcationVoltageCenter" );
												
     viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_LOW, 
												IVI_VAL_DIRECT_USER_CALL, specifcationVoltageLow), 4, "specifcationVoltageLow" );}
												
     else
     	 checkErr( error = CHR6310A_ERROR_SPEC_VALUE );  
     
	 checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Specification_Current                                                         
 * Purpose:  It sets the current specification for CV Mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Specification_Current (ViSession vi,
                                                      ViReal64 specifcationCurrentHigh,
                                                      ViReal64 specifcationCurrentCenter,
                                                      ViReal64 specifcationCurrentLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
    if (( specifcationCurrentHigh >= specifcationCurrentCenter ) && ( specifcationCurrentCenter >= specifcationCurrentLow )){ 
    
     viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_CURRENT_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, specifcationCurrentHigh), 2, "specifcationCurrentHigh" );
												
     viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_CURRENT_CENTER, 
												IVI_VAL_DIRECT_USER_CALL, specifcationCurrentCenter), 3, "specifcationCurrentCenter" );
												
     viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_CURRENT_LOW, 
												IVI_VAL_DIRECT_USER_CALL, specifcationCurrentLow), 4, "specifcationCurrentLow" );}
												
     else
     	 checkErr( error = CHR6310A_ERROR_SPEC_VALUE );  
												
	 checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Specification_Power                                                         
 * Purpose:  It sets the power specification for LED Mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Specification_Power (ViSession vi,
                                                    ViReal64 specifcationPowerHigh,
                                                    ViReal64 specifcationPowerCenter,
                                                    ViReal64 specifcationPowerLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
    if (( specifcationPowerHigh >= specifcationPowerCenter ) && ( specifcationPowerCenter >= specifcationPowerLow )){ 
    
     viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_POWER_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, specifcationPowerHigh), 2, "specifcationPowerHigh" );
												
     viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_POWER_CENTER, 
												IVI_VAL_DIRECT_USER_CALL, specifcationPowerCenter), 3, "specifcationPowerCenter" );
												
     viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_POWER_LOW, 
												IVI_VAL_DIRECT_USER_CALL, specifcationPowerLow), 4, "specifcationPowerLow" );}
												
     else
     	 checkErr( error = CHR6310A_ERROR_SPEC_VALUE );  
												
	 checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Specification_Test                                                         
 * Purpose:  It starts or closes the specification test.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Specification_Test (ViSession vi,
                                                   ViBoolean specificationTest)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];  
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
    viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_TEST, 
												IVI_VAL_DIRECT_USER_CALL, specificationTest), 2, "specificationTest" );
            
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Specification_Unit                                                         
 * Purpose:  It returns the specific entry mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Specification_Unit (ViSession vi,
                                                   ViBoolean *getSpecificationUnit)
{
	ViStatus	error = VI_SUCCESS;

    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getSpecificationUnit == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Specification Unit");
    
    viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_SPECIFICATION_UNIT, 
												IVI_VAL_DIRECT_USER_CALL, getSpecificationUnit));
            
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Spec_Volt_Result                                                         
 * Purpose:  It requests the GO-NG result refer to the voltage specification.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Spec_Volt_Result (ViSession vi,
                                                 ViChar getSpec_VoltResult[])
{
	ViStatus	error = VI_SUCCESS;
	ViChar      rdBuffer[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	if ( getSpec_VoltResult == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Specification Voltage Result");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "SPEC:VOLT?\n"));
		
    	viCheckErr( viScanf (io, "%s",rdBuffer));  
	
    if (!strcmp(rdBuffer,"0"))
        strcpy(getSpec_VoltResult,"NG");
        
       else if (!strcmp(rdBuffer,"1")) 
				strcpy(getSpec_VoltResult,"GO"); 
				
	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Spec_Curr_Result                                                         
 * Purpose:  It requests the GO-NG result refer to the current specification.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Spec_Curr_Result (ViSession vi,
                                                 ViChar getSpec_CurrResult[])
{
	ViStatus	error = VI_SUCCESS;
	ViChar      rdBuffer[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	if ( getSpec_CurrResult == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Specification Current Result");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "SPEC:CURR?\n"));
		
    	viCheckErr( viScanf (io, "%s",rdBuffer));  
	
    if (!strcmp(rdBuffer,"0"))
        strcpy(getSpec_CurrResult,"NG");
        
       else if (!strcmp(rdBuffer,"1")) 
				strcpy(getSpec_CurrResult,"GO"); 
				
	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Spec_All_Channel_Result                                                         
 * Purpose:  It requests GO-NG result reference to all channel specification.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Spec_All_Channel_Result (ViSession vi,
                                                        ViChar getSpec_All_ChannelResult[])
{
	ViStatus	error = VI_SUCCESS;
	ViChar      rdBuffer[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	if ( getSpec_All_ChannelResult == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Specification All Channel Result");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "SPEC?\n"));
		
    	viCheckErr( viScanf (io, "%s",rdBuffer));  
	
    if (!strcmp(rdBuffer,"0"))
        strcpy(getSpec_All_ChannelResult,"NG");
        
       else if (!strcmp(rdBuffer,"1")) 
				strcpy(getSpec_All_ChannelResult,"GO"); 
				
	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Specification_Voltage                                                         
 * Purpose:  It returns the voltage specification for CC,CR,CP Mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Specification_Voltage (ViSession vi,
                                                      ViReal64 *specifcationVoltageHigh,
                                                      ViReal64 *specifcationVoltageCenter,
                                                      ViReal64 *specifcationVoltageLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( specifcationVoltageHigh == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Specification Voltage High");
    	
	if ( specifcationVoltageCenter == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Get Specification Voltage Center");
    	
	if ( specifcationVoltageLow == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 4, "Null address for Get Specification Voltage Low");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
     viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, specifcationVoltageHigh));
												
     viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_CENTER, 
												IVI_VAL_DIRECT_USER_CALL, specifcationVoltageCenter));
												
     viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_LOW, 
												IVI_VAL_DIRECT_USER_CALL, specifcationVoltageLow));
												
	 checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Specification_Current                                                         
 * Purpose:  It returns the current specification for CV Mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Specification_Current (ViSession vi,
                                                      ViReal64 *specifcationCurrentHigh,
                                                      ViReal64 *specifcationCurrentCenter,
                                                      ViReal64 *specifcationCurrentLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( specifcationCurrentHigh == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Specification Current High");
    	
	if ( specifcationCurrentCenter == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Get Specification Current Center");
    	
	if ( specifcationCurrentLow == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 4, "Null address for Get Specification Current Low");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
     viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_CURRENT_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, specifcationCurrentHigh));
												
     viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_CURRENT_CENTER, 
												IVI_VAL_DIRECT_USER_CALL, specifcationCurrentCenter));
												
     viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_CURRENT_LOW, 
												IVI_VAL_DIRECT_USER_CALL, specifcationCurrentLow));
												
	 checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Specification_Power                                                         
 * Purpose:  It returns the power specification for LED Mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Specification_Power (ViSession vi,
                                                    ViReal64 *specifcationPowerHigh,
                                                    ViReal64 *specifcationPowerCenter,
                                                    ViReal64 *specifcationPowerLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( specifcationPowerHigh == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Specification Power High");
    	
	if ( specifcationPowerCenter == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Get Specification Power Center");
    	
	if ( specifcationPowerLow == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 4, "Null address for Get Specification Power Low");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
     viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_POWER_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, specifcationPowerHigh));
												
     viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_POWER_CENTER, 
												IVI_VAL_DIRECT_USER_CALL, specifcationPowerCenter));
												
     viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_POWER_LOW, 
												IVI_VAL_DIRECT_USER_CALL, specifcationPowerLow));
												
	 checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Specification_Test                                                         
 * Purpose:  It returns the specification test.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Specification_Test (ViSession vi,
                                                   ViBoolean *getSpecificationTest)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getSpecificationTest == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Specification Test");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
     viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_SPECIFICATION_TEST, 
												IVI_VAL_DIRECT_USER_CALL, getSpecificationTest));
												
	 checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Fetch_Voltage                                                         
 * Purpose:  It returns the real time voltage measured at the input of the
 *           load module.                          
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Fetch_Voltage (ViSession vi,
                                          ViReal64 *fetchVoltage)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	if ( fetchVoltage == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Fetch Voltage");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "FETCH:VOLT?\n"));
		
	    viCheckErr( viScanf (io, "%Lf",fetchVoltage));     		

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Fetch_Current                                                         
 * Purpose:  It returns the real time current measured at the input of the
 *           load module.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Fetch_Current (ViSession vi,
                                          ViReal64 *fetchCurrent)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	 
    
	if ( fetchCurrent == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Fetch Current");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "FETCH:CURR?\n"));
		
	    viCheckErr( viScanf (io, "%Lf",fetchCurrent));     		

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Fetch_Power                                                         
 * Purpose:  It returns the real time power measured at the input of the
 *           load module.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Fetch_Power (ViSession vi,
                                        ViReal64 *fetchPower)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	if ( fetchPower == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Fetch Power");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "FETCH:POW?\n"));
		
	    viCheckErr( viScanf (io, "%Lf",fetchPower));     		

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Fetch_Status                                                         
 * Purpose:  It returns the real time status of the load module.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Fetch_Status (ViSession vi,
                                         ViInt32 *fetchStatus)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	   
    
	if ( fetchStatus == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Fetch Status");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "FETCH:STAT?\n"));
		
	    viCheckErr( viScanf (io, "%d",fetchStatus));     		

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Fetch_All_Voltage                                                         
 * Purpose:  It returns the real time voltage measured at the input of the
 *           all load module.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Fetch_All_Voltage (ViSession vi,
                                              ViChar fetchAllVoltage[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	if ( fetchAllVoltage == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Fetch All Voltage");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "FETCH:ALLV?\n"));
		
	    viCheckErr( viScanf (io, "%s",fetchAllVoltage));  

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Fetch_All_Current                                                         
 * Purpose:  It returns the real time current measured at the input of the
 *           all load module.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Fetch_All_Current (ViSession vi,
                                              ViChar fetchAllCurrent[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	 
    
	if ( fetchAllCurrent == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Fetch All Current");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "FETCH:ALLC?\n"));
		
	    viCheckErr( viScanf (io, "%s",fetchAllCurrent));  

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Fetch_All_Power                                                         
 * Purpose:  It returns the real time power measured at the input of the
 *           all load module.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Fetch_All_Power (ViSession vi,
                                            ViChar fetchAllPower[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	      
    
	if ( fetchAllPower == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Fetch All Power");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "FETCH:ALLP?\n"));
		
	    viCheckErr( viScanf (io, "%s",fetchAllPower));  

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Fetch_Time                                                         
 * Purpose:  It returns the time measured in timing mode.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Fetch_Time (ViSession vi,
                                       ViChar fetchTime[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	  
    
	if ( fetchTime == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Fetch Time");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "FETCH:TIME?\n"));
		
	    viCheckErr( viScanf (io, "%s",fetchTime));  

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Measure_Input                                                         
 * Purpose:  It selects the input port of electronic load to measure the voltage.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Measure_Input (ViSession vi,
                                              ViBoolean measureInput)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_MEASURE_INPUT, 
												IVI_VAL_DIRECT_USER_CALL, measureInput), 2, "measureInput" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Measure_Scan                                                         
 * Purpose:  It sets the scanning mode of frame to load module.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Measure_Scan (ViSession vi,
                                             ViBoolean measureScan)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_MEASURE_SCAN, 
												IVI_VAL_DIRECT_USER_CALL, measureScan), 2, "measureScan" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Measure_Input                                                         
 * Purpose:  It returns the input port which has been set. 
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Measure_Input (ViSession vi,
                                              ViBoolean *getMeasureInput)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getMeasureInput == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Measure Input");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_MEASURE_INPUT, 
												IVI_VAL_DIRECT_USER_CALL, getMeasureInput));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Measure_Scan                                                         
 * Purpose:  It returns the scanning mode of the frame. 
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Measure_Scan (ViSession vi,
                                             ViBoolean *getMeasureScan)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getMeasureScan == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Measure Scan");
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_MEASURE_SCAN, 
												IVI_VAL_DIRECT_USER_CALL, getMeasureScan));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Measure_Voltage                                                         
 * Purpose:  It returns the voltage measured at the input of electronic load.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Measure_Voltage (ViSession vi,
                                            ViReal64 *measureVoltage)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	 
    
	if ( measureVoltage == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Measure Voltage");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "MEAS:VOLT?\n"));
		
	    viCheckErr( viScanf (io, "%Lf",measureVoltage));     		

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Measure_Current                                                         
 * Purpose:  It returns the current measured at the input of electronic load.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Measure_Current (ViSession vi,
                                            ViReal64 *measureCurrent)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	if ( measureCurrent == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Measure Current");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "MEAS:CURR?\n"));
		
	    viCheckErr( viScanf (io, "%Lf",measureCurrent));     		

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Measure_Power                                                         
 * Purpose:  It returns the power measured at the input of electronic load.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Measure_Power (ViSession vi,
                                          ViReal64 *measurePower)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	if ( measurePower == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Measure Power");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "MEAS:POW?\n"));
		
	    viCheckErr( viScanf (io, "%Lf",measurePower));     		

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Measure_All_Voltage                                                         
 * Purpose:  It returns the voltage measured at the input of all load modules.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Measure_All_Voltage (ViSession vi,
                                                ViChar measureAllVoltage[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	 
    
	if ( measureAllVoltage == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Measure All Voltage");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "MEAS:ALLV?\n"));
		
	    viCheckErr( viScanf (io, "%s",measureAllVoltage));  

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Measure_All_Current                                                         
 * Purpose:  It returns the current measured at the input of all load modules.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Measure_All_Current (ViSession vi,
                                                ViChar measureAllCurrent[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	if ( measureAllCurrent == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Measure All Current");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "MEAS:ALLC?\n"));
		
	    viCheckErr( viScanf (io, "%s",measureAllCurrent));  

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Measure_All_Power                                                         
 * Purpose:  It returns the power measured at the input of all load modules.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_Measure_All_Power (ViSession vi,
                                              ViChar measureAllPower[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	      
    
	if ( measureAllPower == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Measure All Power");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "MEAS:ALLP?\n"));
		
	    viCheckErr( viScanf (io, "%s",measureAllPower));  

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_reset                                                         
 * Purpose:  This function resets the instrument.                          
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_reset (ViSession vi)
{
    ViStatus    error = VI_SUCCESS;

    checkErr( Ivi_LockSession (vi, VI_NULL));

	if (!Ivi_Simulating(vi))                /* call only when locked */
	    {
	    ViSession   io = Ivi_IOSession(vi); /* call only when locked */
	    
	    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));
	    viCheckErr( viPrintf (io, "*RST\n"));
	    }
	
	checkErr( Chr6310A_DefaultInstrSetup (vi));                                
	
	checkErr( Chr6310A_CheckStatus (vi));                                      

Error:
    Ivi_UnlockSession (vi, VI_NULL);
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_self_test                                                       
 * Purpose:  This function executes the instrument self-test and returns the 
 *           result.                                                         
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_self_test (ViSession vi, ViInt16 *testResult, 
                                      ViChar testMessage[])
{
    ViStatus    error = VI_SUCCESS;

    checkErr( Ivi_LockSession (vi, VI_NULL));

    if (testResult == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Test Result");
    if (testMessage == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Test Message");

	viCheckWarn( VI_WARN_NSUP_SELF_TEST);

Error:
    Ivi_UnlockSession(vi, VI_NULL);
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_error_query                                                     
 * Purpose:  This function queries the instrument error queue and returns   
 *           the result.                                                     
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_error_query (ViSession vi, ViInt32 *errCode, 
                                        ViChar errMessage[])
{
    ViStatus    error = VI_SUCCESS;
    
    checkErr( Ivi_LockSession (vi, VI_NULL));
    
    if (errCode == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Error Code");
    if (errMessage == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Error Message");

	if (!Ivi_Simulating(vi))                /* call only when locked */
	    {
	    ViSession   io = Ivi_IOSession(vi); /* call only when locked */
	
	    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));
	    viCheckErr( viPrintf(io, "*ESR?\n"));
	
		viCheckErr( viScanf (io, "%ld", errCode)); 
		
	    if (*errCode == 0)
	    strcpy (errMessage, "No error.");
	    else if (*errCode == 4)
	    strcpy (errMessage, "Query error.");
	    else if (*errCode == 8)
	    strcpy (errMessage, "Device Dependent error.");
	    else if (*errCode == 16)
	    strcpy (errMessage, "Execution error.");
	    else if (*errCode == 32)
	    strcpy (errMessage, "Command error.");
	    else
	    strcpy (errMessage, "Instrument error.");

	    }
	else
	    {
	        /* Simulate Error Query */
	    *errCode = 0;
	    strcpy (errMessage, "No error.");
	    
	    }

Error:
    Ivi_UnlockSession(vi, VI_NULL);
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_error_message                                                  
 * Purpose:  This function translates the error codes returned by this       
 *           instrument driver into user-readable strings.  
 *
 *           Note:  The caller can pass VI_NULL for the vi parameter.  This 
 *           is useful if one of the init functions fail.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_error_message (ViSession vi, ViStatus errorCode,
                                          ViChar errorMessage[256])
{
    ViStatus    error = VI_SUCCESS;
    
    static      IviStringValueTable errorTable = 
        {
            {CHR6310A_ERROR_MODEL_NOT_SUPPORT,  "Not available for L2 in this model"},
         	{CHR6310A_ERROR_INVALID_MODEL,      "Invalid model of load module."},  
            {CHR6310A_ERROR_INVALID_FUNCTION,   "Invalid Function,63110A of load or 63113A of load do not support."}, 
            {CHR6310A_ERROR_LOAD_STATE,         "Invalid Load state,please disable digital I/O first."},
            {CHR6310A_ERROR_LOAD_SHORT_STATE,   "Invalid Load short state,please enable Load ON first."},
            {CHR6310A_ERROR_SPEC_VALUE,         "Invalid Specification Value,please check High-Spec. more than Center-Spec. and Center Spec more than Low-Spec."},                 
         	{CHR6310A_ERROR_INVALID_MODEL_OCP,  "Invalid Model,this model of load module do not support this setting."},  
         	{CHR6310A_ERROR_INVALID_MODEL_OPP,  "Invalid Model,this model of load module do not support this setting."},  
         	{CHR6310A_ERROR_INVALID_MODE,       "Invalid Mode,this model of load module do not support this setting."},
         	{CHR6310A_ERROR_INVALID_VALUE,      "Invalid Value,this model of load module do not support this setting."},             	
            {VI_NULL,                               VI_NULL}
        };
        
    if (vi)
        Ivi_LockSession(vi, VI_NULL);

        /* all VISA and IVI error codes are handled as well as codes in the table */
    if (errorMessage == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Error Message");

    checkErr( Ivi_GetSpecificDriverStatusDesc(vi, errorCode, errorMessage, errorTable));

Error:
    if (vi)
        Ivi_UnlockSession(vi, VI_NULL);
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_revision_query                                                  
 * Purpose:  This function returns the driver and instrument revisions.      
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_revision_query (ViSession vi, ViChar driverRev[], 
                                           ViChar instrRev[])
{
    ViStatus    error = VI_SUCCESS;
    
    checkErr( Ivi_LockSession (vi, VI_NULL));

    if (driverRev == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Driver Revision");
    if (instrRev == VI_NULL)
        viCheckParm( IVI_ERROR_INVALID_PARAMETER, 3, "Null address for Instrument Revision");

    checkErr( Ivi_GetAttributeViString (vi, VI_NULL, CHR6310A_ATTR_SPECIFIC_DRIVER_REVISION, 
                                        0, 256, driverRev));
    checkErr( Ivi_GetAttributeViString (vi, "", CHR6310A_ATTR_INSTRUMENT_FIRMWARE_REVISION, 
                                        0, 256, instrRev));
    checkErr( Chr6310A_CheckStatus (vi));

Error:    
    Ivi_UnlockSession(vi, VI_NULL);
    return error;
}


/*****************************************************************************
 * Function: Chr6310A_GetAttribute<type> Functions                                    
 * Purpose:  These functions enable the instrument driver user to get 
 *           attribute values directly.  There are typesafe versions for 
 *           ViInt32, ViReal64, ViString, ViBoolean, and ViSession attributes.                                         
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_GetAttributeViInt32 (ViSession vi, ViConstString channelName, 
                                                ViAttr attributeId, ViInt32 *value)
{                                                                                                           
    return Ivi_GetAttributeViInt32 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                    value);
}                                                                                                           
ViStatus _VI_FUNC Chr6310A_GetAttributeViReal64 (ViSession vi, ViConstString channelName, 
                                                 ViAttr attributeId, ViReal64 *value)
{                                                                                                           
    return Ivi_GetAttributeViReal64 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                     value);
}                                                                                                           
ViStatus _VI_FUNC Chr6310A_GetAttributeViString (ViSession vi, ViConstString channelName, 
                                                 ViAttr attributeId, ViInt32 bufSize, 
                                                 ViChar value[]) 
{   
    return Ivi_GetAttributeViString (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                     bufSize, value);
}   
ViStatus _VI_FUNC Chr6310A_GetAttributeViBoolean (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViBoolean *value)
{                                                                                                           
    return Ivi_GetAttributeViBoolean (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}                                                                                                           
ViStatus _VI_FUNC Chr6310A_GetAttributeViSession (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViSession *value)
{                                                                                                           
    return Ivi_GetAttributeViSession (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}                                                                                                           

/*****************************************************************************
 * Function: Chr6310A_CLS  
 * Purpose:This command clears the following registers
1.	Channel Status Event registers for all channels
2.	Channel Summary Event register
3.	Questionable Status Event register
4.  Standard Event Status Event register
5.  Operation Status Event register
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_CLS (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "*CLS\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_ESE  
 * Purpose:  This command sets the condition of the Standard Event Status Enable 
 *           register, which determines which events of the Standard Event Status 
 *           Event register are allowed to set the ESB of the Status Byte register.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_ESE (ViSession vi, ViInt32 setESE)     
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_STANDARD_EVENT_STATUS_ENABLE, 
												IVI_VAL_DIRECT_USER_CALL, setESE), 2, "setESE" );
    
	checkErr( Chr6310A_CheckStatus(vi) );	 
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);     
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_ESR  
 * Purpose:  This command programs the Standard Event Status Enable Register bits. 
 *           If one or more of the enable events of the Standard Event Status is set, 
 *           the ESB of Status Byte Register is set too.		
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_ESR (ViSession vi, ViInt32 *getESR)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getESR == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get ESR");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_STANDARD_EVENT_STATUS_REGISTER, 
												IVI_VAL_DIRECT_USER_CALL, getESR));
    
	checkErr( Chr6310A_CheckStatus(vi) );	 
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Identification_String                                                  
 * Purpose:  This query requests the Electronic Frame (6310A) to identify itself. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Identification_String (ViSession vi,
                                                      ViChar getIdentificationString[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	if ( getIdentificationString == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Identification String");
    
	viCheckErr( Ivi_GetAttributeViString( vi, VI_NULL, CHR6310A_ATTR_ID_QUERY_RESPONSE, 
												0, BUFFER_SIZE,getIdentificationString));
    
	checkErr( Chr6310A_CheckStatus(vi) );	     

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPC                                                  
 * Purpose:  This command causes the interface to set the OPC bit (bit 0) of the 
 *           Standard Event Status register when the Electronic Frame (6310A) has 
 *           completed all pending operations. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPC (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	      
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "*OPC\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OPC                                                  
 * Purpose:  This query returns an ASCII "1" when all pending operations are 
 *           completed. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OPC (ViSession vi, ViInt32 *getOPC)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	   
    
	if ( getOPC == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OPC");
    
	if (!Ivi_Simulating(vi))
    {
	
		ViSession	io= Ivi_IOSession(vi);   
 	    viCheckErr( viPrintf (io, "*OPC?\n"));

	    viCheckErr( viScanf (io, "%d",getOPC));
	
	 }

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );  	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_RCL                                                  
 * Purpose:  This command restores the electronic load to a state that was 
 *           previously stored in memory with the *SAV command to the specified 
 *           location. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_RCL (ViSession vi, ViInt32 setRCL)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  		
	
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_RECALL_INSTRUMENT_STATE, 
												IVI_VAL_DIRECT_USER_CALL, setRCL), 2, "setRCL" );
	
	checkErr( Chr6310A_CheckStatus(vi) );  	 
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_RDT                                                  
 * Purpose:  This command returns the types of Electronic Frame (6314A).
 *           If channel does not exist, it returns 0.  If channel exists, 
 *           it returns the types like 63103A, 63102A, 63107R, 63107L..
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_RDT (ViSession vi, ViChar getRDT[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	if ( getRDT == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get RDT");
    
	if (!Ivi_Simulating(vi))
    {
	
		ViSession	io= Ivi_IOSession(vi);   
 	    viCheckErr( viPrintf (io, "*RDT?\n"));

	    viCheckErr( viScanf (io, "%s",getRDT));
	
	 }
	 
    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );  	 
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_RST  
 * Purpose:  This command forces an ABORt, *CLS, LOAD=PROT=CLE command.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_RST (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "*RST\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_SAV                                                  
 * Purpose:  This command stores the present state of the single electronic load 
 *           and the states of all channels of the multiple loads in a specified 
 *           location in memory.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_SAV (ViSession vi, ViInt32 setSAV)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
	
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_SAVE_INSTRUMENT_STATE, 
												IVI_VAL_DIRECT_USER_CALL, setSAV), 2, "setSAV" );
	
	checkErr( Chr6310A_CheckStatus(vi) );	 	

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_SRE                                                  
 * Purpose:  This command sets the condition of the Service Request Enable 
 *           register, which determines which events of the Status Byte register 
 *           (see *STB) are allowed to set the MSS( Master Status Summary) bit.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_SRE (ViSession vi, ViInt32 setSRE)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
	
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_SERVICE_REQUEST_ENABLE, 
												IVI_VAL_DIRECT_USER_CALL, setSRE), 2, "setSRE" );
	
	checkErr( Chr6310A_CheckStatus(vi) );	 	
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_STB                                                  
 * Purpose:  This query reads the Status Byte register. Note that the MSS 
 *           (Master Summary Status) bit instead of RQS bit is returned in Bit 6.  
 *           This bit indicates if the electronic load has at least one reason for 
 *           requesting service.  *STB? does not clear the Status Byte register, 
 *           which is cleared only when subsequent action has cleared all its set bits.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_STB (ViSession vi, ViInt32 *getSTB)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	if ( getSTB == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get STB");
    
	if (!Ivi_Simulating(vi))
    {
	
		ViSession	io= Ivi_IOSession(vi);   
 	    viCheckErr( viPrintf (io, "*STB?\n"));

	    viCheckErr( viScanf (io, "%d",getSTB));
	
	 }
	 
    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );  	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_ABORT                                                  
 * Purpose:  It sets all electronic loads to "OFF".
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_ABORT (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "ABOR\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}
/*****************************************************************************
 * Function: Get_Configure_LEDLCRL_Range                                                
 
******************************************************************************/

ViStatus _VI_FUNC Chr6310A_Get_Configure_LEDLCRL_Range (ViSession vi,
                                                        ViBoolean *LEDLCRLRange)
{
{	   ViStatus	error = VI_SUCCESS;
       ViString    Model="", Multi_Chan; 
       ViInt32     CHANN=0;
       ViChar      MultiChan[BUFFER_SIZE];
      
       checkErr( Ivi_LockSession (vi, VI_NULL));
         Fmt(MultiChan, "%s<%i", CHANN);
       	
    viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHANN)); 
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHANN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
   
       if ((!strcmp(Model,"63113A")))
       {
viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_LEDLCRL, 
												IVI_VAL_DIRECT_USER_CALL,LEDLCRLRange));	
	  }	
		else
		checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	checkErr( Chr6310A_CheckStatus(vi) );												
Error:
Ivi_UnlockSession (vi, VI_NULL);
return error;
}
}

/*****************************************************************************
 * Function: Chr6310A_Set_Channel_Load                                                  
 * Purpose:  It selects a specific channel by which the coming channel-specific 
 *           command will be received and executed.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Channel_Load (ViSession vi,
                                             ViInt32 channel)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));    
    
	viCheckParm( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, channel), 2, "channel" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Channel_Active                                                  
 * Purpose:  It enables or disables the load module. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Channel_Active (ViSession vi,
                                               ViBoolean channelActive)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE];    	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));
    Fmt(Multi_Chan, "%s<%i", CHAN);   	
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CHANNEL_ACTIVE, 
												IVI_VAL_DIRECT_USER_CALL, channelActive), 2, "channelActive" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Channel_Synchronized                                                  
 * Purpose:  It sets the load module to receive synchronized command action of 
 *           RUN ABORT or not. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Channel_Synchronized (ViSession vi,
                                                     ViBoolean channelSynchronized)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;  	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CHANNEL_SYNCHRONIZED, 
												IVI_VAL_DIRECT_USER_CALL, channelSynchronized), 2, "channelSynchronized" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Channel_Load                                                  
 * Purpose:  It returns the current specific channel.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Channel_Load (ViSession vi,
                                             ViInt32 *getChannelLoad)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));   	
    
	if ( getChannelLoad == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Channel Load");
	
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, getChannelLoad));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Min_of_Channel_Load                                                  
 * Purpose:  It returns the current min specific channel.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Min_of_Channel_Load (ViSession vi,
                                                    ViInt32 *getMin_ofChannelLoad)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));   
    
	if ( getMin_ofChannelLoad == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Min of Channel Load");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_QUERY_MIN_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, getMin_ofChannelLoad));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Max_of_Channel_Load                                                  
 * Purpose:  It returns the current max specific channel.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Max_of_Channel_Load (ViSession vi,
                                                    ViInt32 *getMax_ofChannelLoad)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));   
    
	if ( getMax_ofChannelLoad == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get MAX of Channel Load");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_QUERY_MAX_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, getMax_ofChannelLoad));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Channel_Synchronized                                                  
 * Purpose:  It returns to the load module and makes it receive synchronized 
 *           command status.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Channel_Synchronized (ViSession vi,
                                                     ViBoolean *getChannelSynchronized)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;  	    	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getChannelSynchronized == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Channel Synchronized");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));         
    Fmt(Multi_Chan, "%s<%i", CHAN);     
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CHANNEL_SYNCHRONIZED, 
												IVI_VAL_DIRECT_USER_CALL, getChannelSynchronized));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Channel_ID                                                  
 * Purpose:  This query requests the module to identify itself. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Channel_ID (ViSession vi,
                                           ViChar getChannelID[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	if ( getChannelID == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Channel ID");
    
	if (!Ivi_Simulating(vi))
    {
	
		ViSession	io= Ivi_IOSession(vi);   
 	    viCheckErr( viPrintf (io, "CHAN:ID?\n"));

	    viCheckErr( viScanf (io, "%s",getChannelID));
	
	 }
	 
    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );  	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Von                                                  
 * Purpose:  It sets the voltage of sink current on. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Von (ViSession vi,
                                              ViReal64 configureVon)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;  		
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));      
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VON, 
												IVI_VAL_DIRECT_USER_CALL, configureVon), 2, "configureVon" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Voltage_Range                                                  
 * Purpose:  It sets the voltage measurement range in CC mode. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Voltage_Range (ViSession vi,
                                                        ViReal64 voltageRange)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,Mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model=""; 	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);      

	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE,IVI_VAL_DIRECT_USER_CALL, &Mode)); 
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
     
    if ((strcmp(Model,"63105A")) && (strcmp(Model,"63108A")))
    
        if ( Mode != 0 && Mode != 2)
	    viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, voltageRange), 2, "voltageRange" );
    
        else if (( Mode == 0 || Mode == 2) && ( voltageRange <=125 ))
	    viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, voltageRange), 2, "voltageRange" );
    
    	else
    	     checkErr( error = CHR6310A_ERROR_INVALID_VALUE );
    	     
    else
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, voltageRange), 2, "voltageRange" );
    
	checkErr( Chr6310A_CheckStatus(vi) );	 
																	

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Voltage_Latch                                                  
 * Purpose:  It sets the action type of Von. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Voltage_Latch (ViSession vi,
                                                        ViBoolean voltageLatch)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE];    	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));
    Fmt(Multi_Chan, "%s<%i", CHAN);   	
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VOLTAGE_LATCH, 
												IVI_VAL_DIRECT_USER_CALL, voltageLatch), 2, "voltageLatch" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Von_Latch_Reset                                                  
 * Purpose:  It resets the Von signal. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Von_Latch_Reset (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "CONF:VOLT:LATC:RES\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Auto_Load                                                  
 * Purpose:  It sets if the load module will do Auto Load On during power-on. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Auto_Load (ViSession vi,
                                                    ViBoolean autoLoad)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));   	
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_AUTO_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, autoLoad), 2, "autoLoad" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Auto_Mode                                                  
 * Purpose:  It sets the Auto Load On to LOAD ON or PROGRAM. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Auto_Mode (ViSession vi,
                                                    ViBoolean autoMode)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));   	
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_AUTO_MODE, 
												IVI_VAL_DIRECT_USER_CALL, autoMode), 2, "autoMode" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Sound                                                  
 * Purpose:  It sets the buffer sound of load module to ON or OFF. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Sound (ViSession vi,
                                                ViBoolean sound)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_SOUND, 
												IVI_VAL_DIRECT_USER_CALL, sound), 2, "sound" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Remote                                                  
 * Purpose:  It sets the status of remote control (only effective in RS232C). 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Remote (ViSession vi,
                                                 ViBoolean remote)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_REMOTE, 
												IVI_VAL_DIRECT_USER_CALL, remote), 2, "remote" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Save                                                  
 * Purpose:  It stores the data of CONFigure into EEPROM. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Save (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "CONF:SAV\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	
    
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Load                                                  
 * Purpose:  The value at the setting of load module as LOADON is the one 
 *           changed by the rotary knob (UPDATED/1) or the original set value 
 *           (OLD/0). 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Load (ViSession vi,
                                               ViBoolean load)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, load), 2, "load" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Timing_State                                                  
 * Purpose:  It sets the timing function to ON or OFF. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Timing_State (ViSession vi,
                                                       ViBoolean timingState)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE];    	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));
    Fmt(Multi_Chan, "%s<%i", CHAN);   	
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_TIMING_STATE, 
												IVI_VAL_DIRECT_USER_CALL, timingState), 2, "timingState" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Timing_Trigger                                                  
 * Purpose:  It sets the voltage for Timing function at time out. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Timing_Trigger (ViSession vi,
                                                         ViReal64 timingTrigger)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;  		
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));      
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_TIMING_TRIGGER, 
												IVI_VAL_DIRECT_USER_CALL, timingTrigger), 2, "timingTrigger" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Timing_Timeout                                                  
 * Purpose:  It sets timeout for Timing function from 1ms to 24 hr. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Timing_Timeout (ViSession vi,
                                                         ViInt32 timingTimeout)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;  		
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));      
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_TIMING_TIMEOUT, 
												IVI_VAL_DIRECT_USER_CALL, timingTimeout), 2, "timingTimeout" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Voff_State                                                  
 * Purpose:  It sets VOFF function ON or OFF. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Voff_State (ViSession vi,
                                                     ViBoolean VoffState)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;  		
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));      
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VOFF_STATE, 
												IVI_VAL_DIRECT_USER_CALL, VoffState), 2, "VoffState" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Voff_Final_V                                                  
 * Purpose:  It sets the final loading voltage. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Voff_Final_V (ViSession vi,
                                                       ViReal64 configureVoffFinalV)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;  		
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));      
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VOFF_FINAL_V, 
												IVI_VAL_DIRECT_USER_CALL, configureVoffFinalV), 2, "configureVoffFinalV" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Measure_Average                                                  
 * Purpose:  It sets the average number of times for measurement. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Measure_Average (ViSession vi,
                                                          ViInt32 measureAverage)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;  		
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));      
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_MEASURE_AVERAGE, 
												IVI_VAL_DIRECT_USER_CALL, measureAverage), 2, "measureAverage" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Digital_IO                                                  
 * Purpose:  It sets the Digital IO to ON or OFF. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Digital_IO (ViSession vi,
                                                     ViBoolean digitalIO)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_DIGITAL_IO, 
												IVI_VAL_DIRECT_USER_CALL, digitalIO), 2, "digitalIO" );
												
	checkErr( Chr6310A_CheckStatus(vi) );												

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Key                                                  
 * Purpose:  It sets if change the MEAS key on the Module to Static/Dynamic. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Key (ViSession vi,
                                              ViBoolean key)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;  		
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));      
    Fmt(Multi_Chan, "%s<%i", CHAN);      
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_KEY, 
												IVI_VAL_DIRECT_USER_CALL, key), 2, "key" );
												
	checkErr( Chr6310A_CheckStatus(vi) );												

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Configure_Echo                                                  
 * Purpose:  It sets to reply new or old Model Name when querying the device's 
 *           model name. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Configure_Echo (ViSession vi,
                                               ViBoolean echo)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_ECHO, 
												IVI_VAL_DIRECT_USER_CALL, echo), 2, "echo" );
												
	checkErr( Chr6310A_CheckStatus(vi) );												

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}
/*****************************************************************************
 * Function: Chr6310A_Set_LEDLCRL_Range                                                  
******************************************************************************/

ViStatus _VI_FUNC Chr6310A_Set_LEDLCRL_Range (ViBoolean LEDLCRLRange,
                                              ViSession vi)
{	   ViStatus	error = VI_SUCCESS;
       ViString    Model=""; 
       ViInt32     CHAN;
       checkErr( Ivi_LockSession (vi, VI_NULL));
       
       	
    viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN)); 
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model,VI_NULL));
    
       if ((!strcmp(Model,"63113A")))
       {
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_LEDLCRL, 
												IVI_VAL_DIRECT_USER_CALL,LEDLCRLRange), 2, "LEDLCRLRange" );
		}	
		else
		checkErr( error = CHR6310A_ERROR_INVALID_MODEL );
	checkErr( Chr6310A_CheckStatus(vi) );												
Error:
Ivi_UnlockSession (vi, VI_NULL);
return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Von                                                  
 * Purpose:  It returns the setting Von value. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Von (ViSession vi,
                                              ViReal64 *getConfigureVon)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureVon == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Von");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));         
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VON, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureVon));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Voltage_Range                                                  
 * Purpose:  It returns the Voltage range. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Voltage_Range (ViSession vi,
                                                        ViReal64 *get_Configure_Voltage_Range)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( get_Configure_Voltage_Range == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Voltage Range");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_GET_CONFIGURE_VOLTAGE_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, get_Configure_Voltage_Range));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Voltage_Latch                                                  
 * Purpose:  It returns the action type of Von. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Voltage_Latch (ViSession vi,
                                                        ViBoolean *getConfigureVoltageLatch)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureVoltageLatch == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Voltage Latch");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VOLTAGE_LATCH, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureVoltageLatch));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Auto_Load                                                  
 * Purpose:  It returns the status of Auto Load On. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Auto_Load (ViSession vi,
                                                    ViBoolean *getConfigureAutoLoad)
{
	ViStatus	error = VI_SUCCESS;
    
    checkErr( Ivi_LockSession (vi, VI_NULL));   
    
	if ( getConfigureAutoLoad == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Auto Load");
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_AUTO_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureAutoLoad));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Auto_Mode                                                  
 * Purpose:  It returns the execution type of Auto Load On. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Auto_Mode (ViSession vi,
                                                    ViBoolean *getConfigureAutoMode)
{
	ViStatus	error = VI_SUCCESS;
    
    checkErr( Ivi_LockSession (vi, VI_NULL));   
    
	if ( getConfigureAutoMode == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Auto Mode");
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_AUTO_MODE, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureAutoMode));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Auto_Mode                                                  
 * Purpose:  It returns the control status of the load module's buzzer sound. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Sound (ViSession vi,
                                                ViBoolean *getConfigureSound)
{
	ViStatus	error = VI_SUCCESS;
    
    checkErr( Ivi_LockSession (vi, VI_NULL));   
    
	if ( getConfigureSound == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Sound");
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_SOUND, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureSound));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Load                                                  
 * Purpose:  It returns the load module as LOADON setting to be UPDATE or
 *           OLD. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Load (ViSession vi,
                                               ViBoolean *getConfigureLoad)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));   
    
	if ( getConfigureLoad == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Load");
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureLoad));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);    
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Timing_State                                                  
 *           It returns the timing function setting to be ON or OFF.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Timing_State (ViSession vi,
                                                       ViBoolean *getConfigureTimingState)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureTimingState == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Timing State");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_TIMING_STATE, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureTimingState));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Timing_Trigger                                                  
 * Purpose:  It returns the load module as LOADON setting to be UPDATE or
 *           OLD. 
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Timing_Trigger (ViSession vi,
                                                         ViReal64 *getConfigureTimingTrigger)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureTimingTrigger == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Timing Trigger");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_TIMING_TRIGGER, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureTimingTrigger));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Timing_Timeout                                                  
 * Purpose:  It returns the timeout set.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Timing_Timeout (ViSession vi,
                                                         ViInt32 *getConfigureTimingTimeout)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureTimingTimeout == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Timing Timeout");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_TIMING_TIMEOUT, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureTimingTimeout));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Voff_State                                                  
 * Purpose:  It returns the VOFF function setting to be ON or OFF.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Voff_State (ViSession vi,
                                                     ViBoolean *getConfigureVoffState)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureVoffState == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Voff State");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VOFF_STATE, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureVoffState));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Voff_Final_V                                                  
 * Purpose:  It returns the final loading voltage set.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Voff_Final_V (ViSession vi,
                                                       ViReal64 *getConfigureVoffFinalV)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureVoffFinalV == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Voff Final V");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_VOFF_FINAL_V, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureVoffFinalV));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Measure_Average                                                  
 * Purpose:  It returns the average times set.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Measure_Average (ViSession vi,
                                                          ViInt32 *getConfigureMeasureAverage)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureMeasureAverage == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Measure Average");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_MEASURE_AVERAGE, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureMeasureAverage));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Digital_IO                                                  
 * Purpose:  It returns the Digital IO setting to be ON or OFF.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Digital_IO (ViSession vi,
                                                     ViBoolean *getConfigureDigitalIO)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));   
    
	if ( getConfigureDigitalIO == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Digital IO");
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_DIGITAL_IO, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureDigitalIO));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);    
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Key                                                  
 * Purpose:  It returns the key setting.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Key (ViSession vi,
                                              ViBoolean *getConfigureKey)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getConfigureKey == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Key");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_CONFIGURE_KEY, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureKey));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);    
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Configure_Echo                                                  
 * Purpose:  It returns the ECHO setting to be NEW or OLD.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Configure_Echo (ViSession vi,
                                               ViBoolean *getConfigureEcho)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));   
    
	if ( getConfigureEcho == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Configure Echo");
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_ECHO, 
												IVI_VAL_DIRECT_USER_CALL, getConfigureEcho));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);    
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Static_L1                                                  
 * Purpose:  It sets the Static Load Current of constant current mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Static_L1 (ViSession vi,
                                                  ViReal64 CCStaticL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_STATIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, CCStaticL1), 2, "CCStaticL1" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Static_L2                                                  
 * Purpose:  It sets the Static Load Current of constant current mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Static_L2 (ViSession vi,
                                                  ViReal64 CCStaticL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (  !(  (!strcmp(Model,"63102A")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63110A"))  )  ){  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_STATIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, CCStaticL2), 2, "CCStaticL2" );}
    else
    	checkErr( error = CHR6310A_ERROR_MODEL_NOT_SUPPORT );   
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Static_Rise_Slew_Rate                                                  
 * Purpose:  It sets the current rise slew rate of constant current static mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Static_Rise_Slew_Rate (ViSession vi,
                                                         ViReal64 CCStaticRiseSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A")) )  
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_STATIC_RISE_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, CCStaticRiseSlewRate), 2, "CCStaticRiseSlewRate" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Static_Fall_Slew_Rate                                                  
 * Purpose:  It sets the current fall slew rate of constant current static mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Static_Fall_Slew_Rate (ViSession vi,
                                                         ViReal64 CCStaticFallSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";  
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A")) )  
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_STATIC_FALL_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, CCStaticFallSlewRate), 2, "CCStaticFallSlewRate" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Dynamic_L1                                                  
 * Purpose:  It sets the Dynamic Load Current during constant current mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_L1 (ViSession vi,
                                                   ViReal64 CCDynamicL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";     
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 

	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A")) )
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, CCDynamicL1), 2, "CCDynamicL1" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Dynamic_L2                                                  
 * Purpose:  It sets the Dynamic Load Current during constant current mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_L2 (ViSession vi,
                                                   ViReal64 CCDynamicL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A"))) 
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, CCDynamicL2), 2, "CCDynamicL2" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Dynamic_Rise_Slew_Rate                                                  
 * Purpose:  It sets the current rise slew rate of constant current dynamic mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_Rise_Slew_Rate (ViSession vi,
                                                          ViReal64 CCDynamicRiseSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
       if  (strcmp(Model,"63110A"))

	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_RISE_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, CCDynamicRiseSlewRate), 2, "CCDynamicRiseSlewRate" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Dynamic_Fall_Slew_Rate                                                  
 * Purpose:  It sets the current fall slew rate of constant current dynamic mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_Fall_Slew_Rate (ViSession vi,
                                                          ViReal64 CCDynamicFallSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";       
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if  (strcmp(Model,"63110A"))
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_FALL_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, CCDynamicFallSlewRate), 2, "CCDynamicFallSlewRate" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Dynamic_T1                                                  
 * Purpose:  It sets the duration parameter T1 of dynamic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_T1 (ViSession vi,
                                                   ViReal64 CCDynamicT1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";    	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))  
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_T1, 
												IVI_VAL_DIRECT_USER_CALL, CCDynamicT1), 2, "CCDynamicT1" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CC_Dynamic_T2                                                  
 * Purpose:  It sets the duration parameter T2 of dynamic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CC_Dynamic_T2 (ViSession vi,
                                                   ViReal64 CCDynamicT2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))  
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_T2, 
												IVI_VAL_DIRECT_USER_CALL, CCDynamicT2), 2, "CCDynamicT2" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Static_L1                                                  
 * Purpose:  It returns the set current value of Static Load L1.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Static_L1 (ViSession vi,
                                                  ViReal64 *getCCStaticL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCStaticL1 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Static L1");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_STATIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, getCCStaticL1));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Static_L2                                                  
 * Purpose:  It returns the set current value of Static Load L2.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Static_L2 (ViSession vi,
                                                  ViReal64 *getCCStaticL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCStaticL2 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Static L2");
    	
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63105A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63112A"))){  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_STATIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, getCCStaticL2));}
    else
    	checkErr( error = CHR6310A_ERROR_MODEL_NOT_SUPPORT );   
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Static_Rise_Slew_Rate                                                  
 * Purpose:  It returns the rise slew rate of static load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Static_Rise_Slew_Rate (ViSession vi,
                                                         ViReal64 *getCCStaticRiseSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";   
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCStaticRiseSlewRate == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Static Rise Slew Rate");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A")) )  
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_STATIC_RISE_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, getCCStaticRiseSlewRate));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Static_Fall_Slew_Rate                                                  
 * Purpose:  It returns the fall slew rate of static load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Static_Fall_Slew_Rate (ViSession vi,
                                                         ViReal64 *getCCStaticFallSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";  
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCStaticFallSlewRate == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Static Fall Slew Rate");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
   if ((strcmp(Model,"63110A")) )  
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_STATIC_FALL_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, getCCStaticFallSlewRate));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Dynamic_L1                                                  
 * Purpose:  It returns the setting current in dynamic load L1.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_L1 (ViSession vi,
                                                   ViReal64 *getCCDynamicL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";   
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCDynamicL1 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Dynamic L1");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, getCCDynamicL1));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Dynamic_L2                                                  
 * Purpose:  It returns the setting current in dynamic load L2.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_L2 (ViSession vi,
                                                   ViReal64 *getCCDynamicL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";  
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCDynamicL2 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Dynamic L2");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))  
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, getCCDynamicL2));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Dynamic_Rise_Slew_Rate                                                  
 * Purpose:  It returns the rise slew rate of dynamic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_Rise_Slew_Rate (ViSession vi,
                                                          ViReal64 *getCCDynamicRiseSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";   
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCDynamicRiseSlewRate == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Dynamic Rise Slew Rate");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
   if ((strcmp(Model,"63110A")) )
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_RISE_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, getCCDynamicRiseSlewRate));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Dynamic_Fall_Slew_Rate                                                  
 * Purpose:  It returns the fall slew rate of dynamic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_Fall_Slew_Rate (ViSession vi,
                                                          ViReal64 *getCCDynamicFallSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";     
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCDynamicFallSlewRate == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Dynamic Fall Slew Rate");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
   if ((strcmp(Model,"63110A")) )
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_FALL_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, getCCDynamicFallSlewRate));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Dynamic_T1                                                  
 * Purpose:  It returns the dynamic duration parameter T1.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_T1 (ViSession vi,
                                                   ViReal64 *getCCDynamicT1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";     
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCDynamicT1 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Dynamic T1");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))  
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_T1, 
												IVI_VAL_DIRECT_USER_CALL, getCCDynamicT1));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CC_Dynamic_T2                                                  
 * Purpose:  It returns the dynamic duration parameter T2.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CC_Dynamic_T2 (ViSession vi,
                                                   ViReal64 *getCCDynamicT2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";   
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCCDynamicT2 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CC Dynamic T2");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))  
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_CURRENT_DYNAMIC_T2, 
												IVI_VAL_DIRECT_USER_CALL, getCCDynamicT2));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CR_Static_L1                                                  
 * Purpose:  It sets the static resistance level of constant resistance mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CR_Static_L1 (ViSession vi,
                                             ViReal64 CRStaticL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_RESISTANCE_STATIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, CRStaticL1), 2, "CRStaticL1" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CR_Static_L2                                                  
 * Purpose:  It sets the static resistance level of constant resistance mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CR_Static_L2 (ViSession vi,
                                             ViReal64 CRStaticL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (!((!strcmp(Model,"63102A")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63110A")))){  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_RESISTANCE_STATIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, CRStaticL2), 2, "CRStaticL2" );}
    else
    	checkErr( error = CHR6310A_ERROR_MODEL_NOT_SUPPORT );   
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CR_Static_Rise_Slew_Rate                                                  
 * Purpose:  It sets the resistive rise slew rate of constant resistance.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CR_Static_Rise_Slew_Rate (ViSession vi,
                                                         ViReal64 CRStaticRiseSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_RESISTANCE_STATIC_RISE_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, CRStaticRiseSlewRate), 2, "CRStaticRiseSlewRate" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CR_Static_Fall_Slew_Rate                                                  
 * Purpose:  It sets the resistive fall slew rate of constant resistance.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CR_Static_Fall_Slew_Rate (ViSession vi,
                                                         ViReal64 CRStaticFallSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_RESISTANCE_STATIC_FALL_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, CRStaticFallSlewRate), 2, "CRStaticFallSlewRate" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CR_Static_L1                                                  
 * Purpose:  It returns the set resistance of the value of Load L1.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CR_Static_L1 (ViSession vi,
                                             ViReal64 *getCRStaticL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCRStaticL1 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CR Static L1");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_RESISTANCE_STATIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, getCRStaticL1));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CR_Static_L2                                                 
 * Purpose:  It returns the set resistance of the value of Load L2.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CR_Static_L2 (ViSession vi,
                                             ViReal64 *getCRStaticL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCRStaticL2 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CR Static L2");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63105A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63112A"))){  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_RESISTANCE_STATIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, getCRStaticL2));}
    else
    	checkErr( error = CHR6310A_ERROR_MODEL_NOT_SUPPORT );   
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CR_Static_Rise_Slew_Rate                                                 
 * Purpose:  It returns the CR rise slew rate.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CR_Static_Rise_Slew_Rate (ViSession vi,
                                                         ViReal64 *getCRStaticRiseSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";   
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCRStaticRiseSlewRate == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CR Static Rise Slew Rate");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))  
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_RESISTANCE_STATIC_RISE_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, getCRStaticRiseSlewRate));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CR_Static_Fall_Slew_Rate                                                 
 * Purpose:  It returns the CR fall slew rate.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CR_Static_Fall_Slew_Rate (ViSession vi,
                                                         ViReal64 *getCRStaticFallSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";     
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCRStaticFallSlewRate == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CR Static Fall Slew Rate");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_RESISTANCE_STATIC_FALL_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, getCRStaticFallSlewRate));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CV_Static_L1                                                 
 * Purpose:  It sets the voltage of static load during constant voltage mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CV_Static_L1 (ViSession vi,
                                             ViReal64 CVStaticL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_VOLTAGE_STATIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, CVStaticL1), 2, "CVStaticL1" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CV_Static_L2                                                 
 * Purpose:  It sets the voltage of static load during constant voltage mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CV_Static_L2 (ViSession vi,
                                             ViReal64 CVStaticL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (!((!strcmp(Model,"63102A")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63110A")))){  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_VOLTAGE_STATIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, CVStaticL2), 2, "CVStaticL2" );}
    else
    	checkErr( error = CHR6310A_ERROR_MODEL_NOT_SUPPORT );   
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CV_Current_Limit                                                 
 * Purpose:  It sets the current limit of constant voltage mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CV_Current_Limit (ViSession vi,
                                                 ViReal64 CVCurrentLimit)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_VOLTAGE_CURRENT_LIMIT, 
												IVI_VAL_DIRECT_USER_CALL, CVCurrentLimit), 2, "CVCurrentLimit" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CV_Response_Speed                                                 
 * Purpose:  It sets the response speed of CV mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CV_Response_Speed (ViSession vi,
                                                  ViBoolean CVResponseSpeed)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   	  
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A")) 
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_VOLTAGE_RESPONSE_SPEED, 
												IVI_VAL_DIRECT_USER_CALL, CVResponseSpeed), 2, "CVResponseSpeed" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CV_Static_L1                                                 
 * Purpose:  It returns the set voltage value of load L1.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CV_Static_L1 (ViSession vi,
                                             ViReal64 *getCVStaticL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCVStaticL1 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CV Static L1");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_VOLTAGE_STATIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, getCVStaticL1));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CV_Static_L2                                                 
 * Purpose:  It returns the set voltage value of load L2.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CV_Static_L2 (ViSession vi,
                                             ViReal64 *getCVStaticL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCVStaticL2 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CV Static L2");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63105A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63112A"))){  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_VOLTAGE_STATIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, getCVStaticL2));}
    else
    	checkErr( error = CHR6310A_ERROR_MODEL_NOT_SUPPORT );   
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CV_Current_Limit                                                 
 * Purpose:  It returns the current limit of constant voltage mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CV_Current_Limit (ViSession vi,
                                                 ViReal64 *getCVCurrentLimit)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCVCurrentLimit == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CV Current Limit");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_VOLTAGE_CURRENT_LIMIT, 
												IVI_VAL_DIRECT_USER_CALL, getCVCurrentLimit));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CV_Response_Speed                                                 
 * Purpose:  It returns the response speed of CV mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CV_Response_Speed (ViSession vi,
                                                  ViBoolean *getCVResponseSpeed)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	ViString    Model="";         
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCVResponseSpeed == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CV Response Speed");
    	
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if (strcmp(Model,"63110A"))
    
    viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_VOLTAGE_RESPONSE_SPEED, 
												IVI_VAL_DIRECT_USER_CALL, getCVResponseSpeed));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
            
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CP_Static_L1                                                 
 * Purpose:  It sets the static power level of constant power mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CP_Static_L1 (ViSession vi,
                                             ViReal64 CPStaticL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	ViString    Model="";   
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A"))&&(strcmp(Model,"63113A")))  //SAM
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_POWER_STATIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, CPStaticL1), 2, "CPStaticL1" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CP_Static_L2                                                 
 * Purpose:  It sets the static power level of constant power mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CP_Static_L2 (ViSession vi,
                                             ViReal64 CPStaticL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));

    if ((strcmp(Model,"63110A"))&&(strcmp(Model,"63113A")))		   //SAM
    
    {
	
    if (!((!strcmp(Model,"63102A")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63107AL")))){  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_POWER_STATIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, CPStaticL2), 2, "CPStaticL2" );}
    else
    	checkErr( error = CHR6310A_ERROR_MODEL_NOT_SUPPORT );}  
    	
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );  
    	
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}
                                             
/*****************************************************************************
 * Function: Chr6310A_Set_CP_Static_Rise_Slew_Rate                                                 
 * Purpose:  It sets the resistive rise slew rate of constant power.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CP_Static_Rise_Slew_Rate (ViSession vi,
                                                         ViReal64 CPStaticRiseSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	ViString    Model="";      
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A"))&&(strcmp(Model,"63113A"))) //SAM
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_POWER_STATIC_RISE_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, CPStaticRiseSlewRate), 2, "CPStaticRiseSlewRate" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_CP_Static_Fall_Slew_Rate                                                 
 * Purpose:  It sets the resistive fall slew rate of constant power.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_CP_Static_Fall_Slew_Rate (ViSession vi,
                                                         ViReal64 CPStaticFallSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	ViString    Model="";      
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A"))&&(strcmp(Model,"63113A"))) //SAM
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_POWER_STATIC_RISE_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, CPStaticFallSlewRate), 2, "CPStaticFallSlewRate" );
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CP_Static_L1                                                 
 * Purpose:  It returns the set power of the value of Load L1.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CP_Static_L1 (ViSession vi,
                                             ViReal64 *getCPStaticL1)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	ViString    Model="";     
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCPStaticL1 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CP Static L1");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A"))&&(strcmp(Model,"63113A")))		//SAM

	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_POWER_STATIC_L1, 
												IVI_VAL_DIRECT_USER_CALL, getCPStaticL1));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CP_Static_L2                                                 
 * Purpose:  It returns the set power of the value of Load L2.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CP_Static_L2 (ViSession vi,
                                             ViReal64 *getCPStaticL2)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	ViString    Model="";   	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCPStaticL2 == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CP Static L2");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
   if ((strcmp(Model,"63110A"))&&(strcmp(Model,"63113A")))   //SAM
   {
    
    if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63105A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63112A"))){  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_POWER_STATIC_L2, 
												IVI_VAL_DIRECT_USER_CALL, getCPStaticL2));}
    else
    	checkErr( error = CHR6310A_ERROR_MODEL_NOT_SUPPORT );}
    	
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );
    	
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CP_Static_Rise_Slew_Rate                                                 
 * Purpose:  It returns the CP rise slew rate.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CP_Static_Rise_Slew_Rate (ViSession vi,
                                                         ViReal64 *getCPStaticRiseSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";    
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCPStaticRiseSlewRate == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CP Static Rise Slew Rate");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A"))&&(strcmp(Model,"63113A"))) //SAM
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_POWER_STATIC_RISE_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, getCPStaticRiseSlewRate));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_CP_Static_Fall_Slew_Rate                                                 
 * Purpose:  It returns the CP fall slew rate.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_CP_Static_Fall_Slew_Rate (ViSession vi,
                                                         ViReal64 *getCPStaticFallSlewRate)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];   
	ViString    Model="";   
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getCPStaticFallSlewRate == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get CP Static Fall Slew Rate");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  

    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	
    if ((strcmp(Model,"63110A"))&&(strcmp(Model,"63113A"))) //SAM
	
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_POWER_STATIC_FALL_SLEW, 
												IVI_VAL_DIRECT_USER_CALL, getCPStaticFallSlewRate));
    else
    	checkErr( error = CHR6310A_ERROR_INVALID_FUNCTION );   

	checkErr( Chr6310A_CheckStatus(vi) );	  
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OCP_Test                                                 
 * Purpose:  It executes or cancels the OCP Test.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OCP_Test (ViSession vi,
                                         ViBoolean OCPTest)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OCP_TEST, 
												IVI_VAL_DIRECT_USER_CALL, OCPTest), 2, "OCPTest" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OCP_Range                                                 
 * Purpose:  It sets the range for OCP execution.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OCP_Range (ViSession vi,
                                          ViBoolean OCPRange)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViString    Model="";  	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	ViReal64    VRange;
/*	ViInt32		ID_NUMS=10,ID_INDEX=0;	
	ViAttr		ID[10]={CHR6310A_ATTR_OCP_START_CURRENT_MIN, CHR6310A_ATTR_OCP_START_CURRENT_MAX, CHR6310A_ATTR_OCP_END_CURRENT_MIN, 
	                    CHR6310A_ATTR_OCP_END_CURRENT_MAX, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MIN, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MAX, 
	                    CHR6310A_ATTR_OCP_SPECIFICATION_L_MIN, CHR6310A_ATTR_OCP_SPECIFICATION_L_MAX, CHR6310A_ATTR_OCP_SPECIFICATION_H_MIN, 
	                    CHR6310A_ATTR_OCP_SPECIFICATION_H_MAX
						};
*/	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_GET_CONFIGURE_VOLTAGE_RANGE,IVI_VAL_DIRECT_USER_CALL, &VRange));         
/*
   	for (ID_INDEX=0;ID_INDEX<ID_NUMS;ID_INDEX++)
    viCheckErr(Ivi_InvalidateAttribute (vi, Multi_Chan, ID[ID_INDEX]));
*/	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	if ((strcmp(Model,"63107AR")) && (strcmp(Model,"63105A")) && (strcmp(Model,"63108A")))
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OCP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, OCPRange), 2, "OCPRange" );  
    else 
        if ((!strcmp(Model,"63107AR")) && ( OCPRange != 1 ))
	        viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OCP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, OCPRange), 2, "OCPRange" );  
												
        else if ((!strcmp(Model,"63105A")) && ( VRange <=125 ))
	        viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OCP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, OCPRange), 2, "OCPRange" );  
												
        else if ((!strcmp(Model,"63105A")) && ( VRange >=126 ) && ( OCPRange == 1 ))
	        viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OCP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, OCPRange), 2, "OCPRange" );  
												
        else if ((!strcmp(Model,"63108A")) && ( VRange <=125 ))
	        viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OCP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, OCPRange), 2, "OCPRange" );  
												
        else if ((!strcmp(Model,"63108A")) && ( VRange >=126 ) && ( OCPRange == 1 ))
	        viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OCP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, OCPRange), 2, "OCPRange" );  
											
        else
            checkErr( error = CHR6310A_ERROR_INVALID_MODEL_OCP );     
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OCP_Start_Current                                                 
 * Purpose:  It sets the start current for OCP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OCP_Start_Current (ViSession vi,
                                                 ViReal64 OCPStartCurrent)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_START_CURRENT, 
												IVI_VAL_DIRECT_USER_CALL, OCPStartCurrent), 2, "OCPStartCurrent" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OCP_End_Current                                                 
 * Purpose:  It sets the end current for OCP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OCP_End_Current (ViSession vi,
                                               ViReal64 OCPEndCurrent)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_END_CURRENT, 
												IVI_VAL_DIRECT_USER_CALL, OCPEndCurrent), 2, "OCPEndCurrent" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OCP_Step_Count                                                 
 * Purpose:  It sets the step count for OCP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OCP_Step_Count (ViSession vi,
                                               ViInt32 OCPStepCount)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_OCP_STEP_COUNT, 
												IVI_VAL_DIRECT_USER_CALL, OCPStepCount), 2, "OCPStepCount" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OCP_Dwell_Time                                                 
 * Purpose:  It sets the dwell time for OCP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OCP_Dwell_Time (ViSession vi,
                                               ViInt32 OCPDwellTime)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_OCP_DWELL_TIME, 
												IVI_VAL_DIRECT_USER_CALL, OCPDwellTime), 2, "OCPDwellTime" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OCP_Trigger_Voltage                                                 
 * Purpose:  It sets the trigger voltage for OCP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OCP_Trigger_Voltage (ViSession vi,
                                                    ViReal64 OCPTriggerVoltage)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE, 
												IVI_VAL_DIRECT_USER_CALL, OCPTriggerVoltage), 2, "OCPTriggerVoltage" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OCP_Specification_Low                                                 
 * Purpose:  It sets the low level current of specification for OCP test
 *           mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OCP_Specification_Low (ViSession vi,
                                                      ViReal64 OCPSpecificationLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_SPECIFICATION_LOW, 
												IVI_VAL_DIRECT_USER_CALL, OCPSpecificationLow), 2, "OCPSpecificationLow" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OCP_Specification_High                                                 
 * Purpose:  It sets the high level current of specification for OCP test
 *           mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OCP_Specification_High (ViSession vi,
                                                       ViReal64 OCPSpecificationHigh)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_SPECIFICATION_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, OCPSpecificationHigh), 2, "OCPSpecificationHigh" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Query_OCP_Result                                                 
 * Purpose:  It returns the result of OCP test function.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Query_OCP_Result (ViSession vi,
                                             ViChar OCPResult[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	if ( OCPResult == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Query OCP Result");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "OCP:RES?\n"));
		
	    viCheckErr( viScanf (io, "%s",OCPResult));  

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OCP_Range                                                 
 * Purpose:  It returns the range set for OCP.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OCP_Range (ViSession vi,
                                          ViBoolean *getOCPRange)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOCPRange == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OCP Range");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OCP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, getOCPRange));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OCP_Start_Current                                                 
 * Purpose:  It returns the start current for OCP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OCP_Start_Current (ViSession vi,
                                                  ViReal64 *getOCPStartCurrent)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOCPStartCurrent == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OCP Start Current");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_START_CURRENT, 
												IVI_VAL_DIRECT_USER_CALL, getOCPStartCurrent));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OCP_End_Current                                                 
 * Purpose:  It returns the end current for OCP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OCP_End_Current (ViSession vi,
                                                ViReal64 *getOCPEndCurrent)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOCPEndCurrent == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OCP End Current");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_END_CURRENT, 
												IVI_VAL_DIRECT_USER_CALL, getOCPEndCurrent));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OCP_Step_Count                                                 
 * Purpose:  It returns the step count for OCP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OCP_Step_Count (ViSession vi,
                                               ViInt32 *getOCPStepCount)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOCPStepCount == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OCP Step Count");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_OCP_STEP_COUNT, 
												IVI_VAL_DIRECT_USER_CALL, getOCPStepCount));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OCP_Dwell_Time                                                 
 * Purpose:  It returns the dwell time for OCP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OCP_Dwell_Time (ViSession vi,
                                               ViInt32 *getOCPDwellTime)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOCPDwellTime == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OCP Dwell Time");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_OCP_DWELL_TIME, 
												IVI_VAL_DIRECT_USER_CALL, getOCPDwellTime));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OCP_Trigger_Voltage                                                 
 * Purpose:  It returns the setting trigger voltage value.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OCP_Trigger_Voltage (ViSession vi,
                                                    ViReal64 *getOCPTriggerVoltage)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOCPTriggerVoltage == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OCP Trigger Voltage");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE, 
												IVI_VAL_DIRECT_USER_CALL, getOCPTriggerVoltage));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OCP_Specification_Low                                                 
 * Purpose:  It returns the setting low level current of specification value.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OCP_Specification_Low (ViSession vi,
                                                      ViReal64 *getOCPSpecificationLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOCPSpecificationLow == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OCP Specification Low");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_SPECIFICATION_LOW, 
												IVI_VAL_DIRECT_USER_CALL, getOCPSpecificationLow));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OCP_Specification_High                                                 
 * Purpose:  It returns the setting high level current of specification value.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OCP_Specification_High (ViSession vi,
                                                       ViReal64 *getOCPSpecificationHigh)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOCPSpecificationHigh == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OCP Specification High");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OCP_SPECIFICATION_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, getOCPSpecificationHigh));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPP_Test                                                 
 * Purpose:  It executes or cancels the OPP Test.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPP_Test (ViSession vi,
                                         ViBoolean OPPTest)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OPP_TEST, 
												IVI_VAL_DIRECT_USER_CALL, OPPTest), 2, "OPPTest" );
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPP_Range                                                 
 * Purpose:  It sets the range for OPP execution.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPP_Range (ViSession vi,
                                          ViBoolean OPPRange)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViString    Model="";  	
	ViChar      Multi_Chan[BUFFER_SIZE]; 
/*	ViInt32		ID_NUMS=10,ID_INDEX=0;	
	ViAttr		ID[10]={CHR6310A_ATTR_OPP_START_POWER_MIN, CHR6310A_ATTR_OPP_START_POWER_MAX, CHR6310A_ATTR_OPP_END_POWER_MIN, 
	                    CHR6310A_ATTR_OPP_END_POWER_MAX, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MIN, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MAX, 
	                    CHR6310A_ATTR_OPP_SPECIFICATION_L_MIN, CHR6310A_ATTR_OPP_SPECIFICATION_L_MAX, CHR6310A_ATTR_OPP_SPECIFICATION_H_MIN, 
	                    CHR6310A_ATTR_OPP_SPECIFICATION_H_MAX
						};
*/	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
/*
   	for (ID_INDEX=0;ID_INDEX<ID_NUMS;ID_INDEX++)
    viCheckErr(Ivi_InvalidateAttribute (vi, Multi_Chan, ID[ID_INDEX]));
*/	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	if (strcmp(Model,"63107AR"))
	
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OPP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, OPPRange), 2, "OPPRange" );  
    else 
        if ( OPPRange != 1 )
	        viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OPP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, OPPRange), 2, "OPPRange" );  
        else
            checkErr( error = CHR6310A_ERROR_INVALID_MODEL_OPP );     
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPP_Start_Power                                                 
 * Purpose:  It sets the start power for OPP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPP_Start_Power (ViSession vi,
                                                ViReal64 OPPStartPower)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_START_POWER, 
												IVI_VAL_DIRECT_USER_CALL, OPPStartPower), 2, "OPPStartPower" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPP_End_Power                                                 
 * Purpose:  It sets the end power for OPP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPP_End_Power (ViSession vi,
                                              ViReal64 OPPEndPower)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_END_POWER, 
												IVI_VAL_DIRECT_USER_CALL, OPPEndPower), 2, "OPPEndPower" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPP_Step_Count                                                 
 * Purpose:  It sets the step count for OPP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPP_Step_Count (ViSession vi,
                                               ViInt32 OPPStepCount)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_OPP_STEP_COUNT, 
												IVI_VAL_DIRECT_USER_CALL, OPPStepCount), 2, "OPPStepCount" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPP_Dwell_Time                                                 
 * Purpose:  It sets the dwell time for OPP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPP_Dwell_Time (ViSession vi,
                                               ViInt32 OPPDwellTime)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_OPP_DWELL_TIME, 
												IVI_VAL_DIRECT_USER_CALL, OPPDwellTime), 2, "OPPDwellTime" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPP_Trigger_Voltage                                                 
 * Purpose:  It sets the trigger voltage for OPP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPP_Trigger_Voltage (ViSession vi,
                                                    ViReal64 OPPTriggerVoltage)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE, 
												IVI_VAL_DIRECT_USER_CALL, OPPTriggerVoltage), 2, "OCPTriggerVoltage" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPP_Specification_Low                                                 
 * Purpose:  It sets the low level power of specification for OPP test
 *           mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPP_Specification_Low (ViSession vi,
                                                      ViReal64 OPPSpecificationLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_SPECIFICATION_LOW, 
												IVI_VAL_DIRECT_USER_CALL, OPPSpecificationLow), 2, "OPPSpecificationLow" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_OPP_Specification_Low                                                 
 * Purpose:  It sets the high level power of specification for OPP test
 *           mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_OPP_Specification_High (ViSession vi,
                                                       ViReal64 OPPSpecificationHigh)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);      
	
	viCheckParm( Ivi_SetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_SPECIFICATION_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, OPPSpecificationHigh), 2, "OPPSpecificationHigh" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Query_OPP_Result                                                 
 * Purpose:  It returns the result of OPP test function.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Query_OPP_Result (ViSession vi,
                                             ViChar OPPResult[])
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	if ( OPPResult == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Query OPP Result");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "OPP:RES?\n"));
		
	    viCheckErr( viScanf (io, "%s",OPPResult));  

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OPP_Range                                                 
 * Purpose:  It returns the range set for OPP.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OPP_Range (ViSession vi,
                                          ViBoolean *getOPPRange)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOPPRange == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OPP Range");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_OPP_RANGE, 
												IVI_VAL_DIRECT_USER_CALL, getOPPRange));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OPP_Start_Power                                                 
 * Purpose:  It returns the setting start power value.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OPP_Start_Power (ViSession vi,
                                                ViReal64 *getOPPStartPower)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOPPStartPower == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OPP Start Power");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_START_POWER, 
												IVI_VAL_DIRECT_USER_CALL, getOPPStartPower));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OPP_End_Power                                                 
 * Purpose:  It returns the setting end power value.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OPP_End_Power (ViSession vi,
                                              ViReal64 *getOPPEndPower)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOPPEndPower == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OPP End Power");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_END_POWER, 
												IVI_VAL_DIRECT_USER_CALL, getOPPEndPower));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OPP_Step_Count                                                 
 * Purpose:  It returns the step count for OPP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OPP_Step_Count (ViSession vi,
                                               ViInt32 *getOPPStepCount)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOPPStepCount == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OPP Step Count");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_OPP_STEP_COUNT, 
												IVI_VAL_DIRECT_USER_CALL, getOPPStepCount));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OPP_Dwell_Time                                                 
 * Purpose:  It returns the dwell time for OPP test mode.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OPP_Dwell_Time (ViSession vi,
                                               ViInt32 *getOPPDwellTime)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOPPDwellTime == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OPP Dwell Time");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_OPP_DWELL_TIME, 
												IVI_VAL_DIRECT_USER_CALL, getOPPDwellTime));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OPP_Trigger_Voltage                                                 
 * Purpose:  It returns the setting trigger voltage value.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OPP_Trigger_Voltage (ViSession vi,
                                                    ViReal64 *getOPPTriggerVoltage)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOPPTriggerVoltage == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OPP Trigger Voltage");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE, 
												IVI_VAL_DIRECT_USER_CALL, getOPPTriggerVoltage));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OPP_Specification_Low                                                 
 * Purpose:  It returns the setting low level power of specification value.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OPP_Specification_Low (ViSession vi,
                                                      ViReal64 *getOPPSpecificationLow)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOPPSpecificationLow == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OPP Specification Low");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_SPECIFICATION_LOW, 
												IVI_VAL_DIRECT_USER_CALL, getOPPSpecificationLow));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_OPP_Specification_High                                                 
 * Purpose:  It returns the setting high level power of specification value.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_OPP_Specification_High (ViSession vi,
                                                       ViReal64 *getOPPSpecificationHigh)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getOPPSpecificationHigh == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get OPP Specification High");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_OPP_SPECIFICATION_HIGH, 
												IVI_VAL_DIRECT_USER_CALL, getOPPSpecificationHigh));

	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Mode                                                  
 * Purpose:  This command sets operational modes of the electronic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Mode (ViSession vi, ViInt32 mode)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];  
	ViString    Model="";
	ViReal64    VRange;
/*	ViInt32		ID_NUMS=42,ID_INDEX=0;	
	ViAttr		ID[42]={CHR6310A_ATTR_CC_STATIC_MIN_L1, CHR6310A_ATTR_CC_STATIC_MAX_L1, CHR6310A_ATTR_CC_STATIC_MIN_L2, CHR6310A_ATTR_CC_STATIC_MAX_L2,
						CHR6310A_ATTR_CC_RISE_SLEWRATE_MAX, CHR6310A_ATTR_CC_RISE_SLEWRATE_MIN, CHR6310A_ATTR_CC_FALL_SLEWRATE_MIN, CHR6310A_ATTR_CC_FALL_SLEWRATE_MAX,
						CHR6310A_ATTR_CC_DYNAMIC_MIN_L1, CHR6310A_ATTR_CC_DYNAMIC_MAX_L1, CHR6310A_ATTR_CC_DYNAMIC_MIN_L2, CHR6310A_ATTR_CC_DYNAMIC_MAX_L2, 						
						CHR6310A_ATTR_CC_DYNAMIC_RISE_MIN, CHR6310A_ATTR_CC_DYNAMIC_RISE_MAX, CHR6310A_ATTR_CC_DYNAMIC_FALL_MIN, CHR6310A_ATTR_CC_DYNAMIC_FALL_MAX, 						
						CHR6310A_ATTR_CC_DYNAMIC_T1_MIN, CHR6310A_ATTR_CC_DYNAMIC_T1_MAX, CHR6310A_ATTR_CC_DYNAMIC_T2_MIN, CHR6310A_ATTR_CC_DYNAMIC_T2_MAX,
						CHR6310A_ATTR_CR_STATIC_MIN_L1, CHR6310A_ATTR_CR_STATIC_MAX_L1, CHR6310A_ATTR_CR_STATIC_MIN_L2, CHR6310A_ATTR_CR_STATIC_MAX_L2,
						CHR6310A_ATTR_CR_RISE_SLEW_MIN, CHR6310A_ATTR_CR_RISE_SLEW_MAX, CHR6310A_ATTR_CR_FALL_SLEW_MIN, CHR6310A_ATTR_CR_FALL_SLEW_MAX,
						CHR6310A_ATTR_CV_STATIC_MIN_L1, CHR6310A_ATTR_CV_STATIC_MAX_L1, CHR6310A_ATTR_CV_STATIC_MIN_L2, CHR6310A_ATTR_CV_STATIC_MAX_L2,
						CHR6310A_ATTR_CV_CURRENT_MIN, CHR6310A_ATTR_CV_CURRENT_MAX, CHR6310A_ATTR_CP_STATIC_MIN_L1, CHR6310A_ATTR_CP_STATIC_MAX_L1,
						CHR6310A_ATTR_CP_STATIC_MIN_L2, CHR6310A_ATTR_CP_STATIC_MAX_L2, CHR6310A_ATTR_CP_RISE_SLEW_MIN, CHR6310A_ATTR_CP_RISE_SLEW_MAX,
						CHR6310A_ATTR_CP_FALL_SLEW_MIN, CHR6310A_ATTR_CP_FALL_SLEW_MAX
						};
*/	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN); 
    
	viCheckErr( Ivi_GetAttributeViReal64( vi, Multi_Chan, CHR6310A_ATTR_GET_CONFIGURE_VOLTAGE_RANGE,IVI_VAL_DIRECT_USER_CALL, &VRange));      
/*    
   	for (ID_INDEX=0;ID_INDEX<ID_NUMS;ID_INDEX++)
    viCheckErr(Ivi_InvalidateAttribute (vi, Multi_Chan, ID[ID_INDEX]));
*/    
    viCheckErr( Ivi_GetViInt32EntryFromIndex (CHAN-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	if (((strcmp(Model,"63107AR")) && (strcmp(Model,"63110A")) && (strcmp(Model,"63113A"))&& (strcmp(Model,"63105A")) && (strcmp(Model,"63108A"))) && ( mode != 9 && mode != 10 ))
    
     viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE, 
												IVI_VAL_DIRECT_USER_CALL, mode), 2, "mode" );
    else
        if ((!strcmp(Model,"63107AR")) && ( mode != 1 && mode != 3 && mode != 8 && mode != 9 && mode != 10))
            viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE, 
												IVI_VAL_DIRECT_USER_CALL, mode), 2, "mode" );
												
        else if ((!strcmp(Model,"63110A")) && ( mode != 2 && mode != 3 && mode != 7 && mode != 8 ))
            viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE, 
												IVI_VAL_DIRECT_USER_CALL, mode), 2, "mode" );
        else if ((!strcmp(Model,"63113A")) && ( mode != 7 && mode != 8 ))
            viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE, 
												IVI_VAL_DIRECT_USER_CALL, mode), 2, "mode" );
        												
												
        else if ((!strcmp(Model,"63105A")) && (( VRange >= 126 ) && ( mode != 0 && mode != 2 )) && ( mode != 9 && mode != 10 ))
            viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE, 
												IVI_VAL_DIRECT_USER_CALL, mode), 2, "mode" );

        else if ((!strcmp(Model,"63105A")) && (( VRange <= 125 ) &&  ( mode != 9 && mode != 10 )))
            viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE, 
												IVI_VAL_DIRECT_USER_CALL, mode), 2, "mode" );
												
        else if ((!strcmp(Model,"63108A")) && (( VRange >= 126 ) && ( mode != 0 && mode != 2 )) && ( mode != 9 && mode != 10 ))
            viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE, 
												IVI_VAL_DIRECT_USER_CALL, mode), 2, "mode" );

        else if ((!strcmp(Model,"63108A")) && (( VRange <= 125 ) &&  ( mode != 9 && mode != 10 )))
            viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE, 
												IVI_VAL_DIRECT_USER_CALL, mode), 2, "mode" );
												
        else
            checkErr( error = CHR6310A_ERROR_INVALID_MODE );     
            
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Mode                                                  
 * Purpose:  It returns the operational mode of the electronic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Mode (ViSession vi,
                                     ViChar getMode[])
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0,mode; 	
	ViChar      Multi_Chan[BUFFER_SIZE];
	ViString    cmd;      	
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_MODE, 
												IVI_VAL_DIRECT_USER_CALL, &mode));
	viCheckErr( Ivi_GetViInt32EntryFromIndex (mode, &attrModeRangeTable, VI_NULL, VI_NULL,
												VI_NULL, &cmd, VI_NULL));											
	Fmt	(getMode, "%s", cmd);
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Load_State                                                  
 * Purpose:  The LOAD command makes the electronic load active/on or
 *           inactive/off.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Load_State (ViSession vi,
                                           ViBoolean loadState)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViBoolean   Digi_io;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_DIGITAL_IO,IVI_VAL_DIRECT_USER_CALL, &Digi_io)); 
	if ( Digi_io == 0 )
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LOAD_STATE, 
												IVI_VAL_DIRECT_USER_CALL, loadState), 2, "loadState" );
	
	else
	    checkErr ( error = CHR6310A_ERROR_LOAD_STATE);   	
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Load_Short_State                                                  
 * Purpose:  It activates or inactivates the short-circuited simulation.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Load_Short_State (ViSession vi,
                                                 ViBoolean loadShortState)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViBoolean   Load_State;
	ViChar      Multi_Chan[BUFFER_SIZE];
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LOAD_STATE,IVI_VAL_DIRECT_USER_CALL, &Load_State)); 
	if ( Load_State == 1 )
	viCheckParm( Ivi_SetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LOAD_SHORT_STATE, 
												IVI_VAL_DIRECT_USER_CALL, loadShortState), 2, "loadShortState" );
	
	else
	    checkErr ( error = CHR6310A_ERROR_LOAD_SHORT_STATE);   	
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Load_Short_Key                                                  
 * Purpose:  It sets the mode of short key in the electronic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Load_Short_Key (ViSession vi,
                                               ViBoolean loadShortKey)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckParm( Ivi_SetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_LOAD_SHORT_KEY, 
												IVI_VAL_DIRECT_USER_CALL, loadShortKey), 2, "loadShortKey" );
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Load_Protection_Clear                                                  
 * Purpose:  This command resets or returns status of the electronic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Load_Protection_Clear (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "LOAD:PROT:CLE\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Load_Clear                                                  
 * Purpose:  It clears all data and return it to default.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Load_Clear (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "LOAD:CLE\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Set_Load_Save                                                  
 * Purpose:  It saves the current data as default.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Set_Load_Save (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "LOAD:SAV\n"));

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Load_State                                                  
 * Purpose:  It returns if the electronic load is active.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Load_State (ViSession vi,
                                           ViBoolean *getLoadState)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViBoolean   Digi_io;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getLoadState == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Load State");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_DIGITAL_IO,IVI_VAL_DIRECT_USER_CALL, &Digi_io)); 
	if ( Digi_io == 0 )
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LOAD_STATE, 
												IVI_VAL_DIRECT_USER_CALL, getLoadState));
	
	else
	    checkErr ( error = CHR6310A_ERROR_LOAD_STATE);   	
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Load_Short_State                                                  
 * Purpose:  It returns the short-circuit simulation state.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Load_Short_State (ViSession vi,
                                                 ViBoolean *getLoadShortState)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0;
	ViBoolean   Load_State;
	ViChar      Multi_Chan[BUFFER_SIZE]; 
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getLoadShortState == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Load Short State");
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
	
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LOAD_STATE,IVI_VAL_DIRECT_USER_CALL, &Load_State)); 
	if ( Load_State == 1 )
	viCheckErr( Ivi_GetAttributeViBoolean( vi, Multi_Chan, CHR6310A_ATTR_LOAD_SHORT_STATE, 
												IVI_VAL_DIRECT_USER_CALL, getLoadShortState));
	
	else
	    checkErr ( error = CHR6310A_ERROR_LOAD_SHORT_STATE);   	
	
	checkErr( Chr6310A_CheckStatus(vi) );

Error:
    Ivi_UnlockSession (vi, VI_NULL);      
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Load_Short_Key                                                  
 * Purpose:  It returns the mode of short key in the electronic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Load_Short_Key (ViSession vi,
                                               ViBoolean *getLoadShortKey)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if ( getLoadShortKey == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Load Short Key");
    
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_LOAD_SHORT_KEY, 
												IVI_VAL_DIRECT_USER_CALL, getLoadShortKey));
												
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Get_Load_Protection_Status                                                  
 * Purpose:  It returns the electronic load status.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Get_Load_Protection_Status (ViSession vi,
                                                       ViInt32 *loadProtectionStatus)
{
	ViStatus	error = VI_SUCCESS;
	
    checkErr( Ivi_LockSession (vi, VI_NULL));  	
    
	if ( loadProtectionStatus == VI_NULL )
    	viCheckParm( IVI_ERROR_INVALID_PARAMETER, 2, "Null address for Get Load Protection Status");
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		viCheckErr( viPrintf( io, "LOAD:PROT?\n"));
		
	    viCheckErr( viScanf (io, "%d",loadProtectionStatus));     		

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );
	
Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_RUN                                                  
 * Purpose:  It sets all electronic loads to "ON".
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_RUN (ViSession vi)
{
	ViStatus	error = VI_SUCCESS;
	ViBoolean   Digi_io;
	
    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	if (!Ivi_Simulating(vi))
    {
    
		ViSession	io= Ivi_IOSession(vi);
		
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_CONFIGURE_DIGITAL_IO,IVI_VAL_DIRECT_USER_CALL, &Digi_io));
	
	if ( Digi_io == 0 )
		viCheckErr( viPrintf( io, "RUN\n"));  	
	else
	    checkErr ( error = CHR6310A_ERROR_LOAD_STATE);   	

	}

    checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));     
    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
    
	checkErr( Chr6310A_CheckStatus(vi) );	

Error:
    Ivi_UnlockSession (vi, VI_NULL);  
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_Show_Display                                                  
 * Purpose:  It sets the the display mode of the electronic load.
******************************************************************************/
ViStatus _VI_FUNC Chr6310A_Show_Display (ViSession vi,
                                         ViInt32 display)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32     CHAN=0; 	
	ViChar      Multi_Chan[BUFFER_SIZE];  

    checkErr( Ivi_LockSession (vi, VI_NULL)); 
    
	viCheckErr( Ivi_GetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD,IVI_VAL_DIRECT_USER_CALL, &CHAN));       
    Fmt(Multi_Chan, "%s<%i", CHAN);  
    
     viCheckParm( Ivi_SetAttributeViInt32( vi, Multi_Chan, CHR6310A_ATTR_SHOW_DISPLAY, 
												IVI_VAL_DIRECT_USER_CALL, display), 2, "display" );
            
	checkErr( Chr6310A_CheckStatus(vi) );	 

Error:
    Ivi_UnlockSession (vi, VI_NULL);   
	return error;
}

/*****************************************************************************
 * Function: Chr6310A_SetAttribute<type> Functions                                    
 * Purpose:  These functions enable the instrument driver user to set 
 *           attribute values directly.  There are typesafe versions for 
 *           ViInt32, ViReal64, ViString, ViBoolean, and ViSession datatypes.                                         
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_SetAttributeViInt32 (ViSession vi, ViConstString channelName, 
                                                ViAttr attributeId, ViInt32 value)
{                                                                                                           
    return Ivi_SetAttributeViInt32 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                    value);
}                                                                                                           
ViStatus _VI_FUNC Chr6310A_SetAttributeViReal64 (ViSession vi, ViConstString channelName, 
                                                 ViAttr attributeId, ViReal64 value)
{                                                                                                           
    return Ivi_SetAttributeViReal64 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                     value);
}                                                                                                           
ViStatus _VI_FUNC Chr6310A_SetAttributeViString (ViSession vi, ViConstString channelName, 
                                                 ViAttr attributeId, ViConstString value) 
{   
    return Ivi_SetAttributeViString (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                     value);
}   
ViStatus _VI_FUNC Chr6310A_SetAttributeViBoolean (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViBoolean value)
{                                                                                                           
    return Ivi_SetAttributeViBoolean (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}                                                                                                           
ViStatus _VI_FUNC Chr6310A_SetAttributeViSession (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViSession value)
{                                                                                                           
    return Ivi_SetAttributeViSession (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}                                                                                                           

/*****************************************************************************
 * Function: Chr6310A_CheckAttribute<type> Functions                                  
 * Purpose:  These functions enable the instrument driver user to check  
 *           attribute values directly.  These functions check the value you
 *           specify even if you set the CHR6310A_ATTR_RANGE_CHECK 
 *           attribute to VI_FALSE.  There are typesafe versions for ViInt32, 
 *           ViReal64, ViString, ViBoolean, and ViSession datatypes.                         
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_CheckAttributeViInt32 (ViSession vi, ViConstString channelName, 
                                                  ViAttr attributeId, ViInt32 value)
{                                                                                                           
    return Ivi_CheckAttributeViInt32 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                      value);
}
ViStatus _VI_FUNC Chr6310A_CheckAttributeViReal64 (ViSession vi, ViConstString channelName, 
                                                   ViAttr attributeId, ViReal64 value)
{                                                                                                           
    return Ivi_CheckAttributeViReal64 (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                       value);
}                                                                                                           
ViStatus _VI_FUNC Chr6310A_CheckAttributeViString (ViSession vi, ViConstString channelName, 
                                                   ViAttr attributeId, ViConstString value)  
{   
    return Ivi_CheckAttributeViString (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                       value);
}   
ViStatus _VI_FUNC Chr6310A_CheckAttributeViBoolean (ViSession vi, ViConstString channelName, 
                                                    ViAttr attributeId, ViBoolean value)
{                                                                                                           
    return Ivi_CheckAttributeViBoolean (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                        value);
}                                                                                                           
ViStatus _VI_FUNC Chr6310A_CheckAttributeViSession (ViSession vi, ViConstString channelName, 
                                                    ViAttr attributeId, ViSession value)
{                                                                                                           
    return Ivi_CheckAttributeViSession (vi, channelName, attributeId, IVI_VAL_DIRECT_USER_CALL, 
                                        value);
}                                                                                                           

/*****************************************************************************
 * Function: Chr6310A_GetNextCoercionRecord                        
 * Purpose:  This function enables the instrument driver user to obtain
 *           the coercion information associated with the IVI session.                                                              
 *           This function retrieves and clears the oldest instance in which 
 *           the instrument driver coerced a value the instrument driver user
 *           specified to another value.                     
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_GetNextCoercionRecord (ViSession vi,
                                                  ViInt32 bufferSize,
                                                  ViChar  record[])
{
    return Ivi_GetNextCoercionString (vi, bufferSize, record);
}

/*****************************************************************************
 * Function: Chr6310A_LockSession and Chr6310A_UnlockSession Functions                        
 * Purpose:  These functions enable the instrument driver user to lock the 
 *           session around a sequence of driver calls during which other
 *           execution threads must not disturb the instrument state.
 *                                                                          
 *           NOTE:  The callerHasLock parameter must be a local variable 
 *           initialized to VI_FALSE and passed by reference, or you can pass 
 *           VI_NULL.                     
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_LockSession (ViSession vi, ViBoolean *callerHasLock)  
{                                              
    return Ivi_LockSession(vi,callerHasLock);      
}                                              
ViStatus _VI_FUNC Chr6310A_UnlockSession (ViSession vi, ViBoolean *callerHasLock) 
{                                              
    return Ivi_UnlockSession(vi,callerHasLock);    
}   

/*****************************************************************************
 * Function: Chr6310A_GetErrorInfo and Chr6310A_ClearErrorInfo Functions                       
 * Purpose:  These functions enable the instrument driver user to  
 *           get or clear the error information the driver associates with the
 *           IVI session.                                                        
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_GetErrorInfo (ViSession vi, ViStatus *primaryError, 
                                         ViStatus *secondaryError, ViChar errorElaboration[256])  
{                                                                                                           
    return Ivi_GetErrorInfo(vi, primaryError, secondaryError, errorElaboration);                                
}                                                                                                           
ViStatus _VI_FUNC Chr6310A_ClearErrorInfo (ViSession vi)                                                        
{                                                                                                           
    return Ivi_ClearErrorInfo (vi);                                                                             
}

/*****************************************************************************
 * Function: WriteInstrData and ReadInstrData Functions                      
 * Purpose:  These functions enable the instrument driver user to  
 *           write and read commands directly to and from the instrument.            
 *                                                                           
 *           Note:  These functions bypass the IVI attribute state caching.  
 *                  WriteInstrData invalidates the cached values for all 
 *                  attributes.
 *****************************************************************************/
ViStatus _VI_FUNC Chr6310A_WriteInstrData (ViSession vi, ViConstString writeBuffer)   
{   
    return Ivi_WriteInstrData (vi, writeBuffer);    
}   
ViStatus _VI_FUNC Chr6310A_ReadInstrData (ViSession vi, ViInt32 numBytes, 
                                          ViChar rdBuf[], ViInt32 *bytesRead)  
{   
    return Ivi_ReadInstrData (vi, numBytes, rdBuf, bytesRead);   
} 

/*****************************************************************************
 *-------------------- Utility Functions (Not Exported) ---------------------*
 *****************************************************************************/

/*****************************************************************************
 * Function: Chr6310A_CheckStatus                                                 
 * Purpose:  This function checks the status of the instrument to detect 
 *           whether the instrument has encountered an error.  This function  
 *           is called at the end of most exported driver functions.  If the    
 *           instrument reports an error, this function returns      
 *           IVI_ERROR_INSTR_SPECIFIC.  The user can set the 
 *           IVI_ATTR_QUERY_INSTR_STATUS attribute to VI_FALSE to disable this 
 *           check and increase execution speed.                                   
 *
 *           Note:  Call this function only when the session is locked.
 *****************************************************************************/
static ViStatus Chr6310A_CheckStatus (ViSession vi)
{
    ViStatus    error = VI_SUCCESS;

    if (Ivi_QueryInstrStatus (vi) && Ivi_NeedToCheckStatus (vi) && !Ivi_Simulating (vi))
        {
        checkErr( Chr6310A_CheckStatusCallback (vi, Ivi_IOSession(vi)));
        checkErr( Ivi_SetNeedToCheckStatus (vi, VI_FALSE));
        }
        
Error:
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_WaitForOPC                                                  
 * Purpose:  This function waits for the instrument to complete the      
 *           execution of all pending operations.  This function is a        
 *           wrapper for the WaitForOPCCallback.  It can be called from 
 *           other instrument driver functions. 
 *
 *           The maxTime parameter specifies the maximum time to wait for
 *           operation complete in milliseconds.
 *
 *           Note:  Call this function only when the session is locked.
 *****************************************************************************/
static ViStatus Chr6310A_WaitForOPC (ViSession vi, ViInt32 maxTime) 
{
    ViStatus    error = VI_SUCCESS;

    if (!Ivi_Simulating(vi))
        {
        ViInt32  oldOPCTimeout; 
        
        checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR6310A_ATTR_OPC_TIMEOUT, 
                                           0, &oldOPCTimeout));
        Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR6310A_ATTR_OPC_TIMEOUT,        
                                 0, maxTime);

        error = Chr6310A_WaitForOPCCallback (vi, Ivi_IOSession(vi));

        Ivi_SetAttributeViInt32 (vi, VI_NULL, CHR6310A_ATTR_OPC_TIMEOUT, 
                                 0, oldOPCTimeout);
        viCheckErr( error);
        }
Error:
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_DefaultInstrSetup                                               
 * Purpose:  This function sends a default setup to the instrument.  The    
 *           Chr6310A_reset function calls this function.  The 
 *           Chr6310A_IviInit function calls this function when the
 *           user passes VI_FALSE for the reset parameter.  This function is 
 *           useful for configuring settings that other instrument driver 
 *           functions require.    
 *
 *           Note:  Call this function only when the session is locked.
 *****************************************************************************/
static ViStatus Chr6310A_DefaultInstrSetup (ViSession vi)
{
    ViStatus    error = VI_SUCCESS;
        
    /* Invalidate all attributes */
    checkErr( Ivi_InvalidateAllAttributes (vi));
    
    if (!Ivi_Simulating(vi))
        {
        ViSession   io = Ivi_IOSession(vi); /* call only when locked */

        checkErr( Ivi_SetNeedToCheckStatus (vi, VI_TRUE));
	
		viCheckErr( viPrintf (io, "*CLS\n"));
	
        }
Error:
    return error;
}

/*****************************************************************************
 * Function: ReadToFile and WriteFromFile Functions                          
 * Purpose:  Functions for instrument driver developers to read/write        
 *           instrument data to/from a file.                                 
 *****************************************************************************/
static ViStatus Chr6310A_ReadToFile (ViSession vi, ViConstString filename, 
                                     ViInt32 maxBytesToRead, ViInt32 fileAction, 
                                     ViInt32 *totalBytesWritten)  
{   
    return Ivi_ReadToFile (vi, filename, maxBytesToRead, fileAction, totalBytesWritten);  
}   
static ViStatus Chr6310A_WriteFromFile (ViSession vi, ViConstString filename, 
                                        ViInt32 maxBytesToWrite, ViInt32 byteOffset, 
                                        ViInt32 *totalBytesWritten) 
{   
    return Ivi_WriteFromFile (vi, filename, maxBytesToWrite, byteOffset, totalBytesWritten); 
}

/*****************************************************************************
 *------------------------ Global Session Callbacks -------------------------*
 *****************************************************************************/

/*****************************************************************************
 * Function: Chr6310A_CheckStatusCallback                                               
 * Purpose:  This function queries the instrument to determine if it has 
 *           encountered an error.  If the instrument has encountered an 
 *           error, this function returns the IVI_ERROR_INSTRUMENT_SPECIFIC 
 *           error code.  This function is called by the 
 *           Chr6310A_CheckStatus utility function.  The 
 *           Ivi_SetAttribute and Ivi_GetAttribute functions invoke this 
 *           function when the optionFlags parameter includes the
 *           IVI_VAL_DIRECT_USER_CALL flag.
 *           
 *           The user can disable calls to this function by setting the 
 *           IVI_ATTR_QUERY_INSTR_STATUS attribute to VI_FALSE.  The driver can 
 *           disable the check status callback for a particular attribute by 
 *           setting the IVI_VAL_DONT_CHECK_STATUS flag.
 *****************************************************************************/
static ViStatus _VI_FUNC Chr6310A_CheckStatusCallback (ViSession vi, ViSession io)
{
    ViStatus    error = VI_SUCCESS;
	ViInt16     esr = 0;                 
	
		viCheckErr( viPrintf (io, "*ESR?\n"));	
	    viCheckErr( viScanf (io, "%d", &esr));
	
	    if ((esr & IEEE_488_2_QUERY_ERROR_BIT) ||
	        (esr & IEEE_488_2_DEVICE_DEPENDENT_ERROR_BIT) ||
	        (esr & IEEE_488_2_EXECUTION_ERROR_BIT) ||
	        (esr & IEEE_488_2_COMMAND_ERROR_BIT))
	            {     
	            viCheckErr( IVI_ERROR_INSTR_SPECIFIC);
	            }
	
Error:
    return error;
}

/*****************************************************************************
 * Function: Chr6310A_WaitForOPCCallback                                               
 * Purpose:  This function waits until the instrument has finished processing 
 *           all pending operations.  This function is called by the 
 *           Chr6310A_WaitForOPC utility function.  The IVI engine invokes
 *           this function in the following two cases:
 *           - Before invoking the read callback for attributes for which the 
 *             IVI_VAL_WAIT_FOR_OPC_BEFORE_READS flag is set.
 *           - After invoking the write callback for attributes for which the 
 *             IVI_VAL_WAIT_FOR_OPC_AFTER_WRITES flag is set.
 *****************************************************************************/
static ViStatus _VI_FUNC Chr6310A_WaitForOPCCallback (ViSession vi, ViSession io)
{
    ViStatus    error = VI_SUCCESS;
    /*================================================================*
    ViInt32     opcTimeout;
    ViUInt16    statusByte;

    checkErr( Ivi_GetAttributeViInt32 (vi, VI_NULL, CHR6310A_ATTR_OPC_TIMEOUT, 
                                       0, &opcTimeout));

    viCheckErr( viEnableEvent (io, VI_EVENT_SERVICE_REQ, VI_QUEUE, VI_NULL));

    viCheckErr( viPrintf (io, "*OPC"));

        // wait for SRQ 
    viCheckErr( viWaitOnEvent (io, VI_EVENT_SERVICE_REQ, opcTimeout, VI_NULL, VI_NULL));
    viCheckErr( viDisableEvent (io, VI_EVENT_SERVICE_REQ, VI_QUEUE));

        // clean up after SRQ 
    viCheckErr( viBufWrite (io, "*CLS", 4, VI_NULL));
    viCheckErr( viReadSTB (io, &statusByte));
    
Error:
    viDiscardEvents (io, VI_EVENT_SERVICE_REQ, VI_QUEUE);
     *================================================================*/

    return error;
}

/*****************************************************************************
 *----------------- Attribute Range Tables and Callbacks --------------------*
 *****************************************************************************/

/*- CHR6310A_ATTR_ID_QUERY_RESPONSE -*/

static ViStatus _VI_FUNC Chr6310AAttrIdQueryResponse_ReadCallback (ViSession vi, 
                                                                   ViSession io,
                                                                   ViConstString channelName, 
                                                                   ViAttr attributeId,
                                                                   const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      rdBuffer[BUFFER_SIZE];
    ViUInt32    retCnt;
    
    viCheckErr( viPrintf (io, "*IDN?\n"));
    viCheckErr( viRead (io, rdBuffer, BUFFER_SIZE-1, &retCnt));
    rdBuffer[retCnt] = 0;

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, rdBuffer));
    
Error:
    return error;
}
    
/*- CHR6310A_ATTR_SPECIFIC_DRIVER_REVISION -*/

static ViStatus _VI_FUNC Chr6310AAttrDriverRevision_ReadCallback (ViSession vi, 
                                                                  ViSession io,
                                                                  ViConstString channelName, 
                                                                  ViAttr attributeId,
                                                                  const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      driverRevision[256];
    
    
    sprintf (driverRevision, 
             "Driver: Chr6310A %.2f, Compiler: %s %3.2f, "
             "Components: IVIEngine %.2f, VISA-Spec %.2f",
             CHR6310A_MAJOR_VERSION + CHR6310A_MINOR_VERSION/1000.0, 
             IVI_COMPILER_NAME, IVI_COMPILER_VER_NUM, 
             IVI_ENGINE_MAJOR_VERSION + IVI_ENGINE_MINOR_VERSION/1000.0, 
             Ivi_ConvertVISAVer(VI_SPEC_VERSION));

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, driverRevision));    
Error:
    return error;
}


/*- CHR6310A_ATTR_INSTRUMENT_FIRMWARE_REVISION -*/

static ViStatus _VI_FUNC Chr6310AAttrFirmwareRevision_ReadCallback (ViSession vi, 
                                                                    ViSession io,
                                                                    ViConstString channelName, 
                                                                    ViAttr attributeId,
                                                                    const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      instrRev[BUFFER_SIZE];
    
    viCheckErr( viPrintf (io, "*IDN?\n"));

	viCheckErr( viScanf (io, "%*[^,],%*[^,],%*[^,],%256[^\n]", instrRev));

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, instrRev));
    
Error:
    return error;
}

/*- CHR6310A_ATTR_INSTRUMENT_MANUFACTURER -*/

static ViStatus _VI_FUNC Chr6310AAttrInstrumentManufacturer_ReadCallback (ViSession vi, 
                                                                          ViSession io,
                                                                          ViConstString channelName, 
                                                                          ViAttr attributeId,
                                                                          const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      rdBuffer[BUFFER_SIZE];
    ViChar      idQ[BUFFER_SIZE];
    
    checkErr( Ivi_GetAttributeViString (vi, "", CHR6310A_ATTR_ID_QUERY_RESPONSE,
                                        0, BUFFER_SIZE, idQ));
    sscanf (idQ, "%256[^,]", rdBuffer);

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, rdBuffer));
    
Error:
    return error;
}

    

/*- CHR6310A_ATTR_INSTRUMENT_MODEL -*/

static ViStatus _VI_FUNC Chr6310AAttrInstrumentModel_ReadCallback (ViSession vi, 
                                                                   ViSession io,
                                                                   ViConstString channelName, 
                                                                   ViAttr attributeId,
                                                                   const ViConstString cacheValue)
{
    ViStatus    error = VI_SUCCESS;
    ViChar      rdBuffer[BUFFER_SIZE];
    ViChar      idQ[BUFFER_SIZE];
    
    checkErr( Ivi_GetAttributeViString (vi, "", CHR6310A_ATTR_ID_QUERY_RESPONSE,
                                        0, BUFFER_SIZE, idQ));
    sscanf (idQ, "%*[^,],%256[^,]", rdBuffer);

    checkErr( Ivi_SetValInStringCallback (vi, attributeId, rdBuffer));
    
Error:
    return error;
}
    


static ViStatus _VI_FUNC Chr6310AAttrStandardEventStatusEnable_ReadCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "*ESE?\n"));

	viCheckErr( viScanf (io, "%d",value));
	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrStandardEventStatusEnable_WriteCallback (ViSession vi,
                                                                              ViSession io,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "*ESE %d\n", value));       	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrRecallInstrumentState_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "*RCL %d\n", value));    	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSaveInstrumentState_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "*SAV %d\n", value));    		

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrServiceRequestEnable_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "*SRE?\n"));

	viCheckErr( viScanf (io, "%d",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrServiceRequestEnable_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "*SRE %d\n", value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrChannelLoad_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "CHAN?\n"));

	viCheckErr( viScanf (io, "%d",value));
	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrChannelLoad_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CHAN %d\n", value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrQueryMinChannelLoad_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "CHAN? MIN\n"));

	viCheckErr( viScanf (io, "%d",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrQueryMaxChannelLoad_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "CHAN? MAX\n"));

	viCheckErr( viScanf (io, "%d",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrChannelActive_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CHAN:ACT %d\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrChannelSynchronized_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CHAN:SYNC?\n"));

	viCheckErr( viScanf (io, "%d",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrChannelSynchronized_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CHAN:SYNC %d\n", value));  	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVon_RangeTableCallback (ViSession vi,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel,mode;
	ViReal64    getVRange;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_GET_CONFIGURE_VOLTAGE_RANGE,IVI_VAL_DIRECT_USER_CALL, &getVRange));
	viCheckErr( Ivi_GetAttributeViInt32( vi, channelName, CHR6310A_ATTR_MODE,IVI_VAL_DIRECT_USER_CALL, &mode));  
	
	if (( mode ==0 ) || ( mode == 1) || ( mode == 2) || ( mode == 3))
	{
       if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || ( !strcmp(Model,"63112A")) ){
          if ( getVRange >=0 && getVRange <= 16 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 16, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 16 && getVRange <= 80 )
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));} 
             
       else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A"))){            
          if ( getVRange >=0 && getVRange <= 125 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 125, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 125 && getVRange <= 500 )
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
             
       else if (!strcmp(Model,"63110A")){            
          if ( getVRange >=0 && getVRange <= 100 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 100 && getVRange <= 500 )
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
                
       else if (!strcmp(Model,"63113A")){            
          if ( getVRange >=0 && getVRange <= 60 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 60 && getVRange <= 300 )
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));}
                
       else if (!strcmp(Model,"63123A")){            
          if ( getVRange >=0 && getVRange <= 24 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 24, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 24 && getVRange <= 120 )
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));}
                
                
                
                
      
     }          
     
     else if (( mode == 4 ) || ( mode == 5 )){
         
          if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63112A")) ){
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 16, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));} 
             
          else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A"))){  
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 125, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
             
          else if (!strcmp(Model,"63110A")){   
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
         
          
          else if (!strcmp(Model,"63113A")){   
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));} 
        
          else if (!strcmp(Model,"63123A")){   
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 24, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));} 
             
             
             
         }   
         
     else if (( mode == 6 ) || ( mode == 7) || ( mode == 8 )){
         
          if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63112A")) ){
    
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));} 
             
          else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63110A"))){            
             
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));}   
          else if ( (!strcmp(Model,"63113A"))){            
             
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));}  
          else if ( (!strcmp(Model,"63123A"))){            
             
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));}   
         
          
         }     
         
     else {
         
          if (!strcmp(Model,"63110A"))
          {
          
    		 if ( mode == 9) 
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));
           } 
          if (!strcmp(Model,"63113A"))
          {
          
    		 if ( mode == 9) 
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));
           } 
           
         }   
             
Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVon_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:VOLT:ON?\n"));

	viCheckErr( viScanf (io, "%Lf",value));
	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVon_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:VOLT:ON %.3f\n", value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoltageRange_RangeTableCallback (ViSession vi,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
    
    if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63112A")) /*|| (!strcmp(Model,"63123A"))*/){
    
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63110A"))){            
             
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));}  
             
   
    else if (!strcmp(Model,"63113A")){            
             
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));}  //sam  
    else if (!strcmp(Model,"63123A")){            
             
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));}  //sam  
    

             
Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrGetConfigureVoltageRange_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:VOLT:RANG?\n"));

	viCheckErr( viScanf (io, "%Lf",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoltageRange_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:VOLT:RANG %.3f\n", value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoltageLatch_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:VOLT:LATC?\n"));

	viCheckErr( viScanf (io, "%d",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoltageLatch_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:VOLT:LATC %d\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureAutoLoad_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "CONF:AUTO:LOAD?\n"));

	viCheckErr( viScanf (io, "%d",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureAutoLoad_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:AUTO:LOAD %d\n", value));  	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureAutoMode_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;

 	viCheckErr( viPrintf (io, "CONF:AUTO:MODE?\n"));

	viCheckErr( viScanf (io, "%d",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureAutoMode_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:AUTO:MODE %d\n", value));  	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureSound_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:SOUND?\n", value));	
	
	viCheckErr( viScanf (io, "%d",value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureSound_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;

	viCheckErr( viPrintf (io, "CONF:SOUND %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureRemote_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:REM %d\n", value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureLoad_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:LOAD?\n", value));	
	
	viCheckErr( viScanf (io, "%d",value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureLoad_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:LOAD %d\n", value)); 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingState_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:TIM:STAT?\n"));

	viCheckErr( viScanf (io, "%d",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingState_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:TIM:STAT %d\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTrigger_RangeTableCallback (ViSession vi,
                                                                                ViConstString channelName,
                                                                                ViAttr attributeId,
                                                                                IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViReal64    getVRange;  	
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE,IVI_VAL_DIRECT_USER_CALL, &getVRange));
    
    if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || ( !strcmp(Model,"63112A")) ){
       if ( getVRange >=0 && getVRange <= 16 )	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 16, VI_NULL, VI_NULL, VI_NULL));
             
             else if ( getVRange > 16 && getVRange <= 80 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A"))){            
       if ( getVRange >=0 && getVRange <= 125 )	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 125, VI_NULL, VI_NULL, VI_NULL));
             
             else if ( getVRange > 125 && getVRange <= 500 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if (!strcmp(Model,"63110A")){            
       if ( getVRange >=0 && getVRange <= 100 )	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));
             
             else if ( getVRange > 100 && getVRange <= 500 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
    else if (!strcmp(Model,"63113A")){            
       if ( getVRange >=0 && getVRange <= 60)	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL));
             
             else if ( getVRange > 60 && getVRange <= 300 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));} 
    else if (!strcmp(Model,"63123A")){            
       if ( getVRange >=0 && getVRange <= 24)	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 24, VI_NULL, VI_NULL, VI_NULL));
             
             else if ( getVRange > 24 && getVRange <= 120 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));} 
   
   
             
Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTrigger_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:TIM:TRIG?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTrigger_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:TIM:TRIG %.3f\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTimeout_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:TIM:TIMEOUT?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureTimingTimeout_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:TIM:TIMEOUT %d\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffState_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:VOFF:STAT?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffState_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:VOFF:STAT %d\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffFinalV_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel,mode;
	ViReal64    getVRange;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE,IVI_VAL_DIRECT_USER_CALL, &getVRange));
	viCheckErr( Ivi_GetAttributeViInt32( vi, channelName, CHR6310A_ATTR_MODE,IVI_VAL_DIRECT_USER_CALL, &mode));  
	
	if (( mode ==0 ) || ( mode == 1) || ( mode == 2) || ( mode == 3))
	{
       if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || ( !strcmp(Model,"63112A")) ){
          if ( getVRange >=0 && getVRange <= 16 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 16, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 16 && getVRange <= 80 )
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));} 
             
       else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A"))){            
          if ( getVRange >=0 && getVRange <= 125 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 125, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 125 && getVRange <= 500 )
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
             
       else if (!strcmp(Model,"63110A")){            
          if ( getVRange >=0 && getVRange <= 100 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 100 && getVRange <= 500 )
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
       else if (!strcmp(Model,"63113A")){            
          if ( getVRange >=0 && getVRange <= 60 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 60 && getVRange <= 300 )
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0,300, VI_NULL, VI_NULL, VI_NULL));} 
       else if (!strcmp(Model,"63123A")){            
          if ( getVRange >=0 && getVRange <= 24 )	
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 24, VI_NULL, VI_NULL, VI_NULL));
             
                else if ( getVRange > 24 && getVRange <= 120)
                viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0,120, VI_NULL, VI_NULL, VI_NULL));} 
      
      
     }          
     
     else if (( mode == 4 ) || ( mode == 5 )){
         
          if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63112A"))){
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 16, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));} 
             
          else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A"))){  
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 125, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
             
          else if (!strcmp(Model,"63110A")){   
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));}
          else if (!strcmp(Model,"63113A")){   
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));} 
          else if (!strcmp(Model,"63123A")){   
          
    		 if ( mode == 4 )
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 24, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));} 
         
         
             
         }   
         
     else if (( mode == 6 ) || ( mode == 7) || ( mode ==8 )){
         
          if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63112A")) || (!strcmp(Model,"63123A"))){
    
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));} 
             
          else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63110A"))){            
             
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));}   
          else if ( (!strcmp(Model,"63113A"))){            
             
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));}
          else if ( (!strcmp(Model,"63123A"))){            
             
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));}   
          
         
         }     
         
     else {
         
          if (!strcmp(Model,"63110A")){
          
    		 if ( mode == 9) 
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
          if (!strcmp(Model,"63113A")){
          
    		 if ( mode == 9) 
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL)); 
    		 else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));} 
          
         }   
             
Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffFinalV_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:VOFF:FINALVOLT?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureVoffFinalV_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:VOFF:FINALVOLT %.3f\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureMeasureAverage_ReadCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:MEAS:AVE?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureMeasureAverage_WriteCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:MEAS:AVE %d\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureDigitalIo_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "CONF:DIGIT?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureDigitalIo_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:DIGIT %d\n", value));   		

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureKey_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:KEY?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureKey_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CONF:KEY %d\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureEcho_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "CONF:ECHO?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrConfigureEcho_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:ECHO %d\n", value));  	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL1_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_STATIC_MIN_L1,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_STATIC_MAX_L1,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL1_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:L1?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL1_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "CURR:STAT:L1 %.3f\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrMode_ReadCallback (ViSession vi,
                                                        ViSession io,
                                                        ViConstString channelName,
                                                        ViAttr attributeId,
                                                        ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
    ViChar      rdBuffer[BUFFER_SIZE];  	
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "MODE?\n"));

	viCheckErr( viScanf (io, "%s",rdBuffer));  
	
    if (!strcmp(rdBuffer,"CCL"))
        *value = 0;
        
       else if (!strcmp(rdBuffer,"CCH")) 
				*value = 1; 
				
       else if (!strcmp(rdBuffer,"CCDL")) 
				*value = 2; 
				
       else if (!strcmp(rdBuffer,"CCDH")) 
				*value = 3; 
				
       else if (!strcmp(rdBuffer,"CRL")) 
				*value = 4; 
				
       else if (!strcmp(rdBuffer,"CRH")) 
				*value = 5; 
				
       else if (!strcmp(rdBuffer,"CV")) 
				*value = 6; 
				
       else if (!strcmp(rdBuffer,"CPL")) 
				*value = 7; 
				
       else if (!strcmp(rdBuffer,"CPH")) 
				*value = 8; 
				
       else if (!strcmp(rdBuffer,"LEDL")) 
				*value = 9; 

       else if (!strcmp(rdBuffer,"LEDH")) 
				*value = 10; 
				
				
Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrMode_WriteCallback (ViSession vi,
                                                         ViSession io,
                                                         ViConstString channelName,
                                                         ViAttr attributeId,
                                                         ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString    cmdString="";
	ViInt32		ChannelNumber;
	ViInt32		ID_NUMS=42,ID_INDEX=0;	
	ViAttr		ID[42]={CHR6310A_ATTR_CC_STATIC_MIN_L1, CHR6310A_ATTR_CC_STATIC_MAX_L1, CHR6310A_ATTR_CC_STATIC_MIN_L2, CHR6310A_ATTR_CC_STATIC_MAX_L2,
						CHR6310A_ATTR_CC_RISE_SLEWRATE_MAX, CHR6310A_ATTR_CC_RISE_SLEWRATE_MIN, CHR6310A_ATTR_CC_FALL_SLEWRATE_MIN, CHR6310A_ATTR_CC_FALL_SLEWRATE_MAX,
						CHR6310A_ATTR_CC_DYNAMIC_MIN_L1, CHR6310A_ATTR_CC_DYNAMIC_MAX_L1, CHR6310A_ATTR_CC_DYNAMIC_MIN_L2, CHR6310A_ATTR_CC_DYNAMIC_MAX_L2, 						
						CHR6310A_ATTR_CC_DYNAMIC_RISE_MIN, CHR6310A_ATTR_CC_DYNAMIC_RISE_MAX, CHR6310A_ATTR_CC_DYNAMIC_FALL_MIN, CHR6310A_ATTR_CC_DYNAMIC_FALL_MAX, 						
						CHR6310A_ATTR_CC_DYNAMIC_T1_MIN, CHR6310A_ATTR_CC_DYNAMIC_T1_MAX, CHR6310A_ATTR_CC_DYNAMIC_T2_MIN, CHR6310A_ATTR_CC_DYNAMIC_T2_MAX,
						CHR6310A_ATTR_CR_STATIC_MIN_L1, CHR6310A_ATTR_CR_STATIC_MAX_L1, CHR6310A_ATTR_CR_STATIC_MIN_L2, CHR6310A_ATTR_CR_STATIC_MAX_L2,
						CHR6310A_ATTR_CR_RISE_SLEW_MIN, CHR6310A_ATTR_CR_RISE_SLEW_MAX, CHR6310A_ATTR_CR_FALL_SLEW_MIN, CHR6310A_ATTR_CR_FALL_SLEW_MAX,
						CHR6310A_ATTR_CV_STATIC_MIN_L1, CHR6310A_ATTR_CV_STATIC_MAX_L1, CHR6310A_ATTR_CV_STATIC_MIN_L2, CHR6310A_ATTR_CV_STATIC_MAX_L2,
						CHR6310A_ATTR_CV_CURRENT_MIN, CHR6310A_ATTR_CV_CURRENT_MAX, CHR6310A_ATTR_CP_STATIC_MIN_L1, CHR6310A_ATTR_CP_STATIC_MAX_L1,
						CHR6310A_ATTR_CP_STATIC_MIN_L2, CHR6310A_ATTR_CP_STATIC_MAX_L2, CHR6310A_ATTR_CP_RISE_SLEW_MIN, CHR6310A_ATTR_CP_RISE_SLEW_MAX,
						CHR6310A_ATTR_CP_FALL_SLEW_MIN, CHR6310A_ATTR_CP_FALL_SLEW_MAX
						};
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	
   	for (ID_INDEX=0;ID_INDEX<ID_NUMS;ID_INDEX++)
    viCheckErr(Ivi_InvalidateAttribute (vi, channelName, ID[ID_INDEX]));//when mode change then invalid attribute
	
    viCheckErr(Ivi_GetViInt32EntryFromValue (value, &attrModeRangeTable, VI_NULL, VI_NULL, VI_NULL, VI_NULL, &cmdString, VI_NULL));	
	viCheckErr( viPrintf (io, "MODE %s\n", cmdString));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL2_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;  	
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
   
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_STATIC_MIN_L2,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_STATIC_MAX_L2,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL2_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:L2?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticL2_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CURR:STAT:L2 %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcStaticMaxL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:L1? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  
	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcStaticMinL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:L1? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  
	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcStaticMaxL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value){
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:L2? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  
	
Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcStaticMinL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:L2? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcRiseSlewrateMax_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:RISE? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  
	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcRiseSlewrateMin_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:RISE? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcFallSlewrateMax_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:FALL? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcFallSlewrateMin_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:FALL? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticRiseSlew_RangeTableCallback (ViSession vi,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;  	
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
   
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_RISE_SLEWRATE_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_RISE_SLEWRATE_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticRiseSlew_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:RISE?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticRiseSlew_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CURR:STAT:RISE %.5f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticFallSlew_RangeTableCallback (ViSession vi,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;  	
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
   
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_FALL_SLEWRATE_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_FALL_SLEWRATE_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticFallSlew_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:STAT:FALL?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentStaticFallSlew_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CURR:STAT:FALL %.5f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicMaxL1_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:L1? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  
	
Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicMinL1_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:L1? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicMaxL2_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:L2? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicMinL2_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:L2? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL1_RangeTableCallback (ViSession vi,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;  	
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
   
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_MIN_L1,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_MAX_L1,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL1_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:L1?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL1_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CURR:DYN:L1 %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL2_RangeTableCallback (ViSession vi,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;  	
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
   
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_MIN_L2,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_MAX_L2,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL2_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value){
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:L2?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicL2_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value){
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CURR:DYN:L2 %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicRiseMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:RISE? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicRiseMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:RISE? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicFallMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:FALL? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicFallMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:FALL? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicRiseSlew_RangeTableCallback (ViSession vi,
                                                                                ViConstString channelName,
                                                                                ViAttr attributeId,
                                                                                IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;  	
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
   
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_RISE_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_RISE_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicRiseSlew_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:RISE?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicRiseSlew_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CURR:DYN:RISE %.5f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicFallSlew_RangeTableCallback (ViSession vi,
                                                                                ViConstString channelName,
                                                                                ViAttr attributeId,
                                                                                IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;  	
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
   
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_FALL_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_FALL_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicFallSlew_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:FALL?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicFallSlew_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CURR:DYN:FALL %.5f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicT1Max_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:T1? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicT1Min_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:T1? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicT2Max_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:T2? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCcDynamicT2Min_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:T2? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT1_RangeTableCallback (ViSession vi,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;  	
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
   
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_T1_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_T1_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT1_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:T1?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT1_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CURR:DYN:T1 %.6f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT2_RangeTableCallback (ViSession vi,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;  	
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
   
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_T2_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CC_DYNAMIC_T2_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT2_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CURR:DYN:T2?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCurrentDynamicT2_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CURR:DYN:T2 %.6f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLoadState_ReadCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "LOAD?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLoadState_WriteCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "LOAD %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLoadShortState_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "LOAD:SHOR?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLoadShortState_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "LOAD:SHOR %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLoadShortKey_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "LOAD:SHOR:KEY?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLoadShortKey_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "LOAD:SHOR:KEY %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrMeasureInput_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "MEAS:INP?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrMeasureInput_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "MEAS:INP %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrMeasureScan_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "MEAS:SCAN?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrMeasureScan_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "MEAS:SCAN %d\n", value));    	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramFile_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:FILE?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramFile_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:FILE %d\n", value));    	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramActive_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:ACT?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramActive_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:ACT %d\n", value));  	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramChain_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:CHA?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramChain_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:CHA %d\n", value));  	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTimeMax_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:OFFT? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTimeMin_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:OFFT? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTimeMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:ONT? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTimeMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:ONT? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTimeMax_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:PFDT? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTimeMin_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:PFDT? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTime_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64    min,max;
	
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_PROGRAM_ON_TIME_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_PROGRAM_ON_TIME_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTime_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:ONT?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOnTime_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;

    viCheckErr( viPrintf (io, "PROG:ONT %.1f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTime_RangeTableCallback (ViSession vi,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64    min,max;
	
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_PROGRAM_OFF_TIME_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_PROGRAM_OFF_TIME_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTime_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:OFFT?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramOffTime_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:OFFT %.1f\n", value));    	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTime_RangeTableCallback (ViSession vi,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViReal64    min,max;
	
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTime_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:PFDT?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramPfDelayTime_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:PFDT %.1f\n", value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSequence_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:SEQ?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSequence_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:SEQ %d\n", value));    	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSequenceMode_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
    ViChar      rdBuffer[BUFFER_SIZE];  	
	
 	viCheckErr( viPrintf (io, "PROG:SEQ:MODE?\n"));

	viCheckErr( viScanf (io, "%s",rdBuffer));  
	
    if (!strcmp(rdBuffer,"SKIP"))
        *value = 0;
        
       else if (!strcmp(rdBuffer,"AUTO")) 
				*value = 1; 
				
       else if (!strcmp(rdBuffer,"MANUAL")) 
				*value = 2; 
				
       else if (!strcmp(rdBuffer,"EXT")) 
				*value = 3; 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSequenceMode_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString    cmdString="";
	
    viCheckErr(Ivi_GetViInt32EntryFromValue (value, &attrSequenceModeRangeTable, VI_NULL, VI_NULL, VI_NULL, VI_NULL, &cmdString, VI_NULL));	
	viCheckErr( viPrintf (io, "PROG:SEQ:MODE %s\n", cmdString));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSequenceShortChannel_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:SEQ:SHOR:CHAN?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSequenceShortChannel_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:SEQ:SHOR:CHAN %d\n", value));    	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSequenceShortTime_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:SEQ:SHOR:TIME?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSequenceShortTime_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:SEQ:SHOR:TIME %.1f\n", value));  	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramRun_ReadCallback (ViSession vi,
                                                              ViSession io,
                                                              ViConstString channelName,
                                                              ViAttr attributeId,
                                                              ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "PROG:RUN?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramRun_WriteCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:RUN %d\n", value));    	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrProgramKey_WriteCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "PROG:KEY %d\n", value));    	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCrStaticMaxL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:L1? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCrStaticMinL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:L1? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCrStaticMaxL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:L2? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCrStaticMinL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:L2? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCrRiseSlewMax_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:RISE? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCrRiseSlewMin_ReadCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:RISE? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCrFallSlewMax_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:FALL? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCrFallSlewMin_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:FALL? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL1_RangeTableCallback (ViSession vi,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CR_STATIC_MIN_L1,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CR_STATIC_MAX_L1,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL1_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:L1?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL1_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "RES:L1 %.5f\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL2_RangeTableCallback (ViSession vi,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CR_STATIC_MIN_L2,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CR_STATIC_MAX_L2,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL2_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:L2?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticL2_WriteCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "RES:L2 %.5f\n", value));   	 	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticRiseSlew_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CR_RISE_SLEW_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CR_RISE_SLEW_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticRiseSlew_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:RISE?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticRiseSlew_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "RES:RISE %.5f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticFallSlew_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CR_FALL_SLEW_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CR_FALL_SLEW_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticFallSlew_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "RES:FALL?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrResistanceStaticFallSlew_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "RES:FALL %.5f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrShowDisplay_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViString    cmdString="";
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr(Ivi_GetViInt32EntryFromValue (value, &attrShowDisplayRangeTable, VI_NULL, VI_NULL, VI_NULL, VI_NULL, &cmdString, VI_NULL));	
	viCheckErr( viPrintf (io, "SHOW:DISP %s\n", cmdString));  
	
Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationUnit_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "SPEC:UNIT?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationUnit_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	
    viCheckErr( viPrintf (io, "SPEC:UNIT %d\n", value)); 
    
Error:
	return error;
}


static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageCenter_RangeTableCallback (ViSession vi,
                                                                                    ViConstString channelName,
                                                                                    ViAttr attributeId,
                                                                                    IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
    
    if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63112A"))  ){
 
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));}
             
    else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63110A"))){        
    
                  viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));} 
    
    else if ((!strcmp(Model,"63113A")))
             
            {
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));
    		}
    else if	((!strcmp(Model,"63123A")))
            {
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));
    		}
            

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageCenter_ReadCallback (ViSession vi,
                                                                              ViSession io,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:VOLT:C?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageCenter_WriteCallback (ViSession vi,
                                                                               ViSession io,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "SPEC:VOLT:C %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageHigh_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViBoolean   Spec_Unit;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_SPECIFICATION_UNIT,IVI_VAL_DIRECT_USER_CALL, &Spec_Unit));
    
    if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) || (!strcmp(Model,"63112A")) ){
    	if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63110A"))){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if ((!strcmp(Model,"63113A")))
             
            {if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL)); 
             
    		}
    else if	((!strcmp(Model,"63123A")))
            {if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));
             else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));
    		}
    
    

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageHigh_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:VOLT:H?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageHigh_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "SPEC:VOLT:H %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageLow_RangeTableCallback (ViSession vi,
                                                                                 ViConstString channelName,
                                                                                 ViAttr attributeId,
                                                                                 IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViBoolean   Spec_Unit;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_SPECIFICATION_UNIT,IVI_VAL_DIRECT_USER_CALL, &Spec_Unit));
    
    if ((!strcmp(Model,"63101A")) || (!strcmp(Model,"63102A")) || (!strcmp(Model,"63103A")) || (!strcmp(Model,"63106A")) || (!strcmp(Model,"63107AL")) || (!strcmp(Model,"63107AR")) ||(!strcmp(Model,"63112A")) ){
    	if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 80, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if ((!strcmp(Model,"63105A")) || (!strcmp(Model,"63108A")) || (!strcmp(Model,"63110A"))){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 500, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
    else if ((!strcmp(Model,"63113A")))
             
            {if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL)); 
             
    		}
    else if	((!strcmp(Model,"63123A")))
            {if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));
             else
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));
    		}
    
    

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageLow_ReadCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:VOLT:L?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationVoltageLow_WriteCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "SPEC:VOLT:L %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentCenter_RangeTableCallback (ViSession vi,
                                                                                    ViConstString channelName,
                                                                                    ViAttr attributeId,
                                                                                    IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
    
    if (!strcmp(Model,"63101A")){
    
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 40, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if (!strcmp(Model,"63102A")){        
    	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 20, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63103A")){        
    		
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63105A")){        
    		
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 10, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63106A")){        
    
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63107AL")){        
    	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 40, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63107AR")){        
    	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 5, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63108A")){        
    	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 20, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if (!strcmp(Model,"63110A")){        
    	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 2, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63112A")){        
    	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 240, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if (!strcmp(Model,"63123A")){        
    	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 70, VI_NULL, VI_NULL, VI_NULL));} 
    else if (!strcmp(Model,"63113A")){        
    	
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 20, VI_NULL, VI_NULL, VI_NULL));} 
    

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentCenter_ReadCallback (ViSession vi,
                                                                              ViSession io,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:CURR:C?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentCenter_WriteCallback (ViSession vi,
                                                                               ViSession io,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "SPEC:CURR:C %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentHigh_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViBoolean   Spec_Unit;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_SPECIFICATION_UNIT,IVI_VAL_DIRECT_USER_CALL, &Spec_Unit));
    
    if (!strcmp(Model,"63101A")){
    	if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 40, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if (!strcmp(Model,"63102A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 20, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63103A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63105A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 10, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63106A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63107AL")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 40, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63107AR")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 5, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63108A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 20, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63110A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 2, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63112A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 240, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63123A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 70, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
    else if (!strcmp(Model,"63113A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 20, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentHigh_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:CURR:H?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentHigh_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "SPEC:CURR:H %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentLow_RangeTableCallback (ViSession vi,
                                                                                 ViConstString channelName,
                                                                                 ViAttr attributeId,
                                                                                 IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViBoolean   Spec_Unit;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_SPECIFICATION_UNIT,IVI_VAL_DIRECT_USER_CALL, &Spec_Unit));
    
    if (!strcmp(Model,"63101A")){
    	if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 40, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
             
    else if (!strcmp(Model,"63102A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 20, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63103A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 60, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63105A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 10, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63106A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 120, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63107AL")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 40, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63107AR")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 5, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63108A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 20, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63110A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 2, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 

    else if (!strcmp(Model,"63112A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 240, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    else if (!strcmp(Model,"63123A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 70, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
    else if (!strcmp(Model,"63123A")){        
    		 if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 20, VI_NULL, VI_NULL, VI_NULL)); 
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentLow_ReadCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:CURR:L?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationCurrentLow_WriteCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "SPEC:CURR:L %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationTest_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:TEST?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationTest_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViString    cmdString="";
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "SPEC:TEST %d\n", value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCvStaticMaxL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:L1? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCvStaticMinL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:L1? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCvStaticMaxL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:L2? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCvStaticMinL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:L2? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCvCurrentMax_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:CURR? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCvCurrentMin_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:CURR? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL1_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CV_STATIC_MIN_L1,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CV_STATIC_MAX_L1,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL1_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:L1?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL1_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "VOLT:L1 %.2f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL2_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CV_STATIC_MIN_L2,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CV_STATIC_MAX_L2,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL2_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:L2?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageStaticL2_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "VOLT:L2 %.2f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageCurrentLimit_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CV_CURRENT_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CV_CURRENT_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageCurrentLimit_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:CURR?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageCurrentLimit_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "VOLT:CURR %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageResponseSpeed_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "VOLT:MODE?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrVoltageResponseSpeed_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "VOLT:MODE %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCpFallSlewMax_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:FALL? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCpFallSlewMin_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:FALL? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCpRiseSlewMax_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:RISE? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCpRiseSlewMin_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:RISE? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCpStaticMaxL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:L1? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCpStaticMinL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:L1? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCpStaticMaxL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:L2? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrCpStaticMinL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:L2? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL1_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CP_STATIC_MIN_L1,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CP_STATIC_MAX_L1,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL1_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:L1?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL1_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "POW:STAT:L1 %.4f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL2_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CP_STATIC_MIN_L2,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CP_STATIC_MAX_L2,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL2_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:L2?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticL2_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "POW:STAT:L2 %.4f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticFallSlew_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CP_FALL_SLEW_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CP_FALL_SLEW_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticFallSlew_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:FALL?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticFallSlew_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "POW:STAT:FALL %.4f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticRiseSlew_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CP_RISE_SLEW_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_CP_RISE_SLEW_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticRiseSlew_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "POW:STAT:RISE?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrPowerStaticRiseSlew_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "POW:STAT:RISE %.4f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpTest_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OCP %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpRange_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:RANG?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpRange_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
	ViInt32		ID_NUMS=10,ID_INDEX=0;	
	ViAttr		ID[10]={CHR6310A_ATTR_OCP_START_CURRENT_MIN, CHR6310A_ATTR_OCP_START_CURRENT_MAX, CHR6310A_ATTR_OCP_END_CURRENT_MIN, 
	                    CHR6310A_ATTR_OCP_END_CURRENT_MAX, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MIN, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MAX, 
	                    CHR6310A_ATTR_OCP_SPECIFICATION_L_MIN, CHR6310A_ATTR_OCP_SPECIFICATION_L_MAX, CHR6310A_ATTR_OCP_SPECIFICATION_H_MIN, 
	                    CHR6310A_ATTR_OCP_SPECIFICATION_H_MAX
						};
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
												
   	for (ID_INDEX=0;ID_INDEX<ID_NUMS;ID_INDEX++)
    viCheckErr(Ivi_InvalidateAttribute (vi, channelName, ID[ID_INDEX]));
												
    viCheckErr( viPrintf (io, "OCP:RANG %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrentMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:IEND? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrentMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:IEND? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrentMax_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:ISTA? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrentMin_ReadCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:ISTA? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltageMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:TRIG:VOLT? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltageMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:TRIG:VOLT? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:SPEC:L? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:SPEC:L? MIN\n"));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:SPEC:H? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:SPEC:H? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrent_RangeTableCallback (ViSession vi,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_START_CURRENT_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_START_CURRENT_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrent_ReadCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:ISTA?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpStartCurrent_WriteCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OCP:ISTA %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrent_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_END_CURRENT_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_END_CURRENT_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrent_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:IEND?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpEndCurrent_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OCP:IEND %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpStepCount_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:STEP?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpStepCount_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OCP:STEP %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpDwellTime_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OCP:DWEL?\n")); 
    
	viCheckErr( viScanf (io, "%d",value)); 
	
Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpDwellTime_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OCP:DWEL %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltage_RangeTableCallback (ViSession vi,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltage_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:TRIG:VOLT?\n"));

	viCheckErr( viScanf (io, "%Lf",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpTriggerVoltage_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "OCP:TRIG:VOLT %.3f\n", value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLow_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_SPECIFICATION_L_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_SPECIFICATION_L_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLow_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:SPEC:L?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationLow_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OCP:SPEC:L %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHigh_RangeTableCallback (ViSession vi,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_SPECIFICATION_H_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OCP_SPECIFICATION_H_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHigh_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OCP:SPEC:H?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOcpSpecificationHigh_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OCP:SPEC:H %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppEndPowerMax_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:PEND? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppEndPowerMin_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:PEND? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppStartPowerMax_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:PSTA? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppStartPowerMin_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:PSTA? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltageMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:TRIG:VOLT? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltageMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:TRIG:VOLT? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:SPEC:H? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:SPEC:H? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLMax_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:SPEC:L? MAX\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLMin_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:SPEC:L? MIN\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppTest_WriteCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OPP %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppRange_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:RANG?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppRange_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
	ViInt32		ID_NUMS=10,ID_INDEX=0;	
	ViAttr		ID[10]={CHR6310A_ATTR_OPP_START_POWER_MIN, CHR6310A_ATTR_OPP_START_POWER_MAX, CHR6310A_ATTR_OPP_END_POWER_MIN, 
	                    CHR6310A_ATTR_OPP_END_POWER_MAX, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MIN, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MAX, 
	                    CHR6310A_ATTR_OPP_SPECIFICATION_L_MIN, CHR6310A_ATTR_OPP_SPECIFICATION_L_MAX, CHR6310A_ATTR_OPP_SPECIFICATION_H_MIN, 
	                    CHR6310A_ATTR_OPP_SPECIFICATION_H_MAX
						};
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
												
   	for (ID_INDEX=0;ID_INDEX<ID_NUMS;ID_INDEX++)
    viCheckErr(Ivi_InvalidateAttribute (vi, channelName, ID[ID_INDEX]));
												
    viCheckErr( viPrintf (io, "OPP:RANG %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppStartPower_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_START_POWER_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_START_POWER_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppStartPower_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:PSTA?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppStartPower_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OPP:PSTA %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppEndPower_RangeTableCallback (ViSession vi,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     IviRangeTablePtr *rangeTablePtr){
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_END_POWER_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_END_POWER_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppEndPower_ReadCallback (ViSession vi,
                                                               ViSession io,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               ViReal64 *value){
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:PEND?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppEndPower_WriteCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViReal64 value){
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OPP:PEND %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppStepCount_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:STEP?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppStepCount_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OPP:STEP %d\n", value)); 


Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppDwellTime_ReadCallback (ViSession vi,
                                                                ViSession io,
                                                                ViConstString channelName,
                                                                ViAttr attributeId,
                                                                ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OPP:DWEL?\n")); 
    
	viCheckErr( viScanf (io, "%d",value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppDwellTime_WriteCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OPP:DWEL %d\n", value)); 


Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltage_RangeTableCallback (ViSession vi,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltage_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:TRIG:VOLT?\n"));

	viCheckErr( viScanf (io, "%Lf",value));

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppTriggerVoltage_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
	viCheckErr( viPrintf (io, "OPP:TRIG:VOLT %.3f\n", value));   	

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLow_RangeTableCallback (ViSession vi,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_SPECIFICATION_L_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_SPECIFICATION_L_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLow_ReadCallback (ViSession vi,
                                                                       ViSession io,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:SPEC:L?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationLow_WriteCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OPP:SPEC:L %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHigh_RangeTableCallback (ViSession vi,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;  	
	ViReal64    min,max;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
             
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_SPECIFICATION_H_MIN,IVI_VAL_DIRECT_USER_CALL, &min));     
	viCheckErr( Ivi_GetAttributeViReal64( vi, channelName, CHR6310A_ATTR_OPP_SPECIFICATION_H_MAX,IVI_VAL_DIRECT_USER_CALL, &max)); 
    viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, min, max, VI_NULL, VI_NULL, VI_NULL));   

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHigh_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "OPP:SPEC:H?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrOppSpecificationHigh_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "OPP:SPEC:H %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedVoltageOut_RangeTableCallback (ViSession vi,
                                                                       ViConstString channelName,
                                                                       ViAttr attributeId,
                                                                       IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel,mode;
	ViString    Model;
	
	channel=atoi(channelName);
	
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));   
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViInt32( vi, channelName, CHR6310A_ATTR_MODE,IVI_VAL_DIRECT_USER_CALL, &mode));
    
    if  (!strcmp(Model,"63110A")) 
    {
    if ( mode == 9 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0.004, 100, VI_NULL, VI_NULL, VI_NULL));
             
    else if ( mode == 10 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0.02, 500, VI_NULL, VI_NULL, VI_NULL)); 
	}
	if  (!strcmp(Model,"63113A"))
	{
    if ( mode == 9 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0.0012, 60, VI_NULL, VI_NULL, VI_NULL));
             
    else if ( mode == 10 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0.006, 300, VI_NULL, VI_NULL, VI_NULL)); 
	
	} 
	
Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedVoltageOut_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "LED:VO?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedVoltageOut_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "LED:VO %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedCurrentOut_ReadCallback (ViSession vi,
                                                                 ViSession io,
                                                                 ViConstString channelName,
                                                                 ViAttr attributeId,
                                                                 ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "LED:IO?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedCurrentOut_WriteCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "LED:IO %.5f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedRdCoefficient_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "LED:RD:COEFF?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedRdCoefficient_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "LED:RD:COEFF %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedRdOhm_RangeTableCallback (ViSession vi,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel,mode;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
	viCheckErr( Ivi_GetAttributeViInt32( vi, channelName, CHR6310A_ATTR_MODE,IVI_VAL_DIRECT_USER_CALL, &mode));
    
    if ( mode == 9 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 3, 1000, VI_NULL, VI_NULL, VI_NULL));
             
    else if ( mode == 10 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 10, 10000, VI_NULL, VI_NULL, VI_NULL)); 

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedRdOhm_ReadCallback (ViSession vi,
                                                            ViSession io,
                                                            ViConstString channelName,
                                                            ViAttr attributeId,
                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "LED:RD:OHM?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedRdOhm_WriteCallback (ViSession vi,
                                                             ViSession io,
                                                             ViConstString channelName,
                                                             ViAttr attributeId,
                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "LED:RD:OHM %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedVf_RangeTableCallback (ViSession vi,
                                                               ViConstString channelName,
                                                               ViAttr attributeId,
                                                               IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel,mode;
	ViString    Model;
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));  
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViInt32( vi, channelName, CHR6310A_ATTR_MODE,IVI_VAL_DIRECT_USER_CALL, &mode));
    
    
    if  (!strcmp(Model,"63110A"))
    {
    if ( mode == 9 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0.000, 100, VI_NULL, VI_NULL, VI_NULL));
             
    else if ( mode == 10 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0.00, 500, VI_NULL, VI_NULL, VI_NULL)); 
         
    }
    
	if  (!strcmp(Model,"63113A"))
    {
    if ( mode == 9 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0.000, 60, VI_NULL, VI_NULL, VI_NULL));
             
    else if ( mode == 10 )
         viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0.00, 300, VI_NULL, VI_NULL, VI_NULL)); 
         
    }
	
Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedVf_ReadCallback (ViSession vi,
                                                         ViSession io,
                                                         ViConstString channelName,
                                                         ViAttr attributeId,
                                                         ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "LED:VF?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedVf_WriteCallback (ViSession vi,
                                                          ViSession io,
                                                          ViConstString channelName,
                                                          ViAttr attributeId,
                                                          ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "LED:VF %.4f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureCurrentRange_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:CURR:RANG?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureCurrentRange_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CONF:CURR:RANG %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRdSelect_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:RDSEL?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRdSelect_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CONF:RDSEL %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRr_ReadCallback (ViSession vi,
                                                                  ViSession io,
                                                                  ViConstString channelName,
                                                                  ViAttr attributeId,
                                                                  ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:RR?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRr_WriteCallback (ViSession vi,
                                                                   ViSession io,
                                                                   ViConstString channelName,
                                                                   ViAttr attributeId,
                                                                   ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CONF:RR %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRrSelect_ReadCallback (ViSession vi,
                                                                        ViSession io,
                                                                        ViConstString channelName,
                                                                        ViAttr attributeId,
                                                                        ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:RRSEL?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRrSelect_WriteCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CONF:RRSEL %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRrSet_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:RRSET?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureRrSet_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CONF:RRSET %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureShort_ReadCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:SHOR?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureShort_WriteCallback (ViSession vi,
                                                                      ViSession io,
                                                                      ViConstString channelName,
                                                                      ViAttr attributeId,
                                                                      ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CONF:SHOR %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureResponseSelect_ReadCallback (ViSession vi,
                                                                              ViSession io,
                                                                              ViConstString channelName,
                                                                              ViAttr attributeId,
                                                                              ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:RESP:SEL?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureResponseSelect_WriteCallback (ViSession vi,
                                                                               ViSession io,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CONF:RESP:SEL %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureResponseSet_ReadCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViInt32 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:RESP:SET?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureResponseSet_WriteCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViInt32 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CONF:RESP:SET %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureSetAllLed_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViBoolean *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "CONF:SETALLLED?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrLedConfigureSetAllLed_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViBoolean value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "CONF:SETALLLED %d\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerCenter_RangeTableCallback (ViSession vi,
                                                                                  ViConstString channelName,
                                                                                  ViAttr attributeId,
                                                                                  IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
    
    if (!strcmp(Model,"63110A")){
 
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));}
    if (!strcmp(Model,"63113A")){
 
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));}
             

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerCenter_ReadCallback (ViSession vi,
                                                                            ViSession io,
                                                                            ViConstString channelName,
                                                                            ViAttr attributeId,
                                                                            ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:POW:C?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerCenter_WriteCallback (ViSession vi,
                                                                             ViSession io,
                                                                             ViConstString channelName,
                                                                             ViAttr attributeId,
                                                                             ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "SPEC:POW:C %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerHigh_RangeTableCallback (ViSession vi,
                                                                                ViConstString channelName,
                                                                                ViAttr attributeId,
                                                                                IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViBoolean   Spec_Unit;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_SPECIFICATION_UNIT,IVI_VAL_DIRECT_USER_CALL, &Spec_Unit));
    
    if (!strcmp(Model,"63110A")){
    	if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 
    if (!strcmp(Model,"63113A")){
    	if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerHigh_ReadCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:POW:H?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerHigh_WriteCallback (ViSession vi,
                                                                           ViSession io,
                                                                           ViConstString channelName,
                                                                           ViAttr attributeId,
                                                                           ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "SPEC:POW:H %.3f\n", value)); 

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerLow_RangeTableCallback (ViSession vi,
                                                                               ViConstString channelName,
                                                                               ViAttr attributeId,
                                                                               IviRangeTablePtr *rangeTablePtr)
{
	ViStatus	error = VI_SUCCESS;
	IviRangeTablePtr	tblPtr = VI_NULL;
	ViInt32     channel;
	ViBoolean   Spec_Unit;
	ViString    Model="";
	
	channel=atoi(channelName);
    viCheckErr(Ivi_GetStoredRangeTablePtr (vi, attributeId, &tblPtr));       	
    viCheckErr( Ivi_GetViInt32EntryFromIndex (channel-1, &attrChannelLoadRangeTable, VI_NULL, VI_NULL, VI_NULL, &Model, VI_NULL));
	viCheckErr( Ivi_GetAttributeViBoolean( vi, VI_NULL, CHR6310A_ATTR_SPECIFICATION_UNIT,IVI_VAL_DIRECT_USER_CALL, &Spec_Unit));
    
    if (!strcmp(Model,"63110A")){
    	if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
    if (!strcmp(Model,"63113A")){
    	if ( Spec_Unit == 1)
             viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 300, VI_NULL, VI_NULL, VI_NULL));
             else
                 viCheckErr(Ivi_SetRangeTableEntry (tblPtr, VI_NULL, 0, 100, VI_NULL, VI_NULL, VI_NULL));} 
                 

Error:
	*rangeTablePtr = tblPtr;
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerLow_ReadCallback (ViSession vi,
                                                                         ViSession io,
                                                                         ViConstString channelName,
                                                                         ViAttr attributeId,
                                                                         ViReal64 *value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
 	viCheckErr( viPrintf (io, "SPEC:POW:L?\n"));

	viCheckErr( viScanf (io, "%Lf",value));  

Error:
	return error;
}

static ViStatus _VI_FUNC Chr6310AAttrSpecificationPowerLow_WriteCallback (ViSession vi,
                                                                          ViSession io,
                                                                          ViConstString channelName,
                                                                          ViAttr attributeId,
                                                                          ViReal64 value)
{
	ViStatus	error = VI_SUCCESS;
	ViInt32		ChannelNumber;
		
	ChannelNumber=atoi(channelName);
	
	viCheckErr( Ivi_SetAttributeViInt32( vi, VI_NULL, CHR6310A_ATTR_CHANNEL_LOAD, 
												IVI_VAL_DIRECT_USER_CALL, ChannelNumber));
    viCheckErr( viPrintf (io, "SPEC:POW:L %.3f\n", value)); 

Error:
	return error;
}


static ViStatus _VI_FUNC Chr6310AAttrConfigureLedlcrl_ReadCallback (ViSession vi,
                                                                    ViSession io,
                                                                    ViConstString channelName,
                                                                    ViAttr attributeId,
                                                                    ViBoolean *value)
{
ViStatus	error = VI_SUCCESS;
	
 	viCheckErr( viPrintf (io, "CONF:LEDLCRL:RANG?\n"));

	viCheckErr( viScanf (io, "%d",value));  

Error:
	return error;}

static ViStatus _VI_FUNC Chr6310AAttrConfigureLedlcrl_WriteCallback (ViSession vi,
                                                                     ViSession io,
                                                                     ViConstString channelName,
                                                                     ViAttr attributeId,
                                                                     ViBoolean value)
{
ViStatus	error = VI_SUCCESS;
	
	viCheckErr( viPrintf (io, "CONF:LEDLCRL:RANG %d\n", value));  	

Error:
	return error;}

/*****************************************************************************
 * Function: Chr6310A_InitAttributes                                                  
 * Purpose:  This function adds attributes to the IVI session, initializes   
 *           instrument attributes, and sets attribute invalidation          
 *           dependencies.                                                   
 *****************************************************************************/
static ViStatus Chr6310A_InitAttributes (ViSession vi)
{
    ViStatus    error = VI_SUCCESS;
    ViInt32     flags = 0;
    
        /*- Initialize instrument attributes --------------------------------*/            

    checkErr( Ivi_SetAttributeViInt32 (vi, "", CHR6310A_ATTR_SPECIFIC_DRIVER_MAJOR_VERSION,
                                       0, CHR6310A_MAJOR_VERSION));
    checkErr( Ivi_SetAttributeViInt32 (vi, "", CHR6310A_ATTR_SPECIFIC_DRIVER_MINOR_VERSION,
                                       0, CHR6310A_MINOR_VERSION));
    checkErr( Ivi_SetAttrReadCallbackViString (vi, CHR6310A_ATTR_SPECIFIC_DRIVER_REVISION,
                                               Chr6310AAttrDriverRevision_ReadCallback));
    checkErr( Ivi_SetAttributeViInt32 (vi, "", CHR6310A_ATTR_SPECIFIC_DRIVER_CLASS_SPEC_MAJOR_VERSION,
                                       0, CHR6310A_CLASS_SPEC_MAJOR_VERSION));
    checkErr( Ivi_SetAttributeViInt32 (vi, "", CHR6310A_ATTR_SPECIFIC_DRIVER_CLASS_SPEC_MINOR_VERSION,
                                       0, CHR6310A_CLASS_SPEC_MINOR_VERSION));
    checkErr( Ivi_SetAttributeViString (vi, "", CHR6310A_ATTR_IO_SESSION_TYPE,
                                        0, CHR6310A_IO_SESSION_TYPE));
    checkErr( Ivi_SetAttributeViString (vi, "", CHR6310A_ATTR_SUPPORTED_INSTRUMENT_MODELS,
                                        0, CHR6310A_SUPPORTED_INSTRUMENT_MODELS));


    checkErr( Ivi_SetAttrReadCallbackViString (vi, CHR6310A_ATTR_INSTRUMENT_FIRMWARE_REVISION,
                                               Chr6310AAttrFirmwareRevision_ReadCallback));

	/*================================================================*
	    This driver uses the id query attribute (or vxi manufacturer id
	    attribute in register-based instrument case) to get the information
	    about the instrument manufacturer. Therefore, callbacks do not perform
	    any I/O and in order to simulate correctly, you must flag this
	    attribute as "use callbacks for simulation".  If you implement this 
	    attribute so that it uses instrument I/O in the callback, then you 
	    must remove this call from the driver.
	 *================================================================*/
	checkErr( Ivi_GetAttributeFlags (vi, CHR6310A_ATTR_INSTRUMENT_MANUFACTURER, &flags));
	checkErr( Ivi_SetAttributeFlags (vi, CHR6310A_ATTR_INSTRUMENT_MANUFACTURER, 
	                                 flags | IVI_VAL_USE_CALLBACKS_FOR_SIMULATION));
	checkErr( Ivi_SetAttrReadCallbackViString (vi, CHR6310A_ATTR_INSTRUMENT_MANUFACTURER,
	                                           Chr6310AAttrInstrumentManufacturer_ReadCallback));

	/*================================================================*
	    This driver uses the id query attribute (or vxi model code
	    attribute in register-based instrument case) to get the information
	    about the instrument model code. Therefore, callbacks do not perform
	    any I/O and in order to simulate correctly, you must flag this
	    attribute as "use callbacks for simulation".  If you implement this 
	    attribute so that it uses instrument I/O in the callback, then you 
	    must remove this call from the driver.
	 *================================================================*/
	checkErr( Ivi_GetAttributeFlags (vi, CHR6310A_ATTR_INSTRUMENT_MODEL, &flags));
	checkErr( Ivi_SetAttributeFlags (vi, CHR6310A_ATTR_INSTRUMENT_MODEL, 
	                                 flags | IVI_VAL_USE_CALLBACKS_FOR_SIMULATION));
	checkErr( Ivi_SetAttrReadCallbackViString (vi, CHR6310A_ATTR_INSTRUMENT_MODEL,
	                                           Chr6310AAttrInstrumentModel_ReadCallback));

    checkErr( Ivi_SetAttributeViString (vi, "", CHR6310A_ATTR_SPECIFIC_DRIVER_VENDOR,
                                        0, CHR6310A_DRIVER_VENDOR));
    checkErr( Ivi_SetAttributeViString (vi, "", CHR6310A_ATTR_SPECIFIC_DRIVER_DESCRIPTION,
                                        0, CHR6310A_DRIVER_DESCRIPTION));
    checkErr( Ivi_SetAttributeViAddr (vi, VI_NULL, IVI_ATTR_OPC_CALLBACK, 0,
                                      Chr6310A_WaitForOPCCallback));
    checkErr( Ivi_SetAttributeViAddr (vi, VI_NULL, IVI_ATTR_CHECK_STATUS_CALLBACK, 0,
                                      Chr6310A_CheckStatusCallback));
	checkErr( Ivi_SetAttributeViBoolean (vi, VI_NULL, IVI_ATTR_SUPPORTS_WR_BUF_OPER_MODE, 
	                                     0, VI_FALSE));


    /*================================================================*
        Set the group capabilities attribute, Remove the extension groups
        this driver does not support, and add additional code to dynamically
        determine how the presence of specific instrument options may affect
        this attribute.
     *================================================================*/
        
	checkErr( Ivi_SetAttributeViString (vi, "", CHR6310A_ATTR_GROUP_CAPABILITIES, 0,
	                                    "None"));
    
        /*- Add instrument-specific attributes ------------------------------*/            
	
	checkErr( Ivi_AddAttributeViString (vi, CHR6310A_ATTR_ID_QUERY_RESPONSE,
	                                    "CHR6310A_ATTR_ID_QUERY_RESPONSE", 
	                                    "Chroma,631",
	                                    IVI_VAL_NOT_USER_WRITABLE,
	                                    Chr6310AAttrIdQueryResponse_ReadCallback,
	                                    VI_NULL));
	                                           
    checkErr( Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_OPC_TIMEOUT,
                                       "CHR6310A_ATTR_OPC_TIMEOUT",
                                       5000, IVI_VAL_HIDDEN | IVI_VAL_DONT_CHECK_STATUS,
                                       VI_NULL, VI_NULL, VI_NULL)); 
	checkErr (Ivi_AddAttributeViInt32 (vi,
	                                   CHR6310A_ATTR_STANDARD_EVENT_STATUS_ENABLE,
	                                   "CHR6310A_ATTR_STANDARD_EVENT_STATUS_ENABLE",
	                                   0, IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrStandardEventStatusEnable_ReadCallback,
	                                   Chr6310AAttrStandardEventStatusEnable_WriteCallback,
	                                   &attrStandardEventStatusEnableRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi,
	                                   CHR6310A_ATTR_STANDARD_EVENT_STATUS_REGISTER,
	                                   "CHR6310A_ATTR_STANDARD_EVENT_STATUS_REGISTER",
	                                   0, IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrStandardEventStatusEnable_ReadCallback,
	                                   VI_NULL,
	                                   &attrStandardEventStatusEnableRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_RECALL_INSTRUMENT_STATE,
	                                   "CHR6310A_ATTR_RECALL_INSTRUMENT_STATE", 0, 0,
	                                   VI_NULL,
	                                   Chr6310AAttrRecallInstrumentState_WriteCallback,
	                                   &attrRecallInstrumentStateRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_SAVE_INSTRUMENT_STATE,
	                                   "CHR6310A_ATTR_SAVE_INSTRUMENT_STATE", 0, 0,
	                                   VI_NULL,
	                                   Chr6310AAttrSaveInstrumentState_WriteCallback,
	                                   &attrSaveInstrumentStateRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_SERVICE_REQUEST_ENABLE,
	                                   "CHR6310A_ATTR_SERVICE_REQUEST_ENABLE", 0, 0,
	                                   Chr6310AAttrServiceRequestEnable_ReadCallback,
	                                   Chr6310AAttrServiceRequestEnable_WriteCallback,
	                                   &attrServiceRequestEnableRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_CHANNEL_LOAD,
	                                   "CHR6310A_ATTR_CHANNEL_LOAD", 1, 0,
	                                   Chr6310AAttrChannelLoad_ReadCallback,
	                                   Chr6310AAttrChannelLoad_WriteCallback,
	                                   &attrChannelListRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_QUERY_MIN_CHANNEL_LOAD,
	                                   "CHR6310A_ATTR_QUERY_MIN_CHANNEL_LOAD", 0, 0,
	                                   Chr6310AAttrQueryMinChannelLoad_ReadCallback,
	                                   VI_NULL, VI_NULL));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_QUERY_MAX_CHANNEL_LOAD,
	                                   "CHR6310A_ATTR_QUERY_MAX_CHANNEL_LOAD", 0, 0,
	                                   Chr6310AAttrQueryMaxChannelLoad_ReadCallback,
	                                   VI_NULL, VI_NULL));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CHANNEL_ACTIVE,
	                                     "CHR6310A_ATTR_CHANNEL_ACTIVE", 0,
	                                     IVI_VAL_MULTI_CHANNEL, VI_NULL,
	                                     Chr6310AAttrChannelActive_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CHANNEL_SYNCHRONIZED,
	                                     "CHR6310A_ATTR_CHANNEL_SYNCHRONIZED", 1,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrChannelSynchronized_ReadCallback,
	                                     Chr6310AAttrChannelSynchronized_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CONFIGURE_VON,
	                                    "CHR6310A_ATTR_CONFIGURE_VON", 1,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrConfigureVon_ReadCallback,
	                                    Chr6310AAttrConfigureVon_WriteCallback,
	                                    &attrConfigureVonRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE,
	                                    "CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE", 80,
	                                    IVI_VAL_MULTI_CHANNEL, VI_NULL,
	                                    Chr6310AAttrConfigureVoltageRange_WriteCallback,
	                                    &attrConfigureVoltageRangeRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi,
	                                    CHR6310A_ATTR_GET_CONFIGURE_VOLTAGE_RANGE,
	                                    "CHR6310A_ATTR_GET_CONFIGURE_VOLTAGE_RANGE",
	                                    0, IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrGetConfigureVoltageRange_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_VOLTAGE_LATCH,
	                                     "CHR6310A_ATTR_CONFIGURE_VOLTAGE_LATCH", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrConfigureVoltageLatch_ReadCallback,
	                                     Chr6310AAttrConfigureVoltageLatch_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_AUTO_LOAD,
	                                     "CHR6310A_ATTR_CONFIGURE_AUTO_LOAD", 0, 0,
	                                     Chr6310AAttrConfigureAutoLoad_ReadCallback,
	                                     Chr6310AAttrConfigureAutoLoad_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_AUTO_MODE,
	                                     "CHR6310A_ATTR_CONFIGURE_AUTO_MODE", 1, 0,
	                                     Chr6310AAttrConfigureAutoMode_ReadCallback,
	                                     Chr6310AAttrConfigureAutoMode_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_SOUND,
	                                     "CHR6310A_ATTR_CONFIGURE_SOUND", 0, 0,
	                                     Chr6310AAttrConfigureSound_ReadCallback,
	                                     Chr6310AAttrConfigureSound_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_REMOTE,
	                                     "CHR6310A_ATTR_CONFIGURE_REMOTE", 0, 0,
	                                     VI_NULL,
	                                     Chr6310AAttrConfigureRemote_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_LOAD,
	                                     "CHR6310A_ATTR_CONFIGURE_LOAD", 0, 0,
	                                     Chr6310AAttrConfigureLoad_ReadCallback,
	                                     Chr6310AAttrConfigureLoad_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_TIMING_STATE,
	                                     "CHR6310A_ATTR_CONFIGURE_TIMING_STATE", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrConfigureTimingState_ReadCallback,
	                                     Chr6310AAttrConfigureTimingState_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CONFIGURE_TIMING_TRIGGER,
	                                    "CHR6310A_ATTR_CONFIGURE_TIMING_TRIGGER", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrConfigureTimingTrigger_ReadCallback,
	                                    Chr6310AAttrConfigureTimingTrigger_WriteCallback,
	                                    &attrConfigureTimingTriggerRangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_CONFIGURE_TIMING_TIMEOUT,
	                                   "CHR6310A_ATTR_CONFIGURE_TIMING_TIMEOUT", 0,
	                                   IVI_VAL_MULTI_CHANNEL,
	                                   Chr6310AAttrConfigureTimingTimeout_ReadCallback,
	                                   Chr6310AAttrConfigureTimingTimeout_WriteCallback,
	                                   &attrConfigureTimingTimeoutRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_VOFF_STATE,
	                                     "CHR6310A_ATTR_CONFIGURE_VOFF_STATE", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrConfigureVoffState_ReadCallback,
	                                     Chr6310AAttrConfigureVoffState_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CONFIGURE_VOFF_FINAL_V,
	                                    "CHR6310A_ATTR_CONFIGURE_VOFF_FINAL_V", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrConfigureVoffFinalV_ReadCallback,
	                                    Chr6310AAttrConfigureVoffFinalV_WriteCallback,
	                                    &attrConfigureVoffFinalVRangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_CONFIGURE_MEASURE_AVERAGE,
	                                   "CHR6310A_ATTR_CONFIGURE_MEASURE_AVERAGE", 10,
	                                   IVI_VAL_MULTI_CHANNEL,
	                                   Chr6310AAttrConfigureMeasureAverage_ReadCallback,
	                                   Chr6310AAttrConfigureMeasureAverage_WriteCallback,
	                                   &attrConfigureMeasureAverageRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_DIGITAL_IO,
	                                     "CHR6310A_ATTR_CONFIGURE_DIGITAL_IO", 0, 0,
	                                     Chr6310AAttrConfigureDigitalIo_ReadCallback,
	                                     Chr6310AAttrConfigureDigitalIo_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_KEY,
	                                     "CHR6310A_ATTR_CONFIGURE_KEY", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrConfigureKey_ReadCallback,
	                                     Chr6310AAttrConfigureKey_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_ECHO,
	                                     "CHR6310A_ATTR_CONFIGURE_ECHO", 0, 0,
	                                     Chr6310AAttrConfigureEcho_ReadCallback,
	                                     Chr6310AAttrConfigureEcho_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_STATIC_L1,
	                                    "CHR6310A_ATTR_CURRENT_STATIC_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentStaticL1_ReadCallback,
	                                    Chr6310AAttrCurrentStaticL1_WriteCallback,
	                                    &attrCurrentStaticL1RangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_MODE, "CHR6310A_ATTR_MODE",
	                                   0,
	                                   IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrMode_ReadCallback,
	                                   Chr6310AAttrMode_WriteCallback,
	                                   &attrModeRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_STATIC_L2,
	                                    "CHR6310A_ATTR_CURRENT_STATIC_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentStaticL2_ReadCallback,
	                                    Chr6310AAttrCurrentStaticL2_WriteCallback,
	                                    &attrCurrentStaticL2RangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_STATIC_MIN_L1,
	                                    "CHR6310A_ATTR_CC_STATIC_MIN_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcStaticMinL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_STATIC_MAX_L1,
	                                    "CHR6310A_ATTR_CC_STATIC_MAX_L1", 20,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcStaticMaxL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_RISE_SLEWRATE_MIN,
	                                    "CHR6310A_ATTR_CC_RISE_SLEWRATE_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcRiseSlewrateMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_RISE_SLEWRATE_MAX,
	                                    "CHR6310A_ATTR_CC_RISE_SLEWRATE_MAX", 0.8,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcRiseSlewrateMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_FALL_SLEWRATE_MIN,
	                                    "CHR6310A_ATTR_CC_FALL_SLEWRATE_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcFallSlewrateMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_FALL_SLEWRATE_MAX,
	                                    "CHR6310A_ATTR_CC_FALL_SLEWRATE_MAX", 0.8,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcFallSlewrateMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_STATIC_RISE_SLEW,
	                                    "CHR6310A_ATTR_CURRENT_STATIC_RISE_SLEW",
	                                    0.1, IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentStaticRiseSlew_ReadCallback,
	                                    Chr6310AAttrCurrentStaticRiseSlew_WriteCallback,
	                                    &attrCurrentStaticRiseslewRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_STATIC_FALL_SLEW,
	                                    "CHR6310A_ATTR_CURRENT_STATIC_FALL_SLEW",
	                                    0.1, IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentStaticFallSlew_ReadCallback,
	                                    Chr6310AAttrCurrentStaticFallSlew_WriteCallback,
	                                    &attrCurrentStaticRiseSlewRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_MIN_L1,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_MIN_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicMinL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_MAX_L1,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_MAX_L1", 20,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicMaxL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_L1,
	                                    "CHR6310A_ATTR_CURRENT_DYNAMIC_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentDynamicL1_ReadCallback,
	                                    Chr6310AAttrCurrentDynamicL1_WriteCallback,
	                                    &attrCurrentDynamicL1RangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_L2,
	                                    "CHR6310A_ATTR_CURRENT_DYNAMIC_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentDynamicL2_ReadCallback,
	                                    Chr6310AAttrCurrentDynamicL2_WriteCallback,
	                                    &attrCurrentDynamicL2RangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_STATIC_MAX_L2,
	                                    "CHR6310A_ATTR_CC_STATIC_MAX_L2", 20,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcStaticMaxL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_STATIC_MIN_L2,
	                                    "CHR6310A_ATTR_CC_STATIC_MIN_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcStaticMinL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_MIN_L2,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_MIN_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicMinL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_MAX_L2,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_MAX_L2", 20,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicMaxL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_RISE_MIN,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_RISE_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicRiseMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_RISE_MAX,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_RISE_MAX", 0.8,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicRiseMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_FALL_MIN,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_FALL_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicFallMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_FALL_MAX,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_FALL_MAX", 0.8,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicFallMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_RISE_SLEW,
	                                    "CHR6310A_ATTR_CURRENT_DYNAMIC_RISE_SLEW",
	                                    0.1, IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentDynamicRiseSlew_ReadCallback,
	                                    Chr6310AAttrCurrentDynamicRiseSlew_WriteCallback,
	                                    &attrCurrentDynamicRiseSlewRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_FALL_SLEW,
	                                    "CHR6310A_ATTR_CURRENT_DYNAMIC_FALL_SLEW",
	                                    0.1, IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentDynamicFallSlew_ReadCallback,
	                                    Chr6310AAttrCurrentDynamicFallSlew_WriteCallback,
	                                    &attrCurrentDynamicFallSlewRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_T1_MIN,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_T1_MIN", 0.000025,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicT1Min_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_T1_MAX,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_T1_MAX", 50,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicT1Max_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_T2_MIN,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_T2_MIN", 0.000025,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicT2Min_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CC_DYNAMIC_T2_MAX,
	                                    "CHR6310A_ATTR_CC_DYNAMIC_T2_MAX", 50,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCcDynamicT2Max_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_T1,
	                                    "CHR6310A_ATTR_CURRENT_DYNAMIC_T1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentDynamicT1_ReadCallback,
	                                    Chr6310AAttrCurrentDynamicT1_WriteCallback,
	                                    &attrCurrentDynamicT1RangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_T1,
	                                         Chr6310AAttrCurrentDynamicT1_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_T2,
	                                    "CHR6310A_ATTR_CURRENT_DYNAMIC_T2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCurrentDynamicT2_ReadCallback,
	                                    Chr6310AAttrCurrentDynamicT2_WriteCallback,
	                                    &attrCurrentDynamicT2RangeTable, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_LOAD_STATE,
	                                     "CHR6310A_ATTR_LOAD_STATE", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrLoadState_ReadCallback,
	                                     Chr6310AAttrLoadState_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_LOAD_SHORT_STATE,
	                                     "CHR6310A_ATTR_LOAD_SHORT_STATE", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrLoadShortState_ReadCallback,
	                                     Chr6310AAttrLoadShortState_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_LOAD_SHORT_KEY,
	                                     "CHR6310A_ATTR_LOAD_SHORT_KEY", 0, 0,
	                                     Chr6310AAttrLoadShortKey_ReadCallback,
	                                     Chr6310AAttrLoadShortKey_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_MEASURE_INPUT,
	                                     "CHR6310A_ATTR_MEASURE_INPUT", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrMeasureInput_ReadCallback,
	                                     Chr6310AAttrMeasureInput_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_MEASURE_SCAN,
	                                     "CHR6310A_ATTR_MEASURE_SCAN", 0, 0,
	                                     Chr6310AAttrMeasureScan_ReadCallback,
	                                     Chr6310AAttrMeasureScan_WriteCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_PROGRAM_FILE,
	                                   "CHR6310A_ATTR_PROGRAM_FILE", 1,
	                                   IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrProgramFile_ReadCallback,
	                                   Chr6310AAttrProgramFile_WriteCallback,
	                                   &attrProgramFileRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_PROGRAM_ACTIVE,
	                                   "CHR6310A_ATTR_PROGRAM_ACTIVE", 0,
	                                   IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrProgramActive_ReadCallback,
	                                   Chr6310AAttrProgramActive_WriteCallback,
	                                   &attrProgramActiveRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_PROGRAM_CHAIN,
	                                   "CHR6310A_ATTR_PROGRAM_CHAIN", 0,
	                                   IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrProgramChain_ReadCallback,
	                                   Chr6310AAttrProgramChain_WriteCallback,
	                                   &attrProgramChainRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_PROGRAM_ON_TIME_MIN,
	                                    "CHR6310A_ATTR_PROGRAM_ON_TIME_MIN", 0, 0,
	                                    Chr6310AAttrProgramOnTimeMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_PROGRAM_ON_TIME_MAX,
	                                    "CHR6310A_ATTR_PROGRAM_ON_TIME_MAX", 30, 0,
	                                    Chr6310AAttrProgramOnTimeMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_PROGRAM_OFF_TIME_MIN,
	                                    "CHR6310A_ATTR_PROGRAM_OFF_TIME_MIN", 0, 0,
	                                    Chr6310AAttrProgramOffTimeMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_PROGRAM_OFF_TIME_MAX,
	                                    "CHR6310A_ATTR_PROGRAM_OFF_TIME_MAX", 30, 0,
	                                    Chr6310AAttrProgramOffTimeMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME_MIN,
	                                    "CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME_MIN", 0,
	                                    0,
	                                    Chr6310AAttrProgramPfDelayTimeMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME_MAX,
	                                    "CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME_MAX",
	                                    30, 0,
	                                    Chr6310AAttrProgramPfDelayTimeMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_PROGRAM_ON_TIME,
	                                    "CHR6310A_ATTR_PROGRAM_ON_TIME", 0,
	                                    IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrProgramOnTime_ReadCallback,
	                                    Chr6310AAttrProgramOnTime_WriteCallback,
	                                    &attrProgramOnTimeRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_PROGRAM_OFF_TIME,
	                                    "CHR6310A_ATTR_PROGRAM_OFF_TIME", 0,
	                                    IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrProgramOffTime_ReadCallback,
	                                    Chr6310AAttrProgramOffTime_WriteCallback,
	                                    &attrProgramOffTimeRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_PROGRAM_OFF_TIME,
	                                         Chr6310AAttrProgramOffTime_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME,
	                                    "CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME", 0,
	                                    IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrProgramPfDelayTime_ReadCallback,
	                                    Chr6310AAttrProgramPfDelayTime_WriteCallback,
	                                    &attrProgramPfDelayTimeRangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_SEQUENCE,
	                                   "CHR6310A_ATTR_SEQUENCE", 1,
	                                   IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrSequence_ReadCallback,
	                                   Chr6310AAttrSequence_WriteCallback,
	                                   &attrSequenceRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_SEQUENCE_MODE,
	                                   "CHR6310A_ATTR_SEQUENCE_MODE", 0,
	                                   IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrSequenceMode_ReadCallback,
	                                   Chr6310AAttrSequenceMode_WriteCallback,
	                                   &attrSequenceModeRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_SEQUENCE_SHORT_CHANNEL,
	                                   "CHR6310A_ATTR_SEQUENCE_SHORT_CHANNEL", 0,
	                                   IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrSequenceShortChannel_ReadCallback,
	                                   Chr6310AAttrSequenceShortChannel_WriteCallback,
	                                   &attrSequenceShortChannelRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_SEQUENCE_SHORT_TIME,
	                                    "CHR6310A_ATTR_SEQUENCE_SHORT_TIME", 0,
	                                    IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSequenceShortTime_ReadCallback,
	                                    Chr6310AAttrSequenceShortTime_WriteCallback,
	                                    &attrSequenceShortTimeRangeTable, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_PROGRAM_RUN,
	                                     "CHR6310A_ATTR_PROGRAM_RUN", 0,
	                                     IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrProgramRun_ReadCallback,
	                                     Chr6310AAttrProgramRun_WriteCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_PROGRAM_KEY,
	                                   "CHR6310A_ATTR_PROGRAM_KEY", 0,
	                                   IVI_VAL_NEVER_CACHE, VI_NULL,
	                                   Chr6310AAttrProgramKey_WriteCallback,
	                                   &attrProgramKeyRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CR_STATIC_MIN_L1,
	                                    "CHR6310A_ATTR_CR_STATIC_MIN_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCrStaticMinL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CR_STATIC_MAX_L1,
	                                    "CHR6310A_ATTR_CR_STATIC_MAX_L1", 15000,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCrStaticMaxL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CR_STATIC_MIN_L2,
	                                    "CHR6310A_ATTR_CR_STATIC_MIN_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCrStaticMinL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CR_STATIC_MAX_L2,
	                                    "CHR6310A_ATTR_CR_STATIC_MAX_L2", 15000,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCrStaticMaxL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CR_RISE_SLEW_MIN,
	                                    "CHR6310A_ATTR_CR_RISE_SLEW_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCrRiseSlewMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CR_RISE_SLEW_MAX,
	                                    "CHR6310A_ATTR_CR_RISE_SLEW_MAX", 0.8,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCrRiseSlewMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CR_FALL_SLEW_MIN,
	                                    "CHR6310A_ATTR_CR_FALL_SLEW_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCrFallSlewMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CR_FALL_SLEW_MAX,
	                                    "CHR6310A_ATTR_CR_FALL_SLEW_MAX", 0.8,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCrFallSlewMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_RESISTANCE_STATIC_L1,
	                                    "CHR6310A_ATTR_RESISTANCE_STATIC_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrResistanceStaticL1_ReadCallback,
	                                    Chr6310AAttrResistanceStaticL1_WriteCallback,
	                                    &attrCrStaticL1RangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_RESISTANCE_STATIC_L2,
	                                    "CHR6310A_ATTR_RESISTANCE_STATIC_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrResistanceStaticL2_ReadCallback,
	                                    Chr6310AAttrResistanceStaticL2_WriteCallback,
	                                    &attrResistanceStaticL1RangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi,
	                                    CHR6310A_ATTR_RESISTANCE_STATIC_RISE_SLEW,
	                                    "CHR6310A_ATTR_RESISTANCE_STATIC_RISE_SLEW",
	                                    0.04, IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrResistanceStaticRiseSlew_ReadCallback,
	                                    Chr6310AAttrResistanceStaticRiseSlew_WriteCallback,
	                                    &attrResistanceStaticRiseSlewRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi,
	                                    CHR6310A_ATTR_RESISTANCE_STATIC_FALL_SLEW,
	                                    "CHR6310A_ATTR_RESISTANCE_STATIC_FALL_SLEW",
	                                    0.04, IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrResistanceStaticFallSlew_ReadCallback,
	                                    Chr6310AAttrResistanceStaticFallSlew_WriteCallback,
	                                    &attrResistanceStaticFallSlewRangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_SHOW_DISPLAY,
	                                   "CHR6310A_ATTR_SHOW_DISPLAY", 0,
	                                   IVI_VAL_MULTI_CHANNEL, VI_NULL,
	                                   Chr6310AAttrShowDisplay_WriteCallback,
	                                   &attrShowDisplayRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_SPECIFICATION_UNIT,
	                                     "CHR6310A_ATTR_SPECIFICATION_UNIT", 0,
	                                     IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrSpecificationUnit_ReadCallback,
	                                     Chr6310AAttrSpecificationUnit_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_HIGH,
	                                    "CHR6310A_ATTR_SPECIFICATION_VOLTAGE_HIGH",
	                                    0,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSpecificationVoltageHigh_ReadCallback,
	                                    Chr6310AAttrSpecificationVoltageHigh_WriteCallback,
	                                    &attrSpecificationVoltageHighRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_SPECIFICATION_VOLTAGE_HIGH,
	                                         Chr6310AAttrSpecificationVoltageHigh_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi,
	                                    CHR6310A_ATTR_SPECIFICATION_VOLTAGE_CENTER,
	                                    "CHR6310A_ATTR_SPECIFICATION_VOLTAGE_CENTER",
	                                    0,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSpecificationVoltageCenter_ReadCallback,
	                                    Chr6310AAttrSpecificationVoltageCenter_WriteCallback,
	                                    &attrSpecificationVoltageCenterRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_SPECIFICATION_VOLTAGE_CENTER,
	                                         Chr6310AAttrSpecificationVoltageCenter_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_SPECIFICATION_VOLTAGE_LOW,
	                                    "CHR6310A_ATTR_SPECIFICATION_VOLTAGE_LOW", 0,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSpecificationVoltageLow_ReadCallback,
	                                    Chr6310AAttrSpecificationVoltageLow_WriteCallback,
	                                    &attrSpecificationVoltageLowRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_SPECIFICATION_CURRENT_HIGH,
	                                    "CHR6310A_ATTR_SPECIFICATION_CURRENT_HIGH",
	                                    0,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSpecificationCurrentHigh_ReadCallback,
	                                    Chr6310AAttrSpecificationCurrentHigh_WriteCallback,
	                                    &attrSpecificationCurrentHighRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_SPECIFICATION_CURRENT_HIGH,
	                                         Chr6310AAttrSpecificationCurrentHigh_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi,
	                                    CHR6310A_ATTR_SPECIFICATION_CURRENT_CENTER,
	                                    "CHR6310A_ATTR_SPECIFICATION_CURRENT_CENTER",
	                                    0,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSpecificationCurrentCenter_ReadCallback,
	                                    Chr6310AAttrSpecificationCurrentCenter_WriteCallback,
	                                    &attrSpecificationCurrentCenterRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_SPECIFICATION_CURRENT_CENTER,
	                                         Chr6310AAttrSpecificationCurrentCenter_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_SPECIFICATION_CURRENT_LOW,
	                                    "CHR6310A_ATTR_SPECIFICATION_CURRENT_LOW", 0,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSpecificationCurrentLow_ReadCallback,
	                                    Chr6310AAttrSpecificationCurrentLow_WriteCallback,
	                                    &attrSpecificationCurrentLowRangeTable, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_SPECIFICATION_TEST,
	                                     "CHR6310A_ATTR_SPECIFICATION_TEST", 0,
	                                     IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrSpecificationTest_ReadCallback,
	                                     Chr6310AAttrSpecificationTest_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CV_STATIC_MIN_L1,
	                                    "CHR6310A_ATTR_CV_STATIC_MIN_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCvStaticMinL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CV_STATIC_MAX_L1,
	                                    "CHR6310A_ATTR_CV_STATIC_MAX_L1", 80,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCvStaticMaxL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CV_CURRENT_MIN,
	                                    "CHR6310A_ATTR_CV_CURRENT_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCvCurrentMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CV_CURRENT_MAX,
	                                    "CHR6310A_ATTR_CV_CURRENT_MAX", 20,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCvCurrentMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CV_STATIC_MAX_L2,
	                                    "CHR6310A_ATTR_CV_STATIC_MAX_L2", 80,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCvStaticMaxL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CV_STATIC_MIN_L2,
	                                    "CHR6310A_ATTR_CV_STATIC_MIN_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCvStaticMinL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_VOLTAGE_STATIC_L1,
	                                    "CHR6310A_ATTR_VOLTAGE_STATIC_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrVoltageStaticL1_ReadCallback,
	                                    Chr6310AAttrVoltageStaticL1_WriteCallback,
	                                    &attrVoltageL1RangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_VOLTAGE_STATIC_L2,
	                                    "CHR6310A_ATTR_VOLTAGE_STATIC_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrVoltageStaticL2_ReadCallback,
	                                    Chr6310AAttrVoltageStaticL2_WriteCallback,
	                                    &attrVoltageL2RangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_VOLTAGE_CURRENT_LIMIT,
	                                    "CHR6310A_ATTR_VOLTAGE_CURRENT_LIMIT", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrVoltageCurrentLimit_ReadCallback,
	                                    Chr6310AAttrVoltageCurrentLimit_WriteCallback,
	                                    &attrVoltageCurrentLimitRangeTable, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_VOLTAGE_RESPONSE_SPEED,
	                                     "CHR6310A_ATTR_VOLTAGE_RESPONSE_SPEED", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrVoltageResponseSpeed_ReadCallback,
	                                     Chr6310AAttrVoltageResponseSpeed_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CP_STATIC_MIN_L1,
	                                    "CHR6310A_ATTR_CP_STATIC_MIN_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCpStaticMinL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CP_STATIC_MAX_L1,
	                                    "CHR6310A_ATTR_CP_STATIC_MAX_L1", 100,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCpStaticMaxL1_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CP_STATIC_MIN_L2,
	                                    "CHR6310A_ATTR_CP_STATIC_MIN_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCpStaticMinL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CP_STATIC_MAX_L2,
	                                    "CHR6310A_ATTR_CP_STATIC_MAX_L2", 100,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCpStaticMaxL2_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CP_RISE_SLEW_MIN,
	                                    "CHR6310A_ATTR_CP_RISE_SLEW_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCpRiseSlewMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CP_RISE_SLEW_MAX,
	                                    "CHR6310A_ATTR_CP_RISE_SLEW_MAX", 0.8,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCpRiseSlewMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CP_FALL_SLEW_MIN,
	                                    "CHR6310A_ATTR_CP_FALL_SLEW_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCpFallSlewMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_CP_FALL_SLEW_MAX,
	                                    "CHR6310A_ATTR_CP_FALL_SLEW_MAX", 0.8,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrCpFallSlewMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_POWER_STATIC_L1,
	                                    "CHR6310A_ATTR_POWER_STATIC_L1", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrPowerStaticL1_ReadCallback,
	                                    Chr6310AAttrPowerStaticL1_WriteCallback,
	                                    &attrPowerStaticL1RangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_POWER_STATIC_L1,
	                                         Chr6310AAttrPowerStaticL1_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_POWER_STATIC_L2,
	                                    "CHR6310A_ATTR_POWER_STATIC_L2", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrPowerStaticL2_ReadCallback,
	                                    Chr6310AAttrPowerStaticL2_WriteCallback,
	                                    &attrPowerStaticL2RangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_POWER_STATIC_RISE_SLEW,
	                                    "CHR6310A_ATTR_POWER_STATIC_RISE_SLEW", 0.2,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrPowerStaticRiseSlew_ReadCallback,
	                                    Chr6310AAttrPowerStaticRiseSlew_WriteCallback,
	                                    &attrPowerRiseSlewRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_POWER_STATIC_RISE_SLEW,
	                                         Chr6310AAttrPowerStaticRiseSlew_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_POWER_STATIC_FALL_SLEW,
	                                    "CHR6310A_ATTR_POWER_STATIC_FALL_SLEW", 0.2,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrPowerStaticFallSlew_ReadCallback,
	                                    Chr6310AAttrPowerStaticFallSlew_WriteCallback,
	                                    &attrPowerStaticFallSlewRangeTable, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_OCP_TEST,
	                                     "CHR6310A_ATTR_OCP_TEST", 0,
	                                     IVI_VAL_MULTI_CHANNEL, VI_NULL,
	                                     Chr6310AAttrOcpTest_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_OCP_RANGE,
	                                     "CHR6310A_ATTR_OCP_RANGE", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrOcpRange_ReadCallback,
	                                     Chr6310AAttrOcpRange_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_START_CURRENT_MIN,
	                                    "CHR6310A_ATTR_OCP_START_CURRENT_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpStartCurrentMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_START_CURRENT_MAX,
	                                    "CHR6310A_ATTR_OCP_START_CURRENT_MAX", 20,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpStartCurrentMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_END_CURRENT_MIN,
	                                    "CHR6310A_ATTR_OCP_END_CURRENT_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpEndCurrentMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_END_CURRENT_MAX,
	                                    "CHR6310A_ATTR_OCP_END_CURRENT_MAX", 20,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpEndCurrentMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MIN,
	                                    "CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpTriggerVoltageMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MAX,
	                                    "CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE_MAX", 80,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpTriggerVoltageMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_SPECIFICATION_L_MIN,
	                                    "CHR6310A_ATTR_OCP_SPECIFICATION_L_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpSpecificationLMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_SPECIFICATION_L_MAX,
	                                    "CHR6310A_ATTR_OCP_SPECIFICATION_L_MAX", 80,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpSpecificationLMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_SPECIFICATION_H_MIN,
	                                    "CHR6310A_ATTR_OCP_SPECIFICATION_H_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpSpecificationHMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_SPECIFICATION_H_MAX,
	                                    "CHR6310A_ATTR_OCP_SPECIFICATION_H_MAX", 80,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpSpecificationHMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_START_CURRENT,
	                                    "CHR6310A_ATTR_OCP_START_CURRENT", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpStartCurrent_ReadCallback,
	                                    Chr6310AAttrOcpStartCurrent_WriteCallback,
	                                    &attrOcpStartCurrentRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_END_CURRENT,
	                                    "CHR6310A_ATTR_OCP_END_CURRENT", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpEndCurrent_ReadCallback,
	                                    Chr6310AAttrOcpEndCurrent_WriteCallback,
	                                    &attrOcpEndCurrentRangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_OCP_STEP_COUNT,
	                                   "CHR6310A_ATTR_OCP_STEP_COUNT", 1,
	                                   IVI_VAL_MULTI_CHANNEL,
	                                   Chr6310AAttrOcpStepCount_ReadCallback,
	                                   Chr6310AAttrOcpStepCount_WriteCallback,
	                                   &attrOcpStepCountRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_OCP_DWELL_TIME,
	                                   "CHR6310A_ATTR_OCP_DWELL_TIME", 0,
	                                   IVI_VAL_MULTI_CHANNEL,
	                                   Chr6310AAttrOcpDwellTime_ReadCallback,
	                                   Chr6310AAttrOcpDwellTime_WriteCallback,
	                                   &attrOcpDwellTimeRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE,
	                                    "CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpTriggerVoltage_ReadCallback,
	                                    Chr6310AAttrOcpTriggerVoltage_WriteCallback,
	                                    &attrOcpTriggerVoltageRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_SPECIFICATION_LOW,
	                                    "CHR6310A_ATTR_OCP_SPECIFICATION_LOW", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpSpecificationLow_ReadCallback,
	                                    Chr6310AAttrOcpSpecificationLow_WriteCallback,
	                                    &attrOcpSpecificationLowRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OCP_SPECIFICATION_HIGH,
	                                    "CHR6310A_ATTR_OCP_SPECIFICATION_HIGH", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOcpSpecificationHigh_ReadCallback,
	                                    Chr6310AAttrOcpSpecificationHigh_WriteCallback,
	                                    &attrOcpSpecificationHighRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_START_POWER_MIN,
	                                    "CHR6310A_ATTR_OPP_START_POWER_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppStartPowerMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_START_POWER_MAX,
	                                    "CHR6310A_ATTR_OPP_START_POWER_MAX", 100,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppStartPowerMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_END_POWER_MIN,
	                                    "CHR6310A_ATTR_OPP_END_POWER_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppEndPowerMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_END_POWER_MAX,
	                                    "CHR6310A_ATTR_OPP_END_POWER_MAX", 100,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppEndPowerMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MIN,
	                                    "CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppTriggerVoltageMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MAX,
	                                    "CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE_MAX", 80,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppTriggerVoltageMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_SPECIFICATION_L_MIN,
	                                    "CHR6310A_ATTR_OPP_SPECIFICATION_L_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppSpecificationLMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_SPECIFICATION_L_MAX,
	                                    "CHR6310A_ATTR_OPP_SPECIFICATION_L_MAX", 100,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppSpecificationLMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_SPECIFICATION_H_MIN,
	                                    "CHR6310A_ATTR_OPP_SPECIFICATION_H_MIN", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppSpecificationHMin_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_SPECIFICATION_H_MAX,
	                                    "CHR6310A_ATTR_OPP_SPECIFICATION_H_MAX", 100,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppSpecificationHMax_ReadCallback,
	                                    VI_NULL, VI_NULL, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_OPP_TEST,
	                                     "CHR6310A_ATTR_OPP_TEST", 0,
	                                     IVI_VAL_MULTI_CHANNEL, VI_NULL,
	                                     Chr6310AAttrOppTest_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_OPP_RANGE,
	                                     "CHR6310A_ATTR_OPP_RANGE", 0,
	                                     IVI_VAL_MULTI_CHANNEL,
	                                     Chr6310AAttrOppRange_ReadCallback,
	                                     Chr6310AAttrOppRange_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_START_POWER,
	                                    "CHR6310A_ATTR_OPP_START_POWER", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppStartPower_ReadCallback,
	                                    Chr6310AAttrOppStartPower_WriteCallback,
	                                    &attrOcpStartPowerRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_END_POWER,
	                                    "CHR6310A_ATTR_OPP_END_POWER", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppEndPower_ReadCallback,
	                                    Chr6310AAttrOppEndPower_WriteCallback,
	                                    &attrOppStartPowerRangeTable, 0));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_OPP_STEP_COUNT,
	                                   "CHR6310A_ATTR_OPP_STEP_COUNT", 0,
	                                   IVI_VAL_MULTI_CHANNEL,
	                                   Chr6310AAttrOppStepCount_ReadCallback,
	                                   Chr6310AAttrOppStepCount_WriteCallback,
	                                   &attrOppStepCountRangeTable));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_OPP_DWELL_TIME,
	                                   "CHR6310A_ATTR_OPP_DWELL_TIME", 0,
	                                   IVI_VAL_MULTI_CHANNEL,
	                                   Chr6310AAttrOppDwellTime_ReadCallback,
	                                   Chr6310AAttrOppDwellTime_WriteCallback,
	                                   &attrOppDwellTimeRangeTable));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE,
	                                    "CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppTriggerVoltage_ReadCallback,
	                                    Chr6310AAttrOppTriggerVoltage_WriteCallback,
	                                    &attrOppTriggerVoltageRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_SPECIFICATION_LOW,
	                                    "CHR6310A_ATTR_OPP_SPECIFICATION_LOW", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppSpecificationLow_ReadCallback,
	                                    Chr6310AAttrOppSpecificationLow_WriteCallback,
	                                    &attrOppSpecificationLowRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_OPP_SPECIFICATION_HIGH,
	                                    "CHR6310A_ATTR_OPP_SPECIFICATION_HIGH", 0,
	                                    IVI_VAL_MULTI_CHANNEL,
	                                    Chr6310AAttrOppSpecificationHigh_ReadCallback,
	                                    Chr6310AAttrOppSpecificationHigh_WriteCallback,
	                                    &attrOppSpecificationHighRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_LED_VOLTAGE_OUT,
	                                    "CHR6310A_ATTR_LED_VOLTAGE_OUT", 1.5,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrLedVoltageOut_ReadCallback,
	                                    Chr6310AAttrLedVoltageOut_WriteCallback,
	                                    &attrLedVoltageOutRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_LED_CURRENT_OUT,
	                                    "CHR6310A_ATTR_LED_CURRENT_OUT", 1,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrLedCurrentOut_ReadCallback,
	                                    Chr6310AAttrLedCurrentOut_WriteCallback,
	                                    VI_NULL, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_LED_RD_COEFFICIENT,
	                                    "CHR6310A_ATTR_LED_RD_COEFFICIENT", 1,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrLedRdCoefficient_ReadCallback,
	                                    Chr6310AAttrLedRdCoefficient_WriteCallback,
	                                    &attrLedRdCoefficientRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_LED_RD_OHM,
	                                    "CHR6310A_ATTR_LED_RD_OHM", 100,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrLedRdOhm_ReadCallback,
	                                    Chr6310AAttrLedRdOhm_WriteCallback,
	                                    &attrLedRdOhmRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_LED_VF,
	                                    "CHR6310A_ATTR_LED_VF", 1.5,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrLedVf_ReadCallback,
	                                    Chr6310AAttrLedVf_WriteCallback,
	                                    &attrLedVfRangeTable, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi,
	                                     CHR6310A_ATTR_LED_CONFIGURE_CURRENT_RANGE,
	                                     "CHR6310A_ATTR_LED_CONFIGURE_CURRENT_RANGE",
	                                     0,
	                                     IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrLedConfigureCurrentRange_ReadCallback,
	                                     Chr6310AAttrLedConfigureCurrentRange_WriteCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_LED_CONFIGURE_RD_SELECT,
	                                   "CHR6310A_ATTR_LED_CONFIGURE_RD_SELECT", 0,
	                                   IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrLedConfigureRdSelect_ReadCallback,
	                                   Chr6310AAttrLedConfigureRdSelect_WriteCallback,
	                                   &attrConfigureRdSelectRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_LED_CONFIGURE_RR,
	                                     "CHR6310A_ATTR_LED_CONFIGURE_RR", 0,
	                                     IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrLedConfigureRr_ReadCallback,
	                                     Chr6310AAttrLedConfigureRr_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_LED_CONFIGURE_RR_SELECT,
	                                     "CHR6310A_ATTR_LED_CONFIGURE_RR_SELECT", 0,
	                                     IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrLedConfigureRrSelect_ReadCallback,
	                                     Chr6310AAttrLedConfigureRrSelect_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_LED_CONFIGURE_RR_SET,
	                                    "CHR6310A_ATTR_LED_CONFIGURE_RR_SET", 5,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrLedConfigureRrSet_ReadCallback,
	                                    Chr6310AAttrLedConfigureRrSet_WriteCallback,
	                                    &attrLedConfigureRrSetRangeTable, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_LED_CONFIGURE_SHORT,
	                                     "CHR6310A_ATTR_LED_CONFIGURE_SHORT", 0,
	                                     IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrLedConfigureShort_ReadCallback,
	                                     Chr6310AAttrLedConfigureShort_WriteCallback));
	checkErr (Ivi_AddAttributeViBoolean (vi,
	                                     CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SELECT,
	                                     "CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SELECT",
	                                     0,
	                                     IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrLedConfigureResponseSelect_ReadCallback,
	                                     Chr6310AAttrLedConfigureResponseSelect_WriteCallback));
	checkErr (Ivi_AddAttributeViInt32 (vi, CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SET,
	                                   "CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SET", 1,
	                                   IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                   Chr6310AAttrLedConfigureResponseSet_ReadCallback,
	                                   Chr6310AAttrLedConfigureResponseSet_WriteCallback,
	                                   &attrLedConfigureResponseSetRangeTable));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_LED_CONFIGURE_SET_ALL_LED,
	                                     "CHR6310A_ATTR_LED_CONFIGURE_SET_ALL_LED",
	                                     0,
	                                     IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrLedConfigureSetAllLed_ReadCallback,
	                                     Chr6310AAttrLedConfigureSetAllLed_WriteCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_SPECIFICATION_POWER_HIGH,
	                                    "CHR6310A_ATTR_SPECIFICATION_POWER_HIGH", 0,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSpecificationPowerHigh_ReadCallback,
	                                    Chr6310AAttrSpecificationPowerHigh_WriteCallback,
	                                    &attrSpecificationPowHighRangeTable, 0));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_SPECIFICATION_POWER_HIGH,
	                                         Chr6310AAttrSpecificationPowerHigh_RangeTableCallback));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_SPECIFICATION_POWER_CENTER,
	                                    "CHR6310A_ATTR_SPECIFICATION_POWER_CENTER",
	                                    0,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSpecificationPowerCenter_ReadCallback,
	                                    Chr6310AAttrSpecificationPowerCenter_WriteCallback,
	                                    &attrSpecificationPowerCenterRangeTable, 0));
	checkErr (Ivi_AddAttributeViReal64 (vi, CHR6310A_ATTR_SPECIFICATION_POWER_LOW,
	                                    "CHR6310A_ATTR_SPECIFICATION_POWER_LOW", 0,
	                                    IVI_VAL_MULTI_CHANNEL | IVI_VAL_NEVER_CACHE,
	                                    Chr6310AAttrSpecificationPowerLow_ReadCallback,
	                                    Chr6310AAttrSpecificationPowerLow_WriteCallback,
	                                    &attrSpecificationPowerLowRangeTable, 0));
	checkErr (Ivi_AddAttributeViBoolean (vi, CHR6310A_ATTR_CONFIGURE_LEDLCRL,
	                                     "CHR6310A_ATTR_CONFIGURE_LEDLCRL", 0,
	                                     IVI_VAL_NEVER_CACHE,
	                                     Chr6310AAttrConfigureLedlcrl_ReadCallback,
	                                     Chr6310AAttrConfigureLedlcrl_WriteCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_SPECIFICATION_POWER_LOW,
	                                         Chr6310AAttrSpecificationPowerLow_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_SPECIFICATION_POWER_CENTER,
	                                         Chr6310AAttrSpecificationPowerCenter_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_LED_VF,
	                                         Chr6310AAttrLedVf_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_LED_RD_OHM,
	                                         Chr6310AAttrLedRdOhm_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_LED_VOLTAGE_OUT,
	                                         Chr6310AAttrLedVoltageOut_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_OPP_SPECIFICATION_HIGH,
	                                         Chr6310AAttrOppSpecificationHigh_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_OPP_SPECIFICATION_LOW,
	                                         Chr6310AAttrOppSpecificationLow_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_OPP_TRIGGER_VOLTAGE,
	                                         Chr6310AAttrOppTriggerVoltage_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_OPP_END_POWER,
	                                         Chr6310AAttrOppEndPower_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_OPP_START_POWER,
	                                         Chr6310AAttrOppStartPower_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_OCP_SPECIFICATION_HIGH,
	                                         Chr6310AAttrOcpSpecificationHigh_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_OCP_SPECIFICATION_LOW,
	                                         Chr6310AAttrOcpSpecificationLow_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_OCP_TRIGGER_VOLTAGE,
	                                         Chr6310AAttrOcpTriggerVoltage_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_OCP_END_CURRENT,
	                                         Chr6310AAttrOcpEndCurrent_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_OCP_START_CURRENT,
	                                         Chr6310AAttrOcpStartCurrent_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_POWER_STATIC_FALL_SLEW,
	                                         Chr6310AAttrPowerStaticFallSlew_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_POWER_STATIC_L2,
	                                         Chr6310AAttrPowerStaticL2_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_VOLTAGE_CURRENT_LIMIT,
	                                         Chr6310AAttrVoltageCurrentLimit_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_VOLTAGE_STATIC_L2,
	                                         Chr6310AAttrVoltageStaticL2_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_VOLTAGE_STATIC_L1,
	                                         Chr6310AAttrVoltageStaticL1_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_SPECIFICATION_CURRENT_LOW,
	                                         Chr6310AAttrSpecificationCurrentLow_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_SPECIFICATION_VOLTAGE_LOW,
	                                         Chr6310AAttrSpecificationVoltageLow_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_RESISTANCE_STATIC_FALL_SLEW,
	                                         Chr6310AAttrResistanceStaticFallSlew_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_RESISTANCE_STATIC_RISE_SLEW,
	                                         Chr6310AAttrResistanceStaticRiseSlew_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_RESISTANCE_STATIC_L2,
	                                         Chr6310AAttrResistanceStaticL2_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_RESISTANCE_STATIC_L1,
	                                         Chr6310AAttrResistanceStaticL1_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME,
	                                         Chr6310AAttrProgramPfDelayTime_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_PROGRAM_ON_TIME,
	                                         Chr6310AAttrProgramOnTime_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_T2,
	                                         Chr6310AAttrCurrentDynamicT2_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_CURRENT_DYNAMIC_FALL_SLEW,
	                                         Chr6310AAttrCurrentDynamicFallSlew_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_CURRENT_DYNAMIC_RISE_SLEW,
	                                         Chr6310AAttrCurrentDynamicRiseSlew_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_L2,
	                                         Chr6310AAttrCurrentDynamicL2_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_CURRENT_DYNAMIC_L1,
	                                         Chr6310AAttrCurrentDynamicL1_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_CURRENT_STATIC_FALL_SLEW,
	                                         Chr6310AAttrCurrentStaticFallSlew_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_CURRENT_STATIC_RISE_SLEW,
	                                         Chr6310AAttrCurrentStaticRiseSlew_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_CURRENT_STATIC_L2,
	                                         Chr6310AAttrCurrentStaticL2_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_CURRENT_STATIC_L1,
	                                         Chr6310AAttrCurrentStaticL1_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_CONFIGURE_VOFF_FINAL_V,
	                                         Chr6310AAttrConfigureVoffFinalV_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_CONFIGURE_TIMING_TRIGGER,
	                                         Chr6310AAttrConfigureTimingTrigger_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi,
	                                         CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE,
	                                         Chr6310AAttrConfigureVoltageRange_RangeTableCallback));
	checkErr (Ivi_SetAttrRangeTableCallback (vi, CHR6310A_ATTR_CONFIGURE_VON,
	                                         Chr6310AAttrConfigureVon_RangeTableCallback));
                                       
        /*- Setup attribute invalidations -----------------------------------*/            

    /*================================================================*
       
       Set attribute dependencies by calling the additional 
       Ivi_AddAttributeInvalidation functions here.  Remove the dependencies
       that do not apply to your instrument by deleting the calls to
       Ivi_AddAttributeInvalidation.
       When you initially add an attribute, it applies to all channels. 
       If you want it to apply to only a subset, call the 
       Ivi_RestrictAttrToChannels function.

     *================================================================*/

	checkErr (Ivi_AddAttributeInvalidation (vi,
	                                        CHR6310A_ATTR_CONFIGURE_VOLTAGE_RANGE,
	                                        CHR6310A_ATTR_GET_CONFIGURE_VOLTAGE_RANGE,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_PROGRAM_FILE,
	                                        CHR6310A_ATTR_PROGRAM_ACTIVE, VI_TRUE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_PROGRAM_FILE,
	                                        CHR6310A_ATTR_PROGRAM_CHAIN, VI_TRUE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_PROGRAM_FILE,
	                                        CHR6310A_ATTR_PROGRAM_ON_TIME, VI_TRUE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_PROGRAM_FILE,
	                                        CHR6310A_ATTR_PROGRAM_OFF_TIME, VI_TRUE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_PROGRAM_FILE,
	                                        CHR6310A_ATTR_PROGRAM_PF_DELAY_TIME,
	                                        VI_TRUE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_SEQUENCE,
	                                        CHR6310A_ATTR_SEQUENCE_MODE, VI_TRUE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_SEQUENCE,
	                                        CHR6310A_ATTR_SEQUENCE_SHORT_CHANNEL,
	                                        VI_TRUE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_SEQUENCE,
	                                        CHR6310A_ATTR_SEQUENCE_SHORT_TIME,
	                                        VI_TRUE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CONFIGURE_VON, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CONFIGURE_VOFF_FINAL_V,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_STATIC_L1,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_STATIC_L2,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_STATIC_RISE_SLEW,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_STATIC_FALL_SLEW,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_DYNAMIC_L1,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_DYNAMIC_L2,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_DYNAMIC_RISE_SLEW,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_DYNAMIC_FALL_SLEW,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_DYNAMIC_T1,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_CURRENT_DYNAMIC_T2,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_RESISTANCE_STATIC_L1,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_RESISTANCE_STATIC_L2,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_RESISTANCE_STATIC_RISE_SLEW,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_RESISTANCE_STATIC_FALL_SLEW,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_VOLTAGE_STATIC_L1,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_VOLTAGE_STATIC_L2,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_VOLTAGE_CURRENT_LIMIT,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_VOLTAGE_RESPONSE_SPEED,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_POWER_STATIC_L1, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_POWER_STATIC_L2, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_POWER_STATIC_RISE_SLEW,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_POWER_STATIC_FALL_SLEW,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_VOLTAGE_OUT, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CURRENT_OUT, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_RD_COEFFICIENT,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_RD_OHM, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_VF, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CONFIGURE_CURRENT_RANGE,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CONFIGURE_RD_SELECT,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CONFIGURE_RR, VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CONFIGURE_RR_SELECT,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CONFIGURE_RR_SET,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CONFIGURE_SHORT,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SELECT,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CONFIGURE_RESPONSE_SET,
	                                        VI_FALSE));
	checkErr (Ivi_AddAttributeInvalidation (vi, CHR6310A_ATTR_MODE,
	                                        CHR6310A_ATTR_LED_CONFIGURE_SET_ALL_LED,
	                                        VI_FALSE));
Error:
    return error;
}

/*****************************************************************************
 *------------------- End Instrument Driver Source Code ---------------------*
 *****************************************************************************/
